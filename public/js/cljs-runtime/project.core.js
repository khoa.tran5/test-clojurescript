goog.provide('project.core');
project.model.init_store(new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"number","number",1570378438),(0)], null));
project.core.time_travel = (function project$core$time_travel(value){
var G__30832 = cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$4(project.model.app_state,cljs.core.assoc,new cljs.core.Keyword(null,"number","number",1570378438),value);
var G__30833 = console.log(cljs.core.deref(project.model.app_state));
var fexpr__30831 = console.log("the click is saved");
return (fexpr__30831.cljs$core$IFn$_invoke$arity$2 ? fexpr__30831.cljs$core$IFn$_invoke$arity$2(G__30832,G__30833) : fexpr__30831.call(null,G__30832,G__30833));
});
project.core.return_div = (function project$core$return_div(value,msg){
return new cljs.core.PersistentVector(null, 5, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"on-click","on-click",1632826543),(function (){
return project.core.time_travel(value);
}),new cljs.core.Keyword(null,"style","style",-496642736),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"color","color",1011675173),"blue"], null)], null),value," ",msg], null);
});
project.core.home_page = (function project$core$home_page(){
var show = (function (){var fexpr__30834 = project.model.query();
return (fexpr__30834.cljs$core$IFn$_invoke$arity$1 ? fexpr__30834.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"db","db",993250759)) : fexpr__30834.call(null,new cljs.core.Keyword(null,"db","db",993250759)));
})();
var current_number = new cljs.core.Keyword(null,"number","number",1570378438).cljs$core$IFn$_invoke$arity$1(cljs.core.deref(project.model.app_state));
return new cljs.core.PersistentVector(null, 6, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632),"counter ",current_number,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"input","input",556931961),new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"type","type",1174270348),"button",new cljs.core.Keyword(null,"value","value",305978217),"Increment!",new cljs.core.Keyword(null,"on-click","on-click",1632826543),(function (){
return project.model.dispatch("increase");
})], null)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"input","input",556931961),new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"type","type",1174270348),"button",new cljs.core.Keyword(null,"value","value",305978217),"decrement!",new cljs.core.Keyword(null,"on-click","on-click",1632826543),(function (){
return project.model.dispatch("decrease");
})], null)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632),(function (){var iter__4652__auto__ = (function project$core$home_page_$_iter__30835(s__30836){
return (new cljs.core.LazySeq(null,(function (){
var s__30836__$1 = s__30836;
while(true){
var temp__5753__auto__ = cljs.core.seq(s__30836__$1);
if(temp__5753__auto__){
var s__30836__$2 = temp__5753__auto__;
if(cljs.core.chunked_seq_QMARK_(s__30836__$2)){
var c__4650__auto__ = cljs.core.chunk_first(s__30836__$2);
var size__4651__auto__ = cljs.core.count(c__4650__auto__);
var b__30838 = cljs.core.chunk_buffer(size__4651__auto__);
if((function (){var i__30837 = (0);
while(true){
if((i__30837 < size__4651__auto__)){
var vec__30839 = cljs.core._nth(c__4650__auto__,i__30837);
var value = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__30839,(0),null);
var msg = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__30839,(1),null);
cljs.core.chunk_append(b__30838,new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [project.core.return_div,value,msg], null));

var G__30845 = (i__30837 + (1));
i__30837 = G__30845;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons(cljs.core.chunk(b__30838),project$core$home_page_$_iter__30835(cljs.core.chunk_rest(s__30836__$2)));
} else {
return cljs.core.chunk_cons(cljs.core.chunk(b__30838),null);
}
} else {
var vec__30842 = cljs.core.first(s__30836__$2);
var value = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__30842,(0),null);
var msg = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__30842,(1),null);
return cljs.core.cons(new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [project.core.return_div,value,msg], null),project$core$home_page_$_iter__30835(cljs.core.rest(s__30836__$2)));
}
} else {
return null;
}
break;
}
}),null,null));
});
return iter__4652__auto__(show);
})()], null)], null);
});
project.core.mount_root = (function project$core$mount_root(){
return reagent.dom.render.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [project.core.home_page], null),document.getElementById("app"));
});
project.core.init_BANG_ = (function project$core$init_BANG_(){
return project.core.mount_root();
});
goog.exportSymbol('project.core.init_BANG_', project.core.init_BANG_);

//# sourceMappingURL=project.core.js.map
