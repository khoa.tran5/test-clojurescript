goog.provide('shadow.cljs.devtools.client.browser');
shadow.cljs.devtools.client.browser.devtools_msg = (function shadow$cljs$devtools$client$browser$devtools_msg(var_args){
var args__4870__auto__ = [];
var len__4864__auto___37808 = arguments.length;
var i__4865__auto___37809 = (0);
while(true){
if((i__4865__auto___37809 < len__4864__auto___37808)){
args__4870__auto__.push((arguments[i__4865__auto___37809]));

var G__37810 = (i__4865__auto___37809 + (1));
i__4865__auto___37809 = G__37810;
continue;
} else {
}
break;
}

var argseq__4871__auto__ = ((((1) < args__4870__auto__.length))?(new cljs.core.IndexedSeq(args__4870__auto__.slice((1)),(0),null)):null);
return shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__4871__auto__);
});

(shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic = (function (msg,args){
if(shadow.cljs.devtools.client.env.log){
if(cljs.core.seq(shadow.cljs.devtools.client.env.log_style)){
return console.log.apply(console,cljs.core.into_array.cljs$core$IFn$_invoke$arity$1(cljs.core.into.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [["%cshadow-cljs: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(msg)].join(''),shadow.cljs.devtools.client.env.log_style], null),args)));
} else {
return console.log.apply(console,cljs.core.into_array.cljs$core$IFn$_invoke$arity$1(cljs.core.into.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [["shadow-cljs: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(msg)].join('')], null),args)));
}
} else {
return null;
}
}));

(shadow.cljs.devtools.client.browser.devtools_msg.cljs$lang$maxFixedArity = (1));

/** @this {Function} */
(shadow.cljs.devtools.client.browser.devtools_msg.cljs$lang$applyTo = (function (seq37303){
var G__37304 = cljs.core.first(seq37303);
var seq37303__$1 = cljs.core.next(seq37303);
var self__4851__auto__ = this;
return self__4851__auto__.cljs$core$IFn$_invoke$arity$variadic(G__37304,seq37303__$1);
}));

shadow.cljs.devtools.client.browser.script_eval = (function shadow$cljs$devtools$client$browser$script_eval(code){
return goog.globalEval(code);
});
shadow.cljs.devtools.client.browser.do_js_load = (function shadow$cljs$devtools$client$browser$do_js_load(sources){
var seq__37307 = cljs.core.seq(sources);
var chunk__37308 = null;
var count__37309 = (0);
var i__37310 = (0);
while(true){
if((i__37310 < count__37309)){
var map__37317 = chunk__37308.cljs$core$IIndexed$_nth$arity$2(null,i__37310);
var map__37317__$1 = cljs.core.__destructure_map(map__37317);
var src = map__37317__$1;
var resource_id = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37317__$1,new cljs.core.Keyword(null,"resource-id","resource-id",-1308422582));
var output_name = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37317__$1,new cljs.core.Keyword(null,"output-name","output-name",-1769107767));
var resource_name = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37317__$1,new cljs.core.Keyword(null,"resource-name","resource-name",2001617100));
var js = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37317__$1,new cljs.core.Keyword(null,"js","js",1768080579));
$CLJS.SHADOW_ENV.setLoaded(output_name);

shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("load JS",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([resource_name], 0));

shadow.cljs.devtools.client.env.before_load_src(src);

try{shadow.cljs.devtools.client.browser.script_eval([cljs.core.str.cljs$core$IFn$_invoke$arity$1(js),"\n//# sourceURL=",cljs.core.str.cljs$core$IFn$_invoke$arity$1($CLJS.SHADOW_ENV.scriptBase),cljs.core.str.cljs$core$IFn$_invoke$arity$1(output_name)].join(''));
}catch (e37318){var e_37814 = e37318;
if(shadow.cljs.devtools.client.env.log){
console.error(["Failed to load ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(resource_name)].join(''),e_37814);
} else {
}

throw (new Error(["Failed to load ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(resource_name),": ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(e_37814.message)].join('')));
}

var G__37815 = seq__37307;
var G__37816 = chunk__37308;
var G__37817 = count__37309;
var G__37818 = (i__37310 + (1));
seq__37307 = G__37815;
chunk__37308 = G__37816;
count__37309 = G__37817;
i__37310 = G__37818;
continue;
} else {
var temp__5753__auto__ = cljs.core.seq(seq__37307);
if(temp__5753__auto__){
var seq__37307__$1 = temp__5753__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__37307__$1)){
var c__4679__auto__ = cljs.core.chunk_first(seq__37307__$1);
var G__37819 = cljs.core.chunk_rest(seq__37307__$1);
var G__37820 = c__4679__auto__;
var G__37821 = cljs.core.count(c__4679__auto__);
var G__37822 = (0);
seq__37307 = G__37819;
chunk__37308 = G__37820;
count__37309 = G__37821;
i__37310 = G__37822;
continue;
} else {
var map__37319 = cljs.core.first(seq__37307__$1);
var map__37319__$1 = cljs.core.__destructure_map(map__37319);
var src = map__37319__$1;
var resource_id = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37319__$1,new cljs.core.Keyword(null,"resource-id","resource-id",-1308422582));
var output_name = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37319__$1,new cljs.core.Keyword(null,"output-name","output-name",-1769107767));
var resource_name = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37319__$1,new cljs.core.Keyword(null,"resource-name","resource-name",2001617100));
var js = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37319__$1,new cljs.core.Keyword(null,"js","js",1768080579));
$CLJS.SHADOW_ENV.setLoaded(output_name);

shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("load JS",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([resource_name], 0));

shadow.cljs.devtools.client.env.before_load_src(src);

try{shadow.cljs.devtools.client.browser.script_eval([cljs.core.str.cljs$core$IFn$_invoke$arity$1(js),"\n//# sourceURL=",cljs.core.str.cljs$core$IFn$_invoke$arity$1($CLJS.SHADOW_ENV.scriptBase),cljs.core.str.cljs$core$IFn$_invoke$arity$1(output_name)].join(''));
}catch (e37320){var e_37823 = e37320;
if(shadow.cljs.devtools.client.env.log){
console.error(["Failed to load ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(resource_name)].join(''),e_37823);
} else {
}

throw (new Error(["Failed to load ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(resource_name),": ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(e_37823.message)].join('')));
}

var G__37824 = cljs.core.next(seq__37307__$1);
var G__37825 = null;
var G__37826 = (0);
var G__37827 = (0);
seq__37307 = G__37824;
chunk__37308 = G__37825;
count__37309 = G__37826;
i__37310 = G__37827;
continue;
}
} else {
return null;
}
}
break;
}
});
shadow.cljs.devtools.client.browser.do_js_reload = (function shadow$cljs$devtools$client$browser$do_js_reload(msg,sources,complete_fn,failure_fn){
return shadow.cljs.devtools.client.env.do_js_reload.cljs$core$IFn$_invoke$arity$4(cljs.core.assoc.cljs$core$IFn$_invoke$arity$variadic(msg,new cljs.core.Keyword(null,"log-missing-fn","log-missing-fn",732676765),(function (fn_sym){
return null;
}),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"log-call-async","log-call-async",183826192),(function (fn_sym){
return shadow.cljs.devtools.client.browser.devtools_msg(["call async ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(fn_sym)].join(''));
}),new cljs.core.Keyword(null,"log-call","log-call",412404391),(function (fn_sym){
return shadow.cljs.devtools.client.browser.devtools_msg(["call ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(fn_sym)].join(''));
})], 0)),(function (){
return shadow.cljs.devtools.client.browser.do_js_load(sources);
}),complete_fn,failure_fn);
});
/**
 * when (require '["some-str" :as x]) is done at the REPL we need to manually call the shadow.js.require for it
 * since the file only adds the shadow$provide. only need to do this for shadow-js.
 */
shadow.cljs.devtools.client.browser.do_js_requires = (function shadow$cljs$devtools$client$browser$do_js_requires(js_requires){
var seq__37329 = cljs.core.seq(js_requires);
var chunk__37330 = null;
var count__37331 = (0);
var i__37332 = (0);
while(true){
if((i__37332 < count__37331)){
var js_ns = chunk__37330.cljs$core$IIndexed$_nth$arity$2(null,i__37332);
var require_str_37828 = ["var ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(js_ns)," = shadow.js.require(\"",cljs.core.str.cljs$core$IFn$_invoke$arity$1(js_ns),"\");"].join('');
shadow.cljs.devtools.client.browser.script_eval(require_str_37828);


var G__37829 = seq__37329;
var G__37830 = chunk__37330;
var G__37831 = count__37331;
var G__37832 = (i__37332 + (1));
seq__37329 = G__37829;
chunk__37330 = G__37830;
count__37331 = G__37831;
i__37332 = G__37832;
continue;
} else {
var temp__5753__auto__ = cljs.core.seq(seq__37329);
if(temp__5753__auto__){
var seq__37329__$1 = temp__5753__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__37329__$1)){
var c__4679__auto__ = cljs.core.chunk_first(seq__37329__$1);
var G__37833 = cljs.core.chunk_rest(seq__37329__$1);
var G__37834 = c__4679__auto__;
var G__37835 = cljs.core.count(c__4679__auto__);
var G__37836 = (0);
seq__37329 = G__37833;
chunk__37330 = G__37834;
count__37331 = G__37835;
i__37332 = G__37836;
continue;
} else {
var js_ns = cljs.core.first(seq__37329__$1);
var require_str_37837 = ["var ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(js_ns)," = shadow.js.require(\"",cljs.core.str.cljs$core$IFn$_invoke$arity$1(js_ns),"\");"].join('');
shadow.cljs.devtools.client.browser.script_eval(require_str_37837);


var G__37838 = cljs.core.next(seq__37329__$1);
var G__37839 = null;
var G__37840 = (0);
var G__37841 = (0);
seq__37329 = G__37838;
chunk__37330 = G__37839;
count__37331 = G__37840;
i__37332 = G__37841;
continue;
}
} else {
return null;
}
}
break;
}
});
shadow.cljs.devtools.client.browser.handle_build_complete = (function shadow$cljs$devtools$client$browser$handle_build_complete(runtime,p__37336){
var map__37337 = p__37336;
var map__37337__$1 = cljs.core.__destructure_map(map__37337);
var msg = map__37337__$1;
var info = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37337__$1,new cljs.core.Keyword(null,"info","info",-317069002));
var reload_info = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37337__$1,new cljs.core.Keyword(null,"reload-info","reload-info",1648088086));
var warnings = cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentVector.EMPTY,cljs.core.distinct.cljs$core$IFn$_invoke$arity$1((function (){var iter__4652__auto__ = (function shadow$cljs$devtools$client$browser$handle_build_complete_$_iter__37339(s__37340){
return (new cljs.core.LazySeq(null,(function (){
var s__37340__$1 = s__37340;
while(true){
var temp__5753__auto__ = cljs.core.seq(s__37340__$1);
if(temp__5753__auto__){
var xs__6308__auto__ = temp__5753__auto__;
var map__37345 = cljs.core.first(xs__6308__auto__);
var map__37345__$1 = cljs.core.__destructure_map(map__37345);
var src = map__37345__$1;
var resource_name = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37345__$1,new cljs.core.Keyword(null,"resource-name","resource-name",2001617100));
var warnings = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37345__$1,new cljs.core.Keyword(null,"warnings","warnings",-735437651));
if(cljs.core.not(new cljs.core.Keyword(null,"from-jar","from-jar",1050932827).cljs$core$IFn$_invoke$arity$1(src))){
var iterys__4648__auto__ = ((function (s__37340__$1,map__37345,map__37345__$1,src,resource_name,warnings,xs__6308__auto__,temp__5753__auto__,map__37337,map__37337__$1,msg,info,reload_info){
return (function shadow$cljs$devtools$client$browser$handle_build_complete_$_iter__37339_$_iter__37341(s__37342){
return (new cljs.core.LazySeq(null,((function (s__37340__$1,map__37345,map__37345__$1,src,resource_name,warnings,xs__6308__auto__,temp__5753__auto__,map__37337,map__37337__$1,msg,info,reload_info){
return (function (){
var s__37342__$1 = s__37342;
while(true){
var temp__5753__auto____$1 = cljs.core.seq(s__37342__$1);
if(temp__5753__auto____$1){
var s__37342__$2 = temp__5753__auto____$1;
if(cljs.core.chunked_seq_QMARK_(s__37342__$2)){
var c__4650__auto__ = cljs.core.chunk_first(s__37342__$2);
var size__4651__auto__ = cljs.core.count(c__4650__auto__);
var b__37344 = cljs.core.chunk_buffer(size__4651__auto__);
if((function (){var i__37343 = (0);
while(true){
if((i__37343 < size__4651__auto__)){
var warning = cljs.core._nth(c__4650__auto__,i__37343);
cljs.core.chunk_append(b__37344,cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(warning,new cljs.core.Keyword(null,"resource-name","resource-name",2001617100),resource_name));

var G__37842 = (i__37343 + (1));
i__37343 = G__37842;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons(cljs.core.chunk(b__37344),shadow$cljs$devtools$client$browser$handle_build_complete_$_iter__37339_$_iter__37341(cljs.core.chunk_rest(s__37342__$2)));
} else {
return cljs.core.chunk_cons(cljs.core.chunk(b__37344),null);
}
} else {
var warning = cljs.core.first(s__37342__$2);
return cljs.core.cons(cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(warning,new cljs.core.Keyword(null,"resource-name","resource-name",2001617100),resource_name),shadow$cljs$devtools$client$browser$handle_build_complete_$_iter__37339_$_iter__37341(cljs.core.rest(s__37342__$2)));
}
} else {
return null;
}
break;
}
});})(s__37340__$1,map__37345,map__37345__$1,src,resource_name,warnings,xs__6308__auto__,temp__5753__auto__,map__37337,map__37337__$1,msg,info,reload_info))
,null,null));
});})(s__37340__$1,map__37345,map__37345__$1,src,resource_name,warnings,xs__6308__auto__,temp__5753__auto__,map__37337,map__37337__$1,msg,info,reload_info))
;
var fs__4649__auto__ = cljs.core.seq(iterys__4648__auto__(warnings));
if(fs__4649__auto__){
return cljs.core.concat.cljs$core$IFn$_invoke$arity$2(fs__4649__auto__,shadow$cljs$devtools$client$browser$handle_build_complete_$_iter__37339(cljs.core.rest(s__37340__$1)));
} else {
var G__37843 = cljs.core.rest(s__37340__$1);
s__37340__$1 = G__37843;
continue;
}
} else {
var G__37844 = cljs.core.rest(s__37340__$1);
s__37340__$1 = G__37844;
continue;
}
} else {
return null;
}
break;
}
}),null,null));
});
return iter__4652__auto__(new cljs.core.Keyword(null,"sources","sources",-321166424).cljs$core$IFn$_invoke$arity$1(info));
})()));
if(shadow.cljs.devtools.client.env.log){
var seq__37346_37845 = cljs.core.seq(warnings);
var chunk__37347_37846 = null;
var count__37348_37847 = (0);
var i__37349_37848 = (0);
while(true){
if((i__37349_37848 < count__37348_37847)){
var map__37354_37849 = chunk__37347_37846.cljs$core$IIndexed$_nth$arity$2(null,i__37349_37848);
var map__37354_37850__$1 = cljs.core.__destructure_map(map__37354_37849);
var w_37851 = map__37354_37850__$1;
var msg_37852__$1 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37354_37850__$1,new cljs.core.Keyword(null,"msg","msg",-1386103444));
var line_37853 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37354_37850__$1,new cljs.core.Keyword(null,"line","line",212345235));
var column_37854 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37354_37850__$1,new cljs.core.Keyword(null,"column","column",2078222095));
var resource_name_37855 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37354_37850__$1,new cljs.core.Keyword(null,"resource-name","resource-name",2001617100));
console.warn(["BUILD-WARNING in ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(resource_name_37855)," at [",cljs.core.str.cljs$core$IFn$_invoke$arity$1(line_37853),":",cljs.core.str.cljs$core$IFn$_invoke$arity$1(column_37854),"]\n\t",cljs.core.str.cljs$core$IFn$_invoke$arity$1(msg_37852__$1)].join(''));


var G__37856 = seq__37346_37845;
var G__37857 = chunk__37347_37846;
var G__37858 = count__37348_37847;
var G__37859 = (i__37349_37848 + (1));
seq__37346_37845 = G__37856;
chunk__37347_37846 = G__37857;
count__37348_37847 = G__37858;
i__37349_37848 = G__37859;
continue;
} else {
var temp__5753__auto___37860 = cljs.core.seq(seq__37346_37845);
if(temp__5753__auto___37860){
var seq__37346_37861__$1 = temp__5753__auto___37860;
if(cljs.core.chunked_seq_QMARK_(seq__37346_37861__$1)){
var c__4679__auto___37862 = cljs.core.chunk_first(seq__37346_37861__$1);
var G__37863 = cljs.core.chunk_rest(seq__37346_37861__$1);
var G__37864 = c__4679__auto___37862;
var G__37865 = cljs.core.count(c__4679__auto___37862);
var G__37866 = (0);
seq__37346_37845 = G__37863;
chunk__37347_37846 = G__37864;
count__37348_37847 = G__37865;
i__37349_37848 = G__37866;
continue;
} else {
var map__37359_37867 = cljs.core.first(seq__37346_37861__$1);
var map__37359_37868__$1 = cljs.core.__destructure_map(map__37359_37867);
var w_37869 = map__37359_37868__$1;
var msg_37870__$1 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37359_37868__$1,new cljs.core.Keyword(null,"msg","msg",-1386103444));
var line_37871 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37359_37868__$1,new cljs.core.Keyword(null,"line","line",212345235));
var column_37872 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37359_37868__$1,new cljs.core.Keyword(null,"column","column",2078222095));
var resource_name_37873 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37359_37868__$1,new cljs.core.Keyword(null,"resource-name","resource-name",2001617100));
console.warn(["BUILD-WARNING in ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(resource_name_37873)," at [",cljs.core.str.cljs$core$IFn$_invoke$arity$1(line_37871),":",cljs.core.str.cljs$core$IFn$_invoke$arity$1(column_37872),"]\n\t",cljs.core.str.cljs$core$IFn$_invoke$arity$1(msg_37870__$1)].join(''));


var G__37874 = cljs.core.next(seq__37346_37861__$1);
var G__37875 = null;
var G__37876 = (0);
var G__37877 = (0);
seq__37346_37845 = G__37874;
chunk__37347_37846 = G__37875;
count__37348_37847 = G__37876;
i__37349_37848 = G__37877;
continue;
}
} else {
}
}
break;
}
} else {
}

if((!(shadow.cljs.devtools.client.env.autoload))){
return shadow.cljs.devtools.client.hud.load_end_success();
} else {
if(((cljs.core.empty_QMARK_(warnings)) || (shadow.cljs.devtools.client.env.ignore_warnings))){
var sources_to_get = shadow.cljs.devtools.client.env.filter_reload_sources(info,reload_info);
if(cljs.core.not(cljs.core.seq(sources_to_get))){
return shadow.cljs.devtools.client.hud.load_end_success();
} else {
if(cljs.core.seq(cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(msg,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"reload-info","reload-info",1648088086),new cljs.core.Keyword(null,"after-load","after-load",-1278503285)], null)))){
} else {
shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("reloading code but no :after-load hooks are configured!",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["https://shadow-cljs.github.io/docs/UsersGuide.html#_lifecycle_hooks"], 0));
}

return shadow.cljs.devtools.client.shared.load_sources(runtime,sources_to_get,(function (p1__37334_SHARP_){
return shadow.cljs.devtools.client.browser.do_js_reload(msg,p1__37334_SHARP_,shadow.cljs.devtools.client.hud.load_end_success,shadow.cljs.devtools.client.hud.load_failure);
}));
}
} else {
return null;
}
}
});
shadow.cljs.devtools.client.browser.page_load_uri = (cljs.core.truth_(goog.global.document)?goog.Uri.parse(document.location.href):null);
shadow.cljs.devtools.client.browser.match_paths = (function shadow$cljs$devtools$client$browser$match_paths(old,new$){
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2("file",shadow.cljs.devtools.client.browser.page_load_uri.getScheme())){
var rel_new = cljs.core.subs.cljs$core$IFn$_invoke$arity$2(new$,(1));
if(((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(old,rel_new)) || (clojure.string.starts_with_QMARK_(old,[rel_new,"?"].join(''))))){
return rel_new;
} else {
return null;
}
} else {
var node_uri = goog.Uri.parse(old);
var node_uri_resolved = shadow.cljs.devtools.client.browser.page_load_uri.resolve(node_uri);
var node_abs = node_uri_resolved.getPath();
var and__4251__auto__ = ((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$1(shadow.cljs.devtools.client.browser.page_load_uri.hasSameDomainAs(node_uri))) || (cljs.core.not(node_uri.hasDomain())));
if(and__4251__auto__){
var and__4251__auto____$1 = cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(node_abs,new$);
if(and__4251__auto____$1){
return new$;
} else {
return and__4251__auto____$1;
}
} else {
return and__4251__auto__;
}
}
});
shadow.cljs.devtools.client.browser.handle_asset_update = (function shadow$cljs$devtools$client$browser$handle_asset_update(p__37364){
var map__37365 = p__37364;
var map__37365__$1 = cljs.core.__destructure_map(map__37365);
var msg = map__37365__$1;
var updates = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37365__$1,new cljs.core.Keyword(null,"updates","updates",2013983452));
var reload_info = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37365__$1,new cljs.core.Keyword(null,"reload-info","reload-info",1648088086));
var seq__37366 = cljs.core.seq(updates);
var chunk__37371 = null;
var count__37372 = (0);
var i__37373 = (0);
while(true){
if((i__37373 < count__37372)){
var path = chunk__37371.cljs$core$IIndexed$_nth$arity$2(null,i__37373);
if(clojure.string.ends_with_QMARK_(path,"css")){
var seq__37560_37880 = cljs.core.seq(cljs.core.array_seq.cljs$core$IFn$_invoke$arity$1(document.querySelectorAll("link[rel=\"stylesheet\"]")));
var chunk__37564_37881 = null;
var count__37565_37882 = (0);
var i__37566_37883 = (0);
while(true){
if((i__37566_37883 < count__37565_37882)){
var node_37884 = chunk__37564_37881.cljs$core$IIndexed$_nth$arity$2(null,i__37566_37883);
if(cljs.core.not(node_37884.shadow$old)){
var path_match_37888 = shadow.cljs.devtools.client.browser.match_paths(node_37884.getAttribute("href"),path);
if(cljs.core.truth_(path_match_37888)){
var new_link_37889 = (function (){var G__37602 = node_37884.cloneNode(true);
G__37602.setAttribute("href",[cljs.core.str.cljs$core$IFn$_invoke$arity$1(path_match_37888),"?r=",cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.rand.cljs$core$IFn$_invoke$arity$0())].join(''));

return G__37602;
})();
(node_37884.shadow$old = true);

(new_link_37889.onload = ((function (seq__37560_37880,chunk__37564_37881,count__37565_37882,i__37566_37883,seq__37366,chunk__37371,count__37372,i__37373,new_link_37889,path_match_37888,node_37884,path,map__37365,map__37365__$1,msg,updates,reload_info){
return (function (e){
var seq__37603_37893 = cljs.core.seq(cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(msg,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"reload-info","reload-info",1648088086),new cljs.core.Keyword(null,"asset-load","asset-load",-1925902322)], null)));
var chunk__37605_37894 = null;
var count__37606_37895 = (0);
var i__37607_37896 = (0);
while(true){
if((i__37607_37896 < count__37606_37895)){
var map__37614_37897 = chunk__37605_37894.cljs$core$IIndexed$_nth$arity$2(null,i__37607_37896);
var map__37614_37898__$1 = cljs.core.__destructure_map(map__37614_37897);
var task_37899 = map__37614_37898__$1;
var fn_str_37900 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37614_37898__$1,new cljs.core.Keyword(null,"fn-str","fn-str",-1348506402));
var fn_sym_37901 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37614_37898__$1,new cljs.core.Keyword(null,"fn-sym","fn-sym",1423988510));
var fn_obj_37902 = goog.getObjectByName(fn_str_37900,$CLJS);
shadow.cljs.devtools.client.browser.devtools_msg(["call ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(fn_sym_37901)].join(''));

(fn_obj_37902.cljs$core$IFn$_invoke$arity$2 ? fn_obj_37902.cljs$core$IFn$_invoke$arity$2(path,new_link_37889) : fn_obj_37902.call(null,path,new_link_37889));


var G__37903 = seq__37603_37893;
var G__37904 = chunk__37605_37894;
var G__37905 = count__37606_37895;
var G__37906 = (i__37607_37896 + (1));
seq__37603_37893 = G__37903;
chunk__37605_37894 = G__37904;
count__37606_37895 = G__37905;
i__37607_37896 = G__37906;
continue;
} else {
var temp__5753__auto___37907 = cljs.core.seq(seq__37603_37893);
if(temp__5753__auto___37907){
var seq__37603_37908__$1 = temp__5753__auto___37907;
if(cljs.core.chunked_seq_QMARK_(seq__37603_37908__$1)){
var c__4679__auto___37909 = cljs.core.chunk_first(seq__37603_37908__$1);
var G__37910 = cljs.core.chunk_rest(seq__37603_37908__$1);
var G__37911 = c__4679__auto___37909;
var G__37912 = cljs.core.count(c__4679__auto___37909);
var G__37913 = (0);
seq__37603_37893 = G__37910;
chunk__37605_37894 = G__37911;
count__37606_37895 = G__37912;
i__37607_37896 = G__37913;
continue;
} else {
var map__37618_37914 = cljs.core.first(seq__37603_37908__$1);
var map__37618_37915__$1 = cljs.core.__destructure_map(map__37618_37914);
var task_37916 = map__37618_37915__$1;
var fn_str_37917 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37618_37915__$1,new cljs.core.Keyword(null,"fn-str","fn-str",-1348506402));
var fn_sym_37918 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37618_37915__$1,new cljs.core.Keyword(null,"fn-sym","fn-sym",1423988510));
var fn_obj_37919 = goog.getObjectByName(fn_str_37917,$CLJS);
shadow.cljs.devtools.client.browser.devtools_msg(["call ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(fn_sym_37918)].join(''));

(fn_obj_37919.cljs$core$IFn$_invoke$arity$2 ? fn_obj_37919.cljs$core$IFn$_invoke$arity$2(path,new_link_37889) : fn_obj_37919.call(null,path,new_link_37889));


var G__37920 = cljs.core.next(seq__37603_37908__$1);
var G__37921 = null;
var G__37922 = (0);
var G__37923 = (0);
seq__37603_37893 = G__37920;
chunk__37605_37894 = G__37921;
count__37606_37895 = G__37922;
i__37607_37896 = G__37923;
continue;
}
} else {
}
}
break;
}

return goog.dom.removeNode(node_37884);
});})(seq__37560_37880,chunk__37564_37881,count__37565_37882,i__37566_37883,seq__37366,chunk__37371,count__37372,i__37373,new_link_37889,path_match_37888,node_37884,path,map__37365,map__37365__$1,msg,updates,reload_info))
);

shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("load CSS",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([path_match_37888], 0));

goog.dom.insertSiblingAfter(new_link_37889,node_37884);


var G__37924 = seq__37560_37880;
var G__37925 = chunk__37564_37881;
var G__37926 = count__37565_37882;
var G__37927 = (i__37566_37883 + (1));
seq__37560_37880 = G__37924;
chunk__37564_37881 = G__37925;
count__37565_37882 = G__37926;
i__37566_37883 = G__37927;
continue;
} else {
var G__37928 = seq__37560_37880;
var G__37929 = chunk__37564_37881;
var G__37930 = count__37565_37882;
var G__37931 = (i__37566_37883 + (1));
seq__37560_37880 = G__37928;
chunk__37564_37881 = G__37929;
count__37565_37882 = G__37930;
i__37566_37883 = G__37931;
continue;
}
} else {
var G__37932 = seq__37560_37880;
var G__37933 = chunk__37564_37881;
var G__37934 = count__37565_37882;
var G__37935 = (i__37566_37883 + (1));
seq__37560_37880 = G__37932;
chunk__37564_37881 = G__37933;
count__37565_37882 = G__37934;
i__37566_37883 = G__37935;
continue;
}
} else {
var temp__5753__auto___37936 = cljs.core.seq(seq__37560_37880);
if(temp__5753__auto___37936){
var seq__37560_37937__$1 = temp__5753__auto___37936;
if(cljs.core.chunked_seq_QMARK_(seq__37560_37937__$1)){
var c__4679__auto___37938 = cljs.core.chunk_first(seq__37560_37937__$1);
var G__37939 = cljs.core.chunk_rest(seq__37560_37937__$1);
var G__37940 = c__4679__auto___37938;
var G__37941 = cljs.core.count(c__4679__auto___37938);
var G__37942 = (0);
seq__37560_37880 = G__37939;
chunk__37564_37881 = G__37940;
count__37565_37882 = G__37941;
i__37566_37883 = G__37942;
continue;
} else {
var node_37943 = cljs.core.first(seq__37560_37937__$1);
if(cljs.core.not(node_37943.shadow$old)){
var path_match_37944 = shadow.cljs.devtools.client.browser.match_paths(node_37943.getAttribute("href"),path);
if(cljs.core.truth_(path_match_37944)){
var new_link_37945 = (function (){var G__37619 = node_37943.cloneNode(true);
G__37619.setAttribute("href",[cljs.core.str.cljs$core$IFn$_invoke$arity$1(path_match_37944),"?r=",cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.rand.cljs$core$IFn$_invoke$arity$0())].join(''));

return G__37619;
})();
(node_37943.shadow$old = true);

(new_link_37945.onload = ((function (seq__37560_37880,chunk__37564_37881,count__37565_37882,i__37566_37883,seq__37366,chunk__37371,count__37372,i__37373,new_link_37945,path_match_37944,node_37943,seq__37560_37937__$1,temp__5753__auto___37936,path,map__37365,map__37365__$1,msg,updates,reload_info){
return (function (e){
var seq__37620_37946 = cljs.core.seq(cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(msg,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"reload-info","reload-info",1648088086),new cljs.core.Keyword(null,"asset-load","asset-load",-1925902322)], null)));
var chunk__37622_37947 = null;
var count__37623_37948 = (0);
var i__37624_37949 = (0);
while(true){
if((i__37624_37949 < count__37623_37948)){
var map__37628_37950 = chunk__37622_37947.cljs$core$IIndexed$_nth$arity$2(null,i__37624_37949);
var map__37628_37951__$1 = cljs.core.__destructure_map(map__37628_37950);
var task_37952 = map__37628_37951__$1;
var fn_str_37953 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37628_37951__$1,new cljs.core.Keyword(null,"fn-str","fn-str",-1348506402));
var fn_sym_37954 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37628_37951__$1,new cljs.core.Keyword(null,"fn-sym","fn-sym",1423988510));
var fn_obj_37955 = goog.getObjectByName(fn_str_37953,$CLJS);
shadow.cljs.devtools.client.browser.devtools_msg(["call ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(fn_sym_37954)].join(''));

(fn_obj_37955.cljs$core$IFn$_invoke$arity$2 ? fn_obj_37955.cljs$core$IFn$_invoke$arity$2(path,new_link_37945) : fn_obj_37955.call(null,path,new_link_37945));


var G__37956 = seq__37620_37946;
var G__37957 = chunk__37622_37947;
var G__37958 = count__37623_37948;
var G__37959 = (i__37624_37949 + (1));
seq__37620_37946 = G__37956;
chunk__37622_37947 = G__37957;
count__37623_37948 = G__37958;
i__37624_37949 = G__37959;
continue;
} else {
var temp__5753__auto___37960__$1 = cljs.core.seq(seq__37620_37946);
if(temp__5753__auto___37960__$1){
var seq__37620_37964__$1 = temp__5753__auto___37960__$1;
if(cljs.core.chunked_seq_QMARK_(seq__37620_37964__$1)){
var c__4679__auto___37965 = cljs.core.chunk_first(seq__37620_37964__$1);
var G__37966 = cljs.core.chunk_rest(seq__37620_37964__$1);
var G__37967 = c__4679__auto___37965;
var G__37968 = cljs.core.count(c__4679__auto___37965);
var G__37969 = (0);
seq__37620_37946 = G__37966;
chunk__37622_37947 = G__37967;
count__37623_37948 = G__37968;
i__37624_37949 = G__37969;
continue;
} else {
var map__37630_37970 = cljs.core.first(seq__37620_37964__$1);
var map__37630_37971__$1 = cljs.core.__destructure_map(map__37630_37970);
var task_37972 = map__37630_37971__$1;
var fn_str_37973 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37630_37971__$1,new cljs.core.Keyword(null,"fn-str","fn-str",-1348506402));
var fn_sym_37974 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37630_37971__$1,new cljs.core.Keyword(null,"fn-sym","fn-sym",1423988510));
var fn_obj_37975 = goog.getObjectByName(fn_str_37973,$CLJS);
shadow.cljs.devtools.client.browser.devtools_msg(["call ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(fn_sym_37974)].join(''));

(fn_obj_37975.cljs$core$IFn$_invoke$arity$2 ? fn_obj_37975.cljs$core$IFn$_invoke$arity$2(path,new_link_37945) : fn_obj_37975.call(null,path,new_link_37945));


var G__37976 = cljs.core.next(seq__37620_37964__$1);
var G__37977 = null;
var G__37978 = (0);
var G__37979 = (0);
seq__37620_37946 = G__37976;
chunk__37622_37947 = G__37977;
count__37623_37948 = G__37978;
i__37624_37949 = G__37979;
continue;
}
} else {
}
}
break;
}

return goog.dom.removeNode(node_37943);
});})(seq__37560_37880,chunk__37564_37881,count__37565_37882,i__37566_37883,seq__37366,chunk__37371,count__37372,i__37373,new_link_37945,path_match_37944,node_37943,seq__37560_37937__$1,temp__5753__auto___37936,path,map__37365,map__37365__$1,msg,updates,reload_info))
);

shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("load CSS",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([path_match_37944], 0));

goog.dom.insertSiblingAfter(new_link_37945,node_37943);


var G__37980 = cljs.core.next(seq__37560_37937__$1);
var G__37981 = null;
var G__37982 = (0);
var G__37983 = (0);
seq__37560_37880 = G__37980;
chunk__37564_37881 = G__37981;
count__37565_37882 = G__37982;
i__37566_37883 = G__37983;
continue;
} else {
var G__37984 = cljs.core.next(seq__37560_37937__$1);
var G__37985 = null;
var G__37986 = (0);
var G__37987 = (0);
seq__37560_37880 = G__37984;
chunk__37564_37881 = G__37985;
count__37565_37882 = G__37986;
i__37566_37883 = G__37987;
continue;
}
} else {
var G__37988 = cljs.core.next(seq__37560_37937__$1);
var G__37989 = null;
var G__37990 = (0);
var G__37991 = (0);
seq__37560_37880 = G__37988;
chunk__37564_37881 = G__37989;
count__37565_37882 = G__37990;
i__37566_37883 = G__37991;
continue;
}
}
} else {
}
}
break;
}


var G__37992 = seq__37366;
var G__37993 = chunk__37371;
var G__37994 = count__37372;
var G__37995 = (i__37373 + (1));
seq__37366 = G__37992;
chunk__37371 = G__37993;
count__37372 = G__37994;
i__37373 = G__37995;
continue;
} else {
var G__37996 = seq__37366;
var G__37997 = chunk__37371;
var G__37998 = count__37372;
var G__37999 = (i__37373 + (1));
seq__37366 = G__37996;
chunk__37371 = G__37997;
count__37372 = G__37998;
i__37373 = G__37999;
continue;
}
} else {
var temp__5753__auto__ = cljs.core.seq(seq__37366);
if(temp__5753__auto__){
var seq__37366__$1 = temp__5753__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__37366__$1)){
var c__4679__auto__ = cljs.core.chunk_first(seq__37366__$1);
var G__38000 = cljs.core.chunk_rest(seq__37366__$1);
var G__38001 = c__4679__auto__;
var G__38002 = cljs.core.count(c__4679__auto__);
var G__38003 = (0);
seq__37366 = G__38000;
chunk__37371 = G__38001;
count__37372 = G__38002;
i__37373 = G__38003;
continue;
} else {
var path = cljs.core.first(seq__37366__$1);
if(clojure.string.ends_with_QMARK_(path,"css")){
var seq__37659_38004 = cljs.core.seq(cljs.core.array_seq.cljs$core$IFn$_invoke$arity$1(document.querySelectorAll("link[rel=\"stylesheet\"]")));
var chunk__37663_38005 = null;
var count__37664_38006 = (0);
var i__37665_38007 = (0);
while(true){
if((i__37665_38007 < count__37664_38006)){
var node_38008 = chunk__37663_38005.cljs$core$IIndexed$_nth$arity$2(null,i__37665_38007);
if(cljs.core.not(node_38008.shadow$old)){
var path_match_38009 = shadow.cljs.devtools.client.browser.match_paths(node_38008.getAttribute("href"),path);
if(cljs.core.truth_(path_match_38009)){
var new_link_38010 = (function (){var G__37734 = node_38008.cloneNode(true);
G__37734.setAttribute("href",[cljs.core.str.cljs$core$IFn$_invoke$arity$1(path_match_38009),"?r=",cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.rand.cljs$core$IFn$_invoke$arity$0())].join(''));

return G__37734;
})();
(node_38008.shadow$old = true);

(new_link_38010.onload = ((function (seq__37659_38004,chunk__37663_38005,count__37664_38006,i__37665_38007,seq__37366,chunk__37371,count__37372,i__37373,new_link_38010,path_match_38009,node_38008,path,seq__37366__$1,temp__5753__auto__,map__37365,map__37365__$1,msg,updates,reload_info){
return (function (e){
var seq__37735_38011 = cljs.core.seq(cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(msg,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"reload-info","reload-info",1648088086),new cljs.core.Keyword(null,"asset-load","asset-load",-1925902322)], null)));
var chunk__37737_38012 = null;
var count__37738_38013 = (0);
var i__37739_38014 = (0);
while(true){
if((i__37739_38014 < count__37738_38013)){
var map__37743_38015 = chunk__37737_38012.cljs$core$IIndexed$_nth$arity$2(null,i__37739_38014);
var map__37743_38016__$1 = cljs.core.__destructure_map(map__37743_38015);
var task_38017 = map__37743_38016__$1;
var fn_str_38018 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37743_38016__$1,new cljs.core.Keyword(null,"fn-str","fn-str",-1348506402));
var fn_sym_38019 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37743_38016__$1,new cljs.core.Keyword(null,"fn-sym","fn-sym",1423988510));
var fn_obj_38021 = goog.getObjectByName(fn_str_38018,$CLJS);
shadow.cljs.devtools.client.browser.devtools_msg(["call ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(fn_sym_38019)].join(''));

(fn_obj_38021.cljs$core$IFn$_invoke$arity$2 ? fn_obj_38021.cljs$core$IFn$_invoke$arity$2(path,new_link_38010) : fn_obj_38021.call(null,path,new_link_38010));


var G__38022 = seq__37735_38011;
var G__38023 = chunk__37737_38012;
var G__38024 = count__37738_38013;
var G__38025 = (i__37739_38014 + (1));
seq__37735_38011 = G__38022;
chunk__37737_38012 = G__38023;
count__37738_38013 = G__38024;
i__37739_38014 = G__38025;
continue;
} else {
var temp__5753__auto___38026__$1 = cljs.core.seq(seq__37735_38011);
if(temp__5753__auto___38026__$1){
var seq__37735_38027__$1 = temp__5753__auto___38026__$1;
if(cljs.core.chunked_seq_QMARK_(seq__37735_38027__$1)){
var c__4679__auto___38028 = cljs.core.chunk_first(seq__37735_38027__$1);
var G__38029 = cljs.core.chunk_rest(seq__37735_38027__$1);
var G__38030 = c__4679__auto___38028;
var G__38031 = cljs.core.count(c__4679__auto___38028);
var G__38032 = (0);
seq__37735_38011 = G__38029;
chunk__37737_38012 = G__38030;
count__37738_38013 = G__38031;
i__37739_38014 = G__38032;
continue;
} else {
var map__37744_38033 = cljs.core.first(seq__37735_38027__$1);
var map__37744_38034__$1 = cljs.core.__destructure_map(map__37744_38033);
var task_38035 = map__37744_38034__$1;
var fn_str_38036 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37744_38034__$1,new cljs.core.Keyword(null,"fn-str","fn-str",-1348506402));
var fn_sym_38037 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37744_38034__$1,new cljs.core.Keyword(null,"fn-sym","fn-sym",1423988510));
var fn_obj_38038 = goog.getObjectByName(fn_str_38036,$CLJS);
shadow.cljs.devtools.client.browser.devtools_msg(["call ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(fn_sym_38037)].join(''));

(fn_obj_38038.cljs$core$IFn$_invoke$arity$2 ? fn_obj_38038.cljs$core$IFn$_invoke$arity$2(path,new_link_38010) : fn_obj_38038.call(null,path,new_link_38010));


var G__38039 = cljs.core.next(seq__37735_38027__$1);
var G__38040 = null;
var G__38041 = (0);
var G__38042 = (0);
seq__37735_38011 = G__38039;
chunk__37737_38012 = G__38040;
count__37738_38013 = G__38041;
i__37739_38014 = G__38042;
continue;
}
} else {
}
}
break;
}

return goog.dom.removeNode(node_38008);
});})(seq__37659_38004,chunk__37663_38005,count__37664_38006,i__37665_38007,seq__37366,chunk__37371,count__37372,i__37373,new_link_38010,path_match_38009,node_38008,path,seq__37366__$1,temp__5753__auto__,map__37365,map__37365__$1,msg,updates,reload_info))
);

shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("load CSS",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([path_match_38009], 0));

goog.dom.insertSiblingAfter(new_link_38010,node_38008);


var G__38043 = seq__37659_38004;
var G__38044 = chunk__37663_38005;
var G__38045 = count__37664_38006;
var G__38046 = (i__37665_38007 + (1));
seq__37659_38004 = G__38043;
chunk__37663_38005 = G__38044;
count__37664_38006 = G__38045;
i__37665_38007 = G__38046;
continue;
} else {
var G__38047 = seq__37659_38004;
var G__38048 = chunk__37663_38005;
var G__38049 = count__37664_38006;
var G__38050 = (i__37665_38007 + (1));
seq__37659_38004 = G__38047;
chunk__37663_38005 = G__38048;
count__37664_38006 = G__38049;
i__37665_38007 = G__38050;
continue;
}
} else {
var G__38051 = seq__37659_38004;
var G__38052 = chunk__37663_38005;
var G__38053 = count__37664_38006;
var G__38054 = (i__37665_38007 + (1));
seq__37659_38004 = G__38051;
chunk__37663_38005 = G__38052;
count__37664_38006 = G__38053;
i__37665_38007 = G__38054;
continue;
}
} else {
var temp__5753__auto___38055__$1 = cljs.core.seq(seq__37659_38004);
if(temp__5753__auto___38055__$1){
var seq__37659_38056__$1 = temp__5753__auto___38055__$1;
if(cljs.core.chunked_seq_QMARK_(seq__37659_38056__$1)){
var c__4679__auto___38057 = cljs.core.chunk_first(seq__37659_38056__$1);
var G__38058 = cljs.core.chunk_rest(seq__37659_38056__$1);
var G__38059 = c__4679__auto___38057;
var G__38060 = cljs.core.count(c__4679__auto___38057);
var G__38061 = (0);
seq__37659_38004 = G__38058;
chunk__37663_38005 = G__38059;
count__37664_38006 = G__38060;
i__37665_38007 = G__38061;
continue;
} else {
var node_38063 = cljs.core.first(seq__37659_38056__$1);
if(cljs.core.not(node_38063.shadow$old)){
var path_match_38064 = shadow.cljs.devtools.client.browser.match_paths(node_38063.getAttribute("href"),path);
if(cljs.core.truth_(path_match_38064)){
var new_link_38065 = (function (){var G__37749 = node_38063.cloneNode(true);
G__37749.setAttribute("href",[cljs.core.str.cljs$core$IFn$_invoke$arity$1(path_match_38064),"?r=",cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.rand.cljs$core$IFn$_invoke$arity$0())].join(''));

return G__37749;
})();
(node_38063.shadow$old = true);

(new_link_38065.onload = ((function (seq__37659_38004,chunk__37663_38005,count__37664_38006,i__37665_38007,seq__37366,chunk__37371,count__37372,i__37373,new_link_38065,path_match_38064,node_38063,seq__37659_38056__$1,temp__5753__auto___38055__$1,path,seq__37366__$1,temp__5753__auto__,map__37365,map__37365__$1,msg,updates,reload_info){
return (function (e){
var seq__37750_38069 = cljs.core.seq(cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(msg,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"reload-info","reload-info",1648088086),new cljs.core.Keyword(null,"asset-load","asset-load",-1925902322)], null)));
var chunk__37752_38070 = null;
var count__37753_38071 = (0);
var i__37754_38072 = (0);
while(true){
if((i__37754_38072 < count__37753_38071)){
var map__37761_38073 = chunk__37752_38070.cljs$core$IIndexed$_nth$arity$2(null,i__37754_38072);
var map__37761_38074__$1 = cljs.core.__destructure_map(map__37761_38073);
var task_38075 = map__37761_38074__$1;
var fn_str_38076 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37761_38074__$1,new cljs.core.Keyword(null,"fn-str","fn-str",-1348506402));
var fn_sym_38077 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37761_38074__$1,new cljs.core.Keyword(null,"fn-sym","fn-sym",1423988510));
var fn_obj_38078 = goog.getObjectByName(fn_str_38076,$CLJS);
shadow.cljs.devtools.client.browser.devtools_msg(["call ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(fn_sym_38077)].join(''));

(fn_obj_38078.cljs$core$IFn$_invoke$arity$2 ? fn_obj_38078.cljs$core$IFn$_invoke$arity$2(path,new_link_38065) : fn_obj_38078.call(null,path,new_link_38065));


var G__38079 = seq__37750_38069;
var G__38080 = chunk__37752_38070;
var G__38081 = count__37753_38071;
var G__38082 = (i__37754_38072 + (1));
seq__37750_38069 = G__38079;
chunk__37752_38070 = G__38080;
count__37753_38071 = G__38081;
i__37754_38072 = G__38082;
continue;
} else {
var temp__5753__auto___38083__$2 = cljs.core.seq(seq__37750_38069);
if(temp__5753__auto___38083__$2){
var seq__37750_38084__$1 = temp__5753__auto___38083__$2;
if(cljs.core.chunked_seq_QMARK_(seq__37750_38084__$1)){
var c__4679__auto___38085 = cljs.core.chunk_first(seq__37750_38084__$1);
var G__38086 = cljs.core.chunk_rest(seq__37750_38084__$1);
var G__38087 = c__4679__auto___38085;
var G__38088 = cljs.core.count(c__4679__auto___38085);
var G__38089 = (0);
seq__37750_38069 = G__38086;
chunk__37752_38070 = G__38087;
count__37753_38071 = G__38088;
i__37754_38072 = G__38089;
continue;
} else {
var map__37762_38090 = cljs.core.first(seq__37750_38084__$1);
var map__37762_38091__$1 = cljs.core.__destructure_map(map__37762_38090);
var task_38092 = map__37762_38091__$1;
var fn_str_38093 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37762_38091__$1,new cljs.core.Keyword(null,"fn-str","fn-str",-1348506402));
var fn_sym_38094 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37762_38091__$1,new cljs.core.Keyword(null,"fn-sym","fn-sym",1423988510));
var fn_obj_38095 = goog.getObjectByName(fn_str_38093,$CLJS);
shadow.cljs.devtools.client.browser.devtools_msg(["call ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(fn_sym_38094)].join(''));

(fn_obj_38095.cljs$core$IFn$_invoke$arity$2 ? fn_obj_38095.cljs$core$IFn$_invoke$arity$2(path,new_link_38065) : fn_obj_38095.call(null,path,new_link_38065));


var G__38096 = cljs.core.next(seq__37750_38084__$1);
var G__38097 = null;
var G__38098 = (0);
var G__38099 = (0);
seq__37750_38069 = G__38096;
chunk__37752_38070 = G__38097;
count__37753_38071 = G__38098;
i__37754_38072 = G__38099;
continue;
}
} else {
}
}
break;
}

return goog.dom.removeNode(node_38063);
});})(seq__37659_38004,chunk__37663_38005,count__37664_38006,i__37665_38007,seq__37366,chunk__37371,count__37372,i__37373,new_link_38065,path_match_38064,node_38063,seq__37659_38056__$1,temp__5753__auto___38055__$1,path,seq__37366__$1,temp__5753__auto__,map__37365,map__37365__$1,msg,updates,reload_info))
);

shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("load CSS",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([path_match_38064], 0));

goog.dom.insertSiblingAfter(new_link_38065,node_38063);


var G__38100 = cljs.core.next(seq__37659_38056__$1);
var G__38101 = null;
var G__38102 = (0);
var G__38103 = (0);
seq__37659_38004 = G__38100;
chunk__37663_38005 = G__38101;
count__37664_38006 = G__38102;
i__37665_38007 = G__38103;
continue;
} else {
var G__38104 = cljs.core.next(seq__37659_38056__$1);
var G__38105 = null;
var G__38106 = (0);
var G__38107 = (0);
seq__37659_38004 = G__38104;
chunk__37663_38005 = G__38105;
count__37664_38006 = G__38106;
i__37665_38007 = G__38107;
continue;
}
} else {
var G__38108 = cljs.core.next(seq__37659_38056__$1);
var G__38109 = null;
var G__38110 = (0);
var G__38111 = (0);
seq__37659_38004 = G__38108;
chunk__37663_38005 = G__38109;
count__37664_38006 = G__38110;
i__37665_38007 = G__38111;
continue;
}
}
} else {
}
}
break;
}


var G__38112 = cljs.core.next(seq__37366__$1);
var G__38113 = null;
var G__38114 = (0);
var G__38115 = (0);
seq__37366 = G__38112;
chunk__37371 = G__38113;
count__37372 = G__38114;
i__37373 = G__38115;
continue;
} else {
var G__38116 = cljs.core.next(seq__37366__$1);
var G__38117 = null;
var G__38118 = (0);
var G__38119 = (0);
seq__37366 = G__38116;
chunk__37371 = G__38117;
count__37372 = G__38118;
i__37373 = G__38119;
continue;
}
}
} else {
return null;
}
}
break;
}
});
shadow.cljs.devtools.client.browser.global_eval = (function shadow$cljs$devtools$client$browser$global_eval(js){
if(cljs.core.not_EQ_.cljs$core$IFn$_invoke$arity$2("undefined",typeof(module))){
return eval(js);
} else {
return (0,eval)(js);;
}
});
shadow.cljs.devtools.client.browser.repl_init = (function shadow$cljs$devtools$client$browser$repl_init(runtime,p__37765){
var map__37766 = p__37765;
var map__37766__$1 = cljs.core.__destructure_map(map__37766);
var repl_state = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37766__$1,new cljs.core.Keyword(null,"repl-state","repl-state",-1733780387));
return shadow.cljs.devtools.client.shared.load_sources(runtime,cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentVector.EMPTY,cljs.core.remove.cljs$core$IFn$_invoke$arity$2(shadow.cljs.devtools.client.env.src_is_loaded_QMARK_,new cljs.core.Keyword(null,"repl-sources","repl-sources",723867535).cljs$core$IFn$_invoke$arity$1(repl_state))),(function (sources){
shadow.cljs.devtools.client.browser.do_js_load(sources);

return shadow.cljs.devtools.client.browser.devtools_msg("ready!");
}));
});
shadow.cljs.devtools.client.browser.runtime_info = (((typeof SHADOW_CONFIG !== 'undefined'))?shadow.json.to_clj.cljs$core$IFn$_invoke$arity$1(SHADOW_CONFIG):null);
shadow.cljs.devtools.client.browser.client_info = cljs.core.merge.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([shadow.cljs.devtools.client.browser.runtime_info,new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"host","host",-1558485167),(cljs.core.truth_(goog.global.document)?new cljs.core.Keyword(null,"browser","browser",828191719):new cljs.core.Keyword(null,"browser-worker","browser-worker",1638998282)),new cljs.core.Keyword(null,"user-agent","user-agent",1220426212),[(cljs.core.truth_(goog.userAgent.OPERA)?"Opera":(cljs.core.truth_(goog.userAgent.product.CHROME)?"Chrome":(cljs.core.truth_(goog.userAgent.IE)?"MSIE":(cljs.core.truth_(goog.userAgent.EDGE)?"Edge":(cljs.core.truth_(goog.userAgent.GECKO)?"Firefox":(cljs.core.truth_(goog.userAgent.SAFARI)?"Safari":(cljs.core.truth_(goog.userAgent.WEBKIT)?"Webkit":null)))))))," ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(goog.userAgent.VERSION)," [",cljs.core.str.cljs$core$IFn$_invoke$arity$1(goog.userAgent.PLATFORM),"]"].join(''),new cljs.core.Keyword(null,"dom","dom",-1236537922),(!((goog.global.document == null)))], null)], 0));
if((typeof shadow !== 'undefined') && (typeof shadow.cljs !== 'undefined') && (typeof shadow.cljs.devtools !== 'undefined') && (typeof shadow.cljs.devtools.client !== 'undefined') && (typeof shadow.cljs.devtools.client.browser !== 'undefined') && (typeof shadow.cljs.devtools.client.browser.ws_was_welcome_ref !== 'undefined')){
} else {
shadow.cljs.devtools.client.browser.ws_was_welcome_ref = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(false);
}
if(((shadow.cljs.devtools.client.env.enabled) && ((shadow.cljs.devtools.client.env.worker_client_id > (0))))){
(shadow.cljs.devtools.client.shared.Runtime.prototype.shadow$remote$runtime$api$IEvalJS$ = cljs.core.PROTOCOL_SENTINEL);

(shadow.cljs.devtools.client.shared.Runtime.prototype.shadow$remote$runtime$api$IEvalJS$_js_eval$arity$2 = (function (this$,code){
var this$__$1 = this;
return shadow.cljs.devtools.client.browser.global_eval(code);
}));

(shadow.cljs.devtools.client.shared.Runtime.prototype.shadow$cljs$devtools$client$shared$IHostSpecific$ = cljs.core.PROTOCOL_SENTINEL);

(shadow.cljs.devtools.client.shared.Runtime.prototype.shadow$cljs$devtools$client$shared$IHostSpecific$do_invoke$arity$2 = (function (this$,p__37771){
var map__37772 = p__37771;
var map__37772__$1 = cljs.core.__destructure_map(map__37772);
var _ = map__37772__$1;
var js = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37772__$1,new cljs.core.Keyword(null,"js","js",1768080579));
var this$__$1 = this;
return shadow.cljs.devtools.client.browser.global_eval(js);
}));

(shadow.cljs.devtools.client.shared.Runtime.prototype.shadow$cljs$devtools$client$shared$IHostSpecific$do_repl_init$arity$4 = (function (runtime,p__37773,done,error){
var map__37774 = p__37773;
var map__37774__$1 = cljs.core.__destructure_map(map__37774);
var repl_sources = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37774__$1,new cljs.core.Keyword(null,"repl-sources","repl-sources",723867535));
var runtime__$1 = this;
return shadow.cljs.devtools.client.shared.load_sources(runtime__$1,cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentVector.EMPTY,cljs.core.remove.cljs$core$IFn$_invoke$arity$2(shadow.cljs.devtools.client.env.src_is_loaded_QMARK_,repl_sources)),(function (sources){
shadow.cljs.devtools.client.browser.do_js_load(sources);

return (done.cljs$core$IFn$_invoke$arity$0 ? done.cljs$core$IFn$_invoke$arity$0() : done.call(null));
}));
}));

(shadow.cljs.devtools.client.shared.Runtime.prototype.shadow$cljs$devtools$client$shared$IHostSpecific$do_repl_require$arity$4 = (function (runtime,p__37776,done,error){
var map__37777 = p__37776;
var map__37777__$1 = cljs.core.__destructure_map(map__37777);
var msg = map__37777__$1;
var sources = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37777__$1,new cljs.core.Keyword(null,"sources","sources",-321166424));
var reload_namespaces = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37777__$1,new cljs.core.Keyword(null,"reload-namespaces","reload-namespaces",250210134));
var js_requires = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37777__$1,new cljs.core.Keyword(null,"js-requires","js-requires",-1311472051));
var runtime__$1 = this;
var sources_to_load = cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentVector.EMPTY,cljs.core.remove.cljs$core$IFn$_invoke$arity$2((function (p__37779){
var map__37780 = p__37779;
var map__37780__$1 = cljs.core.__destructure_map(map__37780);
var src = map__37780__$1;
var provides = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37780__$1,new cljs.core.Keyword(null,"provides","provides",-1634397992));
var and__4251__auto__ = shadow.cljs.devtools.client.env.src_is_loaded_QMARK_(src);
if(cljs.core.truth_(and__4251__auto__)){
return cljs.core.not(cljs.core.some(reload_namespaces,provides));
} else {
return and__4251__auto__;
}
}),sources));
if(cljs.core.not(cljs.core.seq(sources_to_load))){
var G__37792 = cljs.core.PersistentVector.EMPTY;
return (done.cljs$core$IFn$_invoke$arity$1 ? done.cljs$core$IFn$_invoke$arity$1(G__37792) : done.call(null,G__37792));
} else {
return shadow.remote.runtime.shared.call.cljs$core$IFn$_invoke$arity$3(runtime__$1,new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"op","op",-1882987955),new cljs.core.Keyword(null,"cljs-load-sources","cljs-load-sources",-1458295962),new cljs.core.Keyword(null,"to","to",192099007),shadow.cljs.devtools.client.env.worker_client_id,new cljs.core.Keyword(null,"sources","sources",-321166424),cljs.core.into.cljs$core$IFn$_invoke$arity$3(cljs.core.PersistentVector.EMPTY,cljs.core.map.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"resource-id","resource-id",-1308422582)),sources_to_load)], null),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"cljs-sources","cljs-sources",31121610),(function (p__37793){
var map__37794 = p__37793;
var map__37794__$1 = cljs.core.__destructure_map(map__37794);
var msg__$1 = map__37794__$1;
var sources__$1 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37794__$1,new cljs.core.Keyword(null,"sources","sources",-321166424));
try{shadow.cljs.devtools.client.browser.do_js_load(sources__$1);

if(cljs.core.seq(js_requires)){
shadow.cljs.devtools.client.browser.do_js_requires(js_requires);
} else {
}

return (done.cljs$core$IFn$_invoke$arity$1 ? done.cljs$core$IFn$_invoke$arity$1(sources_to_load) : done.call(null,sources_to_load));
}catch (e37795){var ex = e37795;
return (error.cljs$core$IFn$_invoke$arity$1 ? error.cljs$core$IFn$_invoke$arity$1(ex) : error.call(null,ex));
}})], null));
}
}));

shadow.cljs.devtools.client.shared.add_plugin_BANG_(new cljs.core.Keyword("shadow.cljs.devtools.client.browser","client","shadow.cljs.devtools.client.browser/client",-1461019282),cljs.core.PersistentHashSet.EMPTY,(function (p__37797){
var map__37798 = p__37797;
var map__37798__$1 = cljs.core.__destructure_map(map__37798);
var env = map__37798__$1;
var runtime = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37798__$1,new cljs.core.Keyword(null,"runtime","runtime",-1331573996));
var svc = new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"runtime","runtime",-1331573996),runtime], null);
shadow.remote.runtime.api.add_extension(runtime,new cljs.core.Keyword("shadow.cljs.devtools.client.browser","client","shadow.cljs.devtools.client.browser/client",-1461019282),new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"on-welcome","on-welcome",1895317125),(function (){
cljs.core.reset_BANG_(shadow.cljs.devtools.client.browser.ws_was_welcome_ref,true);

shadow.cljs.devtools.client.hud.connection_error_clear_BANG_();

shadow.cljs.devtools.client.env.patch_goog_BANG_();

return shadow.cljs.devtools.client.browser.devtools_msg(["#",cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"client-id","client-id",-464622140).cljs$core$IFn$_invoke$arity$1(cljs.core.deref(new cljs.core.Keyword(null,"state-ref","state-ref",2127874952).cljs$core$IFn$_invoke$arity$1(runtime))))," ready!"].join(''));
}),new cljs.core.Keyword(null,"on-disconnect","on-disconnect",-809021814),(function (e){
if(cljs.core.truth_(cljs.core.deref(shadow.cljs.devtools.client.browser.ws_was_welcome_ref))){
shadow.cljs.devtools.client.hud.connection_error("The Websocket connection was closed!");

return cljs.core.reset_BANG_(shadow.cljs.devtools.client.browser.ws_was_welcome_ref,false);
} else {
return null;
}
}),new cljs.core.Keyword(null,"on-reconnect","on-reconnect",1239988702),(function (e){
return shadow.cljs.devtools.client.hud.connection_error("Reconnecting ...");
}),new cljs.core.Keyword(null,"ops","ops",1237330063),new cljs.core.PersistentArrayMap(null, 8, [new cljs.core.Keyword(null,"access-denied","access-denied",959449406),(function (msg){
cljs.core.reset_BANG_(shadow.cljs.devtools.client.browser.ws_was_welcome_ref,false);

return shadow.cljs.devtools.client.hud.connection_error(["Stale Output! Your loaded JS was not produced by the running shadow-cljs instance."," Is the watch for this build running?"].join(''));
}),new cljs.core.Keyword(null,"cljs-runtime-init","cljs-runtime-init",1305890232),(function (msg){
return shadow.cljs.devtools.client.browser.repl_init(runtime,msg);
}),new cljs.core.Keyword(null,"cljs-asset-update","cljs-asset-update",1224093028),(function (msg){
return shadow.cljs.devtools.client.browser.handle_asset_update(msg);
}),new cljs.core.Keyword(null,"cljs-build-configure","cljs-build-configure",-2089891268),(function (msg){
return null;
}),new cljs.core.Keyword(null,"cljs-build-start","cljs-build-start",-725781241),(function (msg){
shadow.cljs.devtools.client.hud.hud_hide();

shadow.cljs.devtools.client.hud.load_start();

return shadow.cljs.devtools.client.env.run_custom_notify_BANG_(cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(msg,new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Keyword(null,"build-start","build-start",-959649480)));
}),new cljs.core.Keyword(null,"cljs-build-complete","cljs-build-complete",273626153),(function (msg){
var msg__$1 = shadow.cljs.devtools.client.env.add_warnings_to_info(msg);
shadow.cljs.devtools.client.hud.connection_error_clear_BANG_();

shadow.cljs.devtools.client.hud.hud_warnings(msg__$1);

shadow.cljs.devtools.client.browser.handle_build_complete(runtime,msg__$1);

return shadow.cljs.devtools.client.env.run_custom_notify_BANG_(cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(msg__$1,new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Keyword(null,"build-complete","build-complete",-501868472)));
}),new cljs.core.Keyword(null,"cljs-build-failure","cljs-build-failure",1718154990),(function (msg){
shadow.cljs.devtools.client.hud.load_end();

shadow.cljs.devtools.client.hud.hud_error(msg);

return shadow.cljs.devtools.client.env.run_custom_notify_BANG_(cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(msg,new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Keyword(null,"build-failure","build-failure",-2107487466)));
}),new cljs.core.Keyword("shadow.cljs.devtools.client.env","worker-notify","shadow.cljs.devtools.client.env/worker-notify",-1456820670),(function (p__37803){
var map__37804 = p__37803;
var map__37804__$1 = cljs.core.__destructure_map(map__37804);
var event_op = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37804__$1,new cljs.core.Keyword(null,"event-op","event-op",200358057));
var client_id = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37804__$1,new cljs.core.Keyword(null,"client-id","client-id",-464622140));
if(((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"client-disconnect","client-disconnect",640227957),event_op)) && (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(client_id,shadow.cljs.devtools.client.env.worker_client_id)))){
shadow.cljs.devtools.client.hud.connection_error_clear_BANG_();

return shadow.cljs.devtools.client.hud.connection_error("The watch for this build was stopped!");
} else {
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"client-connect","client-connect",-1113973888),event_op)){
shadow.cljs.devtools.client.hud.connection_error_clear_BANG_();

return shadow.cljs.devtools.client.hud.connection_error("The watch for this build was restarted. Reload required!");
} else {
return null;
}
}
})], null)], null));

return svc;
}),(function (p__37805){
var map__37806 = p__37805;
var map__37806__$1 = cljs.core.__destructure_map(map__37806);
var svc = map__37806__$1;
var runtime = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37806__$1,new cljs.core.Keyword(null,"runtime","runtime",-1331573996));
return shadow.remote.runtime.api.del_extension(runtime,new cljs.core.Keyword("shadow.cljs.devtools.client.browser","client","shadow.cljs.devtools.client.browser/client",-1461019282));
}));

shadow.cljs.devtools.client.shared.init_runtime_BANG_(shadow.cljs.devtools.client.browser.client_info,shadow.cljs.devtools.client.websocket.start,shadow.cljs.devtools.client.websocket.send,shadow.cljs.devtools.client.websocket.stop);
} else {
}

//# sourceMappingURL=shadow.cljs.devtools.client.browser.js.map
