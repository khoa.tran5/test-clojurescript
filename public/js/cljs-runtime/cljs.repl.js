goog.provide('cljs.repl');
cljs.repl.print_doc = (function cljs$repl$print_doc(p__35405){
var map__35406 = p__35405;
var map__35406__$1 = cljs.core.__destructure_map(map__35406);
var m = map__35406__$1;
var n = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__35406__$1,new cljs.core.Keyword(null,"ns","ns",441598760));
var nm = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__35406__$1,new cljs.core.Keyword(null,"name","name",1843675177));
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["-------------------------"], 0));

cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([(function (){var or__4253__auto__ = new cljs.core.Keyword(null,"spec","spec",347520401).cljs$core$IFn$_invoke$arity$1(m);
if(cljs.core.truth_(or__4253__auto__)){
return or__4253__auto__;
} else {
return [(function (){var temp__5753__auto__ = new cljs.core.Keyword(null,"ns","ns",441598760).cljs$core$IFn$_invoke$arity$1(m);
if(cljs.core.truth_(temp__5753__auto__)){
var ns = temp__5753__auto__;
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(ns),"/"].join('');
} else {
return null;
}
})(),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"name","name",1843675177).cljs$core$IFn$_invoke$arity$1(m))].join('');
}
})()], 0));

if(cljs.core.truth_(new cljs.core.Keyword(null,"protocol","protocol",652470118).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["Protocol"], 0));
} else {
}

if(cljs.core.truth_(new cljs.core.Keyword(null,"forms","forms",2045992350).cljs$core$IFn$_invoke$arity$1(m))){
var seq__35408_35665 = cljs.core.seq(new cljs.core.Keyword(null,"forms","forms",2045992350).cljs$core$IFn$_invoke$arity$1(m));
var chunk__35409_35666 = null;
var count__35410_35667 = (0);
var i__35411_35668 = (0);
while(true){
if((i__35411_35668 < count__35410_35667)){
var f_35669 = chunk__35409_35666.cljs$core$IIndexed$_nth$arity$2(null,i__35411_35668);
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["  ",f_35669], 0));


var G__35670 = seq__35408_35665;
var G__35671 = chunk__35409_35666;
var G__35672 = count__35410_35667;
var G__35673 = (i__35411_35668 + (1));
seq__35408_35665 = G__35670;
chunk__35409_35666 = G__35671;
count__35410_35667 = G__35672;
i__35411_35668 = G__35673;
continue;
} else {
var temp__5753__auto___35674 = cljs.core.seq(seq__35408_35665);
if(temp__5753__auto___35674){
var seq__35408_35675__$1 = temp__5753__auto___35674;
if(cljs.core.chunked_seq_QMARK_(seq__35408_35675__$1)){
var c__4679__auto___35676 = cljs.core.chunk_first(seq__35408_35675__$1);
var G__35678 = cljs.core.chunk_rest(seq__35408_35675__$1);
var G__35679 = c__4679__auto___35676;
var G__35680 = cljs.core.count(c__4679__auto___35676);
var G__35681 = (0);
seq__35408_35665 = G__35678;
chunk__35409_35666 = G__35679;
count__35410_35667 = G__35680;
i__35411_35668 = G__35681;
continue;
} else {
var f_35687 = cljs.core.first(seq__35408_35675__$1);
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["  ",f_35687], 0));


var G__35688 = cljs.core.next(seq__35408_35675__$1);
var G__35689 = null;
var G__35690 = (0);
var G__35691 = (0);
seq__35408_35665 = G__35688;
chunk__35409_35666 = G__35689;
count__35410_35667 = G__35690;
i__35411_35668 = G__35691;
continue;
}
} else {
}
}
break;
}
} else {
if(cljs.core.truth_(new cljs.core.Keyword(null,"arglists","arglists",1661989754).cljs$core$IFn$_invoke$arity$1(m))){
var arglists_35693 = new cljs.core.Keyword(null,"arglists","arglists",1661989754).cljs$core$IFn$_invoke$arity$1(m);
if(cljs.core.truth_((function (){var or__4253__auto__ = new cljs.core.Keyword(null,"macro","macro",-867863404).cljs$core$IFn$_invoke$arity$1(m);
if(cljs.core.truth_(or__4253__auto__)){
return or__4253__auto__;
} else {
return new cljs.core.Keyword(null,"repl-special-function","repl-special-function",1262603725).cljs$core$IFn$_invoke$arity$1(m);
}
})())){
cljs.core.prn.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([arglists_35693], 0));
} else {
cljs.core.prn.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Symbol(null,"quote","quote",1377916282,null),cljs.core.first(arglists_35693)))?cljs.core.second(arglists_35693):arglists_35693)], 0));
}
} else {
}
}

if(cljs.core.truth_(new cljs.core.Keyword(null,"special-form","special-form",-1326536374).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["Special Form"], 0));

cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([" ",new cljs.core.Keyword(null,"doc","doc",1913296891).cljs$core$IFn$_invoke$arity$1(m)], 0));

if(cljs.core.contains_QMARK_(m,new cljs.core.Keyword(null,"url","url",276297046))){
if(cljs.core.truth_(new cljs.core.Keyword(null,"url","url",276297046).cljs$core$IFn$_invoke$arity$1(m))){
return cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([["\n  Please see http://clojure.org/",cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"url","url",276297046).cljs$core$IFn$_invoke$arity$1(m))].join('')], 0));
} else {
return null;
}
} else {
return cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([["\n  Please see http://clojure.org/special_forms#",cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"name","name",1843675177).cljs$core$IFn$_invoke$arity$1(m))].join('')], 0));
}
} else {
if(cljs.core.truth_(new cljs.core.Keyword(null,"macro","macro",-867863404).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["Macro"], 0));
} else {
}

if(cljs.core.truth_(new cljs.core.Keyword(null,"spec","spec",347520401).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["Spec"], 0));
} else {
}

if(cljs.core.truth_(new cljs.core.Keyword(null,"repl-special-function","repl-special-function",1262603725).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["REPL Special Function"], 0));
} else {
}

cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([" ",new cljs.core.Keyword(null,"doc","doc",1913296891).cljs$core$IFn$_invoke$arity$1(m)], 0));

if(cljs.core.truth_(new cljs.core.Keyword(null,"protocol","protocol",652470118).cljs$core$IFn$_invoke$arity$1(m))){
var seq__35413_35695 = cljs.core.seq(new cljs.core.Keyword(null,"methods","methods",453930866).cljs$core$IFn$_invoke$arity$1(m));
var chunk__35414_35696 = null;
var count__35415_35697 = (0);
var i__35416_35698 = (0);
while(true){
if((i__35416_35698 < count__35415_35697)){
var vec__35427_35699 = chunk__35414_35696.cljs$core$IIndexed$_nth$arity$2(null,i__35416_35698);
var name_35700 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35427_35699,(0),null);
var map__35430_35701 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35427_35699,(1),null);
var map__35430_35702__$1 = cljs.core.__destructure_map(map__35430_35701);
var doc_35703 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__35430_35702__$1,new cljs.core.Keyword(null,"doc","doc",1913296891));
var arglists_35704 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__35430_35702__$1,new cljs.core.Keyword(null,"arglists","arglists",1661989754));
cljs.core.println();

cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([" ",name_35700], 0));

cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([" ",arglists_35704], 0));

if(cljs.core.truth_(doc_35703)){
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([" ",doc_35703], 0));
} else {
}


var G__35705 = seq__35413_35695;
var G__35706 = chunk__35414_35696;
var G__35707 = count__35415_35697;
var G__35708 = (i__35416_35698 + (1));
seq__35413_35695 = G__35705;
chunk__35414_35696 = G__35706;
count__35415_35697 = G__35707;
i__35416_35698 = G__35708;
continue;
} else {
var temp__5753__auto___35709 = cljs.core.seq(seq__35413_35695);
if(temp__5753__auto___35709){
var seq__35413_35710__$1 = temp__5753__auto___35709;
if(cljs.core.chunked_seq_QMARK_(seq__35413_35710__$1)){
var c__4679__auto___35711 = cljs.core.chunk_first(seq__35413_35710__$1);
var G__35712 = cljs.core.chunk_rest(seq__35413_35710__$1);
var G__35713 = c__4679__auto___35711;
var G__35714 = cljs.core.count(c__4679__auto___35711);
var G__35715 = (0);
seq__35413_35695 = G__35712;
chunk__35414_35696 = G__35713;
count__35415_35697 = G__35714;
i__35416_35698 = G__35715;
continue;
} else {
var vec__35433_35716 = cljs.core.first(seq__35413_35710__$1);
var name_35717 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35433_35716,(0),null);
var map__35436_35718 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35433_35716,(1),null);
var map__35436_35719__$1 = cljs.core.__destructure_map(map__35436_35718);
var doc_35720 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__35436_35719__$1,new cljs.core.Keyword(null,"doc","doc",1913296891));
var arglists_35721 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__35436_35719__$1,new cljs.core.Keyword(null,"arglists","arglists",1661989754));
cljs.core.println();

cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([" ",name_35717], 0));

cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([" ",arglists_35721], 0));

if(cljs.core.truth_(doc_35720)){
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([" ",doc_35720], 0));
} else {
}


var G__35723 = cljs.core.next(seq__35413_35710__$1);
var G__35724 = null;
var G__35725 = (0);
var G__35726 = (0);
seq__35413_35695 = G__35723;
chunk__35414_35696 = G__35724;
count__35415_35697 = G__35725;
i__35416_35698 = G__35726;
continue;
}
} else {
}
}
break;
}
} else {
}

if(cljs.core.truth_(n)){
var temp__5753__auto__ = cljs.spec.alpha.get_spec(cljs.core.symbol.cljs$core$IFn$_invoke$arity$2(cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.ns_name(n)),cljs.core.name(nm)));
if(cljs.core.truth_(temp__5753__auto__)){
var fnspec = temp__5753__auto__;
cljs.core.print.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["Spec"], 0));

var seq__35438 = cljs.core.seq(new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"args","args",1315556576),new cljs.core.Keyword(null,"ret","ret",-468222814),new cljs.core.Keyword(null,"fn","fn",-1175266204)], null));
var chunk__35439 = null;
var count__35440 = (0);
var i__35441 = (0);
while(true){
if((i__35441 < count__35440)){
var role = chunk__35439.cljs$core$IIndexed$_nth$arity$2(null,i__35441);
var temp__5753__auto___35727__$1 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(fnspec,role);
if(cljs.core.truth_(temp__5753__auto___35727__$1)){
var spec_35728 = temp__5753__auto___35727__$1;
cljs.core.print.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([["\n ",cljs.core.name(role),":"].join(''),cljs.spec.alpha.describe(spec_35728)], 0));
} else {
}


var G__35729 = seq__35438;
var G__35730 = chunk__35439;
var G__35731 = count__35440;
var G__35732 = (i__35441 + (1));
seq__35438 = G__35729;
chunk__35439 = G__35730;
count__35440 = G__35731;
i__35441 = G__35732;
continue;
} else {
var temp__5753__auto____$1 = cljs.core.seq(seq__35438);
if(temp__5753__auto____$1){
var seq__35438__$1 = temp__5753__auto____$1;
if(cljs.core.chunked_seq_QMARK_(seq__35438__$1)){
var c__4679__auto__ = cljs.core.chunk_first(seq__35438__$1);
var G__35733 = cljs.core.chunk_rest(seq__35438__$1);
var G__35734 = c__4679__auto__;
var G__35735 = cljs.core.count(c__4679__auto__);
var G__35736 = (0);
seq__35438 = G__35733;
chunk__35439 = G__35734;
count__35440 = G__35735;
i__35441 = G__35736;
continue;
} else {
var role = cljs.core.first(seq__35438__$1);
var temp__5753__auto___35737__$2 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(fnspec,role);
if(cljs.core.truth_(temp__5753__auto___35737__$2)){
var spec_35738 = temp__5753__auto___35737__$2;
cljs.core.print.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([["\n ",cljs.core.name(role),":"].join(''),cljs.spec.alpha.describe(spec_35738)], 0));
} else {
}


var G__35739 = cljs.core.next(seq__35438__$1);
var G__35740 = null;
var G__35741 = (0);
var G__35742 = (0);
seq__35438 = G__35739;
chunk__35439 = G__35740;
count__35440 = G__35741;
i__35441 = G__35742;
continue;
}
} else {
return null;
}
}
break;
}
} else {
return null;
}
} else {
return null;
}
}
});
/**
 * Constructs a data representation for a Error with keys:
 *  :cause - root cause message
 *  :phase - error phase
 *  :via - cause chain, with cause keys:
 *           :type - exception class symbol
 *           :message - exception message
 *           :data - ex-data
 *           :at - top stack element
 *  :trace - root cause stack elements
 */
cljs.repl.Error__GT_map = (function cljs$repl$Error__GT_map(o){
var base = (function (t){
return cljs.core.merge.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"type","type",1174270348),(((t instanceof cljs.core.ExceptionInfo))?new cljs.core.Symbol("cljs.core","ExceptionInfo","cljs.core/ExceptionInfo",701839050,null):(((t instanceof Error))?cljs.core.symbol.cljs$core$IFn$_invoke$arity$2("js",t.name):null
))], null),(function (){var temp__5753__auto__ = cljs.core.ex_message(t);
if(cljs.core.truth_(temp__5753__auto__)){
var msg = temp__5753__auto__;
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"message","message",-406056002),msg], null);
} else {
return null;
}
})(),(function (){var temp__5753__auto__ = cljs.core.ex_data(t);
if(cljs.core.truth_(temp__5753__auto__)){
var ed = temp__5753__auto__;
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"data","data",-232669377),ed], null);
} else {
return null;
}
})()], 0));
});
var via = (function (){var via = cljs.core.PersistentVector.EMPTY;
var t = o;
while(true){
if(cljs.core.truth_(t)){
var G__35743 = cljs.core.conj.cljs$core$IFn$_invoke$arity$2(via,t);
var G__35744 = cljs.core.ex_cause(t);
via = G__35743;
t = G__35744;
continue;
} else {
return via;
}
break;
}
})();
var root = cljs.core.peek(via);
return cljs.core.merge.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"via","via",-1904457336),cljs.core.vec(cljs.core.map.cljs$core$IFn$_invoke$arity$2(base,via)),new cljs.core.Keyword(null,"trace","trace",-1082747415),null], null),(function (){var temp__5753__auto__ = cljs.core.ex_message(root);
if(cljs.core.truth_(temp__5753__auto__)){
var root_msg = temp__5753__auto__;
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"cause","cause",231901252),root_msg], null);
} else {
return null;
}
})(),(function (){var temp__5753__auto__ = cljs.core.ex_data(root);
if(cljs.core.truth_(temp__5753__auto__)){
var data = temp__5753__auto__;
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"data","data",-232669377),data], null);
} else {
return null;
}
})(),(function (){var temp__5753__auto__ = new cljs.core.Keyword("clojure.error","phase","clojure.error/phase",275140358).cljs$core$IFn$_invoke$arity$1(cljs.core.ex_data(o));
if(cljs.core.truth_(temp__5753__auto__)){
var phase = temp__5753__auto__;
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"phase","phase",575722892),phase], null);
} else {
return null;
}
})()], 0));
});
/**
 * Returns an analysis of the phase, error, cause, and location of an error that occurred
 *   based on Throwable data, as returned by Throwable->map. All attributes other than phase
 *   are optional:
 *  :clojure.error/phase - keyword phase indicator, one of:
 *    :read-source :compile-syntax-check :compilation :macro-syntax-check :macroexpansion
 *    :execution :read-eval-result :print-eval-result
 *  :clojure.error/source - file name (no path)
 *  :clojure.error/line - integer line number
 *  :clojure.error/column - integer column number
 *  :clojure.error/symbol - symbol being expanded/compiled/invoked
 *  :clojure.error/class - cause exception class symbol
 *  :clojure.error/cause - cause exception message
 *  :clojure.error/spec - explain-data for spec error
 */
cljs.repl.ex_triage = (function cljs$repl$ex_triage(datafied_throwable){
var map__35457 = datafied_throwable;
var map__35457__$1 = cljs.core.__destructure_map(map__35457);
var via = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__35457__$1,new cljs.core.Keyword(null,"via","via",-1904457336));
var trace = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__35457__$1,new cljs.core.Keyword(null,"trace","trace",-1082747415));
var phase = cljs.core.get.cljs$core$IFn$_invoke$arity$3(map__35457__$1,new cljs.core.Keyword(null,"phase","phase",575722892),new cljs.core.Keyword(null,"execution","execution",253283524));
var map__35458 = cljs.core.last(via);
var map__35458__$1 = cljs.core.__destructure_map(map__35458);
var type = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__35458__$1,new cljs.core.Keyword(null,"type","type",1174270348));
var message = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__35458__$1,new cljs.core.Keyword(null,"message","message",-406056002));
var data = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__35458__$1,new cljs.core.Keyword(null,"data","data",-232669377));
var map__35459 = data;
var map__35459__$1 = cljs.core.__destructure_map(map__35459);
var problems = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__35459__$1,new cljs.core.Keyword("cljs.spec.alpha","problems","cljs.spec.alpha/problems",447400814));
var fn = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__35459__$1,new cljs.core.Keyword("cljs.spec.alpha","fn","cljs.spec.alpha/fn",408600443));
var caller = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__35459__$1,new cljs.core.Keyword("cljs.spec.test.alpha","caller","cljs.spec.test.alpha/caller",-398302390));
var map__35460 = new cljs.core.Keyword(null,"data","data",-232669377).cljs$core$IFn$_invoke$arity$1(cljs.core.first(via));
var map__35460__$1 = cljs.core.__destructure_map(map__35460);
var top_data = map__35460__$1;
var source = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__35460__$1,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397));
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3((function (){var G__35462 = phase;
var G__35462__$1 = (((G__35462 instanceof cljs.core.Keyword))?G__35462.fqn:null);
switch (G__35462__$1) {
case "read-source":
var map__35466 = data;
var map__35466__$1 = cljs.core.__destructure_map(map__35466);
var line = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__35466__$1,new cljs.core.Keyword("clojure.error","line","clojure.error/line",-1816287471));
var column = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__35466__$1,new cljs.core.Keyword("clojure.error","column","clojure.error/column",304721553));
var G__35471 = cljs.core.merge.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"data","data",-232669377).cljs$core$IFn$_invoke$arity$1(cljs.core.second(via)),top_data], 0));
var G__35471__$1 = (cljs.core.truth_(source)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__35471,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397),source):G__35471);
var G__35471__$2 = (cljs.core.truth_((function (){var fexpr__35476 = new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, ["NO_SOURCE_PATH",null,"NO_SOURCE_FILE",null], null), null);
return (fexpr__35476.cljs$core$IFn$_invoke$arity$1 ? fexpr__35476.cljs$core$IFn$_invoke$arity$1(source) : fexpr__35476.call(null,source));
})())?cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(G__35471__$1,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397)):G__35471__$1);
if(cljs.core.truth_(message)){
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__35471__$2,new cljs.core.Keyword("clojure.error","cause","clojure.error/cause",-1879175742),message);
} else {
return G__35471__$2;
}

break;
case "compile-syntax-check":
case "compilation":
case "macro-syntax-check":
case "macroexpansion":
var G__35478 = top_data;
var G__35478__$1 = (cljs.core.truth_(source)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__35478,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397),source):G__35478);
var G__35478__$2 = (cljs.core.truth_((function (){var fexpr__35479 = new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, ["NO_SOURCE_PATH",null,"NO_SOURCE_FILE",null], null), null);
return (fexpr__35479.cljs$core$IFn$_invoke$arity$1 ? fexpr__35479.cljs$core$IFn$_invoke$arity$1(source) : fexpr__35479.call(null,source));
})())?cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(G__35478__$1,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397)):G__35478__$1);
var G__35478__$3 = (cljs.core.truth_(type)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__35478__$2,new cljs.core.Keyword("clojure.error","class","clojure.error/class",278435890),type):G__35478__$2);
var G__35478__$4 = (cljs.core.truth_(message)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__35478__$3,new cljs.core.Keyword("clojure.error","cause","clojure.error/cause",-1879175742),message):G__35478__$3);
if(cljs.core.truth_(problems)){
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__35478__$4,new cljs.core.Keyword("clojure.error","spec","clojure.error/spec",2055032595),data);
} else {
return G__35478__$4;
}

break;
case "read-eval-result":
case "print-eval-result":
var vec__35483 = cljs.core.first(trace);
var source__$1 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35483,(0),null);
var method = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35483,(1),null);
var file = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35483,(2),null);
var line = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35483,(3),null);
var G__35486 = top_data;
var G__35486__$1 = (cljs.core.truth_(line)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__35486,new cljs.core.Keyword("clojure.error","line","clojure.error/line",-1816287471),line):G__35486);
var G__35486__$2 = (cljs.core.truth_(file)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__35486__$1,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397),file):G__35486__$1);
var G__35486__$3 = (cljs.core.truth_((function (){var and__4251__auto__ = source__$1;
if(cljs.core.truth_(and__4251__auto__)){
return method;
} else {
return and__4251__auto__;
}
})())?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__35486__$2,new cljs.core.Keyword("clojure.error","symbol","clojure.error/symbol",1544821994),(new cljs.core.PersistentVector(null,2,(5),cljs.core.PersistentVector.EMPTY_NODE,[source__$1,method],null))):G__35486__$2);
var G__35486__$4 = (cljs.core.truth_(type)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__35486__$3,new cljs.core.Keyword("clojure.error","class","clojure.error/class",278435890),type):G__35486__$3);
if(cljs.core.truth_(message)){
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__35486__$4,new cljs.core.Keyword("clojure.error","cause","clojure.error/cause",-1879175742),message);
} else {
return G__35486__$4;
}

break;
case "execution":
var vec__35493 = cljs.core.first(trace);
var source__$1 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35493,(0),null);
var method = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35493,(1),null);
var file = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35493,(2),null);
var line = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35493,(3),null);
var file__$1 = cljs.core.first(cljs.core.remove.cljs$core$IFn$_invoke$arity$2((function (p1__35454_SHARP_){
var or__4253__auto__ = (p1__35454_SHARP_ == null);
if(or__4253__auto__){
return or__4253__auto__;
} else {
var fexpr__35500 = new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, ["NO_SOURCE_PATH",null,"NO_SOURCE_FILE",null], null), null);
return (fexpr__35500.cljs$core$IFn$_invoke$arity$1 ? fexpr__35500.cljs$core$IFn$_invoke$arity$1(p1__35454_SHARP_) : fexpr__35500.call(null,p1__35454_SHARP_));
}
}),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"file","file",-1269645878).cljs$core$IFn$_invoke$arity$1(caller),file], null)));
var err_line = (function (){var or__4253__auto__ = new cljs.core.Keyword(null,"line","line",212345235).cljs$core$IFn$_invoke$arity$1(caller);
if(cljs.core.truth_(or__4253__auto__)){
return or__4253__auto__;
} else {
return line;
}
})();
var G__35515 = new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword("clojure.error","class","clojure.error/class",278435890),type], null);
var G__35515__$1 = (cljs.core.truth_(err_line)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__35515,new cljs.core.Keyword("clojure.error","line","clojure.error/line",-1816287471),err_line):G__35515);
var G__35515__$2 = (cljs.core.truth_(message)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__35515__$1,new cljs.core.Keyword("clojure.error","cause","clojure.error/cause",-1879175742),message):G__35515__$1);
var G__35515__$3 = (cljs.core.truth_((function (){var or__4253__auto__ = fn;
if(cljs.core.truth_(or__4253__auto__)){
return or__4253__auto__;
} else {
var and__4251__auto__ = source__$1;
if(cljs.core.truth_(and__4251__auto__)){
return method;
} else {
return and__4251__auto__;
}
}
})())?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__35515__$2,new cljs.core.Keyword("clojure.error","symbol","clojure.error/symbol",1544821994),(function (){var or__4253__auto__ = fn;
if(cljs.core.truth_(or__4253__auto__)){
return or__4253__auto__;
} else {
return (new cljs.core.PersistentVector(null,2,(5),cljs.core.PersistentVector.EMPTY_NODE,[source__$1,method],null));
}
})()):G__35515__$2);
var G__35515__$4 = (cljs.core.truth_(file__$1)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__35515__$3,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397),file__$1):G__35515__$3);
if(cljs.core.truth_(problems)){
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__35515__$4,new cljs.core.Keyword("clojure.error","spec","clojure.error/spec",2055032595),data);
} else {
return G__35515__$4;
}

break;
default:
throw (new Error(["No matching clause: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(G__35462__$1)].join('')));

}
})(),new cljs.core.Keyword("clojure.error","phase","clojure.error/phase",275140358),phase);
});
/**
 * Returns a string from exception data, as produced by ex-triage.
 *   The first line summarizes the exception phase and location.
 *   The subsequent lines describe the cause.
 */
cljs.repl.ex_str = (function cljs$repl$ex_str(p__35558){
var map__35564 = p__35558;
var map__35564__$1 = cljs.core.__destructure_map(map__35564);
var triage_data = map__35564__$1;
var phase = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__35564__$1,new cljs.core.Keyword("clojure.error","phase","clojure.error/phase",275140358));
var source = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__35564__$1,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397));
var line = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__35564__$1,new cljs.core.Keyword("clojure.error","line","clojure.error/line",-1816287471));
var column = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__35564__$1,new cljs.core.Keyword("clojure.error","column","clojure.error/column",304721553));
var symbol = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__35564__$1,new cljs.core.Keyword("clojure.error","symbol","clojure.error/symbol",1544821994));
var class$ = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__35564__$1,new cljs.core.Keyword("clojure.error","class","clojure.error/class",278435890));
var cause = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__35564__$1,new cljs.core.Keyword("clojure.error","cause","clojure.error/cause",-1879175742));
var spec = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__35564__$1,new cljs.core.Keyword("clojure.error","spec","clojure.error/spec",2055032595));
var loc = [cljs.core.str.cljs$core$IFn$_invoke$arity$1((function (){var or__4253__auto__ = source;
if(cljs.core.truth_(or__4253__auto__)){
return or__4253__auto__;
} else {
return "<cljs repl>";
}
})()),":",cljs.core.str.cljs$core$IFn$_invoke$arity$1((function (){var or__4253__auto__ = line;
if(cljs.core.truth_(or__4253__auto__)){
return or__4253__auto__;
} else {
return (1);
}
})()),(cljs.core.truth_(column)?[":",cljs.core.str.cljs$core$IFn$_invoke$arity$1(column)].join(''):"")].join('');
var class_name = cljs.core.name((function (){var or__4253__auto__ = class$;
if(cljs.core.truth_(or__4253__auto__)){
return or__4253__auto__;
} else {
return "";
}
})());
var simple_class = class_name;
var cause_type = ((cljs.core.contains_QMARK_(new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, ["RuntimeException",null,"Exception",null], null), null),simple_class))?"":[" (",simple_class,")"].join(''));
var format = goog.string.format;
var G__35580 = phase;
var G__35580__$1 = (((G__35580 instanceof cljs.core.Keyword))?G__35580.fqn:null);
switch (G__35580__$1) {
case "read-source":
return (format.cljs$core$IFn$_invoke$arity$3 ? format.cljs$core$IFn$_invoke$arity$3("Syntax error reading source at (%s).\n%s\n",loc,cause) : format.call(null,"Syntax error reading source at (%s).\n%s\n",loc,cause));

break;
case "macro-syntax-check":
var G__35588 = "Syntax error macroexpanding %sat (%s).\n%s";
var G__35589 = (cljs.core.truth_(symbol)?[cljs.core.str.cljs$core$IFn$_invoke$arity$1(symbol)," "].join(''):"");
var G__35590 = loc;
var G__35591 = (cljs.core.truth_(spec)?(function (){var sb__4795__auto__ = (new goog.string.StringBuffer());
var _STAR_print_newline_STAR__orig_val__35592_35769 = cljs.core._STAR_print_newline_STAR_;
var _STAR_print_fn_STAR__orig_val__35593_35770 = cljs.core._STAR_print_fn_STAR_;
var _STAR_print_newline_STAR__temp_val__35594_35771 = true;
var _STAR_print_fn_STAR__temp_val__35595_35772 = (function (x__4796__auto__){
return sb__4795__auto__.append(x__4796__auto__);
});
(cljs.core._STAR_print_newline_STAR_ = _STAR_print_newline_STAR__temp_val__35594_35771);

(cljs.core._STAR_print_fn_STAR_ = _STAR_print_fn_STAR__temp_val__35595_35772);

try{cljs.spec.alpha.explain_out(cljs.core.update.cljs$core$IFn$_invoke$arity$3(spec,new cljs.core.Keyword("cljs.spec.alpha","problems","cljs.spec.alpha/problems",447400814),(function (probs){
return cljs.core.map.cljs$core$IFn$_invoke$arity$2((function (p1__35546_SHARP_){
return cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(p1__35546_SHARP_,new cljs.core.Keyword(null,"in","in",-1531184865));
}),probs);
}))
);
}finally {(cljs.core._STAR_print_fn_STAR_ = _STAR_print_fn_STAR__orig_val__35593_35770);

(cljs.core._STAR_print_newline_STAR_ = _STAR_print_newline_STAR__orig_val__35592_35769);
}
return cljs.core.str.cljs$core$IFn$_invoke$arity$1(sb__4795__auto__);
})():(format.cljs$core$IFn$_invoke$arity$2 ? format.cljs$core$IFn$_invoke$arity$2("%s\n",cause) : format.call(null,"%s\n",cause)));
return (format.cljs$core$IFn$_invoke$arity$4 ? format.cljs$core$IFn$_invoke$arity$4(G__35588,G__35589,G__35590,G__35591) : format.call(null,G__35588,G__35589,G__35590,G__35591));

break;
case "macroexpansion":
var G__35624 = "Unexpected error%s macroexpanding %sat (%s).\n%s\n";
var G__35625 = cause_type;
var G__35626 = (cljs.core.truth_(symbol)?[cljs.core.str.cljs$core$IFn$_invoke$arity$1(symbol)," "].join(''):"");
var G__35627 = loc;
var G__35628 = cause;
return (format.cljs$core$IFn$_invoke$arity$5 ? format.cljs$core$IFn$_invoke$arity$5(G__35624,G__35625,G__35626,G__35627,G__35628) : format.call(null,G__35624,G__35625,G__35626,G__35627,G__35628));

break;
case "compile-syntax-check":
var G__35629 = "Syntax error%s compiling %sat (%s).\n%s\n";
var G__35630 = cause_type;
var G__35631 = (cljs.core.truth_(symbol)?[cljs.core.str.cljs$core$IFn$_invoke$arity$1(symbol)," "].join(''):"");
var G__35632 = loc;
var G__35633 = cause;
return (format.cljs$core$IFn$_invoke$arity$5 ? format.cljs$core$IFn$_invoke$arity$5(G__35629,G__35630,G__35631,G__35632,G__35633) : format.call(null,G__35629,G__35630,G__35631,G__35632,G__35633));

break;
case "compilation":
var G__35634 = "Unexpected error%s compiling %sat (%s).\n%s\n";
var G__35635 = cause_type;
var G__35636 = (cljs.core.truth_(symbol)?[cljs.core.str.cljs$core$IFn$_invoke$arity$1(symbol)," "].join(''):"");
var G__35637 = loc;
var G__35638 = cause;
return (format.cljs$core$IFn$_invoke$arity$5 ? format.cljs$core$IFn$_invoke$arity$5(G__35634,G__35635,G__35636,G__35637,G__35638) : format.call(null,G__35634,G__35635,G__35636,G__35637,G__35638));

break;
case "read-eval-result":
return (format.cljs$core$IFn$_invoke$arity$5 ? format.cljs$core$IFn$_invoke$arity$5("Error reading eval result%s at %s (%s).\n%s\n",cause_type,symbol,loc,cause) : format.call(null,"Error reading eval result%s at %s (%s).\n%s\n",cause_type,symbol,loc,cause));

break;
case "print-eval-result":
return (format.cljs$core$IFn$_invoke$arity$5 ? format.cljs$core$IFn$_invoke$arity$5("Error printing return value%s at %s (%s).\n%s\n",cause_type,symbol,loc,cause) : format.call(null,"Error printing return value%s at %s (%s).\n%s\n",cause_type,symbol,loc,cause));

break;
case "execution":
if(cljs.core.truth_(spec)){
var G__35643 = "Execution error - invalid arguments to %s at (%s).\n%s";
var G__35644 = symbol;
var G__35645 = loc;
var G__35646 = (function (){var sb__4795__auto__ = (new goog.string.StringBuffer());
var _STAR_print_newline_STAR__orig_val__35647_35773 = cljs.core._STAR_print_newline_STAR_;
var _STAR_print_fn_STAR__orig_val__35648_35774 = cljs.core._STAR_print_fn_STAR_;
var _STAR_print_newline_STAR__temp_val__35649_35775 = true;
var _STAR_print_fn_STAR__temp_val__35650_35776 = (function (x__4796__auto__){
return sb__4795__auto__.append(x__4796__auto__);
});
(cljs.core._STAR_print_newline_STAR_ = _STAR_print_newline_STAR__temp_val__35649_35775);

(cljs.core._STAR_print_fn_STAR_ = _STAR_print_fn_STAR__temp_val__35650_35776);

try{cljs.spec.alpha.explain_out(cljs.core.update.cljs$core$IFn$_invoke$arity$3(spec,new cljs.core.Keyword("cljs.spec.alpha","problems","cljs.spec.alpha/problems",447400814),(function (probs){
return cljs.core.map.cljs$core$IFn$_invoke$arity$2((function (p1__35547_SHARP_){
return cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(p1__35547_SHARP_,new cljs.core.Keyword(null,"in","in",-1531184865));
}),probs);
}))
);
}finally {(cljs.core._STAR_print_fn_STAR_ = _STAR_print_fn_STAR__orig_val__35648_35774);

(cljs.core._STAR_print_newline_STAR_ = _STAR_print_newline_STAR__orig_val__35647_35773);
}
return cljs.core.str.cljs$core$IFn$_invoke$arity$1(sb__4795__auto__);
})();
return (format.cljs$core$IFn$_invoke$arity$4 ? format.cljs$core$IFn$_invoke$arity$4(G__35643,G__35644,G__35645,G__35646) : format.call(null,G__35643,G__35644,G__35645,G__35646));
} else {
var G__35652 = "Execution error%s at %s(%s).\n%s\n";
var G__35653 = cause_type;
var G__35654 = (cljs.core.truth_(symbol)?[cljs.core.str.cljs$core$IFn$_invoke$arity$1(symbol)," "].join(''):"");
var G__35655 = loc;
var G__35656 = cause;
return (format.cljs$core$IFn$_invoke$arity$5 ? format.cljs$core$IFn$_invoke$arity$5(G__35652,G__35653,G__35654,G__35655,G__35656) : format.call(null,G__35652,G__35653,G__35654,G__35655,G__35656));
}

break;
default:
throw (new Error(["No matching clause: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(G__35580__$1)].join('')));

}
});
cljs.repl.error__GT_str = (function cljs$repl$error__GT_str(error){
return cljs.repl.ex_str(cljs.repl.ex_triage(cljs.repl.Error__GT_map(error)));
});

//# sourceMappingURL=cljs.repl.js.map
