goog.provide('shadow.remote.runtime.tap_support');
shadow.remote.runtime.tap_support.tap_subscribe = (function shadow$remote$runtime$tap_support$tap_subscribe(p__36368,p__36369){
var map__36370 = p__36368;
var map__36370__$1 = cljs.core.__destructure_map(map__36370);
var svc = map__36370__$1;
var subs_ref = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__36370__$1,new cljs.core.Keyword(null,"subs-ref","subs-ref",-1355989911));
var obj_support = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__36370__$1,new cljs.core.Keyword(null,"obj-support","obj-support",1522559229));
var runtime = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__36370__$1,new cljs.core.Keyword(null,"runtime","runtime",-1331573996));
var map__36371 = p__36369;
var map__36371__$1 = cljs.core.__destructure_map(map__36371);
var msg = map__36371__$1;
var from = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__36371__$1,new cljs.core.Keyword(null,"from","from",1815293044));
var summary = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__36371__$1,new cljs.core.Keyword(null,"summary","summary",380847952));
var history = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__36371__$1,new cljs.core.Keyword(null,"history","history",-247395220));
var num = cljs.core.get.cljs$core$IFn$_invoke$arity$3(map__36371__$1,new cljs.core.Keyword(null,"num","num",1985240673),(10));
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$4(subs_ref,cljs.core.assoc,from,msg);

if(cljs.core.truth_(history)){
return shadow.remote.runtime.shared.reply(runtime,msg,new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"op","op",-1882987955),new cljs.core.Keyword(null,"tap-subscribed","tap-subscribed",-1882247432),new cljs.core.Keyword(null,"history","history",-247395220),cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentVector.EMPTY,cljs.core.map.cljs$core$IFn$_invoke$arity$2((function (oid){
return new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"oid","oid",-768692334),oid,new cljs.core.Keyword(null,"summary","summary",380847952),shadow.remote.runtime.obj_support.obj_describe_STAR_(obj_support,oid)], null);
}),shadow.remote.runtime.obj_support.get_tap_history(obj_support,num)))], null));
} else {
return null;
}
});
shadow.remote.runtime.tap_support.tap_unsubscribe = (function shadow$remote$runtime$tap_support$tap_unsubscribe(p__36374,p__36375){
var map__36376 = p__36374;
var map__36376__$1 = cljs.core.__destructure_map(map__36376);
var subs_ref = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__36376__$1,new cljs.core.Keyword(null,"subs-ref","subs-ref",-1355989911));
var map__36379 = p__36375;
var map__36379__$1 = cljs.core.__destructure_map(map__36379);
var from = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__36379__$1,new cljs.core.Keyword(null,"from","from",1815293044));
return cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(subs_ref,cljs.core.dissoc,from);
});
shadow.remote.runtime.tap_support.request_tap_history = (function shadow$remote$runtime$tap_support$request_tap_history(p__36383,p__36384){
var map__36386 = p__36383;
var map__36386__$1 = cljs.core.__destructure_map(map__36386);
var obj_support = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__36386__$1,new cljs.core.Keyword(null,"obj-support","obj-support",1522559229));
var runtime = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__36386__$1,new cljs.core.Keyword(null,"runtime","runtime",-1331573996));
var map__36387 = p__36384;
var map__36387__$1 = cljs.core.__destructure_map(map__36387);
var msg = map__36387__$1;
var num = cljs.core.get.cljs$core$IFn$_invoke$arity$3(map__36387__$1,new cljs.core.Keyword(null,"num","num",1985240673),(10));
var tap_ids = shadow.remote.runtime.obj_support.get_tap_history(obj_support,num);
return shadow.remote.runtime.shared.reply(runtime,msg,new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"op","op",-1882987955),new cljs.core.Keyword(null,"tap-history","tap-history",-282803347),new cljs.core.Keyword(null,"oids","oids",-1580877688),tap_ids], null));
});
shadow.remote.runtime.tap_support.tool_disconnect = (function shadow$remote$runtime$tap_support$tool_disconnect(p__36391,tid){
var map__36392 = p__36391;
var map__36392__$1 = cljs.core.__destructure_map(map__36392);
var svc = map__36392__$1;
var subs_ref = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__36392__$1,new cljs.core.Keyword(null,"subs-ref","subs-ref",-1355989911));
return cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(subs_ref,cljs.core.dissoc,tid);
});
shadow.remote.runtime.tap_support.start = (function shadow$remote$runtime$tap_support$start(runtime,obj_support){
var subs_ref = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.PersistentArrayMap.EMPTY);
var tap_fn = (function shadow$remote$runtime$tap_support$start_$_runtime_tap(obj){
if((!((obj == null)))){
var oid = shadow.remote.runtime.obj_support.register(obj_support,obj,new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"from","from",1815293044),new cljs.core.Keyword(null,"tap","tap",-1086702463)], null));
var seq__36399 = cljs.core.seq(cljs.core.deref(subs_ref));
var chunk__36400 = null;
var count__36401 = (0);
var i__36402 = (0);
while(true){
if((i__36402 < count__36401)){
var vec__36410 = chunk__36400.cljs$core$IIndexed$_nth$arity$2(null,i__36402);
var tid = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__36410,(0),null);
var tap_config = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__36410,(1),null);
shadow.remote.runtime.api.relay_msg(runtime,new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"op","op",-1882987955),new cljs.core.Keyword(null,"tap","tap",-1086702463),new cljs.core.Keyword(null,"to","to",192099007),tid,new cljs.core.Keyword(null,"oid","oid",-768692334),oid], null));


var G__36421 = seq__36399;
var G__36422 = chunk__36400;
var G__36423 = count__36401;
var G__36424 = (i__36402 + (1));
seq__36399 = G__36421;
chunk__36400 = G__36422;
count__36401 = G__36423;
i__36402 = G__36424;
continue;
} else {
var temp__5753__auto__ = cljs.core.seq(seq__36399);
if(temp__5753__auto__){
var seq__36399__$1 = temp__5753__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__36399__$1)){
var c__4679__auto__ = cljs.core.chunk_first(seq__36399__$1);
var G__36425 = cljs.core.chunk_rest(seq__36399__$1);
var G__36426 = c__4679__auto__;
var G__36427 = cljs.core.count(c__4679__auto__);
var G__36428 = (0);
seq__36399 = G__36425;
chunk__36400 = G__36426;
count__36401 = G__36427;
i__36402 = G__36428;
continue;
} else {
var vec__36413 = cljs.core.first(seq__36399__$1);
var tid = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__36413,(0),null);
var tap_config = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__36413,(1),null);
shadow.remote.runtime.api.relay_msg(runtime,new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"op","op",-1882987955),new cljs.core.Keyword(null,"tap","tap",-1086702463),new cljs.core.Keyword(null,"to","to",192099007),tid,new cljs.core.Keyword(null,"oid","oid",-768692334),oid], null));


var G__36429 = cljs.core.next(seq__36399__$1);
var G__36430 = null;
var G__36431 = (0);
var G__36432 = (0);
seq__36399 = G__36429;
chunk__36400 = G__36430;
count__36401 = G__36431;
i__36402 = G__36432;
continue;
}
} else {
return null;
}
}
break;
}
} else {
return null;
}
});
var svc = new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"runtime","runtime",-1331573996),runtime,new cljs.core.Keyword(null,"obj-support","obj-support",1522559229),obj_support,new cljs.core.Keyword(null,"tap-fn","tap-fn",1573556461),tap_fn,new cljs.core.Keyword(null,"subs-ref","subs-ref",-1355989911),subs_ref], null);
shadow.remote.runtime.api.add_extension(runtime,new cljs.core.Keyword("shadow.remote.runtime.tap-support","ext","shadow.remote.runtime.tap-support/ext",1019069674),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"ops","ops",1237330063),new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"tap-subscribe","tap-subscribe",411179050),(function (p1__36394_SHARP_){
return shadow.remote.runtime.tap_support.tap_subscribe(svc,p1__36394_SHARP_);
}),new cljs.core.Keyword(null,"tap-unsubscribe","tap-unsubscribe",1183890755),(function (p1__36395_SHARP_){
return shadow.remote.runtime.tap_support.tap_unsubscribe(svc,p1__36395_SHARP_);
}),new cljs.core.Keyword(null,"request-tap-history","request-tap-history",-670837812),(function (p1__36396_SHARP_){
return shadow.remote.runtime.tap_support.request_tap_history(svc,p1__36396_SHARP_);
})], null),new cljs.core.Keyword(null,"on-tool-disconnect","on-tool-disconnect",693464366),(function (p1__36397_SHARP_){
return shadow.remote.runtime.tap_support.tool_disconnect(svc,p1__36397_SHARP_);
})], null));

cljs.core.add_tap(tap_fn);

return svc;
});
shadow.remote.runtime.tap_support.stop = (function shadow$remote$runtime$tap_support$stop(p__36416){
var map__36420 = p__36416;
var map__36420__$1 = cljs.core.__destructure_map(map__36420);
var svc = map__36420__$1;
var tap_fn = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__36420__$1,new cljs.core.Keyword(null,"tap-fn","tap-fn",1573556461));
var runtime = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__36420__$1,new cljs.core.Keyword(null,"runtime","runtime",-1331573996));
cljs.core.remove_tap(tap_fn);

return shadow.remote.runtime.api.del_extension(runtime,new cljs.core.Keyword("shadow.remote.runtime.tap-support","ext","shadow.remote.runtime.tap-support/ext",1019069674));
});

//# sourceMappingURL=shadow.remote.runtime.tap_support.js.map
