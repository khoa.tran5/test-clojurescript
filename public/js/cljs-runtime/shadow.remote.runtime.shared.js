goog.provide('shadow.remote.runtime.shared');
shadow.remote.runtime.shared.init_state = (function shadow$remote$runtime$shared$init_state(client_info){
return new cljs.core.PersistentArrayMap(null, 5, [new cljs.core.Keyword(null,"extensions","extensions",-1103629196),cljs.core.PersistentArrayMap.EMPTY,new cljs.core.Keyword(null,"ops","ops",1237330063),cljs.core.PersistentArrayMap.EMPTY,new cljs.core.Keyword(null,"client-info","client-info",1958982504),client_info,new cljs.core.Keyword(null,"call-id-seq","call-id-seq",-1679248218),(0),new cljs.core.Keyword(null,"call-handlers","call-handlers",386605551),cljs.core.PersistentArrayMap.EMPTY], null);
});
shadow.remote.runtime.shared.now = (function shadow$remote$runtime$shared$now(){
return Date.now();
});
shadow.remote.runtime.shared.relay_msg = (function shadow$remote$runtime$shared$relay_msg(runtime,msg){
return shadow.remote.runtime.api.relay_msg(runtime,msg);
});
shadow.remote.runtime.shared.reply = (function shadow$remote$runtime$shared$reply(runtime,p__32416,res){
var map__32417 = p__32416;
var map__32417__$1 = cljs.core.__destructure_map(map__32417);
var call_id = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__32417__$1,new cljs.core.Keyword(null,"call-id","call-id",1043012968));
var from = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__32417__$1,new cljs.core.Keyword(null,"from","from",1815293044));
var res__$1 = (function (){var G__32419 = res;
var G__32419__$1 = (cljs.core.truth_(call_id)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__32419,new cljs.core.Keyword(null,"call-id","call-id",1043012968),call_id):G__32419);
if(cljs.core.truth_(from)){
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__32419__$1,new cljs.core.Keyword(null,"to","to",192099007),from);
} else {
return G__32419__$1;
}
})();
return shadow.remote.runtime.api.relay_msg(runtime,res__$1);
});
shadow.remote.runtime.shared.call = (function shadow$remote$runtime$shared$call(var_args){
var G__32426 = arguments.length;
switch (G__32426) {
case 3:
return shadow.remote.runtime.shared.call.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
case 4:
return shadow.remote.runtime.shared.call.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.remote.runtime.shared.call.cljs$core$IFn$_invoke$arity$3 = (function (runtime,msg,handlers){
return shadow.remote.runtime.shared.call.cljs$core$IFn$_invoke$arity$4(runtime,msg,handlers,(0));
}));

(shadow.remote.runtime.shared.call.cljs$core$IFn$_invoke$arity$4 = (function (p__32434,msg,handlers,timeout_after_ms){
var map__32440 = p__32434;
var map__32440__$1 = cljs.core.__destructure_map(map__32440);
var runtime = map__32440__$1;
var state_ref = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__32440__$1,new cljs.core.Keyword(null,"state-ref","state-ref",2127874952));
var call_id = new cljs.core.Keyword(null,"call-id-seq","call-id-seq",-1679248218).cljs$core$IFn$_invoke$arity$1(cljs.core.deref(state_ref));
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$4(state_ref,cljs.core.update,new cljs.core.Keyword(null,"call-id-seq","call-id-seq",-1679248218),cljs.core.inc);

cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$4(state_ref,cljs.core.assoc_in,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"call-handlers","call-handlers",386605551),call_id], null),new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"handlers","handlers",79528781),handlers,new cljs.core.Keyword(null,"called-at","called-at",607081160),shadow.remote.runtime.shared.now(),new cljs.core.Keyword(null,"msg","msg",-1386103444),msg,new cljs.core.Keyword(null,"timeout","timeout",-318625318),timeout_after_ms], null));

return shadow.remote.runtime.api.relay_msg(runtime,cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(msg,new cljs.core.Keyword(null,"call-id","call-id",1043012968),call_id));
}));

(shadow.remote.runtime.shared.call.cljs$lang$maxFixedArity = 4);

shadow.remote.runtime.shared.trigger_BANG_ = (function shadow$remote$runtime$shared$trigger_BANG_(var_args){
var args__4870__auto__ = [];
var len__4864__auto___32705 = arguments.length;
var i__4865__auto___32706 = (0);
while(true){
if((i__4865__auto___32706 < len__4864__auto___32705)){
args__4870__auto__.push((arguments[i__4865__auto___32706]));

var G__32708 = (i__4865__auto___32706 + (1));
i__4865__auto___32706 = G__32708;
continue;
} else {
}
break;
}

var argseq__4871__auto__ = ((((2) < args__4870__auto__.length))?(new cljs.core.IndexedSeq(args__4870__auto__.slice((2)),(0),null)):null);
return shadow.remote.runtime.shared.trigger_BANG_.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),(arguments[(1)]),argseq__4871__auto__);
});

(shadow.remote.runtime.shared.trigger_BANG_.cljs$core$IFn$_invoke$arity$variadic = (function (p__32479,ev,args){
var map__32481 = p__32479;
var map__32481__$1 = cljs.core.__destructure_map(map__32481);
var runtime = map__32481__$1;
var state_ref = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__32481__$1,new cljs.core.Keyword(null,"state-ref","state-ref",2127874952));
var seq__32482 = cljs.core.seq(cljs.core.vals(new cljs.core.Keyword(null,"extensions","extensions",-1103629196).cljs$core$IFn$_invoke$arity$1(cljs.core.deref(state_ref))));
var chunk__32485 = null;
var count__32486 = (0);
var i__32487 = (0);
while(true){
if((i__32487 < count__32486)){
var ext = chunk__32485.cljs$core$IIndexed$_nth$arity$2(null,i__32487);
var ev_fn = cljs.core.get.cljs$core$IFn$_invoke$arity$2(ext,ev);
if(cljs.core.truth_(ev_fn)){
cljs.core.apply.cljs$core$IFn$_invoke$arity$2(ev_fn,args);


var G__32712 = seq__32482;
var G__32713 = chunk__32485;
var G__32714 = count__32486;
var G__32715 = (i__32487 + (1));
seq__32482 = G__32712;
chunk__32485 = G__32713;
count__32486 = G__32714;
i__32487 = G__32715;
continue;
} else {
var G__32717 = seq__32482;
var G__32718 = chunk__32485;
var G__32719 = count__32486;
var G__32720 = (i__32487 + (1));
seq__32482 = G__32717;
chunk__32485 = G__32718;
count__32486 = G__32719;
i__32487 = G__32720;
continue;
}
} else {
var temp__5753__auto__ = cljs.core.seq(seq__32482);
if(temp__5753__auto__){
var seq__32482__$1 = temp__5753__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__32482__$1)){
var c__4679__auto__ = cljs.core.chunk_first(seq__32482__$1);
var G__32721 = cljs.core.chunk_rest(seq__32482__$1);
var G__32722 = c__4679__auto__;
var G__32723 = cljs.core.count(c__4679__auto__);
var G__32724 = (0);
seq__32482 = G__32721;
chunk__32485 = G__32722;
count__32486 = G__32723;
i__32487 = G__32724;
continue;
} else {
var ext = cljs.core.first(seq__32482__$1);
var ev_fn = cljs.core.get.cljs$core$IFn$_invoke$arity$2(ext,ev);
if(cljs.core.truth_(ev_fn)){
cljs.core.apply.cljs$core$IFn$_invoke$arity$2(ev_fn,args);


var G__32726 = cljs.core.next(seq__32482__$1);
var G__32727 = null;
var G__32728 = (0);
var G__32729 = (0);
seq__32482 = G__32726;
chunk__32485 = G__32727;
count__32486 = G__32728;
i__32487 = G__32729;
continue;
} else {
var G__32730 = cljs.core.next(seq__32482__$1);
var G__32731 = null;
var G__32732 = (0);
var G__32733 = (0);
seq__32482 = G__32730;
chunk__32485 = G__32731;
count__32486 = G__32732;
i__32487 = G__32733;
continue;
}
}
} else {
return null;
}
}
break;
}
}));

(shadow.remote.runtime.shared.trigger_BANG_.cljs$lang$maxFixedArity = (2));

/** @this {Function} */
(shadow.remote.runtime.shared.trigger_BANG_.cljs$lang$applyTo = (function (seq32469){
var G__32470 = cljs.core.first(seq32469);
var seq32469__$1 = cljs.core.next(seq32469);
var G__32471 = cljs.core.first(seq32469__$1);
var seq32469__$2 = cljs.core.next(seq32469__$1);
var self__4851__auto__ = this;
return self__4851__auto__.cljs$core$IFn$_invoke$arity$variadic(G__32470,G__32471,seq32469__$2);
}));

shadow.remote.runtime.shared.welcome = (function shadow$remote$runtime$shared$welcome(p__32514,p__32515){
var map__32519 = p__32514;
var map__32519__$1 = cljs.core.__destructure_map(map__32519);
var runtime = map__32519__$1;
var state_ref = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__32519__$1,new cljs.core.Keyword(null,"state-ref","state-ref",2127874952));
var map__32520 = p__32515;
var map__32520__$1 = cljs.core.__destructure_map(map__32520);
var msg = map__32520__$1;
var client_id = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__32520__$1,new cljs.core.Keyword(null,"client-id","client-id",-464622140));
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$4(state_ref,cljs.core.assoc,new cljs.core.Keyword(null,"client-id","client-id",-464622140),client_id);

var map__32533 = cljs.core.deref(state_ref);
var map__32533__$1 = cljs.core.__destructure_map(map__32533);
var client_info = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__32533__$1,new cljs.core.Keyword(null,"client-info","client-info",1958982504));
var extensions = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__32533__$1,new cljs.core.Keyword(null,"extensions","extensions",-1103629196));
shadow.remote.runtime.shared.relay_msg(runtime,new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"op","op",-1882987955),new cljs.core.Keyword(null,"hello","hello",-245025397),new cljs.core.Keyword(null,"client-info","client-info",1958982504),client_info], null));

return shadow.remote.runtime.shared.trigger_BANG_(runtime,new cljs.core.Keyword(null,"on-welcome","on-welcome",1895317125));
});
shadow.remote.runtime.shared.ping = (function shadow$remote$runtime$shared$ping(runtime,msg){
return shadow.remote.runtime.shared.reply(runtime,msg,new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"op","op",-1882987955),new cljs.core.Keyword(null,"pong","pong",-172484958)], null));
});
shadow.remote.runtime.shared.get_client_id = (function shadow$remote$runtime$shared$get_client_id(p__32539){
var map__32540 = p__32539;
var map__32540__$1 = cljs.core.__destructure_map(map__32540);
var runtime = map__32540__$1;
var state_ref = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__32540__$1,new cljs.core.Keyword(null,"state-ref","state-ref",2127874952));
var or__4253__auto__ = new cljs.core.Keyword(null,"client-id","client-id",-464622140).cljs$core$IFn$_invoke$arity$1(cljs.core.deref(state_ref));
if(cljs.core.truth_(or__4253__auto__)){
return or__4253__auto__;
} else {
throw cljs.core.ex_info.cljs$core$IFn$_invoke$arity$2("runtime has no assigned runtime-id",new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"runtime","runtime",-1331573996),runtime], null));
}
});
shadow.remote.runtime.shared.request_supported_ops = (function shadow$remote$runtime$shared$request_supported_ops(p__32547,msg){
var map__32548 = p__32547;
var map__32548__$1 = cljs.core.__destructure_map(map__32548);
var runtime = map__32548__$1;
var state_ref = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__32548__$1,new cljs.core.Keyword(null,"state-ref","state-ref",2127874952));
return shadow.remote.runtime.shared.reply(runtime,msg,new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"op","op",-1882987955),new cljs.core.Keyword(null,"supported-ops","supported-ops",337914702),new cljs.core.Keyword(null,"ops","ops",1237330063),cljs.core.disj.cljs$core$IFn$_invoke$arity$variadic(cljs.core.set(cljs.core.keys(new cljs.core.Keyword(null,"ops","ops",1237330063).cljs$core$IFn$_invoke$arity$1(cljs.core.deref(state_ref)))),new cljs.core.Keyword(null,"welcome","welcome",-578152123),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"unknown-relay-op","unknown-relay-op",170832753),new cljs.core.Keyword(null,"unknown-op","unknown-op",1900385996),new cljs.core.Keyword(null,"request-supported-ops","request-supported-ops",-1034994502),new cljs.core.Keyword(null,"tool-disconnect","tool-disconnect",189103996)], 0))], null));
});
shadow.remote.runtime.shared.unknown_relay_op = (function shadow$remote$runtime$shared$unknown_relay_op(msg){
return console.warn("unknown-relay-op",msg);
});
shadow.remote.runtime.shared.unknown_op = (function shadow$remote$runtime$shared$unknown_op(msg){
return console.warn("unknown-op",msg);
});
shadow.remote.runtime.shared.add_extension_STAR_ = (function shadow$remote$runtime$shared$add_extension_STAR_(p__32556,key,p__32557){
var map__32558 = p__32556;
var map__32558__$1 = cljs.core.__destructure_map(map__32558);
var state = map__32558__$1;
var extensions = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__32558__$1,new cljs.core.Keyword(null,"extensions","extensions",-1103629196));
var map__32559 = p__32557;
var map__32559__$1 = cljs.core.__destructure_map(map__32559);
var spec = map__32559__$1;
var ops = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__32559__$1,new cljs.core.Keyword(null,"ops","ops",1237330063));
if(cljs.core.contains_QMARK_(extensions,key)){
throw cljs.core.ex_info.cljs$core$IFn$_invoke$arity$2("extension already registered",new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"key","key",-1516042587),key,new cljs.core.Keyword(null,"spec","spec",347520401),spec], null));
} else {
}

return cljs.core.reduce_kv((function (state__$1,op_kw,op_handler){
if(cljs.core.truth_(cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(state__$1,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"ops","ops",1237330063),op_kw], null)))){
throw cljs.core.ex_info.cljs$core$IFn$_invoke$arity$2("op already registered",new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"key","key",-1516042587),key,new cljs.core.Keyword(null,"op","op",-1882987955),op_kw], null));
} else {
}

return cljs.core.assoc_in(state__$1,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"ops","ops",1237330063),op_kw], null),op_handler);
}),cljs.core.assoc_in(state,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"extensions","extensions",-1103629196),key], null),spec),ops);
});
shadow.remote.runtime.shared.add_extension = (function shadow$remote$runtime$shared$add_extension(p__32562,key,spec){
var map__32563 = p__32562;
var map__32563__$1 = cljs.core.__destructure_map(map__32563);
var state_ref = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__32563__$1,new cljs.core.Keyword(null,"state-ref","state-ref",2127874952));
return cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$4(state_ref,shadow.remote.runtime.shared.add_extension_STAR_,key,spec);
});
shadow.remote.runtime.shared.add_defaults = (function shadow$remote$runtime$shared$add_defaults(runtime){
return shadow.remote.runtime.shared.add_extension(runtime,new cljs.core.Keyword("shadow.remote.runtime.shared","defaults","shadow.remote.runtime.shared/defaults",-1821257543),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"ops","ops",1237330063),new cljs.core.PersistentArrayMap(null, 5, [new cljs.core.Keyword(null,"welcome","welcome",-578152123),(function (p1__32566_SHARP_){
return shadow.remote.runtime.shared.welcome(runtime,p1__32566_SHARP_);
}),new cljs.core.Keyword(null,"unknown-relay-op","unknown-relay-op",170832753),(function (p1__32567_SHARP_){
return shadow.remote.runtime.shared.unknown_relay_op(p1__32567_SHARP_);
}),new cljs.core.Keyword(null,"unknown-op","unknown-op",1900385996),(function (p1__32568_SHARP_){
return shadow.remote.runtime.shared.unknown_op(p1__32568_SHARP_);
}),new cljs.core.Keyword(null,"ping","ping",-1670114784),(function (p1__32569_SHARP_){
return shadow.remote.runtime.shared.ping(runtime,p1__32569_SHARP_);
}),new cljs.core.Keyword(null,"request-supported-ops","request-supported-ops",-1034994502),(function (p1__32570_SHARP_){
return shadow.remote.runtime.shared.request_supported_ops(runtime,p1__32570_SHARP_);
})], null)], null));
});
shadow.remote.runtime.shared.del_extension_STAR_ = (function shadow$remote$runtime$shared$del_extension_STAR_(state,key){
var ext = cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(state,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"extensions","extensions",-1103629196),key], null));
if(cljs.core.not(ext)){
return state;
} else {
return cljs.core.reduce_kv((function (state__$1,op_kw,op_handler){
return cljs.core.update_in.cljs$core$IFn$_invoke$arity$4(state__$1,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"ops","ops",1237330063)], null),cljs.core.dissoc,op_kw);
}),cljs.core.update.cljs$core$IFn$_invoke$arity$4(state,new cljs.core.Keyword(null,"extensions","extensions",-1103629196),cljs.core.dissoc,key),new cljs.core.Keyword(null,"ops","ops",1237330063).cljs$core$IFn$_invoke$arity$1(ext));
}
});
shadow.remote.runtime.shared.del_extension = (function shadow$remote$runtime$shared$del_extension(p__32577,key){
var map__32578 = p__32577;
var map__32578__$1 = cljs.core.__destructure_map(map__32578);
var state_ref = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__32578__$1,new cljs.core.Keyword(null,"state-ref","state-ref",2127874952));
return cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(state_ref,shadow.remote.runtime.shared.del_extension_STAR_,key);
});
shadow.remote.runtime.shared.unhandled_call_result = (function shadow$remote$runtime$shared$unhandled_call_result(call_config,msg){
return console.warn("unhandled call result",msg,call_config);
});
shadow.remote.runtime.shared.unhandled_client_not_found = (function shadow$remote$runtime$shared$unhandled_client_not_found(p__32588,msg){
var map__32590 = p__32588;
var map__32590__$1 = cljs.core.__destructure_map(map__32590);
var runtime = map__32590__$1;
var state_ref = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__32590__$1,new cljs.core.Keyword(null,"state-ref","state-ref",2127874952));
return shadow.remote.runtime.shared.trigger_BANG_.cljs$core$IFn$_invoke$arity$variadic(runtime,new cljs.core.Keyword(null,"on-client-not-found","on-client-not-found",-642452849),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([msg], 0));
});
shadow.remote.runtime.shared.reply_unknown_op = (function shadow$remote$runtime$shared$reply_unknown_op(runtime,msg){
return shadow.remote.runtime.shared.reply(runtime,msg,new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"op","op",-1882987955),new cljs.core.Keyword(null,"unknown-op","unknown-op",1900385996),new cljs.core.Keyword(null,"msg","msg",-1386103444),msg], null));
});
shadow.remote.runtime.shared.process = (function shadow$remote$runtime$shared$process(p__32606,p__32607){
var map__32609 = p__32606;
var map__32609__$1 = cljs.core.__destructure_map(map__32609);
var runtime = map__32609__$1;
var state_ref = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__32609__$1,new cljs.core.Keyword(null,"state-ref","state-ref",2127874952));
var map__32610 = p__32607;
var map__32610__$1 = cljs.core.__destructure_map(map__32610);
var msg = map__32610__$1;
var op = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__32610__$1,new cljs.core.Keyword(null,"op","op",-1882987955));
var call_id = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__32610__$1,new cljs.core.Keyword(null,"call-id","call-id",1043012968));
var state = cljs.core.deref(state_ref);
var op_handler = cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(state,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"ops","ops",1237330063),op], null));
if(cljs.core.truth_(call_id)){
var cfg = cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(state,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"call-handlers","call-handlers",386605551),call_id], null));
var call_handler = cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(cfg,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"handlers","handlers",79528781),op], null));
if(cljs.core.truth_(call_handler)){
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$variadic(state_ref,cljs.core.update,new cljs.core.Keyword(null,"call-handlers","call-handlers",386605551),cljs.core.dissoc,cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([call_id], 0));

return (call_handler.cljs$core$IFn$_invoke$arity$1 ? call_handler.cljs$core$IFn$_invoke$arity$1(msg) : call_handler.call(null,msg));
} else {
if(cljs.core.truth_(op_handler)){
return (op_handler.cljs$core$IFn$_invoke$arity$1 ? op_handler.cljs$core$IFn$_invoke$arity$1(msg) : op_handler.call(null,msg));
} else {
return shadow.remote.runtime.shared.unhandled_call_result(cfg,msg);

}
}
} else {
if(cljs.core.truth_(op_handler)){
return (op_handler.cljs$core$IFn$_invoke$arity$1 ? op_handler.cljs$core$IFn$_invoke$arity$1(msg) : op_handler.call(null,msg));
} else {
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"client-not-found","client-not-found",-1754042614),op)){
return shadow.remote.runtime.shared.unhandled_client_not_found(runtime,msg);
} else {
return shadow.remote.runtime.shared.reply_unknown_op(runtime,msg);

}
}
}
});
shadow.remote.runtime.shared.run_on_idle = (function shadow$remote$runtime$shared$run_on_idle(state_ref){
var seq__32648 = cljs.core.seq(cljs.core.vals(new cljs.core.Keyword(null,"extensions","extensions",-1103629196).cljs$core$IFn$_invoke$arity$1(cljs.core.deref(state_ref))));
var chunk__32650 = null;
var count__32651 = (0);
var i__32652 = (0);
while(true){
if((i__32652 < count__32651)){
var map__32661 = chunk__32650.cljs$core$IIndexed$_nth$arity$2(null,i__32652);
var map__32661__$1 = cljs.core.__destructure_map(map__32661);
var on_idle = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__32661__$1,new cljs.core.Keyword(null,"on-idle","on-idle",2044706602));
if(cljs.core.truth_(on_idle)){
(on_idle.cljs$core$IFn$_invoke$arity$0 ? on_idle.cljs$core$IFn$_invoke$arity$0() : on_idle.call(null));


var G__32762 = seq__32648;
var G__32763 = chunk__32650;
var G__32764 = count__32651;
var G__32765 = (i__32652 + (1));
seq__32648 = G__32762;
chunk__32650 = G__32763;
count__32651 = G__32764;
i__32652 = G__32765;
continue;
} else {
var G__32771 = seq__32648;
var G__32772 = chunk__32650;
var G__32773 = count__32651;
var G__32774 = (i__32652 + (1));
seq__32648 = G__32771;
chunk__32650 = G__32772;
count__32651 = G__32773;
i__32652 = G__32774;
continue;
}
} else {
var temp__5753__auto__ = cljs.core.seq(seq__32648);
if(temp__5753__auto__){
var seq__32648__$1 = temp__5753__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__32648__$1)){
var c__4679__auto__ = cljs.core.chunk_first(seq__32648__$1);
var G__32780 = cljs.core.chunk_rest(seq__32648__$1);
var G__32781 = c__4679__auto__;
var G__32782 = cljs.core.count(c__4679__auto__);
var G__32783 = (0);
seq__32648 = G__32780;
chunk__32650 = G__32781;
count__32651 = G__32782;
i__32652 = G__32783;
continue;
} else {
var map__32662 = cljs.core.first(seq__32648__$1);
var map__32662__$1 = cljs.core.__destructure_map(map__32662);
var on_idle = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__32662__$1,new cljs.core.Keyword(null,"on-idle","on-idle",2044706602));
if(cljs.core.truth_(on_idle)){
(on_idle.cljs$core$IFn$_invoke$arity$0 ? on_idle.cljs$core$IFn$_invoke$arity$0() : on_idle.call(null));


var G__32786 = cljs.core.next(seq__32648__$1);
var G__32787 = null;
var G__32788 = (0);
var G__32789 = (0);
seq__32648 = G__32786;
chunk__32650 = G__32787;
count__32651 = G__32788;
i__32652 = G__32789;
continue;
} else {
var G__32790 = cljs.core.next(seq__32648__$1);
var G__32791 = null;
var G__32792 = (0);
var G__32793 = (0);
seq__32648 = G__32790;
chunk__32650 = G__32791;
count__32651 = G__32792;
i__32652 = G__32793;
continue;
}
}
} else {
return null;
}
}
break;
}
});

//# sourceMappingURL=shadow.remote.runtime.shared.js.map
