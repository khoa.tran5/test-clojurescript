goog.provide('shadow.dom');
shadow.dom.transition_supported_QMARK_ = (((typeof window !== 'undefined'))?goog.style.transition.isSupported():null);

/**
 * @interface
 */
shadow.dom.IElement = function(){};

var shadow$dom$IElement$_to_dom$dyn_35796 = (function (this$){
var x__4550__auto__ = (((this$ == null))?null:this$);
var m__4551__auto__ = (shadow.dom._to_dom[goog.typeOf(x__4550__auto__)]);
if((!((m__4551__auto__ == null)))){
return (m__4551__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4551__auto__.cljs$core$IFn$_invoke$arity$1(this$) : m__4551__auto__.call(null,this$));
} else {
var m__4549__auto__ = (shadow.dom._to_dom["_"]);
if((!((m__4549__auto__ == null)))){
return (m__4549__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4549__auto__.cljs$core$IFn$_invoke$arity$1(this$) : m__4549__auto__.call(null,this$));
} else {
throw cljs.core.missing_protocol("IElement.-to-dom",this$);
}
}
});
shadow.dom._to_dom = (function shadow$dom$_to_dom(this$){
if((((!((this$ == null)))) && ((!((this$.shadow$dom$IElement$_to_dom$arity$1 == null)))))){
return this$.shadow$dom$IElement$_to_dom$arity$1(this$);
} else {
return shadow$dom$IElement$_to_dom$dyn_35796(this$);
}
});


/**
 * @interface
 */
shadow.dom.SVGElement = function(){};

var shadow$dom$SVGElement$_to_svg$dyn_35799 = (function (this$){
var x__4550__auto__ = (((this$ == null))?null:this$);
var m__4551__auto__ = (shadow.dom._to_svg[goog.typeOf(x__4550__auto__)]);
if((!((m__4551__auto__ == null)))){
return (m__4551__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4551__auto__.cljs$core$IFn$_invoke$arity$1(this$) : m__4551__auto__.call(null,this$));
} else {
var m__4549__auto__ = (shadow.dom._to_svg["_"]);
if((!((m__4549__auto__ == null)))){
return (m__4549__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4549__auto__.cljs$core$IFn$_invoke$arity$1(this$) : m__4549__auto__.call(null,this$));
} else {
throw cljs.core.missing_protocol("SVGElement.-to-svg",this$);
}
}
});
shadow.dom._to_svg = (function shadow$dom$_to_svg(this$){
if((((!((this$ == null)))) && ((!((this$.shadow$dom$SVGElement$_to_svg$arity$1 == null)))))){
return this$.shadow$dom$SVGElement$_to_svg$arity$1(this$);
} else {
return shadow$dom$SVGElement$_to_svg$dyn_35799(this$);
}
});

shadow.dom.lazy_native_coll_seq = (function shadow$dom$lazy_native_coll_seq(coll,idx){
if((idx < coll.length)){
return (new cljs.core.LazySeq(null,(function (){
return cljs.core.cons((coll[idx]),(function (){var G__34693 = coll;
var G__34694 = (idx + (1));
return (shadow.dom.lazy_native_coll_seq.cljs$core$IFn$_invoke$arity$2 ? shadow.dom.lazy_native_coll_seq.cljs$core$IFn$_invoke$arity$2(G__34693,G__34694) : shadow.dom.lazy_native_coll_seq.call(null,G__34693,G__34694));
})());
}),null,null));
} else {
return null;
}
});

/**
* @constructor
 * @implements {cljs.core.IIndexed}
 * @implements {cljs.core.ICounted}
 * @implements {cljs.core.ISeqable}
 * @implements {cljs.core.IDeref}
 * @implements {shadow.dom.IElement}
*/
shadow.dom.NativeColl = (function (coll){
this.coll = coll;
this.cljs$lang$protocol_mask$partition0$ = 8421394;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(shadow.dom.NativeColl.prototype.cljs$core$IDeref$_deref$arity$1 = (function (this$){
var self__ = this;
var this$__$1 = this;
return self__.coll;
}));

(shadow.dom.NativeColl.prototype.cljs$core$IIndexed$_nth$arity$2 = (function (this$,n){
var self__ = this;
var this$__$1 = this;
return (self__.coll[n]);
}));

(shadow.dom.NativeColl.prototype.cljs$core$IIndexed$_nth$arity$3 = (function (this$,n,not_found){
var self__ = this;
var this$__$1 = this;
var or__4253__auto__ = (self__.coll[n]);
if(cljs.core.truth_(or__4253__auto__)){
return or__4253__auto__;
} else {
return not_found;
}
}));

(shadow.dom.NativeColl.prototype.cljs$core$ICounted$_count$arity$1 = (function (this$){
var self__ = this;
var this$__$1 = this;
return self__.coll.length;
}));

(shadow.dom.NativeColl.prototype.cljs$core$ISeqable$_seq$arity$1 = (function (this$){
var self__ = this;
var this$__$1 = this;
return shadow.dom.lazy_native_coll_seq(self__.coll,(0));
}));

(shadow.dom.NativeColl.prototype.shadow$dom$IElement$ = cljs.core.PROTOCOL_SENTINEL);

(shadow.dom.NativeColl.prototype.shadow$dom$IElement$_to_dom$arity$1 = (function (this$){
var self__ = this;
var this$__$1 = this;
return self__.coll;
}));

(shadow.dom.NativeColl.getBasis = (function (){
return new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"coll","coll",-1006698606,null)], null);
}));

(shadow.dom.NativeColl.cljs$lang$type = true);

(shadow.dom.NativeColl.cljs$lang$ctorStr = "shadow.dom/NativeColl");

(shadow.dom.NativeColl.cljs$lang$ctorPrWriter = (function (this__4491__auto__,writer__4492__auto__,opt__4493__auto__){
return cljs.core._write(writer__4492__auto__,"shadow.dom/NativeColl");
}));

/**
 * Positional factory function for shadow.dom/NativeColl.
 */
shadow.dom.__GT_NativeColl = (function shadow$dom$__GT_NativeColl(coll){
return (new shadow.dom.NativeColl(coll));
});

shadow.dom.native_coll = (function shadow$dom$native_coll(coll){
return (new shadow.dom.NativeColl(coll));
});
shadow.dom.dom_node = (function shadow$dom$dom_node(el){
if((el == null)){
return null;
} else {
if((((!((el == null))))?((((false) || ((cljs.core.PROTOCOL_SENTINEL === el.shadow$dom$IElement$))))?true:false):false)){
return el.shadow$dom$IElement$_to_dom$arity$1(null);
} else {
if(typeof el === 'string'){
return document.createTextNode(el);
} else {
if(typeof el === 'number'){
return document.createTextNode(cljs.core.str.cljs$core$IFn$_invoke$arity$1(el));
} else {
return el;

}
}
}
}
});
shadow.dom.query_one = (function shadow$dom$query_one(var_args){
var G__34730 = arguments.length;
switch (G__34730) {
case 1:
return shadow.dom.query_one.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return shadow.dom.query_one.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.query_one.cljs$core$IFn$_invoke$arity$1 = (function (sel){
return document.querySelector(sel);
}));

(shadow.dom.query_one.cljs$core$IFn$_invoke$arity$2 = (function (sel,root){
return shadow.dom.dom_node(root).querySelector(sel);
}));

(shadow.dom.query_one.cljs$lang$maxFixedArity = 2);

shadow.dom.query = (function shadow$dom$query(var_args){
var G__34754 = arguments.length;
switch (G__34754) {
case 1:
return shadow.dom.query.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return shadow.dom.query.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.query.cljs$core$IFn$_invoke$arity$1 = (function (sel){
return (new shadow.dom.NativeColl(document.querySelectorAll(sel)));
}));

(shadow.dom.query.cljs$core$IFn$_invoke$arity$2 = (function (sel,root){
return (new shadow.dom.NativeColl(shadow.dom.dom_node(root).querySelectorAll(sel)));
}));

(shadow.dom.query.cljs$lang$maxFixedArity = 2);

shadow.dom.by_id = (function shadow$dom$by_id(var_args){
var G__34760 = arguments.length;
switch (G__34760) {
case 2:
return shadow.dom.by_id.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 1:
return shadow.dom.by_id.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.by_id.cljs$core$IFn$_invoke$arity$2 = (function (id,el){
return shadow.dom.dom_node(el).getElementById(id);
}));

(shadow.dom.by_id.cljs$core$IFn$_invoke$arity$1 = (function (id){
return document.getElementById(id);
}));

(shadow.dom.by_id.cljs$lang$maxFixedArity = 2);

shadow.dom.build = shadow.dom.dom_node;
shadow.dom.ev_stop = (function shadow$dom$ev_stop(var_args){
var G__34765 = arguments.length;
switch (G__34765) {
case 1:
return shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 4:
return shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$1 = (function (e){
if(cljs.core.truth_(e.stopPropagation)){
e.stopPropagation();

e.preventDefault();
} else {
(e.cancelBubble = true);

(e.returnValue = false);
}

return e;
}));

(shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$2 = (function (e,el){
shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$1(e);

return el;
}));

(shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$4 = (function (e,el,scope,owner){
shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$1(e);

return el;
}));

(shadow.dom.ev_stop.cljs$lang$maxFixedArity = 4);

/**
 * check wether a parent node (or the document) contains the child
 */
shadow.dom.contains_QMARK_ = (function shadow$dom$contains_QMARK_(var_args){
var G__34841 = arguments.length;
switch (G__34841) {
case 1:
return shadow.dom.contains_QMARK_.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return shadow.dom.contains_QMARK_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.contains_QMARK_.cljs$core$IFn$_invoke$arity$1 = (function (el){
return goog.dom.contains(document,shadow.dom.dom_node(el));
}));

(shadow.dom.contains_QMARK_.cljs$core$IFn$_invoke$arity$2 = (function (parent,el){
return goog.dom.contains(shadow.dom.dom_node(parent),shadow.dom.dom_node(el));
}));

(shadow.dom.contains_QMARK_.cljs$lang$maxFixedArity = 2);

shadow.dom.add_class = (function shadow$dom$add_class(el,cls){
return goog.dom.classlist.add(shadow.dom.dom_node(el),cls);
});
shadow.dom.remove_class = (function shadow$dom$remove_class(el,cls){
return goog.dom.classlist.remove(shadow.dom.dom_node(el),cls);
});
shadow.dom.toggle_class = (function shadow$dom$toggle_class(var_args){
var G__34856 = arguments.length;
switch (G__34856) {
case 2:
return shadow.dom.toggle_class.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return shadow.dom.toggle_class.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.toggle_class.cljs$core$IFn$_invoke$arity$2 = (function (el,cls){
return goog.dom.classlist.toggle(shadow.dom.dom_node(el),cls);
}));

(shadow.dom.toggle_class.cljs$core$IFn$_invoke$arity$3 = (function (el,cls,v){
if(cljs.core.truth_(v)){
return shadow.dom.add_class(el,cls);
} else {
return shadow.dom.remove_class(el,cls);
}
}));

(shadow.dom.toggle_class.cljs$lang$maxFixedArity = 3);

shadow.dom.dom_listen = (cljs.core.truth_((function (){var or__4253__auto__ = (!((typeof document !== 'undefined')));
if(or__4253__auto__){
return or__4253__auto__;
} else {
return document.addEventListener;
}
})())?(function shadow$dom$dom_listen_good(el,ev,handler){
return el.addEventListener(ev,handler,false);
}):(function shadow$dom$dom_listen_ie(el,ev,handler){
try{return el.attachEvent(["on",cljs.core.str.cljs$core$IFn$_invoke$arity$1(ev)].join(''),(function (e){
return (handler.cljs$core$IFn$_invoke$arity$2 ? handler.cljs$core$IFn$_invoke$arity$2(e,el) : handler.call(null,e,el));
}));
}catch (e34862){if((e34862 instanceof Object)){
var e = e34862;
return console.log("didnt support attachEvent",el,e);
} else {
throw e34862;

}
}}));
shadow.dom.dom_listen_remove = (cljs.core.truth_((function (){var or__4253__auto__ = (!((typeof document !== 'undefined')));
if(or__4253__auto__){
return or__4253__auto__;
} else {
return document.removeEventListener;
}
})())?(function shadow$dom$dom_listen_remove_good(el,ev,handler){
return el.removeEventListener(ev,handler,false);
}):(function shadow$dom$dom_listen_remove_ie(el,ev,handler){
return el.detachEvent(["on",cljs.core.str.cljs$core$IFn$_invoke$arity$1(ev)].join(''),handler);
}));
shadow.dom.on_query = (function shadow$dom$on_query(root_el,ev,selector,handler){
var seq__34875 = cljs.core.seq(shadow.dom.query.cljs$core$IFn$_invoke$arity$2(selector,root_el));
var chunk__34876 = null;
var count__34877 = (0);
var i__34878 = (0);
while(true){
if((i__34878 < count__34877)){
var el = chunk__34876.cljs$core$IIndexed$_nth$arity$2(null,i__34878);
var handler_35811__$1 = ((function (seq__34875,chunk__34876,count__34877,i__34878,el){
return (function (e){
return (handler.cljs$core$IFn$_invoke$arity$2 ? handler.cljs$core$IFn$_invoke$arity$2(e,el) : handler.call(null,e,el));
});})(seq__34875,chunk__34876,count__34877,i__34878,el))
;
shadow.dom.dom_listen(el,cljs.core.name(ev),handler_35811__$1);


var G__35812 = seq__34875;
var G__35813 = chunk__34876;
var G__35814 = count__34877;
var G__35815 = (i__34878 + (1));
seq__34875 = G__35812;
chunk__34876 = G__35813;
count__34877 = G__35814;
i__34878 = G__35815;
continue;
} else {
var temp__5753__auto__ = cljs.core.seq(seq__34875);
if(temp__5753__auto__){
var seq__34875__$1 = temp__5753__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__34875__$1)){
var c__4679__auto__ = cljs.core.chunk_first(seq__34875__$1);
var G__35816 = cljs.core.chunk_rest(seq__34875__$1);
var G__35817 = c__4679__auto__;
var G__35818 = cljs.core.count(c__4679__auto__);
var G__35819 = (0);
seq__34875 = G__35816;
chunk__34876 = G__35817;
count__34877 = G__35818;
i__34878 = G__35819;
continue;
} else {
var el = cljs.core.first(seq__34875__$1);
var handler_35820__$1 = ((function (seq__34875,chunk__34876,count__34877,i__34878,el,seq__34875__$1,temp__5753__auto__){
return (function (e){
return (handler.cljs$core$IFn$_invoke$arity$2 ? handler.cljs$core$IFn$_invoke$arity$2(e,el) : handler.call(null,e,el));
});})(seq__34875,chunk__34876,count__34877,i__34878,el,seq__34875__$1,temp__5753__auto__))
;
shadow.dom.dom_listen(el,cljs.core.name(ev),handler_35820__$1);


var G__35821 = cljs.core.next(seq__34875__$1);
var G__35822 = null;
var G__35823 = (0);
var G__35824 = (0);
seq__34875 = G__35821;
chunk__34876 = G__35822;
count__34877 = G__35823;
i__34878 = G__35824;
continue;
}
} else {
return null;
}
}
break;
}
});
shadow.dom.on = (function shadow$dom$on(var_args){
var G__34909 = arguments.length;
switch (G__34909) {
case 3:
return shadow.dom.on.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
case 4:
return shadow.dom.on.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.on.cljs$core$IFn$_invoke$arity$3 = (function (el,ev,handler){
return shadow.dom.on.cljs$core$IFn$_invoke$arity$4(el,ev,handler,false);
}));

(shadow.dom.on.cljs$core$IFn$_invoke$arity$4 = (function (el,ev,handler,capture){
if(cljs.core.vector_QMARK_(ev)){
return shadow.dom.on_query(el,cljs.core.first(ev),cljs.core.second(ev),handler);
} else {
var handler__$1 = (function (e){
return (handler.cljs$core$IFn$_invoke$arity$2 ? handler.cljs$core$IFn$_invoke$arity$2(e,el) : handler.call(null,e,el));
});
return shadow.dom.dom_listen(shadow.dom.dom_node(el),cljs.core.name(ev),handler__$1);
}
}));

(shadow.dom.on.cljs$lang$maxFixedArity = 4);

shadow.dom.remove_event_handler = (function shadow$dom$remove_event_handler(el,ev,handler){
return shadow.dom.dom_listen_remove(shadow.dom.dom_node(el),cljs.core.name(ev),handler);
});
shadow.dom.add_event_listeners = (function shadow$dom$add_event_listeners(el,events){
var seq__34925 = cljs.core.seq(events);
var chunk__34926 = null;
var count__34927 = (0);
var i__34928 = (0);
while(true){
if((i__34928 < count__34927)){
var vec__34946 = chunk__34926.cljs$core$IIndexed$_nth$arity$2(null,i__34928);
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34946,(0),null);
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34946,(1),null);
shadow.dom.on.cljs$core$IFn$_invoke$arity$3(el,k,v);


var G__35829 = seq__34925;
var G__35830 = chunk__34926;
var G__35831 = count__34927;
var G__35832 = (i__34928 + (1));
seq__34925 = G__35829;
chunk__34926 = G__35830;
count__34927 = G__35831;
i__34928 = G__35832;
continue;
} else {
var temp__5753__auto__ = cljs.core.seq(seq__34925);
if(temp__5753__auto__){
var seq__34925__$1 = temp__5753__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__34925__$1)){
var c__4679__auto__ = cljs.core.chunk_first(seq__34925__$1);
var G__35833 = cljs.core.chunk_rest(seq__34925__$1);
var G__35834 = c__4679__auto__;
var G__35835 = cljs.core.count(c__4679__auto__);
var G__35836 = (0);
seq__34925 = G__35833;
chunk__34926 = G__35834;
count__34927 = G__35835;
i__34928 = G__35836;
continue;
} else {
var vec__34956 = cljs.core.first(seq__34925__$1);
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34956,(0),null);
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34956,(1),null);
shadow.dom.on.cljs$core$IFn$_invoke$arity$3(el,k,v);


var G__35837 = cljs.core.next(seq__34925__$1);
var G__35838 = null;
var G__35839 = (0);
var G__35840 = (0);
seq__34925 = G__35837;
chunk__34926 = G__35838;
count__34927 = G__35839;
i__34928 = G__35840;
continue;
}
} else {
return null;
}
}
break;
}
});
shadow.dom.set_style = (function shadow$dom$set_style(el,styles){
var dom = shadow.dom.dom_node(el);
var seq__34969 = cljs.core.seq(styles);
var chunk__34970 = null;
var count__34971 = (0);
var i__34972 = (0);
while(true){
if((i__34972 < count__34971)){
var vec__35003 = chunk__34970.cljs$core$IIndexed$_nth$arity$2(null,i__34972);
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35003,(0),null);
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35003,(1),null);
goog.style.setStyle(dom,cljs.core.name(k),(((v == null))?"":v));


var G__35845 = seq__34969;
var G__35846 = chunk__34970;
var G__35847 = count__34971;
var G__35848 = (i__34972 + (1));
seq__34969 = G__35845;
chunk__34970 = G__35846;
count__34971 = G__35847;
i__34972 = G__35848;
continue;
} else {
var temp__5753__auto__ = cljs.core.seq(seq__34969);
if(temp__5753__auto__){
var seq__34969__$1 = temp__5753__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__34969__$1)){
var c__4679__auto__ = cljs.core.chunk_first(seq__34969__$1);
var G__35849 = cljs.core.chunk_rest(seq__34969__$1);
var G__35850 = c__4679__auto__;
var G__35851 = cljs.core.count(c__4679__auto__);
var G__35852 = (0);
seq__34969 = G__35849;
chunk__34970 = G__35850;
count__34971 = G__35851;
i__34972 = G__35852;
continue;
} else {
var vec__35017 = cljs.core.first(seq__34969__$1);
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35017,(0),null);
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35017,(1),null);
goog.style.setStyle(dom,cljs.core.name(k),(((v == null))?"":v));


var G__35853 = cljs.core.next(seq__34969__$1);
var G__35854 = null;
var G__35855 = (0);
var G__35856 = (0);
seq__34969 = G__35853;
chunk__34970 = G__35854;
count__34971 = G__35855;
i__34972 = G__35856;
continue;
}
} else {
return null;
}
}
break;
}
});
shadow.dom.set_attr_STAR_ = (function shadow$dom$set_attr_STAR_(el,key,value){
var G__35028_35857 = key;
var G__35028_35858__$1 = (((G__35028_35857 instanceof cljs.core.Keyword))?G__35028_35857.fqn:null);
switch (G__35028_35858__$1) {
case "id":
(el.id = cljs.core.str.cljs$core$IFn$_invoke$arity$1(value));

break;
case "class":
(el.className = cljs.core.str.cljs$core$IFn$_invoke$arity$1(value));

break;
case "for":
(el.htmlFor = value);

break;
case "cellpadding":
el.setAttribute("cellPadding",value);

break;
case "cellspacing":
el.setAttribute("cellSpacing",value);

break;
case "colspan":
el.setAttribute("colSpan",value);

break;
case "frameborder":
el.setAttribute("frameBorder",value);

break;
case "height":
el.setAttribute("height",value);

break;
case "maxlength":
el.setAttribute("maxLength",value);

break;
case "role":
el.setAttribute("role",value);

break;
case "rowspan":
el.setAttribute("rowSpan",value);

break;
case "type":
el.setAttribute("type",value);

break;
case "usemap":
el.setAttribute("useMap",value);

break;
case "valign":
el.setAttribute("vAlign",value);

break;
case "width":
el.setAttribute("width",value);

break;
case "on":
shadow.dom.add_event_listeners(el,value);

break;
case "style":
if((value == null)){
} else {
if(typeof value === 'string'){
el.setAttribute("style",value);
} else {
if(cljs.core.map_QMARK_(value)){
shadow.dom.set_style(el,value);
} else {
goog.style.setStyle(el,value);

}
}
}

break;
default:
var ks_35862 = cljs.core.name(key);
if(cljs.core.truth_((function (){var or__4253__auto__ = goog.string.startsWith(ks_35862,"data-");
if(cljs.core.truth_(or__4253__auto__)){
return or__4253__auto__;
} else {
return goog.string.startsWith(ks_35862,"aria-");
}
})())){
el.setAttribute(ks_35862,value);
} else {
(el[ks_35862] = value);
}

}

return el;
});
shadow.dom.set_attrs = (function shadow$dom$set_attrs(el,attrs){
return cljs.core.reduce_kv((function (el__$1,key,value){
shadow.dom.set_attr_STAR_(el__$1,key,value);

return el__$1;
}),shadow.dom.dom_node(el),attrs);
});
shadow.dom.set_attr = (function shadow$dom$set_attr(el,key,value){
return shadow.dom.set_attr_STAR_(shadow.dom.dom_node(el),key,value);
});
shadow.dom.has_class_QMARK_ = (function shadow$dom$has_class_QMARK_(el,cls){
return goog.dom.classlist.contains(shadow.dom.dom_node(el),cls);
});
shadow.dom.merge_class_string = (function shadow$dom$merge_class_string(current,extra_class){
if(cljs.core.seq(current)){
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(current)," ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(extra_class)].join('');
} else {
return extra_class;
}
});
shadow.dom.parse_tag = (function shadow$dom$parse_tag(spec){
var spec__$1 = cljs.core.name(spec);
var fdot = spec__$1.indexOf(".");
var fhash = spec__$1.indexOf("#");
if(((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2((-1),fdot)) && (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2((-1),fhash)))){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [spec__$1,null,null], null);
} else {
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2((-1),fhash)){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [spec__$1.substring((0),fdot),null,clojure.string.replace(spec__$1.substring((fdot + (1))),/\./," ")], null);
} else {
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2((-1),fdot)){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [spec__$1.substring((0),fhash),spec__$1.substring((fhash + (1))),null], null);
} else {
if((fhash > fdot)){
throw ["cant have id after class?",spec__$1].join('');
} else {
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [spec__$1.substring((0),fhash),spec__$1.substring((fhash + (1)),fdot),clojure.string.replace(spec__$1.substring((fdot + (1))),/\./," ")], null);

}
}
}
}
});
shadow.dom.create_dom_node = (function shadow$dom$create_dom_node(tag_def,p__35117){
var map__35120 = p__35117;
var map__35120__$1 = cljs.core.__destructure_map(map__35120);
var props = map__35120__$1;
var class$ = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__35120__$1,new cljs.core.Keyword(null,"class","class",-2030961996));
var tag_props = ({});
var vec__35121 = shadow.dom.parse_tag(tag_def);
var tag_name = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35121,(0),null);
var tag_id = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35121,(1),null);
var tag_classes = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35121,(2),null);
if(cljs.core.truth_(tag_id)){
(tag_props["id"] = tag_id);
} else {
}

if(cljs.core.truth_(tag_classes)){
(tag_props["class"] = shadow.dom.merge_class_string(class$,tag_classes));
} else {
}

var G__35127 = goog.dom.createDom(tag_name,tag_props);
shadow.dom.set_attrs(G__35127,cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(props,new cljs.core.Keyword(null,"class","class",-2030961996)));

return G__35127;
});
shadow.dom.append = (function shadow$dom$append(var_args){
var G__35143 = arguments.length;
switch (G__35143) {
case 1:
return shadow.dom.append.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return shadow.dom.append.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.append.cljs$core$IFn$_invoke$arity$1 = (function (node){
if(cljs.core.truth_(node)){
var temp__5753__auto__ = shadow.dom.dom_node(node);
if(cljs.core.truth_(temp__5753__auto__)){
var n = temp__5753__auto__;
document.body.appendChild(n);

return n;
} else {
return null;
}
} else {
return null;
}
}));

(shadow.dom.append.cljs$core$IFn$_invoke$arity$2 = (function (el,node){
if(cljs.core.truth_(node)){
var temp__5753__auto__ = shadow.dom.dom_node(node);
if(cljs.core.truth_(temp__5753__auto__)){
var n = temp__5753__auto__;
shadow.dom.dom_node(el).appendChild(n);

return n;
} else {
return null;
}
} else {
return null;
}
}));

(shadow.dom.append.cljs$lang$maxFixedArity = 2);

shadow.dom.destructure_node = (function shadow$dom$destructure_node(create_fn,p__35159){
var vec__35161 = p__35159;
var seq__35162 = cljs.core.seq(vec__35161);
var first__35163 = cljs.core.first(seq__35162);
var seq__35162__$1 = cljs.core.next(seq__35162);
var nn = first__35163;
var first__35163__$1 = cljs.core.first(seq__35162__$1);
var seq__35162__$2 = cljs.core.next(seq__35162__$1);
var np = first__35163__$1;
var nc = seq__35162__$2;
var node = vec__35161;
if((nn instanceof cljs.core.Keyword)){
} else {
throw cljs.core.ex_info.cljs$core$IFn$_invoke$arity$2("invalid dom node",new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"node","node",581201198),node], null));
}

if((((np == null)) && ((nc == null)))){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(function (){var G__35166 = nn;
var G__35167 = cljs.core.PersistentArrayMap.EMPTY;
return (create_fn.cljs$core$IFn$_invoke$arity$2 ? create_fn.cljs$core$IFn$_invoke$arity$2(G__35166,G__35167) : create_fn.call(null,G__35166,G__35167));
})(),cljs.core.List.EMPTY], null);
} else {
if(cljs.core.map_QMARK_(np)){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(create_fn.cljs$core$IFn$_invoke$arity$2 ? create_fn.cljs$core$IFn$_invoke$arity$2(nn,np) : create_fn.call(null,nn,np)),nc], null);
} else {
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(function (){var G__35169 = nn;
var G__35170 = cljs.core.PersistentArrayMap.EMPTY;
return (create_fn.cljs$core$IFn$_invoke$arity$2 ? create_fn.cljs$core$IFn$_invoke$arity$2(G__35169,G__35170) : create_fn.call(null,G__35169,G__35170));
})(),cljs.core.conj.cljs$core$IFn$_invoke$arity$2(nc,np)], null);

}
}
});
shadow.dom.make_dom_node = (function shadow$dom$make_dom_node(structure){
var vec__35174 = shadow.dom.destructure_node(shadow.dom.create_dom_node,structure);
var node = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35174,(0),null);
var node_children = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35174,(1),null);
var seq__35180_35868 = cljs.core.seq(node_children);
var chunk__35181_35869 = null;
var count__35182_35870 = (0);
var i__35183_35872 = (0);
while(true){
if((i__35183_35872 < count__35182_35870)){
var child_struct_35876 = chunk__35181_35869.cljs$core$IIndexed$_nth$arity$2(null,i__35183_35872);
var children_35878 = shadow.dom.dom_node(child_struct_35876);
if(cljs.core.seq_QMARK_(children_35878)){
var seq__35237_35879 = cljs.core.seq(cljs.core.map.cljs$core$IFn$_invoke$arity$2(shadow.dom.dom_node,children_35878));
var chunk__35239_35880 = null;
var count__35240_35881 = (0);
var i__35241_35882 = (0);
while(true){
if((i__35241_35882 < count__35240_35881)){
var child_35883 = chunk__35239_35880.cljs$core$IIndexed$_nth$arity$2(null,i__35241_35882);
if(cljs.core.truth_(child_35883)){
shadow.dom.append.cljs$core$IFn$_invoke$arity$2(node,child_35883);


var G__35884 = seq__35237_35879;
var G__35885 = chunk__35239_35880;
var G__35886 = count__35240_35881;
var G__35887 = (i__35241_35882 + (1));
seq__35237_35879 = G__35884;
chunk__35239_35880 = G__35885;
count__35240_35881 = G__35886;
i__35241_35882 = G__35887;
continue;
} else {
var G__35888 = seq__35237_35879;
var G__35889 = chunk__35239_35880;
var G__35890 = count__35240_35881;
var G__35891 = (i__35241_35882 + (1));
seq__35237_35879 = G__35888;
chunk__35239_35880 = G__35889;
count__35240_35881 = G__35890;
i__35241_35882 = G__35891;
continue;
}
} else {
var temp__5753__auto___35892 = cljs.core.seq(seq__35237_35879);
if(temp__5753__auto___35892){
var seq__35237_35893__$1 = temp__5753__auto___35892;
if(cljs.core.chunked_seq_QMARK_(seq__35237_35893__$1)){
var c__4679__auto___35894 = cljs.core.chunk_first(seq__35237_35893__$1);
var G__35895 = cljs.core.chunk_rest(seq__35237_35893__$1);
var G__35896 = c__4679__auto___35894;
var G__35897 = cljs.core.count(c__4679__auto___35894);
var G__35898 = (0);
seq__35237_35879 = G__35895;
chunk__35239_35880 = G__35896;
count__35240_35881 = G__35897;
i__35241_35882 = G__35898;
continue;
} else {
var child_35899 = cljs.core.first(seq__35237_35893__$1);
if(cljs.core.truth_(child_35899)){
shadow.dom.append.cljs$core$IFn$_invoke$arity$2(node,child_35899);


var G__35900 = cljs.core.next(seq__35237_35893__$1);
var G__35901 = null;
var G__35902 = (0);
var G__35903 = (0);
seq__35237_35879 = G__35900;
chunk__35239_35880 = G__35901;
count__35240_35881 = G__35902;
i__35241_35882 = G__35903;
continue;
} else {
var G__35904 = cljs.core.next(seq__35237_35893__$1);
var G__35905 = null;
var G__35906 = (0);
var G__35907 = (0);
seq__35237_35879 = G__35904;
chunk__35239_35880 = G__35905;
count__35240_35881 = G__35906;
i__35241_35882 = G__35907;
continue;
}
}
} else {
}
}
break;
}
} else {
shadow.dom.append.cljs$core$IFn$_invoke$arity$2(node,children_35878);
}


var G__35908 = seq__35180_35868;
var G__35909 = chunk__35181_35869;
var G__35910 = count__35182_35870;
var G__35911 = (i__35183_35872 + (1));
seq__35180_35868 = G__35908;
chunk__35181_35869 = G__35909;
count__35182_35870 = G__35910;
i__35183_35872 = G__35911;
continue;
} else {
var temp__5753__auto___35912 = cljs.core.seq(seq__35180_35868);
if(temp__5753__auto___35912){
var seq__35180_35913__$1 = temp__5753__auto___35912;
if(cljs.core.chunked_seq_QMARK_(seq__35180_35913__$1)){
var c__4679__auto___35914 = cljs.core.chunk_first(seq__35180_35913__$1);
var G__35915 = cljs.core.chunk_rest(seq__35180_35913__$1);
var G__35916 = c__4679__auto___35914;
var G__35917 = cljs.core.count(c__4679__auto___35914);
var G__35918 = (0);
seq__35180_35868 = G__35915;
chunk__35181_35869 = G__35916;
count__35182_35870 = G__35917;
i__35183_35872 = G__35918;
continue;
} else {
var child_struct_35919 = cljs.core.first(seq__35180_35913__$1);
var children_35920 = shadow.dom.dom_node(child_struct_35919);
if(cljs.core.seq_QMARK_(children_35920)){
var seq__35254_35921 = cljs.core.seq(cljs.core.map.cljs$core$IFn$_invoke$arity$2(shadow.dom.dom_node,children_35920));
var chunk__35256_35922 = null;
var count__35257_35923 = (0);
var i__35258_35924 = (0);
while(true){
if((i__35258_35924 < count__35257_35923)){
var child_35925 = chunk__35256_35922.cljs$core$IIndexed$_nth$arity$2(null,i__35258_35924);
if(cljs.core.truth_(child_35925)){
shadow.dom.append.cljs$core$IFn$_invoke$arity$2(node,child_35925);


var G__35926 = seq__35254_35921;
var G__35927 = chunk__35256_35922;
var G__35928 = count__35257_35923;
var G__35929 = (i__35258_35924 + (1));
seq__35254_35921 = G__35926;
chunk__35256_35922 = G__35927;
count__35257_35923 = G__35928;
i__35258_35924 = G__35929;
continue;
} else {
var G__35930 = seq__35254_35921;
var G__35931 = chunk__35256_35922;
var G__35932 = count__35257_35923;
var G__35933 = (i__35258_35924 + (1));
seq__35254_35921 = G__35930;
chunk__35256_35922 = G__35931;
count__35257_35923 = G__35932;
i__35258_35924 = G__35933;
continue;
}
} else {
var temp__5753__auto___35934__$1 = cljs.core.seq(seq__35254_35921);
if(temp__5753__auto___35934__$1){
var seq__35254_35935__$1 = temp__5753__auto___35934__$1;
if(cljs.core.chunked_seq_QMARK_(seq__35254_35935__$1)){
var c__4679__auto___35936 = cljs.core.chunk_first(seq__35254_35935__$1);
var G__35937 = cljs.core.chunk_rest(seq__35254_35935__$1);
var G__35938 = c__4679__auto___35936;
var G__35939 = cljs.core.count(c__4679__auto___35936);
var G__35940 = (0);
seq__35254_35921 = G__35937;
chunk__35256_35922 = G__35938;
count__35257_35923 = G__35939;
i__35258_35924 = G__35940;
continue;
} else {
var child_35941 = cljs.core.first(seq__35254_35935__$1);
if(cljs.core.truth_(child_35941)){
shadow.dom.append.cljs$core$IFn$_invoke$arity$2(node,child_35941);


var G__35942 = cljs.core.next(seq__35254_35935__$1);
var G__35943 = null;
var G__35944 = (0);
var G__35945 = (0);
seq__35254_35921 = G__35942;
chunk__35256_35922 = G__35943;
count__35257_35923 = G__35944;
i__35258_35924 = G__35945;
continue;
} else {
var G__35946 = cljs.core.next(seq__35254_35935__$1);
var G__35947 = null;
var G__35948 = (0);
var G__35949 = (0);
seq__35254_35921 = G__35946;
chunk__35256_35922 = G__35947;
count__35257_35923 = G__35948;
i__35258_35924 = G__35949;
continue;
}
}
} else {
}
}
break;
}
} else {
shadow.dom.append.cljs$core$IFn$_invoke$arity$2(node,children_35920);
}


var G__35950 = cljs.core.next(seq__35180_35913__$1);
var G__35951 = null;
var G__35952 = (0);
var G__35953 = (0);
seq__35180_35868 = G__35950;
chunk__35181_35869 = G__35951;
count__35182_35870 = G__35952;
i__35183_35872 = G__35953;
continue;
}
} else {
}
}
break;
}

return node;
});
(cljs.core.Keyword.prototype.shadow$dom$IElement$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.Keyword.prototype.shadow$dom$IElement$_to_dom$arity$1 = (function (this$){
var this$__$1 = this;
return shadow.dom.make_dom_node(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [this$__$1], null));
}));

(cljs.core.PersistentVector.prototype.shadow$dom$IElement$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.PersistentVector.prototype.shadow$dom$IElement$_to_dom$arity$1 = (function (this$){
var this$__$1 = this;
return shadow.dom.make_dom_node(this$__$1);
}));

(cljs.core.LazySeq.prototype.shadow$dom$IElement$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.LazySeq.prototype.shadow$dom$IElement$_to_dom$arity$1 = (function (this$){
var this$__$1 = this;
return cljs.core.map.cljs$core$IFn$_invoke$arity$2(shadow.dom._to_dom,this$__$1);
}));
if(cljs.core.truth_(((typeof HTMLElement) != 'undefined'))){
(HTMLElement.prototype.shadow$dom$IElement$ = cljs.core.PROTOCOL_SENTINEL);

(HTMLElement.prototype.shadow$dom$IElement$_to_dom$arity$1 = (function (this$){
var this$__$1 = this;
return this$__$1;
}));
} else {
}
if(cljs.core.truth_(((typeof DocumentFragment) != 'undefined'))){
(DocumentFragment.prototype.shadow$dom$IElement$ = cljs.core.PROTOCOL_SENTINEL);

(DocumentFragment.prototype.shadow$dom$IElement$_to_dom$arity$1 = (function (this$){
var this$__$1 = this;
return this$__$1;
}));
} else {
}
/**
 * clear node children
 */
shadow.dom.reset = (function shadow$dom$reset(node){
return goog.dom.removeChildren(shadow.dom.dom_node(node));
});
shadow.dom.remove = (function shadow$dom$remove(node){
if((((!((node == null))))?(((((node.cljs$lang$protocol_mask$partition0$ & (8388608))) || ((cljs.core.PROTOCOL_SENTINEL === node.cljs$core$ISeqable$))))?true:false):false)){
var seq__35292 = cljs.core.seq(node);
var chunk__35293 = null;
var count__35294 = (0);
var i__35295 = (0);
while(true){
if((i__35295 < count__35294)){
var n = chunk__35293.cljs$core$IIndexed$_nth$arity$2(null,i__35295);
(shadow.dom.remove.cljs$core$IFn$_invoke$arity$1 ? shadow.dom.remove.cljs$core$IFn$_invoke$arity$1(n) : shadow.dom.remove.call(null,n));


var G__35956 = seq__35292;
var G__35957 = chunk__35293;
var G__35958 = count__35294;
var G__35959 = (i__35295 + (1));
seq__35292 = G__35956;
chunk__35293 = G__35957;
count__35294 = G__35958;
i__35295 = G__35959;
continue;
} else {
var temp__5753__auto__ = cljs.core.seq(seq__35292);
if(temp__5753__auto__){
var seq__35292__$1 = temp__5753__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__35292__$1)){
var c__4679__auto__ = cljs.core.chunk_first(seq__35292__$1);
var G__35960 = cljs.core.chunk_rest(seq__35292__$1);
var G__35961 = c__4679__auto__;
var G__35962 = cljs.core.count(c__4679__auto__);
var G__35963 = (0);
seq__35292 = G__35960;
chunk__35293 = G__35961;
count__35294 = G__35962;
i__35295 = G__35963;
continue;
} else {
var n = cljs.core.first(seq__35292__$1);
(shadow.dom.remove.cljs$core$IFn$_invoke$arity$1 ? shadow.dom.remove.cljs$core$IFn$_invoke$arity$1(n) : shadow.dom.remove.call(null,n));


var G__35964 = cljs.core.next(seq__35292__$1);
var G__35965 = null;
var G__35966 = (0);
var G__35967 = (0);
seq__35292 = G__35964;
chunk__35293 = G__35965;
count__35294 = G__35966;
i__35295 = G__35967;
continue;
}
} else {
return null;
}
}
break;
}
} else {
return goog.dom.removeNode(node);
}
});
shadow.dom.replace_node = (function shadow$dom$replace_node(old,new$){
return goog.dom.replaceNode(shadow.dom.dom_node(new$),shadow.dom.dom_node(old));
});
shadow.dom.text = (function shadow$dom$text(var_args){
var G__35307 = arguments.length;
switch (G__35307) {
case 2:
return shadow.dom.text.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 1:
return shadow.dom.text.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.text.cljs$core$IFn$_invoke$arity$2 = (function (el,new_text){
return (shadow.dom.dom_node(el).innerText = new_text);
}));

(shadow.dom.text.cljs$core$IFn$_invoke$arity$1 = (function (el){
return shadow.dom.dom_node(el).innerText;
}));

(shadow.dom.text.cljs$lang$maxFixedArity = 2);

shadow.dom.check = (function shadow$dom$check(var_args){
var G__35310 = arguments.length;
switch (G__35310) {
case 1:
return shadow.dom.check.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return shadow.dom.check.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.check.cljs$core$IFn$_invoke$arity$1 = (function (el){
return shadow.dom.check.cljs$core$IFn$_invoke$arity$2(el,true);
}));

(shadow.dom.check.cljs$core$IFn$_invoke$arity$2 = (function (el,checked){
return (shadow.dom.dom_node(el).checked = checked);
}));

(shadow.dom.check.cljs$lang$maxFixedArity = 2);

shadow.dom.checked_QMARK_ = (function shadow$dom$checked_QMARK_(el){
return shadow.dom.dom_node(el).checked;
});
shadow.dom.form_elements = (function shadow$dom$form_elements(el){
return (new shadow.dom.NativeColl(shadow.dom.dom_node(el).elements));
});
shadow.dom.children = (function shadow$dom$children(el){
return (new shadow.dom.NativeColl(shadow.dom.dom_node(el).children));
});
shadow.dom.child_nodes = (function shadow$dom$child_nodes(el){
return (new shadow.dom.NativeColl(shadow.dom.dom_node(el).childNodes));
});
shadow.dom.attr = (function shadow$dom$attr(var_args){
var G__35320 = arguments.length;
switch (G__35320) {
case 2:
return shadow.dom.attr.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return shadow.dom.attr.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.attr.cljs$core$IFn$_invoke$arity$2 = (function (el,key){
return shadow.dom.dom_node(el).getAttribute(cljs.core.name(key));
}));

(shadow.dom.attr.cljs$core$IFn$_invoke$arity$3 = (function (el,key,default$){
var or__4253__auto__ = shadow.dom.dom_node(el).getAttribute(cljs.core.name(key));
if(cljs.core.truth_(or__4253__auto__)){
return or__4253__auto__;
} else {
return default$;
}
}));

(shadow.dom.attr.cljs$lang$maxFixedArity = 3);

shadow.dom.del_attr = (function shadow$dom$del_attr(el,key){
return shadow.dom.dom_node(el).removeAttribute(cljs.core.name(key));
});
shadow.dom.data = (function shadow$dom$data(el,key){
return shadow.dom.dom_node(el).getAttribute(["data-",cljs.core.name(key)].join(''));
});
shadow.dom.set_data = (function shadow$dom$set_data(el,key,value){
return shadow.dom.dom_node(el).setAttribute(["data-",cljs.core.name(key)].join(''),cljs.core.str.cljs$core$IFn$_invoke$arity$1(value));
});
shadow.dom.set_html = (function shadow$dom$set_html(node,text){
return (shadow.dom.dom_node(node).innerHTML = text);
});
shadow.dom.get_html = (function shadow$dom$get_html(node){
return shadow.dom.dom_node(node).innerHTML;
});
shadow.dom.fragment = (function shadow$dom$fragment(var_args){
var args__4870__auto__ = [];
var len__4864__auto___35972 = arguments.length;
var i__4865__auto___35974 = (0);
while(true){
if((i__4865__auto___35974 < len__4864__auto___35972)){
args__4870__auto__.push((arguments[i__4865__auto___35974]));

var G__35975 = (i__4865__auto___35974 + (1));
i__4865__auto___35974 = G__35975;
continue;
} else {
}
break;
}

var argseq__4871__auto__ = ((((0) < args__4870__auto__.length))?(new cljs.core.IndexedSeq(args__4870__auto__.slice((0)),(0),null)):null);
return shadow.dom.fragment.cljs$core$IFn$_invoke$arity$variadic(argseq__4871__auto__);
});

(shadow.dom.fragment.cljs$core$IFn$_invoke$arity$variadic = (function (nodes){
var fragment = document.createDocumentFragment();
var seq__35324_35976 = cljs.core.seq(nodes);
var chunk__35325_35977 = null;
var count__35326_35978 = (0);
var i__35327_35979 = (0);
while(true){
if((i__35327_35979 < count__35326_35978)){
var node_35980 = chunk__35325_35977.cljs$core$IIndexed$_nth$arity$2(null,i__35327_35979);
fragment.appendChild(shadow.dom._to_dom(node_35980));


var G__35981 = seq__35324_35976;
var G__35982 = chunk__35325_35977;
var G__35983 = count__35326_35978;
var G__35984 = (i__35327_35979 + (1));
seq__35324_35976 = G__35981;
chunk__35325_35977 = G__35982;
count__35326_35978 = G__35983;
i__35327_35979 = G__35984;
continue;
} else {
var temp__5753__auto___35985 = cljs.core.seq(seq__35324_35976);
if(temp__5753__auto___35985){
var seq__35324_35986__$1 = temp__5753__auto___35985;
if(cljs.core.chunked_seq_QMARK_(seq__35324_35986__$1)){
var c__4679__auto___35987 = cljs.core.chunk_first(seq__35324_35986__$1);
var G__35988 = cljs.core.chunk_rest(seq__35324_35986__$1);
var G__35989 = c__4679__auto___35987;
var G__35990 = cljs.core.count(c__4679__auto___35987);
var G__35991 = (0);
seq__35324_35976 = G__35988;
chunk__35325_35977 = G__35989;
count__35326_35978 = G__35990;
i__35327_35979 = G__35991;
continue;
} else {
var node_35992 = cljs.core.first(seq__35324_35986__$1);
fragment.appendChild(shadow.dom._to_dom(node_35992));


var G__35993 = cljs.core.next(seq__35324_35986__$1);
var G__35994 = null;
var G__35995 = (0);
var G__35996 = (0);
seq__35324_35976 = G__35993;
chunk__35325_35977 = G__35994;
count__35326_35978 = G__35995;
i__35327_35979 = G__35996;
continue;
}
} else {
}
}
break;
}

return (new shadow.dom.NativeColl(fragment));
}));

(shadow.dom.fragment.cljs$lang$maxFixedArity = (0));

/** @this {Function} */
(shadow.dom.fragment.cljs$lang$applyTo = (function (seq35323){
var self__4852__auto__ = this;
return self__4852__auto__.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq(seq35323));
}));

/**
 * given a html string, eval all <script> tags and return the html without the scripts
 * don't do this for everything, only content you trust.
 */
shadow.dom.eval_scripts = (function shadow$dom$eval_scripts(s){
var scripts = cljs.core.re_seq(/<script[^>]*?>(.+?)<\/script>/,s);
var seq__35328_35998 = cljs.core.seq(scripts);
var chunk__35329_35999 = null;
var count__35330_36000 = (0);
var i__35331_36001 = (0);
while(true){
if((i__35331_36001 < count__35330_36000)){
var vec__35338_36002 = chunk__35329_35999.cljs$core$IIndexed$_nth$arity$2(null,i__35331_36001);
var script_tag_36003 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35338_36002,(0),null);
var script_body_36004 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35338_36002,(1),null);
eval(script_body_36004);


var G__36005 = seq__35328_35998;
var G__36006 = chunk__35329_35999;
var G__36007 = count__35330_36000;
var G__36008 = (i__35331_36001 + (1));
seq__35328_35998 = G__36005;
chunk__35329_35999 = G__36006;
count__35330_36000 = G__36007;
i__35331_36001 = G__36008;
continue;
} else {
var temp__5753__auto___36009 = cljs.core.seq(seq__35328_35998);
if(temp__5753__auto___36009){
var seq__35328_36010__$1 = temp__5753__auto___36009;
if(cljs.core.chunked_seq_QMARK_(seq__35328_36010__$1)){
var c__4679__auto___36011 = cljs.core.chunk_first(seq__35328_36010__$1);
var G__36012 = cljs.core.chunk_rest(seq__35328_36010__$1);
var G__36013 = c__4679__auto___36011;
var G__36014 = cljs.core.count(c__4679__auto___36011);
var G__36015 = (0);
seq__35328_35998 = G__36012;
chunk__35329_35999 = G__36013;
count__35330_36000 = G__36014;
i__35331_36001 = G__36015;
continue;
} else {
var vec__35341_36016 = cljs.core.first(seq__35328_36010__$1);
var script_tag_36017 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35341_36016,(0),null);
var script_body_36018 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35341_36016,(1),null);
eval(script_body_36018);


var G__36020 = cljs.core.next(seq__35328_36010__$1);
var G__36021 = null;
var G__36022 = (0);
var G__36023 = (0);
seq__35328_35998 = G__36020;
chunk__35329_35999 = G__36021;
count__35330_36000 = G__36022;
i__35331_36001 = G__36023;
continue;
}
} else {
}
}
break;
}

return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3((function (s__$1,p__35344){
var vec__35345 = p__35344;
var script_tag = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35345,(0),null);
var script_body = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35345,(1),null);
return clojure.string.replace(s__$1,script_tag,"");
}),s,scripts);
});
shadow.dom.str__GT_fragment = (function shadow$dom$str__GT_fragment(s){
var el = document.createElement("div");
(el.innerHTML = s);

return (new shadow.dom.NativeColl(goog.dom.childrenToNode_(document,el)));
});
shadow.dom.node_name = (function shadow$dom$node_name(el){
return shadow.dom.dom_node(el).nodeName;
});
shadow.dom.ancestor_by_class = (function shadow$dom$ancestor_by_class(el,cls){
return goog.dom.getAncestorByClass(shadow.dom.dom_node(el),cls);
});
shadow.dom.ancestor_by_tag = (function shadow$dom$ancestor_by_tag(var_args){
var G__35353 = arguments.length;
switch (G__35353) {
case 2:
return shadow.dom.ancestor_by_tag.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return shadow.dom.ancestor_by_tag.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.ancestor_by_tag.cljs$core$IFn$_invoke$arity$2 = (function (el,tag){
return goog.dom.getAncestorByTagNameAndClass(shadow.dom.dom_node(el),cljs.core.name(tag));
}));

(shadow.dom.ancestor_by_tag.cljs$core$IFn$_invoke$arity$3 = (function (el,tag,cls){
return goog.dom.getAncestorByTagNameAndClass(shadow.dom.dom_node(el),cljs.core.name(tag),cljs.core.name(cls));
}));

(shadow.dom.ancestor_by_tag.cljs$lang$maxFixedArity = 3);

shadow.dom.get_value = (function shadow$dom$get_value(dom){
return goog.dom.forms.getValue(shadow.dom.dom_node(dom));
});
shadow.dom.set_value = (function shadow$dom$set_value(dom,value){
return goog.dom.forms.setValue(shadow.dom.dom_node(dom),value);
});
shadow.dom.px = (function shadow$dom$px(value){
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1((value | (0))),"px"].join('');
});
shadow.dom.pct = (function shadow$dom$pct(value){
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(value),"%"].join('');
});
shadow.dom.remove_style_STAR_ = (function shadow$dom$remove_style_STAR_(el,style){
return el.style.removeProperty(cljs.core.name(style));
});
shadow.dom.remove_style = (function shadow$dom$remove_style(el,style){
var el__$1 = shadow.dom.dom_node(el);
return shadow.dom.remove_style_STAR_(el__$1,style);
});
shadow.dom.remove_styles = (function shadow$dom$remove_styles(el,style_keys){
var el__$1 = shadow.dom.dom_node(el);
var seq__35357 = cljs.core.seq(style_keys);
var chunk__35358 = null;
var count__35359 = (0);
var i__35360 = (0);
while(true){
if((i__35360 < count__35359)){
var it = chunk__35358.cljs$core$IIndexed$_nth$arity$2(null,i__35360);
shadow.dom.remove_style_STAR_(el__$1,it);


var G__36032 = seq__35357;
var G__36033 = chunk__35358;
var G__36034 = count__35359;
var G__36035 = (i__35360 + (1));
seq__35357 = G__36032;
chunk__35358 = G__36033;
count__35359 = G__36034;
i__35360 = G__36035;
continue;
} else {
var temp__5753__auto__ = cljs.core.seq(seq__35357);
if(temp__5753__auto__){
var seq__35357__$1 = temp__5753__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__35357__$1)){
var c__4679__auto__ = cljs.core.chunk_first(seq__35357__$1);
var G__36036 = cljs.core.chunk_rest(seq__35357__$1);
var G__36037 = c__4679__auto__;
var G__36038 = cljs.core.count(c__4679__auto__);
var G__36039 = (0);
seq__35357 = G__36036;
chunk__35358 = G__36037;
count__35359 = G__36038;
i__35360 = G__36039;
continue;
} else {
var it = cljs.core.first(seq__35357__$1);
shadow.dom.remove_style_STAR_(el__$1,it);


var G__36040 = cljs.core.next(seq__35357__$1);
var G__36041 = null;
var G__36042 = (0);
var G__36043 = (0);
seq__35357 = G__36040;
chunk__35358 = G__36041;
count__35359 = G__36042;
i__35360 = G__36043;
continue;
}
} else {
return null;
}
}
break;
}
});

/**
* @constructor
 * @implements {cljs.core.IRecord}
 * @implements {cljs.core.IKVReduce}
 * @implements {cljs.core.IEquiv}
 * @implements {cljs.core.IHash}
 * @implements {cljs.core.ICollection}
 * @implements {cljs.core.ICounted}
 * @implements {cljs.core.ISeqable}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.ICloneable}
 * @implements {cljs.core.IPrintWithWriter}
 * @implements {cljs.core.IIterable}
 * @implements {cljs.core.IWithMeta}
 * @implements {cljs.core.IAssociative}
 * @implements {cljs.core.IMap}
 * @implements {cljs.core.ILookup}
*/
shadow.dom.Coordinate = (function (x,y,__meta,__extmap,__hash){
this.x = x;
this.y = y;
this.__meta = __meta;
this.__extmap = __extmap;
this.__hash = __hash;
this.cljs$lang$protocol_mask$partition0$ = 2230716170;
this.cljs$lang$protocol_mask$partition1$ = 139264;
});
(shadow.dom.Coordinate.prototype.cljs$core$ILookup$_lookup$arity$2 = (function (this__4502__auto__,k__4503__auto__){
var self__ = this;
var this__4502__auto____$1 = this;
return this__4502__auto____$1.cljs$core$ILookup$_lookup$arity$3(null,k__4503__auto__,null);
}));

(shadow.dom.Coordinate.prototype.cljs$core$ILookup$_lookup$arity$3 = (function (this__4504__auto__,k35363,else__4505__auto__){
var self__ = this;
var this__4504__auto____$1 = this;
var G__35370 = k35363;
var G__35370__$1 = (((G__35370 instanceof cljs.core.Keyword))?G__35370.fqn:null);
switch (G__35370__$1) {
case "x":
return self__.x;

break;
case "y":
return self__.y;

break;
default:
return cljs.core.get.cljs$core$IFn$_invoke$arity$3(self__.__extmap,k35363,else__4505__auto__);

}
}));

(shadow.dom.Coordinate.prototype.cljs$core$IKVReduce$_kv_reduce$arity$3 = (function (this__4522__auto__,f__4523__auto__,init__4524__auto__){
var self__ = this;
var this__4522__auto____$1 = this;
return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3((function (ret__4525__auto__,p__35371){
var vec__35372 = p__35371;
var k__4526__auto__ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35372,(0),null);
var v__4527__auto__ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35372,(1),null);
return (f__4523__auto__.cljs$core$IFn$_invoke$arity$3 ? f__4523__auto__.cljs$core$IFn$_invoke$arity$3(ret__4525__auto__,k__4526__auto__,v__4527__auto__) : f__4523__auto__.call(null,ret__4525__auto__,k__4526__auto__,v__4527__auto__));
}),init__4524__auto__,this__4522__auto____$1);
}));

(shadow.dom.Coordinate.prototype.cljs$core$IPrintWithWriter$_pr_writer$arity$3 = (function (this__4517__auto__,writer__4518__auto__,opts__4519__auto__){
var self__ = this;
var this__4517__auto____$1 = this;
var pr_pair__4520__auto__ = (function (keyval__4521__auto__){
return cljs.core.pr_sequential_writer(writer__4518__auto__,cljs.core.pr_writer,""," ","",opts__4519__auto__,keyval__4521__auto__);
});
return cljs.core.pr_sequential_writer(writer__4518__auto__,pr_pair__4520__auto__,"#shadow.dom.Coordinate{",", ","}",opts__4519__auto__,cljs.core.concat.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(new cljs.core.PersistentVector(null,2,(5),cljs.core.PersistentVector.EMPTY_NODE,[new cljs.core.Keyword(null,"x","x",2099068185),self__.x],null)),(new cljs.core.PersistentVector(null,2,(5),cljs.core.PersistentVector.EMPTY_NODE,[new cljs.core.Keyword(null,"y","y",-1757859776),self__.y],null))], null),self__.__extmap));
}));

(shadow.dom.Coordinate.prototype.cljs$core$IIterable$_iterator$arity$1 = (function (G__35362){
var self__ = this;
var G__35362__$1 = this;
return (new cljs.core.RecordIter((0),G__35362__$1,2,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"x","x",2099068185),new cljs.core.Keyword(null,"y","y",-1757859776)], null),(cljs.core.truth_(self__.__extmap)?cljs.core._iterator(self__.__extmap):cljs.core.nil_iter())));
}));

(shadow.dom.Coordinate.prototype.cljs$core$IMeta$_meta$arity$1 = (function (this__4500__auto__){
var self__ = this;
var this__4500__auto____$1 = this;
return self__.__meta;
}));

(shadow.dom.Coordinate.prototype.cljs$core$ICloneable$_clone$arity$1 = (function (this__4497__auto__){
var self__ = this;
var this__4497__auto____$1 = this;
return (new shadow.dom.Coordinate(self__.x,self__.y,self__.__meta,self__.__extmap,self__.__hash));
}));

(shadow.dom.Coordinate.prototype.cljs$core$ICounted$_count$arity$1 = (function (this__4506__auto__){
var self__ = this;
var this__4506__auto____$1 = this;
return (2 + cljs.core.count(self__.__extmap));
}));

(shadow.dom.Coordinate.prototype.cljs$core$IHash$_hash$arity$1 = (function (this__4498__auto__){
var self__ = this;
var this__4498__auto____$1 = this;
var h__4360__auto__ = self__.__hash;
if((!((h__4360__auto__ == null)))){
return h__4360__auto__;
} else {
var h__4360__auto____$1 = (function (coll__4499__auto__){
return (145542109 ^ cljs.core.hash_unordered_coll(coll__4499__auto__));
})(this__4498__auto____$1);
(self__.__hash = h__4360__auto____$1);

return h__4360__auto____$1;
}
}));

(shadow.dom.Coordinate.prototype.cljs$core$IEquiv$_equiv$arity$2 = (function (this35364,other35365){
var self__ = this;
var this35364__$1 = this;
return (((!((other35365 == null)))) && ((((this35364__$1.constructor === other35365.constructor)) && (((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(this35364__$1.x,other35365.x)) && (((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(this35364__$1.y,other35365.y)) && (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(this35364__$1.__extmap,other35365.__extmap)))))))));
}));

(shadow.dom.Coordinate.prototype.cljs$core$IMap$_dissoc$arity$2 = (function (this__4512__auto__,k__4513__auto__){
var self__ = this;
var this__4512__auto____$1 = this;
if(cljs.core.contains_QMARK_(new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"y","y",-1757859776),null,new cljs.core.Keyword(null,"x","x",2099068185),null], null), null),k__4513__auto__)){
return cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(cljs.core._with_meta(cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentArrayMap.EMPTY,this__4512__auto____$1),self__.__meta),k__4513__auto__);
} else {
return (new shadow.dom.Coordinate(self__.x,self__.y,self__.__meta,cljs.core.not_empty(cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(self__.__extmap,k__4513__auto__)),null));
}
}));

(shadow.dom.Coordinate.prototype.cljs$core$IAssociative$_contains_key_QMARK_$arity$2 = (function (this__4509__auto__,k35363){
var self__ = this;
var this__4509__auto____$1 = this;
var G__35375 = k35363;
var G__35375__$1 = (((G__35375 instanceof cljs.core.Keyword))?G__35375.fqn:null);
switch (G__35375__$1) {
case "x":
case "y":
return true;

break;
default:
return cljs.core.contains_QMARK_(self__.__extmap,k35363);

}
}));

(shadow.dom.Coordinate.prototype.cljs$core$IAssociative$_assoc$arity$3 = (function (this__4510__auto__,k__4511__auto__,G__35362){
var self__ = this;
var this__4510__auto____$1 = this;
var pred__35376 = cljs.core.keyword_identical_QMARK_;
var expr__35377 = k__4511__auto__;
if(cljs.core.truth_((pred__35376.cljs$core$IFn$_invoke$arity$2 ? pred__35376.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"x","x",2099068185),expr__35377) : pred__35376.call(null,new cljs.core.Keyword(null,"x","x",2099068185),expr__35377)))){
return (new shadow.dom.Coordinate(G__35362,self__.y,self__.__meta,self__.__extmap,null));
} else {
if(cljs.core.truth_((pred__35376.cljs$core$IFn$_invoke$arity$2 ? pred__35376.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"y","y",-1757859776),expr__35377) : pred__35376.call(null,new cljs.core.Keyword(null,"y","y",-1757859776),expr__35377)))){
return (new shadow.dom.Coordinate(self__.x,G__35362,self__.__meta,self__.__extmap,null));
} else {
return (new shadow.dom.Coordinate(self__.x,self__.y,self__.__meta,cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(self__.__extmap,k__4511__auto__,G__35362),null));
}
}
}));

(shadow.dom.Coordinate.prototype.cljs$core$ISeqable$_seq$arity$1 = (function (this__4515__auto__){
var self__ = this;
var this__4515__auto____$1 = this;
return cljs.core.seq(cljs.core.concat.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(new cljs.core.MapEntry(new cljs.core.Keyword(null,"x","x",2099068185),self__.x,null)),(new cljs.core.MapEntry(new cljs.core.Keyword(null,"y","y",-1757859776),self__.y,null))], null),self__.__extmap));
}));

(shadow.dom.Coordinate.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (this__4501__auto__,G__35362){
var self__ = this;
var this__4501__auto____$1 = this;
return (new shadow.dom.Coordinate(self__.x,self__.y,G__35362,self__.__extmap,self__.__hash));
}));

(shadow.dom.Coordinate.prototype.cljs$core$ICollection$_conj$arity$2 = (function (this__4507__auto__,entry__4508__auto__){
var self__ = this;
var this__4507__auto____$1 = this;
if(cljs.core.vector_QMARK_(entry__4508__auto__)){
return this__4507__auto____$1.cljs$core$IAssociative$_assoc$arity$3(null,cljs.core._nth(entry__4508__auto__,(0)),cljs.core._nth(entry__4508__auto__,(1)));
} else {
return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3(cljs.core._conj,this__4507__auto____$1,entry__4508__auto__);
}
}));

(shadow.dom.Coordinate.getBasis = (function (){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"x","x",-555367584,null),new cljs.core.Symbol(null,"y","y",-117328249,null)], null);
}));

(shadow.dom.Coordinate.cljs$lang$type = true);

(shadow.dom.Coordinate.cljs$lang$ctorPrSeq = (function (this__4546__auto__){
return (new cljs.core.List(null,"shadow.dom/Coordinate",null,(1),null));
}));

(shadow.dom.Coordinate.cljs$lang$ctorPrWriter = (function (this__4546__auto__,writer__4547__auto__){
return cljs.core._write(writer__4547__auto__,"shadow.dom/Coordinate");
}));

/**
 * Positional factory function for shadow.dom/Coordinate.
 */
shadow.dom.__GT_Coordinate = (function shadow$dom$__GT_Coordinate(x,y){
return (new shadow.dom.Coordinate(x,y,null,null,null));
});

/**
 * Factory function for shadow.dom/Coordinate, taking a map of keywords to field values.
 */
shadow.dom.map__GT_Coordinate = (function shadow$dom$map__GT_Coordinate(G__35366){
var extmap__4542__auto__ = (function (){var G__35382 = cljs.core.dissoc.cljs$core$IFn$_invoke$arity$variadic(G__35366,new cljs.core.Keyword(null,"x","x",2099068185),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"y","y",-1757859776)], 0));
if(cljs.core.record_QMARK_(G__35366)){
return cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentArrayMap.EMPTY,G__35382);
} else {
return G__35382;
}
})();
return (new shadow.dom.Coordinate(new cljs.core.Keyword(null,"x","x",2099068185).cljs$core$IFn$_invoke$arity$1(G__35366),new cljs.core.Keyword(null,"y","y",-1757859776).cljs$core$IFn$_invoke$arity$1(G__35366),null,cljs.core.not_empty(extmap__4542__auto__),null));
});

shadow.dom.get_position = (function shadow$dom$get_position(el){
var pos = goog.style.getPosition(shadow.dom.dom_node(el));
return shadow.dom.__GT_Coordinate(pos.x,pos.y);
});
shadow.dom.get_client_position = (function shadow$dom$get_client_position(el){
var pos = goog.style.getClientPosition(shadow.dom.dom_node(el));
return shadow.dom.__GT_Coordinate(pos.x,pos.y);
});
shadow.dom.get_page_offset = (function shadow$dom$get_page_offset(el){
var pos = goog.style.getPageOffset(shadow.dom.dom_node(el));
return shadow.dom.__GT_Coordinate(pos.x,pos.y);
});

/**
* @constructor
 * @implements {cljs.core.IRecord}
 * @implements {cljs.core.IKVReduce}
 * @implements {cljs.core.IEquiv}
 * @implements {cljs.core.IHash}
 * @implements {cljs.core.ICollection}
 * @implements {cljs.core.ICounted}
 * @implements {cljs.core.ISeqable}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.ICloneable}
 * @implements {cljs.core.IPrintWithWriter}
 * @implements {cljs.core.IIterable}
 * @implements {cljs.core.IWithMeta}
 * @implements {cljs.core.IAssociative}
 * @implements {cljs.core.IMap}
 * @implements {cljs.core.ILookup}
*/
shadow.dom.Size = (function (w,h,__meta,__extmap,__hash){
this.w = w;
this.h = h;
this.__meta = __meta;
this.__extmap = __extmap;
this.__hash = __hash;
this.cljs$lang$protocol_mask$partition0$ = 2230716170;
this.cljs$lang$protocol_mask$partition1$ = 139264;
});
(shadow.dom.Size.prototype.cljs$core$ILookup$_lookup$arity$2 = (function (this__4502__auto__,k__4503__auto__){
var self__ = this;
var this__4502__auto____$1 = this;
return this__4502__auto____$1.cljs$core$ILookup$_lookup$arity$3(null,k__4503__auto__,null);
}));

(shadow.dom.Size.prototype.cljs$core$ILookup$_lookup$arity$3 = (function (this__4504__auto__,k35384,else__4505__auto__){
var self__ = this;
var this__4504__auto____$1 = this;
var G__35388 = k35384;
var G__35388__$1 = (((G__35388 instanceof cljs.core.Keyword))?G__35388.fqn:null);
switch (G__35388__$1) {
case "w":
return self__.w;

break;
case "h":
return self__.h;

break;
default:
return cljs.core.get.cljs$core$IFn$_invoke$arity$3(self__.__extmap,k35384,else__4505__auto__);

}
}));

(shadow.dom.Size.prototype.cljs$core$IKVReduce$_kv_reduce$arity$3 = (function (this__4522__auto__,f__4523__auto__,init__4524__auto__){
var self__ = this;
var this__4522__auto____$1 = this;
return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3((function (ret__4525__auto__,p__35389){
var vec__35390 = p__35389;
var k__4526__auto__ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35390,(0),null);
var v__4527__auto__ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35390,(1),null);
return (f__4523__auto__.cljs$core$IFn$_invoke$arity$3 ? f__4523__auto__.cljs$core$IFn$_invoke$arity$3(ret__4525__auto__,k__4526__auto__,v__4527__auto__) : f__4523__auto__.call(null,ret__4525__auto__,k__4526__auto__,v__4527__auto__));
}),init__4524__auto__,this__4522__auto____$1);
}));

(shadow.dom.Size.prototype.cljs$core$IPrintWithWriter$_pr_writer$arity$3 = (function (this__4517__auto__,writer__4518__auto__,opts__4519__auto__){
var self__ = this;
var this__4517__auto____$1 = this;
var pr_pair__4520__auto__ = (function (keyval__4521__auto__){
return cljs.core.pr_sequential_writer(writer__4518__auto__,cljs.core.pr_writer,""," ","",opts__4519__auto__,keyval__4521__auto__);
});
return cljs.core.pr_sequential_writer(writer__4518__auto__,pr_pair__4520__auto__,"#shadow.dom.Size{",", ","}",opts__4519__auto__,cljs.core.concat.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(new cljs.core.PersistentVector(null,2,(5),cljs.core.PersistentVector.EMPTY_NODE,[new cljs.core.Keyword(null,"w","w",354169001),self__.w],null)),(new cljs.core.PersistentVector(null,2,(5),cljs.core.PersistentVector.EMPTY_NODE,[new cljs.core.Keyword(null,"h","h",1109658740),self__.h],null))], null),self__.__extmap));
}));

(shadow.dom.Size.prototype.cljs$core$IIterable$_iterator$arity$1 = (function (G__35383){
var self__ = this;
var G__35383__$1 = this;
return (new cljs.core.RecordIter((0),G__35383__$1,2,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"w","w",354169001),new cljs.core.Keyword(null,"h","h",1109658740)], null),(cljs.core.truth_(self__.__extmap)?cljs.core._iterator(self__.__extmap):cljs.core.nil_iter())));
}));

(shadow.dom.Size.prototype.cljs$core$IMeta$_meta$arity$1 = (function (this__4500__auto__){
var self__ = this;
var this__4500__auto____$1 = this;
return self__.__meta;
}));

(shadow.dom.Size.prototype.cljs$core$ICloneable$_clone$arity$1 = (function (this__4497__auto__){
var self__ = this;
var this__4497__auto____$1 = this;
return (new shadow.dom.Size(self__.w,self__.h,self__.__meta,self__.__extmap,self__.__hash));
}));

(shadow.dom.Size.prototype.cljs$core$ICounted$_count$arity$1 = (function (this__4506__auto__){
var self__ = this;
var this__4506__auto____$1 = this;
return (2 + cljs.core.count(self__.__extmap));
}));

(shadow.dom.Size.prototype.cljs$core$IHash$_hash$arity$1 = (function (this__4498__auto__){
var self__ = this;
var this__4498__auto____$1 = this;
var h__4360__auto__ = self__.__hash;
if((!((h__4360__auto__ == null)))){
return h__4360__auto__;
} else {
var h__4360__auto____$1 = (function (coll__4499__auto__){
return (-1228019642 ^ cljs.core.hash_unordered_coll(coll__4499__auto__));
})(this__4498__auto____$1);
(self__.__hash = h__4360__auto____$1);

return h__4360__auto____$1;
}
}));

(shadow.dom.Size.prototype.cljs$core$IEquiv$_equiv$arity$2 = (function (this35385,other35386){
var self__ = this;
var this35385__$1 = this;
return (((!((other35386 == null)))) && ((((this35385__$1.constructor === other35386.constructor)) && (((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(this35385__$1.w,other35386.w)) && (((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(this35385__$1.h,other35386.h)) && (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(this35385__$1.__extmap,other35386.__extmap)))))))));
}));

(shadow.dom.Size.prototype.cljs$core$IMap$_dissoc$arity$2 = (function (this__4512__auto__,k__4513__auto__){
var self__ = this;
var this__4512__auto____$1 = this;
if(cljs.core.contains_QMARK_(new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"w","w",354169001),null,new cljs.core.Keyword(null,"h","h",1109658740),null], null), null),k__4513__auto__)){
return cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(cljs.core._with_meta(cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentArrayMap.EMPTY,this__4512__auto____$1),self__.__meta),k__4513__auto__);
} else {
return (new shadow.dom.Size(self__.w,self__.h,self__.__meta,cljs.core.not_empty(cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(self__.__extmap,k__4513__auto__)),null));
}
}));

(shadow.dom.Size.prototype.cljs$core$IAssociative$_contains_key_QMARK_$arity$2 = (function (this__4509__auto__,k35384){
var self__ = this;
var this__4509__auto____$1 = this;
var G__35401 = k35384;
var G__35401__$1 = (((G__35401 instanceof cljs.core.Keyword))?G__35401.fqn:null);
switch (G__35401__$1) {
case "w":
case "h":
return true;

break;
default:
return cljs.core.contains_QMARK_(self__.__extmap,k35384);

}
}));

(shadow.dom.Size.prototype.cljs$core$IAssociative$_assoc$arity$3 = (function (this__4510__auto__,k__4511__auto__,G__35383){
var self__ = this;
var this__4510__auto____$1 = this;
var pred__35402 = cljs.core.keyword_identical_QMARK_;
var expr__35403 = k__4511__auto__;
if(cljs.core.truth_((pred__35402.cljs$core$IFn$_invoke$arity$2 ? pred__35402.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"w","w",354169001),expr__35403) : pred__35402.call(null,new cljs.core.Keyword(null,"w","w",354169001),expr__35403)))){
return (new shadow.dom.Size(G__35383,self__.h,self__.__meta,self__.__extmap,null));
} else {
if(cljs.core.truth_((pred__35402.cljs$core$IFn$_invoke$arity$2 ? pred__35402.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"h","h",1109658740),expr__35403) : pred__35402.call(null,new cljs.core.Keyword(null,"h","h",1109658740),expr__35403)))){
return (new shadow.dom.Size(self__.w,G__35383,self__.__meta,self__.__extmap,null));
} else {
return (new shadow.dom.Size(self__.w,self__.h,self__.__meta,cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(self__.__extmap,k__4511__auto__,G__35383),null));
}
}
}));

(shadow.dom.Size.prototype.cljs$core$ISeqable$_seq$arity$1 = (function (this__4515__auto__){
var self__ = this;
var this__4515__auto____$1 = this;
return cljs.core.seq(cljs.core.concat.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(new cljs.core.MapEntry(new cljs.core.Keyword(null,"w","w",354169001),self__.w,null)),(new cljs.core.MapEntry(new cljs.core.Keyword(null,"h","h",1109658740),self__.h,null))], null),self__.__extmap));
}));

(shadow.dom.Size.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (this__4501__auto__,G__35383){
var self__ = this;
var this__4501__auto____$1 = this;
return (new shadow.dom.Size(self__.w,self__.h,G__35383,self__.__extmap,self__.__hash));
}));

(shadow.dom.Size.prototype.cljs$core$ICollection$_conj$arity$2 = (function (this__4507__auto__,entry__4508__auto__){
var self__ = this;
var this__4507__auto____$1 = this;
if(cljs.core.vector_QMARK_(entry__4508__auto__)){
return this__4507__auto____$1.cljs$core$IAssociative$_assoc$arity$3(null,cljs.core._nth(entry__4508__auto__,(0)),cljs.core._nth(entry__4508__auto__,(1)));
} else {
return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3(cljs.core._conj,this__4507__auto____$1,entry__4508__auto__);
}
}));

(shadow.dom.Size.getBasis = (function (){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"w","w",1994700528,null),new cljs.core.Symbol(null,"h","h",-1544777029,null)], null);
}));

(shadow.dom.Size.cljs$lang$type = true);

(shadow.dom.Size.cljs$lang$ctorPrSeq = (function (this__4546__auto__){
return (new cljs.core.List(null,"shadow.dom/Size",null,(1),null));
}));

(shadow.dom.Size.cljs$lang$ctorPrWriter = (function (this__4546__auto__,writer__4547__auto__){
return cljs.core._write(writer__4547__auto__,"shadow.dom/Size");
}));

/**
 * Positional factory function for shadow.dom/Size.
 */
shadow.dom.__GT_Size = (function shadow$dom$__GT_Size(w,h){
return (new shadow.dom.Size(w,h,null,null,null));
});

/**
 * Factory function for shadow.dom/Size, taking a map of keywords to field values.
 */
shadow.dom.map__GT_Size = (function shadow$dom$map__GT_Size(G__35387){
var extmap__4542__auto__ = (function (){var G__35412 = cljs.core.dissoc.cljs$core$IFn$_invoke$arity$variadic(G__35387,new cljs.core.Keyword(null,"w","w",354169001),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"h","h",1109658740)], 0));
if(cljs.core.record_QMARK_(G__35387)){
return cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentArrayMap.EMPTY,G__35412);
} else {
return G__35412;
}
})();
return (new shadow.dom.Size(new cljs.core.Keyword(null,"w","w",354169001).cljs$core$IFn$_invoke$arity$1(G__35387),new cljs.core.Keyword(null,"h","h",1109658740).cljs$core$IFn$_invoke$arity$1(G__35387),null,cljs.core.not_empty(extmap__4542__auto__),null));
});

shadow.dom.size__GT_clj = (function shadow$dom$size__GT_clj(size){
return (new shadow.dom.Size(size.width,size.height,null,null,null));
});
shadow.dom.get_size = (function shadow$dom$get_size(el){
return shadow.dom.size__GT_clj(goog.style.getSize(shadow.dom.dom_node(el)));
});
shadow.dom.get_height = (function shadow$dom$get_height(el){
return shadow.dom.get_size(el).h;
});
shadow.dom.get_viewport_size = (function shadow$dom$get_viewport_size(){
return shadow.dom.size__GT_clj(goog.dom.getViewportSize());
});
shadow.dom.first_child = (function shadow$dom$first_child(el){
return (shadow.dom.dom_node(el).children[(0)]);
});
shadow.dom.select_option_values = (function shadow$dom$select_option_values(el){
var native$ = shadow.dom.dom_node(el);
var opts = (native$["options"]);
var a__4738__auto__ = opts;
var l__4739__auto__ = a__4738__auto__.length;
var i = (0);
var ret = cljs.core.PersistentVector.EMPTY;
while(true){
if((i < l__4739__auto__)){
var G__36087 = (i + (1));
var G__36088 = cljs.core.conj.cljs$core$IFn$_invoke$arity$2(ret,(opts[i]["value"]));
i = G__36087;
ret = G__36088;
continue;
} else {
return ret;
}
break;
}
});
shadow.dom.build_url = (function shadow$dom$build_url(path,query_params){
if(cljs.core.empty_QMARK_(query_params)){
return path;
} else {
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(path),"?",clojure.string.join.cljs$core$IFn$_invoke$arity$2("&",cljs.core.map.cljs$core$IFn$_invoke$arity$2((function (p__35446){
var vec__35447 = p__35446;
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35447,(0),null);
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35447,(1),null);
return [cljs.core.name(k),"=",cljs.core.str.cljs$core$IFn$_invoke$arity$1(encodeURIComponent(cljs.core.str.cljs$core$IFn$_invoke$arity$1(v)))].join('');
}),query_params))].join('');
}
});
shadow.dom.redirect = (function shadow$dom$redirect(var_args){
var G__35451 = arguments.length;
switch (G__35451) {
case 1:
return shadow.dom.redirect.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return shadow.dom.redirect.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.redirect.cljs$core$IFn$_invoke$arity$1 = (function (path){
return shadow.dom.redirect.cljs$core$IFn$_invoke$arity$2(path,cljs.core.PersistentArrayMap.EMPTY);
}));

(shadow.dom.redirect.cljs$core$IFn$_invoke$arity$2 = (function (path,query_params){
return (document["location"]["href"] = shadow.dom.build_url(path,query_params));
}));

(shadow.dom.redirect.cljs$lang$maxFixedArity = 2);

shadow.dom.reload_BANG_ = (function shadow$dom$reload_BANG_(){
return (document.location.href = document.location.href);
});
shadow.dom.tag_name = (function shadow$dom$tag_name(el){
var dom = shadow.dom.dom_node(el);
return dom.tagName;
});
shadow.dom.insert_after = (function shadow$dom$insert_after(ref,new$){
var new_node = shadow.dom.dom_node(new$);
goog.dom.insertSiblingAfter(new_node,shadow.dom.dom_node(ref));

return new_node;
});
shadow.dom.insert_before = (function shadow$dom$insert_before(ref,new$){
var new_node = shadow.dom.dom_node(new$);
goog.dom.insertSiblingBefore(new_node,shadow.dom.dom_node(ref));

return new_node;
});
shadow.dom.insert_first = (function shadow$dom$insert_first(ref,new$){
var temp__5751__auto__ = shadow.dom.dom_node(ref).firstChild;
if(cljs.core.truth_(temp__5751__auto__)){
var child = temp__5751__auto__;
return shadow.dom.insert_before(child,new$);
} else {
return shadow.dom.append.cljs$core$IFn$_invoke$arity$2(ref,new$);
}
});
shadow.dom.index_of = (function shadow$dom$index_of(el){
var el__$1 = shadow.dom.dom_node(el);
var i = (0);
while(true){
var ps = el__$1.previousSibling;
if((ps == null)){
return i;
} else {
var G__36092 = ps;
var G__36093 = (i + (1));
el__$1 = G__36092;
i = G__36093;
continue;
}
break;
}
});
shadow.dom.get_parent = (function shadow$dom$get_parent(el){
return goog.dom.getParentElement(shadow.dom.dom_node(el));
});
shadow.dom.parents = (function shadow$dom$parents(el){
var parent = shadow.dom.get_parent(el);
if(cljs.core.truth_(parent)){
return cljs.core.cons(parent,(new cljs.core.LazySeq(null,(function (){
return (shadow.dom.parents.cljs$core$IFn$_invoke$arity$1 ? shadow.dom.parents.cljs$core$IFn$_invoke$arity$1(parent) : shadow.dom.parents.call(null,parent));
}),null,null)));
} else {
return null;
}
});
shadow.dom.matches = (function shadow$dom$matches(el,sel){
return shadow.dom.dom_node(el).matches(sel);
});
shadow.dom.get_next_sibling = (function shadow$dom$get_next_sibling(el){
return goog.dom.getNextElementSibling(shadow.dom.dom_node(el));
});
shadow.dom.get_previous_sibling = (function shadow$dom$get_previous_sibling(el){
return goog.dom.getPreviousElementSibling(shadow.dom.dom_node(el));
});
shadow.dom.xmlns = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(new cljs.core.PersistentArrayMap(null, 2, ["svg","http://www.w3.org/2000/svg","xlink","http://www.w3.org/1999/xlink"], null));
shadow.dom.create_svg_node = (function shadow$dom$create_svg_node(tag_def,props){
var vec__35463 = shadow.dom.parse_tag(tag_def);
var tag_name = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35463,(0),null);
var tag_id = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35463,(1),null);
var tag_classes = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35463,(2),null);
var el = document.createElementNS("http://www.w3.org/2000/svg",tag_name);
if(cljs.core.truth_(tag_id)){
el.setAttribute("id",tag_id);
} else {
}

if(cljs.core.truth_(tag_classes)){
el.setAttribute("class",shadow.dom.merge_class_string(new cljs.core.Keyword(null,"class","class",-2030961996).cljs$core$IFn$_invoke$arity$1(props),tag_classes));
} else {
}

var seq__35467_36094 = cljs.core.seq(props);
var chunk__35468_36095 = null;
var count__35469_36096 = (0);
var i__35470_36097 = (0);
while(true){
if((i__35470_36097 < count__35469_36096)){
var vec__35487_36098 = chunk__35468_36095.cljs$core$IIndexed$_nth$arity$2(null,i__35470_36097);
var k_36099 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35487_36098,(0),null);
var v_36100 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35487_36098,(1),null);
el.setAttributeNS((function (){var temp__5753__auto__ = cljs.core.namespace(k_36099);
if(cljs.core.truth_(temp__5753__auto__)){
var ns = temp__5753__auto__;
return cljs.core.get.cljs$core$IFn$_invoke$arity$2(cljs.core.deref(shadow.dom.xmlns),ns);
} else {
return null;
}
})(),cljs.core.name(k_36099),v_36100);


var G__36101 = seq__35467_36094;
var G__36102 = chunk__35468_36095;
var G__36103 = count__35469_36096;
var G__36104 = (i__35470_36097 + (1));
seq__35467_36094 = G__36101;
chunk__35468_36095 = G__36102;
count__35469_36096 = G__36103;
i__35470_36097 = G__36104;
continue;
} else {
var temp__5753__auto___36105 = cljs.core.seq(seq__35467_36094);
if(temp__5753__auto___36105){
var seq__35467_36106__$1 = temp__5753__auto___36105;
if(cljs.core.chunked_seq_QMARK_(seq__35467_36106__$1)){
var c__4679__auto___36107 = cljs.core.chunk_first(seq__35467_36106__$1);
var G__36108 = cljs.core.chunk_rest(seq__35467_36106__$1);
var G__36109 = c__4679__auto___36107;
var G__36110 = cljs.core.count(c__4679__auto___36107);
var G__36111 = (0);
seq__35467_36094 = G__36108;
chunk__35468_36095 = G__36109;
count__35469_36096 = G__36110;
i__35470_36097 = G__36111;
continue;
} else {
var vec__35490_36112 = cljs.core.first(seq__35467_36106__$1);
var k_36113 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35490_36112,(0),null);
var v_36114 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35490_36112,(1),null);
el.setAttributeNS((function (){var temp__5753__auto____$1 = cljs.core.namespace(k_36113);
if(cljs.core.truth_(temp__5753__auto____$1)){
var ns = temp__5753__auto____$1;
return cljs.core.get.cljs$core$IFn$_invoke$arity$2(cljs.core.deref(shadow.dom.xmlns),ns);
} else {
return null;
}
})(),cljs.core.name(k_36113),v_36114);


var G__36115 = cljs.core.next(seq__35467_36106__$1);
var G__36116 = null;
var G__36117 = (0);
var G__36118 = (0);
seq__35467_36094 = G__36115;
chunk__35468_36095 = G__36116;
count__35469_36096 = G__36117;
i__35470_36097 = G__36118;
continue;
}
} else {
}
}
break;
}

return el;
});
shadow.dom.svg_node = (function shadow$dom$svg_node(el){
if((el == null)){
return null;
} else {
if((((!((el == null))))?((((false) || ((cljs.core.PROTOCOL_SENTINEL === el.shadow$dom$SVGElement$))))?true:false):false)){
return el.shadow$dom$SVGElement$_to_svg$arity$1(null);
} else {
return el;

}
}
});
shadow.dom.make_svg_node = (function shadow$dom$make_svg_node(structure){
var vec__35523 = shadow.dom.destructure_node(shadow.dom.create_svg_node,structure);
var node = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35523,(0),null);
var node_children = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35523,(1),null);
var seq__35532_36121 = cljs.core.seq(node_children);
var chunk__35534_36122 = null;
var count__35535_36123 = (0);
var i__35536_36124 = (0);
while(true){
if((i__35536_36124 < count__35535_36123)){
var child_struct_36128 = chunk__35534_36122.cljs$core$IIndexed$_nth$arity$2(null,i__35536_36124);
if((!((child_struct_36128 == null)))){
if(typeof child_struct_36128 === 'string'){
var text_36129 = (node["textContent"]);
(node["textContent"] = [cljs.core.str.cljs$core$IFn$_invoke$arity$1(text_36129),child_struct_36128].join(''));
} else {
var children_36130 = shadow.dom.svg_node(child_struct_36128);
if(cljs.core.seq_QMARK_(children_36130)){
var seq__35657_36131 = cljs.core.seq(children_36130);
var chunk__35659_36132 = null;
var count__35660_36133 = (0);
var i__35661_36134 = (0);
while(true){
if((i__35661_36134 < count__35660_36133)){
var child_36135 = chunk__35659_36132.cljs$core$IIndexed$_nth$arity$2(null,i__35661_36134);
if(cljs.core.truth_(child_36135)){
node.appendChild(child_36135);


var G__36136 = seq__35657_36131;
var G__36137 = chunk__35659_36132;
var G__36138 = count__35660_36133;
var G__36139 = (i__35661_36134 + (1));
seq__35657_36131 = G__36136;
chunk__35659_36132 = G__36137;
count__35660_36133 = G__36138;
i__35661_36134 = G__36139;
continue;
} else {
var G__36140 = seq__35657_36131;
var G__36141 = chunk__35659_36132;
var G__36142 = count__35660_36133;
var G__36143 = (i__35661_36134 + (1));
seq__35657_36131 = G__36140;
chunk__35659_36132 = G__36141;
count__35660_36133 = G__36142;
i__35661_36134 = G__36143;
continue;
}
} else {
var temp__5753__auto___36144 = cljs.core.seq(seq__35657_36131);
if(temp__5753__auto___36144){
var seq__35657_36145__$1 = temp__5753__auto___36144;
if(cljs.core.chunked_seq_QMARK_(seq__35657_36145__$1)){
var c__4679__auto___36146 = cljs.core.chunk_first(seq__35657_36145__$1);
var G__36147 = cljs.core.chunk_rest(seq__35657_36145__$1);
var G__36148 = c__4679__auto___36146;
var G__36149 = cljs.core.count(c__4679__auto___36146);
var G__36150 = (0);
seq__35657_36131 = G__36147;
chunk__35659_36132 = G__36148;
count__35660_36133 = G__36149;
i__35661_36134 = G__36150;
continue;
} else {
var child_36152 = cljs.core.first(seq__35657_36145__$1);
if(cljs.core.truth_(child_36152)){
node.appendChild(child_36152);


var G__36153 = cljs.core.next(seq__35657_36145__$1);
var G__36154 = null;
var G__36155 = (0);
var G__36156 = (0);
seq__35657_36131 = G__36153;
chunk__35659_36132 = G__36154;
count__35660_36133 = G__36155;
i__35661_36134 = G__36156;
continue;
} else {
var G__36157 = cljs.core.next(seq__35657_36145__$1);
var G__36158 = null;
var G__36159 = (0);
var G__36160 = (0);
seq__35657_36131 = G__36157;
chunk__35659_36132 = G__36158;
count__35660_36133 = G__36159;
i__35661_36134 = G__36160;
continue;
}
}
} else {
}
}
break;
}
} else {
node.appendChild(children_36130);
}
}


var G__36162 = seq__35532_36121;
var G__36163 = chunk__35534_36122;
var G__36164 = count__35535_36123;
var G__36165 = (i__35536_36124 + (1));
seq__35532_36121 = G__36162;
chunk__35534_36122 = G__36163;
count__35535_36123 = G__36164;
i__35536_36124 = G__36165;
continue;
} else {
var G__36166 = seq__35532_36121;
var G__36167 = chunk__35534_36122;
var G__36168 = count__35535_36123;
var G__36169 = (i__35536_36124 + (1));
seq__35532_36121 = G__36166;
chunk__35534_36122 = G__36167;
count__35535_36123 = G__36168;
i__35536_36124 = G__36169;
continue;
}
} else {
var temp__5753__auto___36170 = cljs.core.seq(seq__35532_36121);
if(temp__5753__auto___36170){
var seq__35532_36171__$1 = temp__5753__auto___36170;
if(cljs.core.chunked_seq_QMARK_(seq__35532_36171__$1)){
var c__4679__auto___36172 = cljs.core.chunk_first(seq__35532_36171__$1);
var G__36173 = cljs.core.chunk_rest(seq__35532_36171__$1);
var G__36174 = c__4679__auto___36172;
var G__36175 = cljs.core.count(c__4679__auto___36172);
var G__36176 = (0);
seq__35532_36121 = G__36173;
chunk__35534_36122 = G__36174;
count__35535_36123 = G__36175;
i__35536_36124 = G__36176;
continue;
} else {
var child_struct_36177 = cljs.core.first(seq__35532_36171__$1);
if((!((child_struct_36177 == null)))){
if(typeof child_struct_36177 === 'string'){
var text_36178 = (node["textContent"]);
(node["textContent"] = [cljs.core.str.cljs$core$IFn$_invoke$arity$1(text_36178),child_struct_36177].join(''));
} else {
var children_36179 = shadow.dom.svg_node(child_struct_36177);
if(cljs.core.seq_QMARK_(children_36179)){
var seq__35677_36180 = cljs.core.seq(children_36179);
var chunk__35683_36181 = null;
var count__35684_36182 = (0);
var i__35685_36183 = (0);
while(true){
if((i__35685_36183 < count__35684_36182)){
var child_36184 = chunk__35683_36181.cljs$core$IIndexed$_nth$arity$2(null,i__35685_36183);
if(cljs.core.truth_(child_36184)){
node.appendChild(child_36184);


var G__36189 = seq__35677_36180;
var G__36190 = chunk__35683_36181;
var G__36191 = count__35684_36182;
var G__36192 = (i__35685_36183 + (1));
seq__35677_36180 = G__36189;
chunk__35683_36181 = G__36190;
count__35684_36182 = G__36191;
i__35685_36183 = G__36192;
continue;
} else {
var G__36193 = seq__35677_36180;
var G__36194 = chunk__35683_36181;
var G__36195 = count__35684_36182;
var G__36196 = (i__35685_36183 + (1));
seq__35677_36180 = G__36193;
chunk__35683_36181 = G__36194;
count__35684_36182 = G__36195;
i__35685_36183 = G__36196;
continue;
}
} else {
var temp__5753__auto___36197__$1 = cljs.core.seq(seq__35677_36180);
if(temp__5753__auto___36197__$1){
var seq__35677_36198__$1 = temp__5753__auto___36197__$1;
if(cljs.core.chunked_seq_QMARK_(seq__35677_36198__$1)){
var c__4679__auto___36199 = cljs.core.chunk_first(seq__35677_36198__$1);
var G__36200 = cljs.core.chunk_rest(seq__35677_36198__$1);
var G__36201 = c__4679__auto___36199;
var G__36202 = cljs.core.count(c__4679__auto___36199);
var G__36203 = (0);
seq__35677_36180 = G__36200;
chunk__35683_36181 = G__36201;
count__35684_36182 = G__36202;
i__35685_36183 = G__36203;
continue;
} else {
var child_36204 = cljs.core.first(seq__35677_36198__$1);
if(cljs.core.truth_(child_36204)){
node.appendChild(child_36204);


var G__36205 = cljs.core.next(seq__35677_36198__$1);
var G__36206 = null;
var G__36207 = (0);
var G__36208 = (0);
seq__35677_36180 = G__36205;
chunk__35683_36181 = G__36206;
count__35684_36182 = G__36207;
i__35685_36183 = G__36208;
continue;
} else {
var G__36209 = cljs.core.next(seq__35677_36198__$1);
var G__36210 = null;
var G__36211 = (0);
var G__36212 = (0);
seq__35677_36180 = G__36209;
chunk__35683_36181 = G__36210;
count__35684_36182 = G__36211;
i__35685_36183 = G__36212;
continue;
}
}
} else {
}
}
break;
}
} else {
node.appendChild(children_36179);
}
}


var G__36214 = cljs.core.next(seq__35532_36171__$1);
var G__36215 = null;
var G__36216 = (0);
var G__36217 = (0);
seq__35532_36121 = G__36214;
chunk__35534_36122 = G__36215;
count__35535_36123 = G__36216;
i__35536_36124 = G__36217;
continue;
} else {
var G__36221 = cljs.core.next(seq__35532_36171__$1);
var G__36222 = null;
var G__36223 = (0);
var G__36224 = (0);
seq__35532_36121 = G__36221;
chunk__35534_36122 = G__36222;
count__35535_36123 = G__36223;
i__35536_36124 = G__36224;
continue;
}
}
} else {
}
}
break;
}

return node;
});
(shadow.dom.SVGElement["string"] = true);

(shadow.dom._to_svg["string"] = (function (this$){
if((this$ instanceof cljs.core.Keyword)){
return shadow.dom.make_svg_node(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [this$], null));
} else {
throw cljs.core.ex_info.cljs$core$IFn$_invoke$arity$2("strings cannot be in svgs",new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"this","this",-611633625),this$], null));
}
}));

(cljs.core.PersistentVector.prototype.shadow$dom$SVGElement$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.PersistentVector.prototype.shadow$dom$SVGElement$_to_svg$arity$1 = (function (this$){
var this$__$1 = this;
return shadow.dom.make_svg_node(this$__$1);
}));

(cljs.core.LazySeq.prototype.shadow$dom$SVGElement$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.LazySeq.prototype.shadow$dom$SVGElement$_to_svg$arity$1 = (function (this$){
var this$__$1 = this;
return cljs.core.map.cljs$core$IFn$_invoke$arity$2(shadow.dom._to_svg,this$__$1);
}));

(shadow.dom.SVGElement["null"] = true);

(shadow.dom._to_svg["null"] = (function (_){
return null;
}));
shadow.dom.svg = (function shadow$dom$svg(var_args){
var args__4870__auto__ = [];
var len__4864__auto___36225 = arguments.length;
var i__4865__auto___36226 = (0);
while(true){
if((i__4865__auto___36226 < len__4864__auto___36225)){
args__4870__auto__.push((arguments[i__4865__auto___36226]));

var G__36227 = (i__4865__auto___36226 + (1));
i__4865__auto___36226 = G__36227;
continue;
} else {
}
break;
}

var argseq__4871__auto__ = ((((1) < args__4870__auto__.length))?(new cljs.core.IndexedSeq(args__4870__auto__.slice((1)),(0),null)):null);
return shadow.dom.svg.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__4871__auto__);
});

(shadow.dom.svg.cljs$core$IFn$_invoke$arity$variadic = (function (attrs,children){
return shadow.dom._to_svg(cljs.core.vec(cljs.core.concat.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"svg","svg",856789142),attrs], null),children)));
}));

(shadow.dom.svg.cljs$lang$maxFixedArity = (1));

/** @this {Function} */
(shadow.dom.svg.cljs$lang$applyTo = (function (seq35754){
var G__35755 = cljs.core.first(seq35754);
var seq35754__$1 = cljs.core.next(seq35754);
var self__4851__auto__ = this;
return self__4851__auto__.cljs$core$IFn$_invoke$arity$variadic(G__35755,seq35754__$1);
}));

/**
 * returns a channel for events on el
 * transform-fn should be a (fn [e el] some-val) where some-val will be put on the chan
 * once-or-cleanup handles the removal of the event handler
 * - true: remove after one event
 * - false: never removed
 * - chan: remove on msg/close
 */
shadow.dom.event_chan = (function shadow$dom$event_chan(var_args){
var G__35768 = arguments.length;
switch (G__35768) {
case 2:
return shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
case 4:
return shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$2 = (function (el,event){
return shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$4(el,event,null,false);
}));

(shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$3 = (function (el,event,xf){
return shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$4(el,event,xf,false);
}));

(shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$4 = (function (el,event,xf,once_or_cleanup){
var buf = cljs.core.async.sliding_buffer((1));
var chan = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$2(buf,xf);
var event_fn = (function shadow$dom$event_fn(e){
cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2(chan,e);

if(once_or_cleanup === true){
shadow.dom.remove_event_handler(el,event,shadow$dom$event_fn);

return cljs.core.async.close_BANG_(chan);
} else {
return null;
}
});
shadow.dom.dom_listen(shadow.dom.dom_node(el),cljs.core.name(event),event_fn);

if(cljs.core.truth_((function (){var and__4251__auto__ = once_or_cleanup;
if(cljs.core.truth_(and__4251__auto__)){
return (!(once_or_cleanup === true));
} else {
return and__4251__auto__;
}
})())){
var c__32010__auto___36229 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__32011__auto__ = (function (){var switch__31873__auto__ = (function (state_35781){
var state_val_35782 = (state_35781[(1)]);
if((state_val_35782 === (1))){
var state_35781__$1 = state_35781;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_35781__$1,(2),once_or_cleanup);
} else {
if((state_val_35782 === (2))){
var inst_35778 = (state_35781[(2)]);
var inst_35779 = shadow.dom.remove_event_handler(el,event,event_fn);
var state_35781__$1 = (function (){var statearr_35784 = state_35781;
(statearr_35784[(7)] = inst_35778);

return statearr_35784;
})();
return cljs.core.async.impl.ioc_helpers.return_chan(state_35781__$1,inst_35779);
} else {
return null;
}
}
});
return (function() {
var shadow$dom$state_machine__31874__auto__ = null;
var shadow$dom$state_machine__31874__auto____0 = (function (){
var statearr_35787 = [null,null,null,null,null,null,null,null];
(statearr_35787[(0)] = shadow$dom$state_machine__31874__auto__);

(statearr_35787[(1)] = (1));

return statearr_35787;
});
var shadow$dom$state_machine__31874__auto____1 = (function (state_35781){
while(true){
var ret_value__31875__auto__ = (function (){try{while(true){
var result__31876__auto__ = switch__31873__auto__(state_35781);
if(cljs.core.keyword_identical_QMARK_(result__31876__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__31876__auto__;
}
break;
}
}catch (e35788){var ex__31877__auto__ = e35788;
var statearr_35789_36234 = state_35781;
(statearr_35789_36234[(2)] = ex__31877__auto__);


if(cljs.core.seq((state_35781[(4)]))){
var statearr_35790_36235 = state_35781;
(statearr_35790_36235[(1)] = cljs.core.first((state_35781[(4)])));

} else {
throw ex__31877__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__31875__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__36236 = state_35781;
state_35781 = G__36236;
continue;
} else {
return ret_value__31875__auto__;
}
break;
}
});
shadow$dom$state_machine__31874__auto__ = function(state_35781){
switch(arguments.length){
case 0:
return shadow$dom$state_machine__31874__auto____0.call(this);
case 1:
return shadow$dom$state_machine__31874__auto____1.call(this,state_35781);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
shadow$dom$state_machine__31874__auto__.cljs$core$IFn$_invoke$arity$0 = shadow$dom$state_machine__31874__auto____0;
shadow$dom$state_machine__31874__auto__.cljs$core$IFn$_invoke$arity$1 = shadow$dom$state_machine__31874__auto____1;
return shadow$dom$state_machine__31874__auto__;
})()
})();
var state__32012__auto__ = (function (){var statearr_35791 = f__32011__auto__();
(statearr_35791[(6)] = c__32010__auto___36229);

return statearr_35791;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__32012__auto__);
}));

} else {
}

return chan;
}));

(shadow.dom.event_chan.cljs$lang$maxFixedArity = 4);


//# sourceMappingURL=shadow.dom.js.map
