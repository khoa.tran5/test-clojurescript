goog.provide('cljs.core.async');
goog.scope(function(){
  cljs.core.async.goog$module$goog$array = goog.module.get('goog.array');
});
cljs.core.async.fn_handler = (function cljs$core$async$fn_handler(var_args){
var G__32093 = arguments.length;
switch (G__32093) {
case 1:
return cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$1 = (function (f){
return cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$2(f,true);
}));

(cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$2 = (function (f,blockable){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async32094 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Handler}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async32094 = (function (f,blockable,meta32095){
this.f = f;
this.blockable = blockable;
this.meta32095 = meta32095;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(cljs.core.async.t_cljs$core$async32094.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_32096,meta32095__$1){
var self__ = this;
var _32096__$1 = this;
return (new cljs.core.async.t_cljs$core$async32094(self__.f,self__.blockable,meta32095__$1));
}));

(cljs.core.async.t_cljs$core$async32094.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_32096){
var self__ = this;
var _32096__$1 = this;
return self__.meta32095;
}));

(cljs.core.async.t_cljs$core$async32094.prototype.cljs$core$async$impl$protocols$Handler$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async32094.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return true;
}));

(cljs.core.async.t_cljs$core$async32094.prototype.cljs$core$async$impl$protocols$Handler$blockable_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return self__.blockable;
}));

(cljs.core.async.t_cljs$core$async32094.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return self__.f;
}));

(cljs.core.async.t_cljs$core$async32094.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"f","f",43394975,null),new cljs.core.Symbol(null,"blockable","blockable",-28395259,null),new cljs.core.Symbol(null,"meta32095","meta32095",-757880879,null)], null);
}));

(cljs.core.async.t_cljs$core$async32094.cljs$lang$type = true);

(cljs.core.async.t_cljs$core$async32094.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async32094");

(cljs.core.async.t_cljs$core$async32094.cljs$lang$ctorPrWriter = (function (this__4491__auto__,writer__4492__auto__,opt__4493__auto__){
return cljs.core._write(writer__4492__auto__,"cljs.core.async/t_cljs$core$async32094");
}));

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async32094.
 */
cljs.core.async.__GT_t_cljs$core$async32094 = (function cljs$core$async$__GT_t_cljs$core$async32094(f__$1,blockable__$1,meta32095){
return (new cljs.core.async.t_cljs$core$async32094(f__$1,blockable__$1,meta32095));
});

}

return (new cljs.core.async.t_cljs$core$async32094(f,blockable,cljs.core.PersistentArrayMap.EMPTY));
}));

(cljs.core.async.fn_handler.cljs$lang$maxFixedArity = 2);

/**
 * Returns a fixed buffer of size n. When full, puts will block/park.
 */
cljs.core.async.buffer = (function cljs$core$async$buffer(n){
return cljs.core.async.impl.buffers.fixed_buffer(n);
});
/**
 * Returns a buffer of size n. When full, puts will complete but
 *   val will be dropped (no transfer).
 */
cljs.core.async.dropping_buffer = (function cljs$core$async$dropping_buffer(n){
return cljs.core.async.impl.buffers.dropping_buffer(n);
});
/**
 * Returns a buffer of size n. When full, puts will complete, and be
 *   buffered, but oldest elements in buffer will be dropped (not
 *   transferred).
 */
cljs.core.async.sliding_buffer = (function cljs$core$async$sliding_buffer(n){
return cljs.core.async.impl.buffers.sliding_buffer(n);
});
/**
 * Returns true if a channel created with buff will never block. That is to say,
 * puts into this buffer will never cause the buffer to be full. 
 */
cljs.core.async.unblocking_buffer_QMARK_ = (function cljs$core$async$unblocking_buffer_QMARK_(buff){
if((!((buff == null)))){
if(((false) || ((cljs.core.PROTOCOL_SENTINEL === buff.cljs$core$async$impl$protocols$UnblockingBuffer$)))){
return true;
} else {
if((!buff.cljs$lang$protocol_mask$partition$)){
return cljs.core.native_satisfies_QMARK_(cljs.core.async.impl.protocols.UnblockingBuffer,buff);
} else {
return false;
}
}
} else {
return cljs.core.native_satisfies_QMARK_(cljs.core.async.impl.protocols.UnblockingBuffer,buff);
}
});
/**
 * Creates a channel with an optional buffer, an optional transducer (like (map f),
 *   (filter p) etc or a composition thereof), and an optional exception handler.
 *   If buf-or-n is a number, will create and use a fixed buffer of that size. If a
 *   transducer is supplied a buffer must be specified. ex-handler must be a
 *   fn of one argument - if an exception occurs during transformation it will be called
 *   with the thrown value as an argument, and any non-nil return value will be placed
 *   in the channel.
 */
cljs.core.async.chan = (function cljs$core$async$chan(var_args){
var G__32103 = arguments.length;
switch (G__32103) {
case 0:
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.chan.cljs$core$IFn$_invoke$arity$0 = (function (){
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(null);
}));

(cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1 = (function (buf_or_n){
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$3(buf_or_n,null,null);
}));

(cljs.core.async.chan.cljs$core$IFn$_invoke$arity$2 = (function (buf_or_n,xform){
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$3(buf_or_n,xform,null);
}));

(cljs.core.async.chan.cljs$core$IFn$_invoke$arity$3 = (function (buf_or_n,xform,ex_handler){
var buf_or_n__$1 = ((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(buf_or_n,(0)))?null:buf_or_n);
if(cljs.core.truth_(xform)){
if(cljs.core.truth_(buf_or_n__$1)){
} else {
throw (new Error(["Assert failed: ","buffer must be supplied when transducer is","\n","buf-or-n"].join('')));
}
} else {
}

return cljs.core.async.impl.channels.chan.cljs$core$IFn$_invoke$arity$3(((typeof buf_or_n__$1 === 'number')?cljs.core.async.buffer(buf_or_n__$1):buf_or_n__$1),xform,ex_handler);
}));

(cljs.core.async.chan.cljs$lang$maxFixedArity = 3);

/**
 * Creates a promise channel with an optional transducer, and an optional
 *   exception-handler. A promise channel can take exactly one value that consumers
 *   will receive. Once full, puts complete but val is dropped (no transfer).
 *   Consumers will block until either a value is placed in the channel or the
 *   channel is closed. See chan for the semantics of xform and ex-handler.
 */
cljs.core.async.promise_chan = (function cljs$core$async$promise_chan(var_args){
var G__32105 = arguments.length;
switch (G__32105) {
case 0:
return cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$0 = (function (){
return cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$1(null);
}));

(cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$1 = (function (xform){
return cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$2(xform,null);
}));

(cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$2 = (function (xform,ex_handler){
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$3(cljs.core.async.impl.buffers.promise_buffer(),xform,ex_handler);
}));

(cljs.core.async.promise_chan.cljs$lang$maxFixedArity = 2);

/**
 * Returns a channel that will close after msecs
 */
cljs.core.async.timeout = (function cljs$core$async$timeout(msecs){
return cljs.core.async.impl.timers.timeout(msecs);
});
/**
 * takes a val from port. Must be called inside a (go ...) block. Will
 *   return nil if closed. Will park if nothing is available.
 *   Returns true unless port is already closed
 */
cljs.core.async._LT__BANG_ = (function cljs$core$async$_LT__BANG_(port){
throw (new Error("<! used not in (go ...) block"));
});
/**
 * Asynchronously takes a val from port, passing to fn1. Will pass nil
 * if closed. If on-caller? (default true) is true, and value is
 * immediately available, will call fn1 on calling thread.
 * Returns nil.
 */
cljs.core.async.take_BANG_ = (function cljs$core$async$take_BANG_(var_args){
var G__32112 = arguments.length;
switch (G__32112) {
case 2:
return cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$2 = (function (port,fn1){
return cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$3(port,fn1,true);
}));

(cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$3 = (function (port,fn1,on_caller_QMARK_){
var ret = cljs.core.async.impl.protocols.take_BANG_(port,cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$1(fn1));
if(cljs.core.truth_(ret)){
var val_34526 = cljs.core.deref(ret);
if(cljs.core.truth_(on_caller_QMARK_)){
(fn1.cljs$core$IFn$_invoke$arity$1 ? fn1.cljs$core$IFn$_invoke$arity$1(val_34526) : fn1.call(null,val_34526));
} else {
cljs.core.async.impl.dispatch.run((function (){
return (fn1.cljs$core$IFn$_invoke$arity$1 ? fn1.cljs$core$IFn$_invoke$arity$1(val_34526) : fn1.call(null,val_34526));
}));
}
} else {
}

return null;
}));

(cljs.core.async.take_BANG_.cljs$lang$maxFixedArity = 3);

cljs.core.async.nop = (function cljs$core$async$nop(_){
return null;
});
cljs.core.async.fhnop = cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$1(cljs.core.async.nop);
/**
 * puts a val into port. nil values are not allowed. Must be called
 *   inside a (go ...) block. Will park if no buffer space is available.
 *   Returns true unless port is already closed.
 */
cljs.core.async._GT__BANG_ = (function cljs$core$async$_GT__BANG_(port,val){
throw (new Error(">! used not in (go ...) block"));
});
/**
 * Asynchronously puts a val into port, calling fn1 (if supplied) when
 * complete. nil values are not allowed. Will throw if closed. If
 * on-caller? (default true) is true, and the put is immediately
 * accepted, will call fn1 on calling thread.  Returns nil.
 */
cljs.core.async.put_BANG_ = (function cljs$core$async$put_BANG_(var_args){
var G__32137 = arguments.length;
switch (G__32137) {
case 2:
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
case 4:
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2 = (function (port,val){
var temp__5751__auto__ = cljs.core.async.impl.protocols.put_BANG_(port,val,cljs.core.async.fhnop);
if(cljs.core.truth_(temp__5751__auto__)){
var ret = temp__5751__auto__;
return cljs.core.deref(ret);
} else {
return true;
}
}));

(cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$3 = (function (port,val,fn1){
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$4(port,val,fn1,true);
}));

(cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$4 = (function (port,val,fn1,on_caller_QMARK_){
var temp__5751__auto__ = cljs.core.async.impl.protocols.put_BANG_(port,val,cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$1(fn1));
if(cljs.core.truth_(temp__5751__auto__)){
var retb = temp__5751__auto__;
var ret = cljs.core.deref(retb);
if(cljs.core.truth_(on_caller_QMARK_)){
(fn1.cljs$core$IFn$_invoke$arity$1 ? fn1.cljs$core$IFn$_invoke$arity$1(ret) : fn1.call(null,ret));
} else {
cljs.core.async.impl.dispatch.run((function (){
return (fn1.cljs$core$IFn$_invoke$arity$1 ? fn1.cljs$core$IFn$_invoke$arity$1(ret) : fn1.call(null,ret));
}));
}

return ret;
} else {
return true;
}
}));

(cljs.core.async.put_BANG_.cljs$lang$maxFixedArity = 4);

cljs.core.async.close_BANG_ = (function cljs$core$async$close_BANG_(port){
return cljs.core.async.impl.protocols.close_BANG_(port);
});
cljs.core.async.random_array = (function cljs$core$async$random_array(n){
var a = (new Array(n));
var n__4741__auto___34530 = n;
var x_34531 = (0);
while(true){
if((x_34531 < n__4741__auto___34530)){
(a[x_34531] = x_34531);

var G__34532 = (x_34531 + (1));
x_34531 = G__34532;
continue;
} else {
}
break;
}

cljs.core.async.goog$module$goog$array.shuffle(a);

return a;
});
cljs.core.async.alt_flag = (function cljs$core$async$alt_flag(){
var flag = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(true);
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async32145 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Handler}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async32145 = (function (flag,meta32146){
this.flag = flag;
this.meta32146 = meta32146;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(cljs.core.async.t_cljs$core$async32145.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_32147,meta32146__$1){
var self__ = this;
var _32147__$1 = this;
return (new cljs.core.async.t_cljs$core$async32145(self__.flag,meta32146__$1));
}));

(cljs.core.async.t_cljs$core$async32145.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_32147){
var self__ = this;
var _32147__$1 = this;
return self__.meta32146;
}));

(cljs.core.async.t_cljs$core$async32145.prototype.cljs$core$async$impl$protocols$Handler$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async32145.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.deref(self__.flag);
}));

(cljs.core.async.t_cljs$core$async32145.prototype.cljs$core$async$impl$protocols$Handler$blockable_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return true;
}));

(cljs.core.async.t_cljs$core$async32145.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
cljs.core.reset_BANG_(self__.flag,null);

return true;
}));

(cljs.core.async.t_cljs$core$async32145.getBasis = (function (){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"flag","flag",-1565787888,null),new cljs.core.Symbol(null,"meta32146","meta32146",-1153994031,null)], null);
}));

(cljs.core.async.t_cljs$core$async32145.cljs$lang$type = true);

(cljs.core.async.t_cljs$core$async32145.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async32145");

(cljs.core.async.t_cljs$core$async32145.cljs$lang$ctorPrWriter = (function (this__4491__auto__,writer__4492__auto__,opt__4493__auto__){
return cljs.core._write(writer__4492__auto__,"cljs.core.async/t_cljs$core$async32145");
}));

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async32145.
 */
cljs.core.async.__GT_t_cljs$core$async32145 = (function cljs$core$async$alt_flag_$___GT_t_cljs$core$async32145(flag__$1,meta32146){
return (new cljs.core.async.t_cljs$core$async32145(flag__$1,meta32146));
});

}

return (new cljs.core.async.t_cljs$core$async32145(flag,cljs.core.PersistentArrayMap.EMPTY));
});
cljs.core.async.alt_handler = (function cljs$core$async$alt_handler(flag,cb){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async32155 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Handler}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async32155 = (function (flag,cb,meta32156){
this.flag = flag;
this.cb = cb;
this.meta32156 = meta32156;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(cljs.core.async.t_cljs$core$async32155.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_32157,meta32156__$1){
var self__ = this;
var _32157__$1 = this;
return (new cljs.core.async.t_cljs$core$async32155(self__.flag,self__.cb,meta32156__$1));
}));

(cljs.core.async.t_cljs$core$async32155.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_32157){
var self__ = this;
var _32157__$1 = this;
return self__.meta32156;
}));

(cljs.core.async.t_cljs$core$async32155.prototype.cljs$core$async$impl$protocols$Handler$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async32155.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.active_QMARK_(self__.flag);
}));

(cljs.core.async.t_cljs$core$async32155.prototype.cljs$core$async$impl$protocols$Handler$blockable_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return true;
}));

(cljs.core.async.t_cljs$core$async32155.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
cljs.core.async.impl.protocols.commit(self__.flag);

return self__.cb;
}));

(cljs.core.async.t_cljs$core$async32155.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"flag","flag",-1565787888,null),new cljs.core.Symbol(null,"cb","cb",-2064487928,null),new cljs.core.Symbol(null,"meta32156","meta32156",-1446354060,null)], null);
}));

(cljs.core.async.t_cljs$core$async32155.cljs$lang$type = true);

(cljs.core.async.t_cljs$core$async32155.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async32155");

(cljs.core.async.t_cljs$core$async32155.cljs$lang$ctorPrWriter = (function (this__4491__auto__,writer__4492__auto__,opt__4493__auto__){
return cljs.core._write(writer__4492__auto__,"cljs.core.async/t_cljs$core$async32155");
}));

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async32155.
 */
cljs.core.async.__GT_t_cljs$core$async32155 = (function cljs$core$async$alt_handler_$___GT_t_cljs$core$async32155(flag__$1,cb__$1,meta32156){
return (new cljs.core.async.t_cljs$core$async32155(flag__$1,cb__$1,meta32156));
});

}

return (new cljs.core.async.t_cljs$core$async32155(flag,cb,cljs.core.PersistentArrayMap.EMPTY));
});
/**
 * returns derefable [val port] if immediate, nil if enqueued
 */
cljs.core.async.do_alts = (function cljs$core$async$do_alts(fret,ports,opts){
if((cljs.core.count(ports) > (0))){
} else {
throw (new Error(["Assert failed: ","alts must have at least one channel operation","\n","(pos? (count ports))"].join('')));
}

var flag = cljs.core.async.alt_flag();
var n = cljs.core.count(ports);
var idxs = cljs.core.async.random_array(n);
var priority = new cljs.core.Keyword(null,"priority","priority",1431093715).cljs$core$IFn$_invoke$arity$1(opts);
var ret = (function (){var i = (0);
while(true){
if((i < n)){
var idx = (cljs.core.truth_(priority)?i:(idxs[i]));
var port = cljs.core.nth.cljs$core$IFn$_invoke$arity$2(ports,idx);
var wport = ((cljs.core.vector_QMARK_(port))?(port.cljs$core$IFn$_invoke$arity$1 ? port.cljs$core$IFn$_invoke$arity$1((0)) : port.call(null,(0))):null);
var vbox = (cljs.core.truth_(wport)?(function (){var val = (port.cljs$core$IFn$_invoke$arity$1 ? port.cljs$core$IFn$_invoke$arity$1((1)) : port.call(null,(1)));
return cljs.core.async.impl.protocols.put_BANG_(wport,val,cljs.core.async.alt_handler(flag,((function (i,val,idx,port,wport,flag,n,idxs,priority){
return (function (p1__32166_SHARP_){
var G__32168 = new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [p1__32166_SHARP_,wport], null);
return (fret.cljs$core$IFn$_invoke$arity$1 ? fret.cljs$core$IFn$_invoke$arity$1(G__32168) : fret.call(null,G__32168));
});})(i,val,idx,port,wport,flag,n,idxs,priority))
));
})():cljs.core.async.impl.protocols.take_BANG_(port,cljs.core.async.alt_handler(flag,((function (i,idx,port,wport,flag,n,idxs,priority){
return (function (p1__32167_SHARP_){
var G__32173 = new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [p1__32167_SHARP_,port], null);
return (fret.cljs$core$IFn$_invoke$arity$1 ? fret.cljs$core$IFn$_invoke$arity$1(G__32173) : fret.call(null,G__32173));
});})(i,idx,port,wport,flag,n,idxs,priority))
)));
if(cljs.core.truth_(vbox)){
return cljs.core.async.impl.channels.box(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.deref(vbox),(function (){var or__4253__auto__ = wport;
if(cljs.core.truth_(or__4253__auto__)){
return or__4253__auto__;
} else {
return port;
}
})()], null));
} else {
var G__34544 = (i + (1));
i = G__34544;
continue;
}
} else {
return null;
}
break;
}
})();
var or__4253__auto__ = ret;
if(cljs.core.truth_(or__4253__auto__)){
return or__4253__auto__;
} else {
if(cljs.core.contains_QMARK_(opts,new cljs.core.Keyword(null,"default","default",-1987822328))){
var temp__5753__auto__ = (function (){var and__4251__auto__ = flag.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1(null);
if(cljs.core.truth_(and__4251__auto__)){
return flag.cljs$core$async$impl$protocols$Handler$commit$arity$1(null);
} else {
return and__4251__auto__;
}
})();
if(cljs.core.truth_(temp__5753__auto__)){
var got = temp__5753__auto__;
return cljs.core.async.impl.channels.box(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"default","default",-1987822328).cljs$core$IFn$_invoke$arity$1(opts),new cljs.core.Keyword(null,"default","default",-1987822328)], null));
} else {
return null;
}
} else {
return null;
}
}
});
/**
 * Completes at most one of several channel operations. Must be called
 * inside a (go ...) block. ports is a vector of channel endpoints,
 * which can be either a channel to take from or a vector of
 *   [channel-to-put-to val-to-put], in any combination. Takes will be
 *   made as if by <!, and puts will be made as if by >!. Unless
 *   the :priority option is true, if more than one port operation is
 *   ready a non-deterministic choice will be made. If no operation is
 *   ready and a :default value is supplied, [default-val :default] will
 *   be returned, otherwise alts! will park until the first operation to
 *   become ready completes. Returns [val port] of the completed
 *   operation, where val is the value taken for takes, and a
 *   boolean (true unless already closed, as per put!) for puts.
 * 
 *   opts are passed as :key val ... Supported options:
 * 
 *   :default val - the value to use if none of the operations are immediately ready
 *   :priority true - (default nil) when true, the operations will be tried in order.
 * 
 *   Note: there is no guarantee that the port exps or val exprs will be
 *   used, nor in what order should they be, so they should not be
 *   depended upon for side effects.
 */
cljs.core.async.alts_BANG_ = (function cljs$core$async$alts_BANG_(var_args){
var args__4870__auto__ = [];
var len__4864__auto___34548 = arguments.length;
var i__4865__auto___34552 = (0);
while(true){
if((i__4865__auto___34552 < len__4864__auto___34548)){
args__4870__auto__.push((arguments[i__4865__auto___34552]));

var G__34553 = (i__4865__auto___34552 + (1));
i__4865__auto___34552 = G__34553;
continue;
} else {
}
break;
}

var argseq__4871__auto__ = ((((1) < args__4870__auto__.length))?(new cljs.core.IndexedSeq(args__4870__auto__.slice((1)),(0),null)):null);
return cljs.core.async.alts_BANG_.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__4871__auto__);
});

(cljs.core.async.alts_BANG_.cljs$core$IFn$_invoke$arity$variadic = (function (ports,p__32181){
var map__32182 = p__32181;
var map__32182__$1 = cljs.core.__destructure_map(map__32182);
var opts = map__32182__$1;
throw (new Error("alts! used not in (go ...) block"));
}));

(cljs.core.async.alts_BANG_.cljs$lang$maxFixedArity = (1));

/** @this {Function} */
(cljs.core.async.alts_BANG_.cljs$lang$applyTo = (function (seq32174){
var G__32175 = cljs.core.first(seq32174);
var seq32174__$1 = cljs.core.next(seq32174);
var self__4851__auto__ = this;
return self__4851__auto__.cljs$core$IFn$_invoke$arity$variadic(G__32175,seq32174__$1);
}));

/**
 * Puts a val into port if it's possible to do so immediately.
 *   nil values are not allowed. Never blocks. Returns true if offer succeeds.
 */
cljs.core.async.offer_BANG_ = (function cljs$core$async$offer_BANG_(port,val){
var ret = cljs.core.async.impl.protocols.put_BANG_(port,val,cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$2(cljs.core.async.nop,false));
if(cljs.core.truth_(ret)){
return cljs.core.deref(ret);
} else {
return null;
}
});
/**
 * Takes a val from port if it's possible to do so immediately.
 *   Never blocks. Returns value if successful, nil otherwise.
 */
cljs.core.async.poll_BANG_ = (function cljs$core$async$poll_BANG_(port){
var ret = cljs.core.async.impl.protocols.take_BANG_(port,cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$2(cljs.core.async.nop,false));
if(cljs.core.truth_(ret)){
return cljs.core.deref(ret);
} else {
return null;
}
});
/**
 * Takes elements from the from channel and supplies them to the to
 * channel. By default, the to channel will be closed when the from
 * channel closes, but can be determined by the close?  parameter. Will
 * stop consuming the from channel if the to channel closes
 */
cljs.core.async.pipe = (function cljs$core$async$pipe(var_args){
var G__32193 = arguments.length;
switch (G__32193) {
case 2:
return cljs.core.async.pipe.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.pipe.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.pipe.cljs$core$IFn$_invoke$arity$2 = (function (from,to){
return cljs.core.async.pipe.cljs$core$IFn$_invoke$arity$3(from,to,true);
}));

(cljs.core.async.pipe.cljs$core$IFn$_invoke$arity$3 = (function (from,to,close_QMARK_){
var c__32010__auto___34556 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__32011__auto__ = (function (){var switch__31873__auto__ = (function (state_32227){
var state_val_32229 = (state_32227[(1)]);
if((state_val_32229 === (7))){
var inst_32223 = (state_32227[(2)]);
var state_32227__$1 = state_32227;
var statearr_32231_34558 = state_32227__$1;
(statearr_32231_34558[(2)] = inst_32223);

(statearr_32231_34558[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32229 === (1))){
var state_32227__$1 = state_32227;
var statearr_32233_34559 = state_32227__$1;
(statearr_32233_34559[(2)] = null);

(statearr_32233_34559[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32229 === (4))){
var inst_32206 = (state_32227[(7)]);
var inst_32206__$1 = (state_32227[(2)]);
var inst_32207 = (inst_32206__$1 == null);
var state_32227__$1 = (function (){var statearr_32234 = state_32227;
(statearr_32234[(7)] = inst_32206__$1);

return statearr_32234;
})();
if(cljs.core.truth_(inst_32207)){
var statearr_32235_34567 = state_32227__$1;
(statearr_32235_34567[(1)] = (5));

} else {
var statearr_32238_34568 = state_32227__$1;
(statearr_32238_34568[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32229 === (13))){
var state_32227__$1 = state_32227;
var statearr_32239_34569 = state_32227__$1;
(statearr_32239_34569[(2)] = null);

(statearr_32239_34569[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32229 === (6))){
var inst_32206 = (state_32227[(7)]);
var state_32227__$1 = state_32227;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_32227__$1,(11),to,inst_32206);
} else {
if((state_val_32229 === (3))){
var inst_32225 = (state_32227[(2)]);
var state_32227__$1 = state_32227;
return cljs.core.async.impl.ioc_helpers.return_chan(state_32227__$1,inst_32225);
} else {
if((state_val_32229 === (12))){
var state_32227__$1 = state_32227;
var statearr_32252_34573 = state_32227__$1;
(statearr_32252_34573[(2)] = null);

(statearr_32252_34573[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32229 === (2))){
var state_32227__$1 = state_32227;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_32227__$1,(4),from);
} else {
if((state_val_32229 === (11))){
var inst_32216 = (state_32227[(2)]);
var state_32227__$1 = state_32227;
if(cljs.core.truth_(inst_32216)){
var statearr_32253_34574 = state_32227__$1;
(statearr_32253_34574[(1)] = (12));

} else {
var statearr_32254_34575 = state_32227__$1;
(statearr_32254_34575[(1)] = (13));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32229 === (9))){
var state_32227__$1 = state_32227;
var statearr_32255_34576 = state_32227__$1;
(statearr_32255_34576[(2)] = null);

(statearr_32255_34576[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32229 === (5))){
var state_32227__$1 = state_32227;
if(cljs.core.truth_(close_QMARK_)){
var statearr_32263_34577 = state_32227__$1;
(statearr_32263_34577[(1)] = (8));

} else {
var statearr_32264_34578 = state_32227__$1;
(statearr_32264_34578[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32229 === (14))){
var inst_32221 = (state_32227[(2)]);
var state_32227__$1 = state_32227;
var statearr_32265_34579 = state_32227__$1;
(statearr_32265_34579[(2)] = inst_32221);

(statearr_32265_34579[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32229 === (10))){
var inst_32213 = (state_32227[(2)]);
var state_32227__$1 = state_32227;
var statearr_32266_34581 = state_32227__$1;
(statearr_32266_34581[(2)] = inst_32213);

(statearr_32266_34581[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32229 === (8))){
var inst_32210 = cljs.core.async.close_BANG_(to);
var state_32227__$1 = state_32227;
var statearr_32267_34582 = state_32227__$1;
(statearr_32267_34582[(2)] = inst_32210);

(statearr_32267_34582[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__31874__auto__ = null;
var cljs$core$async$state_machine__31874__auto____0 = (function (){
var statearr_32269 = [null,null,null,null,null,null,null,null];
(statearr_32269[(0)] = cljs$core$async$state_machine__31874__auto__);

(statearr_32269[(1)] = (1));

return statearr_32269;
});
var cljs$core$async$state_machine__31874__auto____1 = (function (state_32227){
while(true){
var ret_value__31875__auto__ = (function (){try{while(true){
var result__31876__auto__ = switch__31873__auto__(state_32227);
if(cljs.core.keyword_identical_QMARK_(result__31876__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__31876__auto__;
}
break;
}
}catch (e32271){var ex__31877__auto__ = e32271;
var statearr_32272_34586 = state_32227;
(statearr_32272_34586[(2)] = ex__31877__auto__);


if(cljs.core.seq((state_32227[(4)]))){
var statearr_32273_34587 = state_32227;
(statearr_32273_34587[(1)] = cljs.core.first((state_32227[(4)])));

} else {
throw ex__31877__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__31875__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__34588 = state_32227;
state_32227 = G__34588;
continue;
} else {
return ret_value__31875__auto__;
}
break;
}
});
cljs$core$async$state_machine__31874__auto__ = function(state_32227){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__31874__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__31874__auto____1.call(this,state_32227);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__31874__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__31874__auto____0;
cljs$core$async$state_machine__31874__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__31874__auto____1;
return cljs$core$async$state_machine__31874__auto__;
})()
})();
var state__32012__auto__ = (function (){var statearr_32275 = f__32011__auto__();
(statearr_32275[(6)] = c__32010__auto___34556);

return statearr_32275;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__32012__auto__);
}));


return to;
}));

(cljs.core.async.pipe.cljs$lang$maxFixedArity = 3);

cljs.core.async.pipeline_STAR_ = (function cljs$core$async$pipeline_STAR_(n,to,xf,from,close_QMARK_,ex_handler,type){
if((n > (0))){
} else {
throw (new Error("Assert failed: (pos? n)"));
}

var jobs = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(n);
var results = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(n);
var process = (function (p__32286){
var vec__32290 = p__32286;
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__32290,(0),null);
var p = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__32290,(1),null);
var job = vec__32290;
if((job == null)){
cljs.core.async.close_BANG_(results);

return null;
} else {
var res = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$3((1),xf,ex_handler);
var c__32010__auto___34591 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__32011__auto__ = (function (){var switch__31873__auto__ = (function (state_32302){
var state_val_32303 = (state_32302[(1)]);
if((state_val_32303 === (1))){
var state_32302__$1 = state_32302;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_32302__$1,(2),res,v);
} else {
if((state_val_32303 === (2))){
var inst_32299 = (state_32302[(2)]);
var inst_32300 = cljs.core.async.close_BANG_(res);
var state_32302__$1 = (function (){var statearr_32306 = state_32302;
(statearr_32306[(7)] = inst_32299);

return statearr_32306;
})();
return cljs.core.async.impl.ioc_helpers.return_chan(state_32302__$1,inst_32300);
} else {
return null;
}
}
});
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__31874__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__31874__auto____0 = (function (){
var statearr_32308 = [null,null,null,null,null,null,null,null];
(statearr_32308[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__31874__auto__);

(statearr_32308[(1)] = (1));

return statearr_32308;
});
var cljs$core$async$pipeline_STAR__$_state_machine__31874__auto____1 = (function (state_32302){
while(true){
var ret_value__31875__auto__ = (function (){try{while(true){
var result__31876__auto__ = switch__31873__auto__(state_32302);
if(cljs.core.keyword_identical_QMARK_(result__31876__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__31876__auto__;
}
break;
}
}catch (e32310){var ex__31877__auto__ = e32310;
var statearr_32311_34593 = state_32302;
(statearr_32311_34593[(2)] = ex__31877__auto__);


if(cljs.core.seq((state_32302[(4)]))){
var statearr_32312_34594 = state_32302;
(statearr_32312_34594[(1)] = cljs.core.first((state_32302[(4)])));

} else {
throw ex__31877__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__31875__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__34595 = state_32302;
state_32302 = G__34595;
continue;
} else {
return ret_value__31875__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__31874__auto__ = function(state_32302){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__31874__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__31874__auto____1.call(this,state_32302);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$pipeline_STAR__$_state_machine__31874__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__31874__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__31874__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__31874__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__31874__auto__;
})()
})();
var state__32012__auto__ = (function (){var statearr_32313 = f__32011__auto__();
(statearr_32313[(6)] = c__32010__auto___34591);

return statearr_32313;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__32012__auto__);
}));


cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2(p,res);

return true;
}
});
var async = (function (p__32317){
var vec__32318 = p__32317;
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__32318,(0),null);
var p = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__32318,(1),null);
var job = vec__32318;
if((job == null)){
cljs.core.async.close_BANG_(results);

return null;
} else {
var res = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
(xf.cljs$core$IFn$_invoke$arity$2 ? xf.cljs$core$IFn$_invoke$arity$2(v,res) : xf.call(null,v,res));

cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2(p,res);

return true;
}
});
var n__4741__auto___34596 = n;
var __34597 = (0);
while(true){
if((__34597 < n__4741__auto___34596)){
var G__32323_34598 = type;
var G__32323_34599__$1 = (((G__32323_34598 instanceof cljs.core.Keyword))?G__32323_34598.fqn:null);
switch (G__32323_34599__$1) {
case "compute":
var c__32010__auto___34601 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (__34597,c__32010__auto___34601,G__32323_34598,G__32323_34599__$1,n__4741__auto___34596,jobs,results,process,async){
return (function (){
var f__32011__auto__ = (function (){var switch__31873__auto__ = ((function (__34597,c__32010__auto___34601,G__32323_34598,G__32323_34599__$1,n__4741__auto___34596,jobs,results,process,async){
return (function (state_32342){
var state_val_32343 = (state_32342[(1)]);
if((state_val_32343 === (1))){
var state_32342__$1 = state_32342;
var statearr_32344_34602 = state_32342__$1;
(statearr_32344_34602[(2)] = null);

(statearr_32344_34602[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32343 === (2))){
var state_32342__$1 = state_32342;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_32342__$1,(4),jobs);
} else {
if((state_val_32343 === (3))){
var inst_32340 = (state_32342[(2)]);
var state_32342__$1 = state_32342;
return cljs.core.async.impl.ioc_helpers.return_chan(state_32342__$1,inst_32340);
} else {
if((state_val_32343 === (4))){
var inst_32332 = (state_32342[(2)]);
var inst_32333 = process(inst_32332);
var state_32342__$1 = state_32342;
if(cljs.core.truth_(inst_32333)){
var statearr_32345_34603 = state_32342__$1;
(statearr_32345_34603[(1)] = (5));

} else {
var statearr_32346_34605 = state_32342__$1;
(statearr_32346_34605[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32343 === (5))){
var state_32342__$1 = state_32342;
var statearr_32347_34607 = state_32342__$1;
(statearr_32347_34607[(2)] = null);

(statearr_32347_34607[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32343 === (6))){
var state_32342__$1 = state_32342;
var statearr_32348_34608 = state_32342__$1;
(statearr_32348_34608[(2)] = null);

(statearr_32348_34608[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32343 === (7))){
var inst_32338 = (state_32342[(2)]);
var state_32342__$1 = state_32342;
var statearr_32349_34611 = state_32342__$1;
(statearr_32349_34611[(2)] = inst_32338);

(statearr_32349_34611[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
});})(__34597,c__32010__auto___34601,G__32323_34598,G__32323_34599__$1,n__4741__auto___34596,jobs,results,process,async))
;
return ((function (__34597,switch__31873__auto__,c__32010__auto___34601,G__32323_34598,G__32323_34599__$1,n__4741__auto___34596,jobs,results,process,async){
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__31874__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__31874__auto____0 = (function (){
var statearr_32350 = [null,null,null,null,null,null,null];
(statearr_32350[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__31874__auto__);

(statearr_32350[(1)] = (1));

return statearr_32350;
});
var cljs$core$async$pipeline_STAR__$_state_machine__31874__auto____1 = (function (state_32342){
while(true){
var ret_value__31875__auto__ = (function (){try{while(true){
var result__31876__auto__ = switch__31873__auto__(state_32342);
if(cljs.core.keyword_identical_QMARK_(result__31876__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__31876__auto__;
}
break;
}
}catch (e32351){var ex__31877__auto__ = e32351;
var statearr_32352_34614 = state_32342;
(statearr_32352_34614[(2)] = ex__31877__auto__);


if(cljs.core.seq((state_32342[(4)]))){
var statearr_32353_34615 = state_32342;
(statearr_32353_34615[(1)] = cljs.core.first((state_32342[(4)])));

} else {
throw ex__31877__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__31875__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__34616 = state_32342;
state_32342 = G__34616;
continue;
} else {
return ret_value__31875__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__31874__auto__ = function(state_32342){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__31874__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__31874__auto____1.call(this,state_32342);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$pipeline_STAR__$_state_machine__31874__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__31874__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__31874__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__31874__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__31874__auto__;
})()
;})(__34597,switch__31873__auto__,c__32010__auto___34601,G__32323_34598,G__32323_34599__$1,n__4741__auto___34596,jobs,results,process,async))
})();
var state__32012__auto__ = (function (){var statearr_32354 = f__32011__auto__();
(statearr_32354[(6)] = c__32010__auto___34601);

return statearr_32354;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__32012__auto__);
});})(__34597,c__32010__auto___34601,G__32323_34598,G__32323_34599__$1,n__4741__auto___34596,jobs,results,process,async))
);


break;
case "async":
var c__32010__auto___34617 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (__34597,c__32010__auto___34617,G__32323_34598,G__32323_34599__$1,n__4741__auto___34596,jobs,results,process,async){
return (function (){
var f__32011__auto__ = (function (){var switch__31873__auto__ = ((function (__34597,c__32010__auto___34617,G__32323_34598,G__32323_34599__$1,n__4741__auto___34596,jobs,results,process,async){
return (function (state_32367){
var state_val_32368 = (state_32367[(1)]);
if((state_val_32368 === (1))){
var state_32367__$1 = state_32367;
var statearr_32370_34618 = state_32367__$1;
(statearr_32370_34618[(2)] = null);

(statearr_32370_34618[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32368 === (2))){
var state_32367__$1 = state_32367;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_32367__$1,(4),jobs);
} else {
if((state_val_32368 === (3))){
var inst_32365 = (state_32367[(2)]);
var state_32367__$1 = state_32367;
return cljs.core.async.impl.ioc_helpers.return_chan(state_32367__$1,inst_32365);
} else {
if((state_val_32368 === (4))){
var inst_32357 = (state_32367[(2)]);
var inst_32358 = async(inst_32357);
var state_32367__$1 = state_32367;
if(cljs.core.truth_(inst_32358)){
var statearr_32371_34622 = state_32367__$1;
(statearr_32371_34622[(1)] = (5));

} else {
var statearr_32373_34623 = state_32367__$1;
(statearr_32373_34623[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32368 === (5))){
var state_32367__$1 = state_32367;
var statearr_32374_34624 = state_32367__$1;
(statearr_32374_34624[(2)] = null);

(statearr_32374_34624[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32368 === (6))){
var state_32367__$1 = state_32367;
var statearr_32375_34625 = state_32367__$1;
(statearr_32375_34625[(2)] = null);

(statearr_32375_34625[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32368 === (7))){
var inst_32363 = (state_32367[(2)]);
var state_32367__$1 = state_32367;
var statearr_32376_34629 = state_32367__$1;
(statearr_32376_34629[(2)] = inst_32363);

(statearr_32376_34629[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
});})(__34597,c__32010__auto___34617,G__32323_34598,G__32323_34599__$1,n__4741__auto___34596,jobs,results,process,async))
;
return ((function (__34597,switch__31873__auto__,c__32010__auto___34617,G__32323_34598,G__32323_34599__$1,n__4741__auto___34596,jobs,results,process,async){
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__31874__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__31874__auto____0 = (function (){
var statearr_32377 = [null,null,null,null,null,null,null];
(statearr_32377[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__31874__auto__);

(statearr_32377[(1)] = (1));

return statearr_32377;
});
var cljs$core$async$pipeline_STAR__$_state_machine__31874__auto____1 = (function (state_32367){
while(true){
var ret_value__31875__auto__ = (function (){try{while(true){
var result__31876__auto__ = switch__31873__auto__(state_32367);
if(cljs.core.keyword_identical_QMARK_(result__31876__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__31876__auto__;
}
break;
}
}catch (e32378){var ex__31877__auto__ = e32378;
var statearr_32379_34634 = state_32367;
(statearr_32379_34634[(2)] = ex__31877__auto__);


if(cljs.core.seq((state_32367[(4)]))){
var statearr_32380_34635 = state_32367;
(statearr_32380_34635[(1)] = cljs.core.first((state_32367[(4)])));

} else {
throw ex__31877__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__31875__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__34636 = state_32367;
state_32367 = G__34636;
continue;
} else {
return ret_value__31875__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__31874__auto__ = function(state_32367){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__31874__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__31874__auto____1.call(this,state_32367);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$pipeline_STAR__$_state_machine__31874__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__31874__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__31874__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__31874__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__31874__auto__;
})()
;})(__34597,switch__31873__auto__,c__32010__auto___34617,G__32323_34598,G__32323_34599__$1,n__4741__auto___34596,jobs,results,process,async))
})();
var state__32012__auto__ = (function (){var statearr_32382 = f__32011__auto__();
(statearr_32382[(6)] = c__32010__auto___34617);

return statearr_32382;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__32012__auto__);
});})(__34597,c__32010__auto___34617,G__32323_34598,G__32323_34599__$1,n__4741__auto___34596,jobs,results,process,async))
);


break;
default:
throw (new Error(["No matching clause: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(G__32323_34599__$1)].join('')));

}

var G__34637 = (__34597 + (1));
__34597 = G__34637;
continue;
} else {
}
break;
}

var c__32010__auto___34638 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__32011__auto__ = (function (){var switch__31873__auto__ = (function (state_32404){
var state_val_32405 = (state_32404[(1)]);
if((state_val_32405 === (7))){
var inst_32400 = (state_32404[(2)]);
var state_32404__$1 = state_32404;
var statearr_32406_34639 = state_32404__$1;
(statearr_32406_34639[(2)] = inst_32400);

(statearr_32406_34639[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32405 === (1))){
var state_32404__$1 = state_32404;
var statearr_32407_34640 = state_32404__$1;
(statearr_32407_34640[(2)] = null);

(statearr_32407_34640[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32405 === (4))){
var inst_32385 = (state_32404[(7)]);
var inst_32385__$1 = (state_32404[(2)]);
var inst_32386 = (inst_32385__$1 == null);
var state_32404__$1 = (function (){var statearr_32409 = state_32404;
(statearr_32409[(7)] = inst_32385__$1);

return statearr_32409;
})();
if(cljs.core.truth_(inst_32386)){
var statearr_32410_34641 = state_32404__$1;
(statearr_32410_34641[(1)] = (5));

} else {
var statearr_32411_34642 = state_32404__$1;
(statearr_32411_34642[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32405 === (6))){
var inst_32385 = (state_32404[(7)]);
var inst_32390 = (state_32404[(8)]);
var inst_32390__$1 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
var inst_32391 = cljs.core.PersistentVector.EMPTY_NODE;
var inst_32392 = [inst_32385,inst_32390__$1];
var inst_32393 = (new cljs.core.PersistentVector(null,2,(5),inst_32391,inst_32392,null));
var state_32404__$1 = (function (){var statearr_32413 = state_32404;
(statearr_32413[(8)] = inst_32390__$1);

return statearr_32413;
})();
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_32404__$1,(8),jobs,inst_32393);
} else {
if((state_val_32405 === (3))){
var inst_32402 = (state_32404[(2)]);
var state_32404__$1 = state_32404;
return cljs.core.async.impl.ioc_helpers.return_chan(state_32404__$1,inst_32402);
} else {
if((state_val_32405 === (2))){
var state_32404__$1 = state_32404;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_32404__$1,(4),from);
} else {
if((state_val_32405 === (9))){
var inst_32397 = (state_32404[(2)]);
var state_32404__$1 = (function (){var statearr_32414 = state_32404;
(statearr_32414[(9)] = inst_32397);

return statearr_32414;
})();
var statearr_32415_34643 = state_32404__$1;
(statearr_32415_34643[(2)] = null);

(statearr_32415_34643[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32405 === (5))){
var inst_32388 = cljs.core.async.close_BANG_(jobs);
var state_32404__$1 = state_32404;
var statearr_32418_34644 = state_32404__$1;
(statearr_32418_34644[(2)] = inst_32388);

(statearr_32418_34644[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32405 === (8))){
var inst_32390 = (state_32404[(8)]);
var inst_32395 = (state_32404[(2)]);
var state_32404__$1 = (function (){var statearr_32420 = state_32404;
(statearr_32420[(10)] = inst_32395);

return statearr_32420;
})();
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_32404__$1,(9),results,inst_32390);
} else {
return null;
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__31874__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__31874__auto____0 = (function (){
var statearr_32421 = [null,null,null,null,null,null,null,null,null,null,null];
(statearr_32421[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__31874__auto__);

(statearr_32421[(1)] = (1));

return statearr_32421;
});
var cljs$core$async$pipeline_STAR__$_state_machine__31874__auto____1 = (function (state_32404){
while(true){
var ret_value__31875__auto__ = (function (){try{while(true){
var result__31876__auto__ = switch__31873__auto__(state_32404);
if(cljs.core.keyword_identical_QMARK_(result__31876__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__31876__auto__;
}
break;
}
}catch (e32422){var ex__31877__auto__ = e32422;
var statearr_32423_34648 = state_32404;
(statearr_32423_34648[(2)] = ex__31877__auto__);


if(cljs.core.seq((state_32404[(4)]))){
var statearr_32425_34649 = state_32404;
(statearr_32425_34649[(1)] = cljs.core.first((state_32404[(4)])));

} else {
throw ex__31877__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__31875__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__34650 = state_32404;
state_32404 = G__34650;
continue;
} else {
return ret_value__31875__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__31874__auto__ = function(state_32404){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__31874__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__31874__auto____1.call(this,state_32404);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$pipeline_STAR__$_state_machine__31874__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__31874__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__31874__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__31874__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__31874__auto__;
})()
})();
var state__32012__auto__ = (function (){var statearr_32427 = f__32011__auto__();
(statearr_32427[(6)] = c__32010__auto___34638);

return statearr_32427;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__32012__auto__);
}));


var c__32010__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__32011__auto__ = (function (){var switch__31873__auto__ = (function (state_32467){
var state_val_32468 = (state_32467[(1)]);
if((state_val_32468 === (7))){
var inst_32463 = (state_32467[(2)]);
var state_32467__$1 = state_32467;
var statearr_32473_34651 = state_32467__$1;
(statearr_32473_34651[(2)] = inst_32463);

(statearr_32473_34651[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32468 === (20))){
var state_32467__$1 = state_32467;
var statearr_32474_34656 = state_32467__$1;
(statearr_32474_34656[(2)] = null);

(statearr_32474_34656[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32468 === (1))){
var state_32467__$1 = state_32467;
var statearr_32475_34657 = state_32467__$1;
(statearr_32475_34657[(2)] = null);

(statearr_32475_34657[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32468 === (4))){
var inst_32430 = (state_32467[(7)]);
var inst_32430__$1 = (state_32467[(2)]);
var inst_32431 = (inst_32430__$1 == null);
var state_32467__$1 = (function (){var statearr_32476 = state_32467;
(statearr_32476[(7)] = inst_32430__$1);

return statearr_32476;
})();
if(cljs.core.truth_(inst_32431)){
var statearr_32477_34658 = state_32467__$1;
(statearr_32477_34658[(1)] = (5));

} else {
var statearr_32478_34659 = state_32467__$1;
(statearr_32478_34659[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32468 === (15))){
var inst_32445 = (state_32467[(8)]);
var state_32467__$1 = state_32467;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_32467__$1,(18),to,inst_32445);
} else {
if((state_val_32468 === (21))){
var inst_32458 = (state_32467[(2)]);
var state_32467__$1 = state_32467;
var statearr_32480_34660 = state_32467__$1;
(statearr_32480_34660[(2)] = inst_32458);

(statearr_32480_34660[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32468 === (13))){
var inst_32460 = (state_32467[(2)]);
var state_32467__$1 = (function (){var statearr_32490 = state_32467;
(statearr_32490[(9)] = inst_32460);

return statearr_32490;
})();
var statearr_32491_34661 = state_32467__$1;
(statearr_32491_34661[(2)] = null);

(statearr_32491_34661[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32468 === (6))){
var inst_32430 = (state_32467[(7)]);
var state_32467__$1 = state_32467;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_32467__$1,(11),inst_32430);
} else {
if((state_val_32468 === (17))){
var inst_32453 = (state_32467[(2)]);
var state_32467__$1 = state_32467;
if(cljs.core.truth_(inst_32453)){
var statearr_32492_34662 = state_32467__$1;
(statearr_32492_34662[(1)] = (19));

} else {
var statearr_32493_34663 = state_32467__$1;
(statearr_32493_34663[(1)] = (20));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32468 === (3))){
var inst_32465 = (state_32467[(2)]);
var state_32467__$1 = state_32467;
return cljs.core.async.impl.ioc_helpers.return_chan(state_32467__$1,inst_32465);
} else {
if((state_val_32468 === (12))){
var inst_32442 = (state_32467[(10)]);
var state_32467__$1 = state_32467;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_32467__$1,(14),inst_32442);
} else {
if((state_val_32468 === (2))){
var state_32467__$1 = state_32467;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_32467__$1,(4),results);
} else {
if((state_val_32468 === (19))){
var state_32467__$1 = state_32467;
var statearr_32500_34664 = state_32467__$1;
(statearr_32500_34664[(2)] = null);

(statearr_32500_34664[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32468 === (11))){
var inst_32442 = (state_32467[(2)]);
var state_32467__$1 = (function (){var statearr_32501 = state_32467;
(statearr_32501[(10)] = inst_32442);

return statearr_32501;
})();
var statearr_32502_34665 = state_32467__$1;
(statearr_32502_34665[(2)] = null);

(statearr_32502_34665[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32468 === (9))){
var state_32467__$1 = state_32467;
var statearr_32508_34666 = state_32467__$1;
(statearr_32508_34666[(2)] = null);

(statearr_32508_34666[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32468 === (5))){
var state_32467__$1 = state_32467;
if(cljs.core.truth_(close_QMARK_)){
var statearr_32517_34667 = state_32467__$1;
(statearr_32517_34667[(1)] = (8));

} else {
var statearr_32521_34668 = state_32467__$1;
(statearr_32521_34668[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32468 === (14))){
var inst_32447 = (state_32467[(11)]);
var inst_32445 = (state_32467[(8)]);
var inst_32445__$1 = (state_32467[(2)]);
var inst_32446 = (inst_32445__$1 == null);
var inst_32447__$1 = cljs.core.not(inst_32446);
var state_32467__$1 = (function (){var statearr_32534 = state_32467;
(statearr_32534[(11)] = inst_32447__$1);

(statearr_32534[(8)] = inst_32445__$1);

return statearr_32534;
})();
if(inst_32447__$1){
var statearr_32535_34669 = state_32467__$1;
(statearr_32535_34669[(1)] = (15));

} else {
var statearr_32536_34672 = state_32467__$1;
(statearr_32536_34672[(1)] = (16));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32468 === (16))){
var inst_32447 = (state_32467[(11)]);
var state_32467__$1 = state_32467;
var statearr_32537_34673 = state_32467__$1;
(statearr_32537_34673[(2)] = inst_32447);

(statearr_32537_34673[(1)] = (17));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32468 === (10))){
var inst_32438 = (state_32467[(2)]);
var state_32467__$1 = state_32467;
var statearr_32541_34674 = state_32467__$1;
(statearr_32541_34674[(2)] = inst_32438);

(statearr_32541_34674[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32468 === (18))){
var inst_32450 = (state_32467[(2)]);
var state_32467__$1 = state_32467;
var statearr_32545_34675 = state_32467__$1;
(statearr_32545_34675[(2)] = inst_32450);

(statearr_32545_34675[(1)] = (17));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32468 === (8))){
var inst_32435 = cljs.core.async.close_BANG_(to);
var state_32467__$1 = state_32467;
var statearr_32546_34676 = state_32467__$1;
(statearr_32546_34676[(2)] = inst_32435);

(statearr_32546_34676[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__31874__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__31874__auto____0 = (function (){
var statearr_32549 = [null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_32549[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__31874__auto__);

(statearr_32549[(1)] = (1));

return statearr_32549;
});
var cljs$core$async$pipeline_STAR__$_state_machine__31874__auto____1 = (function (state_32467){
while(true){
var ret_value__31875__auto__ = (function (){try{while(true){
var result__31876__auto__ = switch__31873__auto__(state_32467);
if(cljs.core.keyword_identical_QMARK_(result__31876__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__31876__auto__;
}
break;
}
}catch (e32550){var ex__31877__auto__ = e32550;
var statearr_32551_34677 = state_32467;
(statearr_32551_34677[(2)] = ex__31877__auto__);


if(cljs.core.seq((state_32467[(4)]))){
var statearr_32552_34678 = state_32467;
(statearr_32552_34678[(1)] = cljs.core.first((state_32467[(4)])));

} else {
throw ex__31877__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__31875__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__34679 = state_32467;
state_32467 = G__34679;
continue;
} else {
return ret_value__31875__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__31874__auto__ = function(state_32467){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__31874__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__31874__auto____1.call(this,state_32467);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$pipeline_STAR__$_state_machine__31874__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__31874__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__31874__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__31874__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__31874__auto__;
})()
})();
var state__32012__auto__ = (function (){var statearr_32553 = f__32011__auto__();
(statearr_32553[(6)] = c__32010__auto__);

return statearr_32553;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__32012__auto__);
}));

return c__32010__auto__;
});
/**
 * Takes elements from the from channel and supplies them to the to
 *   channel, subject to the async function af, with parallelism n. af
 *   must be a function of two arguments, the first an input value and
 *   the second a channel on which to place the result(s). The
 *   presumption is that af will return immediately, having launched some
 *   asynchronous operation whose completion/callback will put results on
 *   the channel, then close! it. Outputs will be returned in order
 *   relative to the inputs. By default, the to channel will be closed
 *   when the from channel closes, but can be determined by the close?
 *   parameter. Will stop consuming the from channel if the to channel
 *   closes. See also pipeline, pipeline-blocking.
 */
cljs.core.async.pipeline_async = (function cljs$core$async$pipeline_async(var_args){
var G__32560 = arguments.length;
switch (G__32560) {
case 4:
return cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
case 5:
return cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$5((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]),(arguments[(4)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$4 = (function (n,to,af,from){
return cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$5(n,to,af,from,true);
}));

(cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$5 = (function (n,to,af,from,close_QMARK_){
return cljs.core.async.pipeline_STAR_(n,to,af,from,close_QMARK_,null,new cljs.core.Keyword(null,"async","async",1050769601));
}));

(cljs.core.async.pipeline_async.cljs$lang$maxFixedArity = 5);

/**
 * Takes elements from the from channel and supplies them to the to
 *   channel, subject to the transducer xf, with parallelism n. Because
 *   it is parallel, the transducer will be applied independently to each
 *   element, not across elements, and may produce zero or more outputs
 *   per input.  Outputs will be returned in order relative to the
 *   inputs. By default, the to channel will be closed when the from
 *   channel closes, but can be determined by the close?  parameter. Will
 *   stop consuming the from channel if the to channel closes.
 * 
 *   Note this is supplied for API compatibility with the Clojure version.
 *   Values of N > 1 will not result in actual concurrency in a
 *   single-threaded runtime.
 */
cljs.core.async.pipeline = (function cljs$core$async$pipeline(var_args){
var G__32571 = arguments.length;
switch (G__32571) {
case 4:
return cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
case 5:
return cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$5((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]),(arguments[(4)]));

break;
case 6:
return cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$6((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]),(arguments[(4)]),(arguments[(5)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$4 = (function (n,to,xf,from){
return cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$5(n,to,xf,from,true);
}));

(cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$5 = (function (n,to,xf,from,close_QMARK_){
return cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$6(n,to,xf,from,close_QMARK_,null);
}));

(cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$6 = (function (n,to,xf,from,close_QMARK_,ex_handler){
return cljs.core.async.pipeline_STAR_(n,to,xf,from,close_QMARK_,ex_handler,new cljs.core.Keyword(null,"compute","compute",1555393130));
}));

(cljs.core.async.pipeline.cljs$lang$maxFixedArity = 6);

/**
 * Takes a predicate and a source channel and returns a vector of two
 *   channels, the first of which will contain the values for which the
 *   predicate returned true, the second those for which it returned
 *   false.
 * 
 *   The out channels will be unbuffered by default, or two buf-or-ns can
 *   be supplied. The channels will close after the source channel has
 *   closed.
 */
cljs.core.async.split = (function cljs$core$async$split(var_args){
var G__32657 = arguments.length;
switch (G__32657) {
case 2:
return cljs.core.async.split.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 4:
return cljs.core.async.split.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.split.cljs$core$IFn$_invoke$arity$2 = (function (p,ch){
return cljs.core.async.split.cljs$core$IFn$_invoke$arity$4(p,ch,null,null);
}));

(cljs.core.async.split.cljs$core$IFn$_invoke$arity$4 = (function (p,ch,t_buf_or_n,f_buf_or_n){
var tc = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(t_buf_or_n);
var fc = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(f_buf_or_n);
var c__32010__auto___34683 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__32011__auto__ = (function (){var switch__31873__auto__ = (function (state_32703){
var state_val_32704 = (state_32703[(1)]);
if((state_val_32704 === (7))){
var inst_32699 = (state_32703[(2)]);
var state_32703__$1 = state_32703;
var statearr_32707_34684 = state_32703__$1;
(statearr_32707_34684[(2)] = inst_32699);

(statearr_32707_34684[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32704 === (1))){
var state_32703__$1 = state_32703;
var statearr_32709_34685 = state_32703__$1;
(statearr_32709_34685[(2)] = null);

(statearr_32709_34685[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32704 === (4))){
var inst_32680 = (state_32703[(7)]);
var inst_32680__$1 = (state_32703[(2)]);
var inst_32681 = (inst_32680__$1 == null);
var state_32703__$1 = (function (){var statearr_32710 = state_32703;
(statearr_32710[(7)] = inst_32680__$1);

return statearr_32710;
})();
if(cljs.core.truth_(inst_32681)){
var statearr_32711_34686 = state_32703__$1;
(statearr_32711_34686[(1)] = (5));

} else {
var statearr_32716_34687 = state_32703__$1;
(statearr_32716_34687[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32704 === (13))){
var state_32703__$1 = state_32703;
var statearr_32725_34688 = state_32703__$1;
(statearr_32725_34688[(2)] = null);

(statearr_32725_34688[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32704 === (6))){
var inst_32680 = (state_32703[(7)]);
var inst_32686 = (p.cljs$core$IFn$_invoke$arity$1 ? p.cljs$core$IFn$_invoke$arity$1(inst_32680) : p.call(null,inst_32680));
var state_32703__$1 = state_32703;
if(cljs.core.truth_(inst_32686)){
var statearr_32734_34689 = state_32703__$1;
(statearr_32734_34689[(1)] = (9));

} else {
var statearr_32735_34690 = state_32703__$1;
(statearr_32735_34690[(1)] = (10));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32704 === (3))){
var inst_32701 = (state_32703[(2)]);
var state_32703__$1 = state_32703;
return cljs.core.async.impl.ioc_helpers.return_chan(state_32703__$1,inst_32701);
} else {
if((state_val_32704 === (12))){
var state_32703__$1 = state_32703;
var statearr_32736_34691 = state_32703__$1;
(statearr_32736_34691[(2)] = null);

(statearr_32736_34691[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32704 === (2))){
var state_32703__$1 = state_32703;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_32703__$1,(4),ch);
} else {
if((state_val_32704 === (11))){
var inst_32680 = (state_32703[(7)]);
var inst_32690 = (state_32703[(2)]);
var state_32703__$1 = state_32703;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_32703__$1,(8),inst_32690,inst_32680);
} else {
if((state_val_32704 === (9))){
var state_32703__$1 = state_32703;
var statearr_32737_34692 = state_32703__$1;
(statearr_32737_34692[(2)] = tc);

(statearr_32737_34692[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32704 === (5))){
var inst_32683 = cljs.core.async.close_BANG_(tc);
var inst_32684 = cljs.core.async.close_BANG_(fc);
var state_32703__$1 = (function (){var statearr_32739 = state_32703;
(statearr_32739[(8)] = inst_32683);

return statearr_32739;
})();
var statearr_32740_34695 = state_32703__$1;
(statearr_32740_34695[(2)] = inst_32684);

(statearr_32740_34695[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32704 === (14))){
var inst_32697 = (state_32703[(2)]);
var state_32703__$1 = state_32703;
var statearr_32741_34696 = state_32703__$1;
(statearr_32741_34696[(2)] = inst_32697);

(statearr_32741_34696[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32704 === (10))){
var state_32703__$1 = state_32703;
var statearr_32743_34697 = state_32703__$1;
(statearr_32743_34697[(2)] = fc);

(statearr_32743_34697[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32704 === (8))){
var inst_32692 = (state_32703[(2)]);
var state_32703__$1 = state_32703;
if(cljs.core.truth_(inst_32692)){
var statearr_32744_34698 = state_32703__$1;
(statearr_32744_34698[(1)] = (12));

} else {
var statearr_32745_34699 = state_32703__$1;
(statearr_32745_34699[(1)] = (13));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__31874__auto__ = null;
var cljs$core$async$state_machine__31874__auto____0 = (function (){
var statearr_32746 = [null,null,null,null,null,null,null,null,null];
(statearr_32746[(0)] = cljs$core$async$state_machine__31874__auto__);

(statearr_32746[(1)] = (1));

return statearr_32746;
});
var cljs$core$async$state_machine__31874__auto____1 = (function (state_32703){
while(true){
var ret_value__31875__auto__ = (function (){try{while(true){
var result__31876__auto__ = switch__31873__auto__(state_32703);
if(cljs.core.keyword_identical_QMARK_(result__31876__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__31876__auto__;
}
break;
}
}catch (e32747){var ex__31877__auto__ = e32747;
var statearr_32748_34700 = state_32703;
(statearr_32748_34700[(2)] = ex__31877__auto__);


if(cljs.core.seq((state_32703[(4)]))){
var statearr_32749_34701 = state_32703;
(statearr_32749_34701[(1)] = cljs.core.first((state_32703[(4)])));

} else {
throw ex__31877__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__31875__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__34702 = state_32703;
state_32703 = G__34702;
continue;
} else {
return ret_value__31875__auto__;
}
break;
}
});
cljs$core$async$state_machine__31874__auto__ = function(state_32703){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__31874__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__31874__auto____1.call(this,state_32703);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__31874__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__31874__auto____0;
cljs$core$async$state_machine__31874__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__31874__auto____1;
return cljs$core$async$state_machine__31874__auto__;
})()
})();
var state__32012__auto__ = (function (){var statearr_32750 = f__32011__auto__();
(statearr_32750[(6)] = c__32010__auto___34683);

return statearr_32750;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__32012__auto__);
}));


return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [tc,fc], null);
}));

(cljs.core.async.split.cljs$lang$maxFixedArity = 4);

/**
 * f should be a function of 2 arguments. Returns a channel containing
 *   the single result of applying f to init and the first item from the
 *   channel, then applying f to that result and the 2nd item, etc. If
 *   the channel closes without yielding items, returns init and f is not
 *   called. ch must close before reduce produces a result.
 */
cljs.core.async.reduce = (function cljs$core$async$reduce(f,init,ch){
var c__32010__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__32011__auto__ = (function (){var switch__31873__auto__ = (function (state_32784){
var state_val_32785 = (state_32784[(1)]);
if((state_val_32785 === (7))){
var inst_32776 = (state_32784[(2)]);
var state_32784__$1 = state_32784;
var statearr_32794_34703 = state_32784__$1;
(statearr_32794_34703[(2)] = inst_32776);

(statearr_32794_34703[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32785 === (1))){
var inst_32751 = init;
var inst_32752 = inst_32751;
var state_32784__$1 = (function (){var statearr_32795 = state_32784;
(statearr_32795[(7)] = inst_32752);

return statearr_32795;
})();
var statearr_32796_34704 = state_32784__$1;
(statearr_32796_34704[(2)] = null);

(statearr_32796_34704[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32785 === (4))){
var inst_32755 = (state_32784[(8)]);
var inst_32755__$1 = (state_32784[(2)]);
var inst_32756 = (inst_32755__$1 == null);
var state_32784__$1 = (function (){var statearr_32797 = state_32784;
(statearr_32797[(8)] = inst_32755__$1);

return statearr_32797;
})();
if(cljs.core.truth_(inst_32756)){
var statearr_32798_34705 = state_32784__$1;
(statearr_32798_34705[(1)] = (5));

} else {
var statearr_32799_34706 = state_32784__$1;
(statearr_32799_34706[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32785 === (6))){
var inst_32755 = (state_32784[(8)]);
var inst_32759 = (state_32784[(9)]);
var inst_32752 = (state_32784[(7)]);
var inst_32759__$1 = (f.cljs$core$IFn$_invoke$arity$2 ? f.cljs$core$IFn$_invoke$arity$2(inst_32752,inst_32755) : f.call(null,inst_32752,inst_32755));
var inst_32760 = cljs.core.reduced_QMARK_(inst_32759__$1);
var state_32784__$1 = (function (){var statearr_32800 = state_32784;
(statearr_32800[(9)] = inst_32759__$1);

return statearr_32800;
})();
if(inst_32760){
var statearr_32801_34707 = state_32784__$1;
(statearr_32801_34707[(1)] = (8));

} else {
var statearr_32802_34708 = state_32784__$1;
(statearr_32802_34708[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32785 === (3))){
var inst_32778 = (state_32784[(2)]);
var state_32784__$1 = state_32784;
return cljs.core.async.impl.ioc_helpers.return_chan(state_32784__$1,inst_32778);
} else {
if((state_val_32785 === (2))){
var state_32784__$1 = state_32784;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_32784__$1,(4),ch);
} else {
if((state_val_32785 === (9))){
var inst_32759 = (state_32784[(9)]);
var inst_32752 = inst_32759;
var state_32784__$1 = (function (){var statearr_32807 = state_32784;
(statearr_32807[(7)] = inst_32752);

return statearr_32807;
})();
var statearr_32808_34709 = state_32784__$1;
(statearr_32808_34709[(2)] = null);

(statearr_32808_34709[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32785 === (5))){
var inst_32752 = (state_32784[(7)]);
var state_32784__$1 = state_32784;
var statearr_32809_34710 = state_32784__$1;
(statearr_32809_34710[(2)] = inst_32752);

(statearr_32809_34710[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32785 === (10))){
var inst_32770 = (state_32784[(2)]);
var state_32784__$1 = state_32784;
var statearr_32810_34711 = state_32784__$1;
(statearr_32810_34711[(2)] = inst_32770);

(statearr_32810_34711[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32785 === (8))){
var inst_32759 = (state_32784[(9)]);
var inst_32766 = cljs.core.deref(inst_32759);
var state_32784__$1 = state_32784;
var statearr_32811_34712 = state_32784__$1;
(statearr_32811_34712[(2)] = inst_32766);

(statearr_32811_34712[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$reduce_$_state_machine__31874__auto__ = null;
var cljs$core$async$reduce_$_state_machine__31874__auto____0 = (function (){
var statearr_32814 = [null,null,null,null,null,null,null,null,null,null];
(statearr_32814[(0)] = cljs$core$async$reduce_$_state_machine__31874__auto__);

(statearr_32814[(1)] = (1));

return statearr_32814;
});
var cljs$core$async$reduce_$_state_machine__31874__auto____1 = (function (state_32784){
while(true){
var ret_value__31875__auto__ = (function (){try{while(true){
var result__31876__auto__ = switch__31873__auto__(state_32784);
if(cljs.core.keyword_identical_QMARK_(result__31876__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__31876__auto__;
}
break;
}
}catch (e32815){var ex__31877__auto__ = e32815;
var statearr_32816_34713 = state_32784;
(statearr_32816_34713[(2)] = ex__31877__auto__);


if(cljs.core.seq((state_32784[(4)]))){
var statearr_32817_34714 = state_32784;
(statearr_32817_34714[(1)] = cljs.core.first((state_32784[(4)])));

} else {
throw ex__31877__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__31875__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__34715 = state_32784;
state_32784 = G__34715;
continue;
} else {
return ret_value__31875__auto__;
}
break;
}
});
cljs$core$async$reduce_$_state_machine__31874__auto__ = function(state_32784){
switch(arguments.length){
case 0:
return cljs$core$async$reduce_$_state_machine__31874__auto____0.call(this);
case 1:
return cljs$core$async$reduce_$_state_machine__31874__auto____1.call(this,state_32784);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$reduce_$_state_machine__31874__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$reduce_$_state_machine__31874__auto____0;
cljs$core$async$reduce_$_state_machine__31874__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$reduce_$_state_machine__31874__auto____1;
return cljs$core$async$reduce_$_state_machine__31874__auto__;
})()
})();
var state__32012__auto__ = (function (){var statearr_32818 = f__32011__auto__();
(statearr_32818[(6)] = c__32010__auto__);

return statearr_32818;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__32012__auto__);
}));

return c__32010__auto__;
});
/**
 * async/reduces a channel with a transformation (xform f).
 *   Returns a channel containing the result.  ch must close before
 *   transduce produces a result.
 */
cljs.core.async.transduce = (function cljs$core$async$transduce(xform,f,init,ch){
var f__$1 = (xform.cljs$core$IFn$_invoke$arity$1 ? xform.cljs$core$IFn$_invoke$arity$1(f) : xform.call(null,f));
var c__32010__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__32011__auto__ = (function (){var switch__31873__auto__ = (function (state_32824){
var state_val_32825 = (state_32824[(1)]);
if((state_val_32825 === (1))){
var inst_32819 = cljs.core.async.reduce(f__$1,init,ch);
var state_32824__$1 = state_32824;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_32824__$1,(2),inst_32819);
} else {
if((state_val_32825 === (2))){
var inst_32821 = (state_32824[(2)]);
var inst_32822 = (f__$1.cljs$core$IFn$_invoke$arity$1 ? f__$1.cljs$core$IFn$_invoke$arity$1(inst_32821) : f__$1.call(null,inst_32821));
var state_32824__$1 = state_32824;
return cljs.core.async.impl.ioc_helpers.return_chan(state_32824__$1,inst_32822);
} else {
return null;
}
}
});
return (function() {
var cljs$core$async$transduce_$_state_machine__31874__auto__ = null;
var cljs$core$async$transduce_$_state_machine__31874__auto____0 = (function (){
var statearr_32829 = [null,null,null,null,null,null,null];
(statearr_32829[(0)] = cljs$core$async$transduce_$_state_machine__31874__auto__);

(statearr_32829[(1)] = (1));

return statearr_32829;
});
var cljs$core$async$transduce_$_state_machine__31874__auto____1 = (function (state_32824){
while(true){
var ret_value__31875__auto__ = (function (){try{while(true){
var result__31876__auto__ = switch__31873__auto__(state_32824);
if(cljs.core.keyword_identical_QMARK_(result__31876__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__31876__auto__;
}
break;
}
}catch (e32835){var ex__31877__auto__ = e32835;
var statearr_32836_34716 = state_32824;
(statearr_32836_34716[(2)] = ex__31877__auto__);


if(cljs.core.seq((state_32824[(4)]))){
var statearr_32837_34717 = state_32824;
(statearr_32837_34717[(1)] = cljs.core.first((state_32824[(4)])));

} else {
throw ex__31877__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__31875__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__34718 = state_32824;
state_32824 = G__34718;
continue;
} else {
return ret_value__31875__auto__;
}
break;
}
});
cljs$core$async$transduce_$_state_machine__31874__auto__ = function(state_32824){
switch(arguments.length){
case 0:
return cljs$core$async$transduce_$_state_machine__31874__auto____0.call(this);
case 1:
return cljs$core$async$transduce_$_state_machine__31874__auto____1.call(this,state_32824);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$transduce_$_state_machine__31874__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$transduce_$_state_machine__31874__auto____0;
cljs$core$async$transduce_$_state_machine__31874__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$transduce_$_state_machine__31874__auto____1;
return cljs$core$async$transduce_$_state_machine__31874__auto__;
})()
})();
var state__32012__auto__ = (function (){var statearr_32838 = f__32011__auto__();
(statearr_32838[(6)] = c__32010__auto__);

return statearr_32838;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__32012__auto__);
}));

return c__32010__auto__;
});
/**
 * Puts the contents of coll into the supplied channel.
 * 
 *   By default the channel will be closed after the items are copied,
 *   but can be determined by the close? parameter.
 * 
 *   Returns a channel which will close after the items are copied.
 */
cljs.core.async.onto_chan_BANG_ = (function cljs$core$async$onto_chan_BANG_(var_args){
var G__32843 = arguments.length;
switch (G__32843) {
case 2:
return cljs.core.async.onto_chan_BANG_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.onto_chan_BANG_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.onto_chan_BANG_.cljs$core$IFn$_invoke$arity$2 = (function (ch,coll){
return cljs.core.async.onto_chan_BANG_.cljs$core$IFn$_invoke$arity$3(ch,coll,true);
}));

(cljs.core.async.onto_chan_BANG_.cljs$core$IFn$_invoke$arity$3 = (function (ch,coll,close_QMARK_){
var c__32010__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__32011__auto__ = (function (){var switch__31873__auto__ = (function (state_32868){
var state_val_32869 = (state_32868[(1)]);
if((state_val_32869 === (7))){
var inst_32850 = (state_32868[(2)]);
var state_32868__$1 = state_32868;
var statearr_32870_34721 = state_32868__$1;
(statearr_32870_34721[(2)] = inst_32850);

(statearr_32870_34721[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32869 === (1))){
var inst_32844 = cljs.core.seq(coll);
var inst_32845 = inst_32844;
var state_32868__$1 = (function (){var statearr_32871 = state_32868;
(statearr_32871[(7)] = inst_32845);

return statearr_32871;
})();
var statearr_32872_34724 = state_32868__$1;
(statearr_32872_34724[(2)] = null);

(statearr_32872_34724[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32869 === (4))){
var inst_32845 = (state_32868[(7)]);
var inst_32848 = cljs.core.first(inst_32845);
var state_32868__$1 = state_32868;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_32868__$1,(7),ch,inst_32848);
} else {
if((state_val_32869 === (13))){
var inst_32862 = (state_32868[(2)]);
var state_32868__$1 = state_32868;
var statearr_32873_34725 = state_32868__$1;
(statearr_32873_34725[(2)] = inst_32862);

(statearr_32873_34725[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32869 === (6))){
var inst_32853 = (state_32868[(2)]);
var state_32868__$1 = state_32868;
if(cljs.core.truth_(inst_32853)){
var statearr_32874_34726 = state_32868__$1;
(statearr_32874_34726[(1)] = (8));

} else {
var statearr_32875_34728 = state_32868__$1;
(statearr_32875_34728[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32869 === (3))){
var inst_32866 = (state_32868[(2)]);
var state_32868__$1 = state_32868;
return cljs.core.async.impl.ioc_helpers.return_chan(state_32868__$1,inst_32866);
} else {
if((state_val_32869 === (12))){
var state_32868__$1 = state_32868;
var statearr_32876_34729 = state_32868__$1;
(statearr_32876_34729[(2)] = null);

(statearr_32876_34729[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32869 === (2))){
var inst_32845 = (state_32868[(7)]);
var state_32868__$1 = state_32868;
if(cljs.core.truth_(inst_32845)){
var statearr_32877_34731 = state_32868__$1;
(statearr_32877_34731[(1)] = (4));

} else {
var statearr_32878_34732 = state_32868__$1;
(statearr_32878_34732[(1)] = (5));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32869 === (11))){
var inst_32859 = cljs.core.async.close_BANG_(ch);
var state_32868__$1 = state_32868;
var statearr_32879_34733 = state_32868__$1;
(statearr_32879_34733[(2)] = inst_32859);

(statearr_32879_34733[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32869 === (9))){
var state_32868__$1 = state_32868;
if(cljs.core.truth_(close_QMARK_)){
var statearr_32880_34734 = state_32868__$1;
(statearr_32880_34734[(1)] = (11));

} else {
var statearr_32881_34735 = state_32868__$1;
(statearr_32881_34735[(1)] = (12));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32869 === (5))){
var inst_32845 = (state_32868[(7)]);
var state_32868__$1 = state_32868;
var statearr_32882_34737 = state_32868__$1;
(statearr_32882_34737[(2)] = inst_32845);

(statearr_32882_34737[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32869 === (10))){
var inst_32864 = (state_32868[(2)]);
var state_32868__$1 = state_32868;
var statearr_32884_34738 = state_32868__$1;
(statearr_32884_34738[(2)] = inst_32864);

(statearr_32884_34738[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32869 === (8))){
var inst_32845 = (state_32868[(7)]);
var inst_32855 = cljs.core.next(inst_32845);
var inst_32845__$1 = inst_32855;
var state_32868__$1 = (function (){var statearr_32885 = state_32868;
(statearr_32885[(7)] = inst_32845__$1);

return statearr_32885;
})();
var statearr_32886_34749 = state_32868__$1;
(statearr_32886_34749[(2)] = null);

(statearr_32886_34749[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__31874__auto__ = null;
var cljs$core$async$state_machine__31874__auto____0 = (function (){
var statearr_32888 = [null,null,null,null,null,null,null,null];
(statearr_32888[(0)] = cljs$core$async$state_machine__31874__auto__);

(statearr_32888[(1)] = (1));

return statearr_32888;
});
var cljs$core$async$state_machine__31874__auto____1 = (function (state_32868){
while(true){
var ret_value__31875__auto__ = (function (){try{while(true){
var result__31876__auto__ = switch__31873__auto__(state_32868);
if(cljs.core.keyword_identical_QMARK_(result__31876__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__31876__auto__;
}
break;
}
}catch (e32889){var ex__31877__auto__ = e32889;
var statearr_32890_34750 = state_32868;
(statearr_32890_34750[(2)] = ex__31877__auto__);


if(cljs.core.seq((state_32868[(4)]))){
var statearr_32891_34751 = state_32868;
(statearr_32891_34751[(1)] = cljs.core.first((state_32868[(4)])));

} else {
throw ex__31877__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__31875__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__34753 = state_32868;
state_32868 = G__34753;
continue;
} else {
return ret_value__31875__auto__;
}
break;
}
});
cljs$core$async$state_machine__31874__auto__ = function(state_32868){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__31874__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__31874__auto____1.call(this,state_32868);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__31874__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__31874__auto____0;
cljs$core$async$state_machine__31874__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__31874__auto____1;
return cljs$core$async$state_machine__31874__auto__;
})()
})();
var state__32012__auto__ = (function (){var statearr_32892 = f__32011__auto__();
(statearr_32892[(6)] = c__32010__auto__);

return statearr_32892;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__32012__auto__);
}));

return c__32010__auto__;
}));

(cljs.core.async.onto_chan_BANG_.cljs$lang$maxFixedArity = 3);

/**
 * Creates and returns a channel which contains the contents of coll,
 *   closing when exhausted.
 */
cljs.core.async.to_chan_BANG_ = (function cljs$core$async$to_chan_BANG_(coll){
var ch = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(cljs.core.bounded_count((100),coll));
cljs.core.async.onto_chan_BANG_.cljs$core$IFn$_invoke$arity$2(ch,coll);

return ch;
});
/**
 * Deprecated - use onto-chan!
 */
cljs.core.async.onto_chan = (function cljs$core$async$onto_chan(var_args){
var G__32895 = arguments.length;
switch (G__32895) {
case 2:
return cljs.core.async.onto_chan.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.onto_chan.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.onto_chan.cljs$core$IFn$_invoke$arity$2 = (function (ch,coll){
return cljs.core.async.onto_chan_BANG_.cljs$core$IFn$_invoke$arity$3(ch,coll,true);
}));

(cljs.core.async.onto_chan.cljs$core$IFn$_invoke$arity$3 = (function (ch,coll,close_QMARK_){
return cljs.core.async.onto_chan_BANG_.cljs$core$IFn$_invoke$arity$3(ch,coll,close_QMARK_);
}));

(cljs.core.async.onto_chan.cljs$lang$maxFixedArity = 3);

/**
 * Deprecated - use to-chan!
 */
cljs.core.async.to_chan = (function cljs$core$async$to_chan(coll){
return cljs.core.async.to_chan_BANG_(coll);
});

/**
 * @interface
 */
cljs.core.async.Mux = function(){};

var cljs$core$async$Mux$muxch_STAR_$dyn_34757 = (function (_){
var x__4550__auto__ = (((_ == null))?null:_);
var m__4551__auto__ = (cljs.core.async.muxch_STAR_[goog.typeOf(x__4550__auto__)]);
if((!((m__4551__auto__ == null)))){
return (m__4551__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4551__auto__.cljs$core$IFn$_invoke$arity$1(_) : m__4551__auto__.call(null,_));
} else {
var m__4549__auto__ = (cljs.core.async.muxch_STAR_["_"]);
if((!((m__4549__auto__ == null)))){
return (m__4549__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4549__auto__.cljs$core$IFn$_invoke$arity$1(_) : m__4549__auto__.call(null,_));
} else {
throw cljs.core.missing_protocol("Mux.muxch*",_);
}
}
});
cljs.core.async.muxch_STAR_ = (function cljs$core$async$muxch_STAR_(_){
if((((!((_ == null)))) && ((!((_.cljs$core$async$Mux$muxch_STAR_$arity$1 == null)))))){
return _.cljs$core$async$Mux$muxch_STAR_$arity$1(_);
} else {
return cljs$core$async$Mux$muxch_STAR_$dyn_34757(_);
}
});


/**
 * @interface
 */
cljs.core.async.Mult = function(){};

var cljs$core$async$Mult$tap_STAR_$dyn_34761 = (function (m,ch,close_QMARK_){
var x__4550__auto__ = (((m == null))?null:m);
var m__4551__auto__ = (cljs.core.async.tap_STAR_[goog.typeOf(x__4550__auto__)]);
if((!((m__4551__auto__ == null)))){
return (m__4551__auto__.cljs$core$IFn$_invoke$arity$3 ? m__4551__auto__.cljs$core$IFn$_invoke$arity$3(m,ch,close_QMARK_) : m__4551__auto__.call(null,m,ch,close_QMARK_));
} else {
var m__4549__auto__ = (cljs.core.async.tap_STAR_["_"]);
if((!((m__4549__auto__ == null)))){
return (m__4549__auto__.cljs$core$IFn$_invoke$arity$3 ? m__4549__auto__.cljs$core$IFn$_invoke$arity$3(m,ch,close_QMARK_) : m__4549__auto__.call(null,m,ch,close_QMARK_));
} else {
throw cljs.core.missing_protocol("Mult.tap*",m);
}
}
});
cljs.core.async.tap_STAR_ = (function cljs$core$async$tap_STAR_(m,ch,close_QMARK_){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mult$tap_STAR_$arity$3 == null)))))){
return m.cljs$core$async$Mult$tap_STAR_$arity$3(m,ch,close_QMARK_);
} else {
return cljs$core$async$Mult$tap_STAR_$dyn_34761(m,ch,close_QMARK_);
}
});

var cljs$core$async$Mult$untap_STAR_$dyn_34762 = (function (m,ch){
var x__4550__auto__ = (((m == null))?null:m);
var m__4551__auto__ = (cljs.core.async.untap_STAR_[goog.typeOf(x__4550__auto__)]);
if((!((m__4551__auto__ == null)))){
return (m__4551__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4551__auto__.cljs$core$IFn$_invoke$arity$2(m,ch) : m__4551__auto__.call(null,m,ch));
} else {
var m__4549__auto__ = (cljs.core.async.untap_STAR_["_"]);
if((!((m__4549__auto__ == null)))){
return (m__4549__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4549__auto__.cljs$core$IFn$_invoke$arity$2(m,ch) : m__4549__auto__.call(null,m,ch));
} else {
throw cljs.core.missing_protocol("Mult.untap*",m);
}
}
});
cljs.core.async.untap_STAR_ = (function cljs$core$async$untap_STAR_(m,ch){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mult$untap_STAR_$arity$2 == null)))))){
return m.cljs$core$async$Mult$untap_STAR_$arity$2(m,ch);
} else {
return cljs$core$async$Mult$untap_STAR_$dyn_34762(m,ch);
}
});

var cljs$core$async$Mult$untap_all_STAR_$dyn_34763 = (function (m){
var x__4550__auto__ = (((m == null))?null:m);
var m__4551__auto__ = (cljs.core.async.untap_all_STAR_[goog.typeOf(x__4550__auto__)]);
if((!((m__4551__auto__ == null)))){
return (m__4551__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4551__auto__.cljs$core$IFn$_invoke$arity$1(m) : m__4551__auto__.call(null,m));
} else {
var m__4549__auto__ = (cljs.core.async.untap_all_STAR_["_"]);
if((!((m__4549__auto__ == null)))){
return (m__4549__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4549__auto__.cljs$core$IFn$_invoke$arity$1(m) : m__4549__auto__.call(null,m));
} else {
throw cljs.core.missing_protocol("Mult.untap-all*",m);
}
}
});
cljs.core.async.untap_all_STAR_ = (function cljs$core$async$untap_all_STAR_(m){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mult$untap_all_STAR_$arity$1 == null)))))){
return m.cljs$core$async$Mult$untap_all_STAR_$arity$1(m);
} else {
return cljs$core$async$Mult$untap_all_STAR_$dyn_34763(m);
}
});

/**
 * Creates and returns a mult(iple) of the supplied channel. Channels
 *   containing copies of the channel can be created with 'tap', and
 *   detached with 'untap'.
 * 
 *   Each item is distributed to all taps in parallel and synchronously,
 *   i.e. each tap must accept before the next item is distributed. Use
 *   buffering/windowing to prevent slow taps from holding up the mult.
 * 
 *   Items received when there are no taps get dropped.
 * 
 *   If a tap puts to a closed channel, it will be removed from the mult.
 */
cljs.core.async.mult = (function cljs$core$async$mult(ch){
var cs = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.PersistentArrayMap.EMPTY);
var m = (function (){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async32909 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.Mult}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.async.Mux}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async32909 = (function (ch,cs,meta32910){
this.ch = ch;
this.cs = cs;
this.meta32910 = meta32910;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(cljs.core.async.t_cljs$core$async32909.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_32911,meta32910__$1){
var self__ = this;
var _32911__$1 = this;
return (new cljs.core.async.t_cljs$core$async32909(self__.ch,self__.cs,meta32910__$1));
}));

(cljs.core.async.t_cljs$core$async32909.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_32911){
var self__ = this;
var _32911__$1 = this;
return self__.meta32910;
}));

(cljs.core.async.t_cljs$core$async32909.prototype.cljs$core$async$Mux$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async32909.prototype.cljs$core$async$Mux$muxch_STAR_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return self__.ch;
}));

(cljs.core.async.t_cljs$core$async32909.prototype.cljs$core$async$Mult$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async32909.prototype.cljs$core$async$Mult$tap_STAR_$arity$3 = (function (_,ch__$1,close_QMARK_){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$4(self__.cs,cljs.core.assoc,ch__$1,close_QMARK_);

return null;
}));

(cljs.core.async.t_cljs$core$async32909.prototype.cljs$core$async$Mult$untap_STAR_$arity$2 = (function (_,ch__$1){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(self__.cs,cljs.core.dissoc,ch__$1);

return null;
}));

(cljs.core.async.t_cljs$core$async32909.prototype.cljs$core$async$Mult$untap_all_STAR_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
cljs.core.reset_BANG_(self__.cs,cljs.core.PersistentArrayMap.EMPTY);

return null;
}));

(cljs.core.async.t_cljs$core$async32909.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"cs","cs",-117024463,null),new cljs.core.Symbol(null,"meta32910","meta32910",-550998729,null)], null);
}));

(cljs.core.async.t_cljs$core$async32909.cljs$lang$type = true);

(cljs.core.async.t_cljs$core$async32909.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async32909");

(cljs.core.async.t_cljs$core$async32909.cljs$lang$ctorPrWriter = (function (this__4491__auto__,writer__4492__auto__,opt__4493__auto__){
return cljs.core._write(writer__4492__auto__,"cljs.core.async/t_cljs$core$async32909");
}));

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async32909.
 */
cljs.core.async.__GT_t_cljs$core$async32909 = (function cljs$core$async$mult_$___GT_t_cljs$core$async32909(ch__$1,cs__$1,meta32910){
return (new cljs.core.async.t_cljs$core$async32909(ch__$1,cs__$1,meta32910));
});

}

return (new cljs.core.async.t_cljs$core$async32909(ch,cs,cljs.core.PersistentArrayMap.EMPTY));
})()
;
var dchan = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
var dctr = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(null);
var done = (function (_){
if((cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$2(dctr,cljs.core.dec) === (0))){
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2(dchan,true);
} else {
return null;
}
});
var c__32010__auto___34766 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__32011__auto__ = (function (){var switch__31873__auto__ = (function (state_33106){
var state_val_33107 = (state_33106[(1)]);
if((state_val_33107 === (7))){
var inst_33099 = (state_33106[(2)]);
var state_33106__$1 = state_33106;
var statearr_33114_34767 = state_33106__$1;
(statearr_33114_34767[(2)] = inst_33099);

(statearr_33114_34767[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33107 === (20))){
var inst_32963 = (state_33106[(7)]);
var inst_33000 = cljs.core.first(inst_32963);
var inst_33001 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_33000,(0),null);
var inst_33002 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_33000,(1),null);
var state_33106__$1 = (function (){var statearr_33117 = state_33106;
(statearr_33117[(8)] = inst_33001);

return statearr_33117;
})();
if(cljs.core.truth_(inst_33002)){
var statearr_33121_34768 = state_33106__$1;
(statearr_33121_34768[(1)] = (22));

} else {
var statearr_33122_34769 = state_33106__$1;
(statearr_33122_34769[(1)] = (23));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33107 === (27))){
var inst_33046 = (state_33106[(9)]);
var inst_32930 = (state_33106[(10)]);
var inst_33040 = (state_33106[(11)]);
var inst_33038 = (state_33106[(12)]);
var inst_33046__$1 = cljs.core._nth(inst_33038,inst_33040);
var inst_33047 = cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$3(inst_33046__$1,inst_32930,done);
var state_33106__$1 = (function (){var statearr_33134 = state_33106;
(statearr_33134[(9)] = inst_33046__$1);

return statearr_33134;
})();
if(cljs.core.truth_(inst_33047)){
var statearr_33135_34770 = state_33106__$1;
(statearr_33135_34770[(1)] = (30));

} else {
var statearr_33136_34771 = state_33106__$1;
(statearr_33136_34771[(1)] = (31));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33107 === (1))){
var state_33106__$1 = state_33106;
var statearr_33137_34772 = state_33106__$1;
(statearr_33137_34772[(2)] = null);

(statearr_33137_34772[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33107 === (24))){
var inst_32963 = (state_33106[(7)]);
var inst_33007 = (state_33106[(2)]);
var inst_33008 = cljs.core.next(inst_32963);
var inst_32939 = inst_33008;
var inst_32940 = null;
var inst_32941 = (0);
var inst_32942 = (0);
var state_33106__$1 = (function (){var statearr_33139 = state_33106;
(statearr_33139[(13)] = inst_32941);

(statearr_33139[(14)] = inst_32942);

(statearr_33139[(15)] = inst_32939);

(statearr_33139[(16)] = inst_32940);

(statearr_33139[(17)] = inst_33007);

return statearr_33139;
})();
var statearr_33140_34773 = state_33106__$1;
(statearr_33140_34773[(2)] = null);

(statearr_33140_34773[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33107 === (39))){
var state_33106__$1 = state_33106;
var statearr_33144_34774 = state_33106__$1;
(statearr_33144_34774[(2)] = null);

(statearr_33144_34774[(1)] = (41));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33107 === (4))){
var inst_32930 = (state_33106[(10)]);
var inst_32930__$1 = (state_33106[(2)]);
var inst_32931 = (inst_32930__$1 == null);
var state_33106__$1 = (function (){var statearr_33148 = state_33106;
(statearr_33148[(10)] = inst_32930__$1);

return statearr_33148;
})();
if(cljs.core.truth_(inst_32931)){
var statearr_33149_34775 = state_33106__$1;
(statearr_33149_34775[(1)] = (5));

} else {
var statearr_33150_34776 = state_33106__$1;
(statearr_33150_34776[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33107 === (15))){
var inst_32941 = (state_33106[(13)]);
var inst_32942 = (state_33106[(14)]);
var inst_32939 = (state_33106[(15)]);
var inst_32940 = (state_33106[(16)]);
var inst_32959 = (state_33106[(2)]);
var inst_32960 = (inst_32942 + (1));
var tmp33141 = inst_32941;
var tmp33142 = inst_32939;
var tmp33143 = inst_32940;
var inst_32939__$1 = tmp33142;
var inst_32940__$1 = tmp33143;
var inst_32941__$1 = tmp33141;
var inst_32942__$1 = inst_32960;
var state_33106__$1 = (function (){var statearr_33152 = state_33106;
(statearr_33152[(13)] = inst_32941__$1);

(statearr_33152[(14)] = inst_32942__$1);

(statearr_33152[(18)] = inst_32959);

(statearr_33152[(15)] = inst_32939__$1);

(statearr_33152[(16)] = inst_32940__$1);

return statearr_33152;
})();
var statearr_33154_34777 = state_33106__$1;
(statearr_33154_34777[(2)] = null);

(statearr_33154_34777[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33107 === (21))){
var inst_33011 = (state_33106[(2)]);
var state_33106__$1 = state_33106;
var statearr_33158_34778 = state_33106__$1;
(statearr_33158_34778[(2)] = inst_33011);

(statearr_33158_34778[(1)] = (18));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33107 === (31))){
var inst_33046 = (state_33106[(9)]);
var inst_33050 = m.cljs$core$async$Mult$untap_STAR_$arity$2(null,inst_33046);
var state_33106__$1 = state_33106;
var statearr_33159_34780 = state_33106__$1;
(statearr_33159_34780[(2)] = inst_33050);

(statearr_33159_34780[(1)] = (32));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33107 === (32))){
var inst_33040 = (state_33106[(11)]);
var inst_33039 = (state_33106[(19)]);
var inst_33038 = (state_33106[(12)]);
var inst_33037 = (state_33106[(20)]);
var inst_33052 = (state_33106[(2)]);
var inst_33053 = (inst_33040 + (1));
var tmp33155 = inst_33039;
var tmp33156 = inst_33038;
var tmp33157 = inst_33037;
var inst_33037__$1 = tmp33157;
var inst_33038__$1 = tmp33156;
var inst_33039__$1 = tmp33155;
var inst_33040__$1 = inst_33053;
var state_33106__$1 = (function (){var statearr_33160 = state_33106;
(statearr_33160[(11)] = inst_33040__$1);

(statearr_33160[(19)] = inst_33039__$1);

(statearr_33160[(12)] = inst_33038__$1);

(statearr_33160[(20)] = inst_33037__$1);

(statearr_33160[(21)] = inst_33052);

return statearr_33160;
})();
var statearr_33161_34782 = state_33106__$1;
(statearr_33161_34782[(2)] = null);

(statearr_33161_34782[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33107 === (40))){
var inst_33066 = (state_33106[(22)]);
var inst_33074 = m.cljs$core$async$Mult$untap_STAR_$arity$2(null,inst_33066);
var state_33106__$1 = state_33106;
var statearr_33162_34783 = state_33106__$1;
(statearr_33162_34783[(2)] = inst_33074);

(statearr_33162_34783[(1)] = (41));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33107 === (33))){
var inst_33056 = (state_33106[(23)]);
var inst_33059 = cljs.core.chunked_seq_QMARK_(inst_33056);
var state_33106__$1 = state_33106;
if(inst_33059){
var statearr_33165_34784 = state_33106__$1;
(statearr_33165_34784[(1)] = (36));

} else {
var statearr_33166_34785 = state_33106__$1;
(statearr_33166_34785[(1)] = (37));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33107 === (13))){
var inst_32953 = (state_33106[(24)]);
var inst_32956 = cljs.core.async.close_BANG_(inst_32953);
var state_33106__$1 = state_33106;
var statearr_33167_34786 = state_33106__$1;
(statearr_33167_34786[(2)] = inst_32956);

(statearr_33167_34786[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33107 === (22))){
var inst_33001 = (state_33106[(8)]);
var inst_33004 = cljs.core.async.close_BANG_(inst_33001);
var state_33106__$1 = state_33106;
var statearr_33168_34787 = state_33106__$1;
(statearr_33168_34787[(2)] = inst_33004);

(statearr_33168_34787[(1)] = (24));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33107 === (36))){
var inst_33056 = (state_33106[(23)]);
var inst_33061 = cljs.core.chunk_first(inst_33056);
var inst_33062 = cljs.core.chunk_rest(inst_33056);
var inst_33063 = cljs.core.count(inst_33061);
var inst_33037 = inst_33062;
var inst_33038 = inst_33061;
var inst_33039 = inst_33063;
var inst_33040 = (0);
var state_33106__$1 = (function (){var statearr_33170 = state_33106;
(statearr_33170[(11)] = inst_33040);

(statearr_33170[(19)] = inst_33039);

(statearr_33170[(12)] = inst_33038);

(statearr_33170[(20)] = inst_33037);

return statearr_33170;
})();
var statearr_33171_34789 = state_33106__$1;
(statearr_33171_34789[(2)] = null);

(statearr_33171_34789[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33107 === (41))){
var inst_33056 = (state_33106[(23)]);
var inst_33076 = (state_33106[(2)]);
var inst_33077 = cljs.core.next(inst_33056);
var inst_33037 = inst_33077;
var inst_33038 = null;
var inst_33039 = (0);
var inst_33040 = (0);
var state_33106__$1 = (function (){var statearr_33173 = state_33106;
(statearr_33173[(11)] = inst_33040);

(statearr_33173[(25)] = inst_33076);

(statearr_33173[(19)] = inst_33039);

(statearr_33173[(12)] = inst_33038);

(statearr_33173[(20)] = inst_33037);

return statearr_33173;
})();
var statearr_33174_34790 = state_33106__$1;
(statearr_33174_34790[(2)] = null);

(statearr_33174_34790[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33107 === (43))){
var state_33106__$1 = state_33106;
var statearr_33177_34791 = state_33106__$1;
(statearr_33177_34791[(2)] = null);

(statearr_33177_34791[(1)] = (44));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33107 === (29))){
var inst_33085 = (state_33106[(2)]);
var state_33106__$1 = state_33106;
var statearr_33180_34792 = state_33106__$1;
(statearr_33180_34792[(2)] = inst_33085);

(statearr_33180_34792[(1)] = (26));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33107 === (44))){
var inst_33096 = (state_33106[(2)]);
var state_33106__$1 = (function (){var statearr_33181 = state_33106;
(statearr_33181[(26)] = inst_33096);

return statearr_33181;
})();
var statearr_33182_34793 = state_33106__$1;
(statearr_33182_34793[(2)] = null);

(statearr_33182_34793[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33107 === (6))){
var inst_33028 = (state_33106[(27)]);
var inst_33027 = cljs.core.deref(cs);
var inst_33028__$1 = cljs.core.keys(inst_33027);
var inst_33029 = cljs.core.count(inst_33028__$1);
var inst_33030 = cljs.core.reset_BANG_(dctr,inst_33029);
var inst_33036 = cljs.core.seq(inst_33028__$1);
var inst_33037 = inst_33036;
var inst_33038 = null;
var inst_33039 = (0);
var inst_33040 = (0);
var state_33106__$1 = (function (){var statearr_33185 = state_33106;
(statearr_33185[(11)] = inst_33040);

(statearr_33185[(27)] = inst_33028__$1);

(statearr_33185[(19)] = inst_33039);

(statearr_33185[(12)] = inst_33038);

(statearr_33185[(20)] = inst_33037);

(statearr_33185[(28)] = inst_33030);

return statearr_33185;
})();
var statearr_33186_34794 = state_33106__$1;
(statearr_33186_34794[(2)] = null);

(statearr_33186_34794[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33107 === (28))){
var inst_33056 = (state_33106[(23)]);
var inst_33037 = (state_33106[(20)]);
var inst_33056__$1 = cljs.core.seq(inst_33037);
var state_33106__$1 = (function (){var statearr_33188 = state_33106;
(statearr_33188[(23)] = inst_33056__$1);

return statearr_33188;
})();
if(inst_33056__$1){
var statearr_33189_34795 = state_33106__$1;
(statearr_33189_34795[(1)] = (33));

} else {
var statearr_33194_34796 = state_33106__$1;
(statearr_33194_34796[(1)] = (34));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33107 === (25))){
var inst_33040 = (state_33106[(11)]);
var inst_33039 = (state_33106[(19)]);
var inst_33043 = (inst_33040 < inst_33039);
var inst_33044 = inst_33043;
var state_33106__$1 = state_33106;
if(cljs.core.truth_(inst_33044)){
var statearr_33199_34797 = state_33106__$1;
(statearr_33199_34797[(1)] = (27));

} else {
var statearr_33204_34798 = state_33106__$1;
(statearr_33204_34798[(1)] = (28));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33107 === (34))){
var state_33106__$1 = state_33106;
var statearr_33205_34808 = state_33106__$1;
(statearr_33205_34808[(2)] = null);

(statearr_33205_34808[(1)] = (35));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33107 === (17))){
var state_33106__$1 = state_33106;
var statearr_33206_34809 = state_33106__$1;
(statearr_33206_34809[(2)] = null);

(statearr_33206_34809[(1)] = (18));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33107 === (3))){
var inst_33101 = (state_33106[(2)]);
var state_33106__$1 = state_33106;
return cljs.core.async.impl.ioc_helpers.return_chan(state_33106__$1,inst_33101);
} else {
if((state_val_33107 === (12))){
var inst_33016 = (state_33106[(2)]);
var state_33106__$1 = state_33106;
var statearr_33207_34810 = state_33106__$1;
(statearr_33207_34810[(2)] = inst_33016);

(statearr_33207_34810[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33107 === (2))){
var state_33106__$1 = state_33106;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_33106__$1,(4),ch);
} else {
if((state_val_33107 === (23))){
var state_33106__$1 = state_33106;
var statearr_33215_34811 = state_33106__$1;
(statearr_33215_34811[(2)] = null);

(statearr_33215_34811[(1)] = (24));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33107 === (35))){
var inst_33083 = (state_33106[(2)]);
var state_33106__$1 = state_33106;
var statearr_33216_34812 = state_33106__$1;
(statearr_33216_34812[(2)] = inst_33083);

(statearr_33216_34812[(1)] = (29));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33107 === (19))){
var inst_32963 = (state_33106[(7)]);
var inst_32989 = cljs.core.chunk_first(inst_32963);
var inst_32990 = cljs.core.chunk_rest(inst_32963);
var inst_32991 = cljs.core.count(inst_32989);
var inst_32939 = inst_32990;
var inst_32940 = inst_32989;
var inst_32941 = inst_32991;
var inst_32942 = (0);
var state_33106__$1 = (function (){var statearr_33217 = state_33106;
(statearr_33217[(13)] = inst_32941);

(statearr_33217[(14)] = inst_32942);

(statearr_33217[(15)] = inst_32939);

(statearr_33217[(16)] = inst_32940);

return statearr_33217;
})();
var statearr_33218_34813 = state_33106__$1;
(statearr_33218_34813[(2)] = null);

(statearr_33218_34813[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33107 === (11))){
var inst_32963 = (state_33106[(7)]);
var inst_32939 = (state_33106[(15)]);
var inst_32963__$1 = cljs.core.seq(inst_32939);
var state_33106__$1 = (function (){var statearr_33220 = state_33106;
(statearr_33220[(7)] = inst_32963__$1);

return statearr_33220;
})();
if(inst_32963__$1){
var statearr_33221_34814 = state_33106__$1;
(statearr_33221_34814[(1)] = (16));

} else {
var statearr_33222_34815 = state_33106__$1;
(statearr_33222_34815[(1)] = (17));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33107 === (9))){
var inst_33018 = (state_33106[(2)]);
var state_33106__$1 = state_33106;
var statearr_33223_34816 = state_33106__$1;
(statearr_33223_34816[(2)] = inst_33018);

(statearr_33223_34816[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33107 === (5))){
var inst_32937 = cljs.core.deref(cs);
var inst_32938 = cljs.core.seq(inst_32937);
var inst_32939 = inst_32938;
var inst_32940 = null;
var inst_32941 = (0);
var inst_32942 = (0);
var state_33106__$1 = (function (){var statearr_33224 = state_33106;
(statearr_33224[(13)] = inst_32941);

(statearr_33224[(14)] = inst_32942);

(statearr_33224[(15)] = inst_32939);

(statearr_33224[(16)] = inst_32940);

return statearr_33224;
})();
var statearr_33225_34818 = state_33106__$1;
(statearr_33225_34818[(2)] = null);

(statearr_33225_34818[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33107 === (14))){
var state_33106__$1 = state_33106;
var statearr_33226_34819 = state_33106__$1;
(statearr_33226_34819[(2)] = null);

(statearr_33226_34819[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33107 === (45))){
var inst_33093 = (state_33106[(2)]);
var state_33106__$1 = state_33106;
var statearr_33227_34820 = state_33106__$1;
(statearr_33227_34820[(2)] = inst_33093);

(statearr_33227_34820[(1)] = (44));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33107 === (26))){
var inst_33028 = (state_33106[(27)]);
var inst_33087 = (state_33106[(2)]);
var inst_33089 = cljs.core.seq(inst_33028);
var state_33106__$1 = (function (){var statearr_33229 = state_33106;
(statearr_33229[(29)] = inst_33087);

return statearr_33229;
})();
if(inst_33089){
var statearr_33230_34821 = state_33106__$1;
(statearr_33230_34821[(1)] = (42));

} else {
var statearr_33231_34822 = state_33106__$1;
(statearr_33231_34822[(1)] = (43));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33107 === (16))){
var inst_32963 = (state_33106[(7)]);
var inst_32987 = cljs.core.chunked_seq_QMARK_(inst_32963);
var state_33106__$1 = state_33106;
if(inst_32987){
var statearr_33232_34823 = state_33106__$1;
(statearr_33232_34823[(1)] = (19));

} else {
var statearr_33233_34824 = state_33106__$1;
(statearr_33233_34824[(1)] = (20));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33107 === (38))){
var inst_33080 = (state_33106[(2)]);
var state_33106__$1 = state_33106;
var statearr_33234_34825 = state_33106__$1;
(statearr_33234_34825[(2)] = inst_33080);

(statearr_33234_34825[(1)] = (35));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33107 === (30))){
var state_33106__$1 = state_33106;
var statearr_33235_34826 = state_33106__$1;
(statearr_33235_34826[(2)] = null);

(statearr_33235_34826[(1)] = (32));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33107 === (10))){
var inst_32942 = (state_33106[(14)]);
var inst_32940 = (state_33106[(16)]);
var inst_32952 = cljs.core._nth(inst_32940,inst_32942);
var inst_32953 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_32952,(0),null);
var inst_32954 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_32952,(1),null);
var state_33106__$1 = (function (){var statearr_33236 = state_33106;
(statearr_33236[(24)] = inst_32953);

return statearr_33236;
})();
if(cljs.core.truth_(inst_32954)){
var statearr_33237_34827 = state_33106__$1;
(statearr_33237_34827[(1)] = (13));

} else {
var statearr_33238_34828 = state_33106__$1;
(statearr_33238_34828[(1)] = (14));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33107 === (18))){
var inst_33014 = (state_33106[(2)]);
var state_33106__$1 = state_33106;
var statearr_33240_34829 = state_33106__$1;
(statearr_33240_34829[(2)] = inst_33014);

(statearr_33240_34829[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33107 === (42))){
var state_33106__$1 = state_33106;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_33106__$1,(45),dchan);
} else {
if((state_val_33107 === (37))){
var inst_32930 = (state_33106[(10)]);
var inst_33056 = (state_33106[(23)]);
var inst_33066 = (state_33106[(22)]);
var inst_33066__$1 = cljs.core.first(inst_33056);
var inst_33071 = cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$3(inst_33066__$1,inst_32930,done);
var state_33106__$1 = (function (){var statearr_33241 = state_33106;
(statearr_33241[(22)] = inst_33066__$1);

return statearr_33241;
})();
if(cljs.core.truth_(inst_33071)){
var statearr_33242_34830 = state_33106__$1;
(statearr_33242_34830[(1)] = (39));

} else {
var statearr_33243_34831 = state_33106__$1;
(statearr_33243_34831[(1)] = (40));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33107 === (8))){
var inst_32941 = (state_33106[(13)]);
var inst_32942 = (state_33106[(14)]);
var inst_32944 = (inst_32942 < inst_32941);
var inst_32945 = inst_32944;
var state_33106__$1 = state_33106;
if(cljs.core.truth_(inst_32945)){
var statearr_33244_34832 = state_33106__$1;
(statearr_33244_34832[(1)] = (10));

} else {
var statearr_33245_34833 = state_33106__$1;
(statearr_33245_34833[(1)] = (11));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$mult_$_state_machine__31874__auto__ = null;
var cljs$core$async$mult_$_state_machine__31874__auto____0 = (function (){
var statearr_33247 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_33247[(0)] = cljs$core$async$mult_$_state_machine__31874__auto__);

(statearr_33247[(1)] = (1));

return statearr_33247;
});
var cljs$core$async$mult_$_state_machine__31874__auto____1 = (function (state_33106){
while(true){
var ret_value__31875__auto__ = (function (){try{while(true){
var result__31876__auto__ = switch__31873__auto__(state_33106);
if(cljs.core.keyword_identical_QMARK_(result__31876__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__31876__auto__;
}
break;
}
}catch (e33248){var ex__31877__auto__ = e33248;
var statearr_33249_34834 = state_33106;
(statearr_33249_34834[(2)] = ex__31877__auto__);


if(cljs.core.seq((state_33106[(4)]))){
var statearr_33250_34835 = state_33106;
(statearr_33250_34835[(1)] = cljs.core.first((state_33106[(4)])));

} else {
throw ex__31877__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__31875__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__34836 = state_33106;
state_33106 = G__34836;
continue;
} else {
return ret_value__31875__auto__;
}
break;
}
});
cljs$core$async$mult_$_state_machine__31874__auto__ = function(state_33106){
switch(arguments.length){
case 0:
return cljs$core$async$mult_$_state_machine__31874__auto____0.call(this);
case 1:
return cljs$core$async$mult_$_state_machine__31874__auto____1.call(this,state_33106);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$mult_$_state_machine__31874__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$mult_$_state_machine__31874__auto____0;
cljs$core$async$mult_$_state_machine__31874__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$mult_$_state_machine__31874__auto____1;
return cljs$core$async$mult_$_state_machine__31874__auto__;
})()
})();
var state__32012__auto__ = (function (){var statearr_33251 = f__32011__auto__();
(statearr_33251[(6)] = c__32010__auto___34766);

return statearr_33251;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__32012__auto__);
}));


return m;
});
/**
 * Copies the mult source onto the supplied channel.
 * 
 *   By default the channel will be closed when the source closes,
 *   but can be determined by the close? parameter.
 */
cljs.core.async.tap = (function cljs$core$async$tap(var_args){
var G__33254 = arguments.length;
switch (G__33254) {
case 2:
return cljs.core.async.tap.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.tap.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.tap.cljs$core$IFn$_invoke$arity$2 = (function (mult,ch){
return cljs.core.async.tap.cljs$core$IFn$_invoke$arity$3(mult,ch,true);
}));

(cljs.core.async.tap.cljs$core$IFn$_invoke$arity$3 = (function (mult,ch,close_QMARK_){
cljs.core.async.tap_STAR_(mult,ch,close_QMARK_);

return ch;
}));

(cljs.core.async.tap.cljs$lang$maxFixedArity = 3);

/**
 * Disconnects a target channel from a mult
 */
cljs.core.async.untap = (function cljs$core$async$untap(mult,ch){
return cljs.core.async.untap_STAR_(mult,ch);
});
/**
 * Disconnects all target channels from a mult
 */
cljs.core.async.untap_all = (function cljs$core$async$untap_all(mult){
return cljs.core.async.untap_all_STAR_(mult);
});

/**
 * @interface
 */
cljs.core.async.Mix = function(){};

var cljs$core$async$Mix$admix_STAR_$dyn_34838 = (function (m,ch){
var x__4550__auto__ = (((m == null))?null:m);
var m__4551__auto__ = (cljs.core.async.admix_STAR_[goog.typeOf(x__4550__auto__)]);
if((!((m__4551__auto__ == null)))){
return (m__4551__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4551__auto__.cljs$core$IFn$_invoke$arity$2(m,ch) : m__4551__auto__.call(null,m,ch));
} else {
var m__4549__auto__ = (cljs.core.async.admix_STAR_["_"]);
if((!((m__4549__auto__ == null)))){
return (m__4549__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4549__auto__.cljs$core$IFn$_invoke$arity$2(m,ch) : m__4549__auto__.call(null,m,ch));
} else {
throw cljs.core.missing_protocol("Mix.admix*",m);
}
}
});
cljs.core.async.admix_STAR_ = (function cljs$core$async$admix_STAR_(m,ch){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mix$admix_STAR_$arity$2 == null)))))){
return m.cljs$core$async$Mix$admix_STAR_$arity$2(m,ch);
} else {
return cljs$core$async$Mix$admix_STAR_$dyn_34838(m,ch);
}
});

var cljs$core$async$Mix$unmix_STAR_$dyn_34840 = (function (m,ch){
var x__4550__auto__ = (((m == null))?null:m);
var m__4551__auto__ = (cljs.core.async.unmix_STAR_[goog.typeOf(x__4550__auto__)]);
if((!((m__4551__auto__ == null)))){
return (m__4551__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4551__auto__.cljs$core$IFn$_invoke$arity$2(m,ch) : m__4551__auto__.call(null,m,ch));
} else {
var m__4549__auto__ = (cljs.core.async.unmix_STAR_["_"]);
if((!((m__4549__auto__ == null)))){
return (m__4549__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4549__auto__.cljs$core$IFn$_invoke$arity$2(m,ch) : m__4549__auto__.call(null,m,ch));
} else {
throw cljs.core.missing_protocol("Mix.unmix*",m);
}
}
});
cljs.core.async.unmix_STAR_ = (function cljs$core$async$unmix_STAR_(m,ch){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mix$unmix_STAR_$arity$2 == null)))))){
return m.cljs$core$async$Mix$unmix_STAR_$arity$2(m,ch);
} else {
return cljs$core$async$Mix$unmix_STAR_$dyn_34840(m,ch);
}
});

var cljs$core$async$Mix$unmix_all_STAR_$dyn_34842 = (function (m){
var x__4550__auto__ = (((m == null))?null:m);
var m__4551__auto__ = (cljs.core.async.unmix_all_STAR_[goog.typeOf(x__4550__auto__)]);
if((!((m__4551__auto__ == null)))){
return (m__4551__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4551__auto__.cljs$core$IFn$_invoke$arity$1(m) : m__4551__auto__.call(null,m));
} else {
var m__4549__auto__ = (cljs.core.async.unmix_all_STAR_["_"]);
if((!((m__4549__auto__ == null)))){
return (m__4549__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4549__auto__.cljs$core$IFn$_invoke$arity$1(m) : m__4549__auto__.call(null,m));
} else {
throw cljs.core.missing_protocol("Mix.unmix-all*",m);
}
}
});
cljs.core.async.unmix_all_STAR_ = (function cljs$core$async$unmix_all_STAR_(m){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mix$unmix_all_STAR_$arity$1 == null)))))){
return m.cljs$core$async$Mix$unmix_all_STAR_$arity$1(m);
} else {
return cljs$core$async$Mix$unmix_all_STAR_$dyn_34842(m);
}
});

var cljs$core$async$Mix$toggle_STAR_$dyn_34844 = (function (m,state_map){
var x__4550__auto__ = (((m == null))?null:m);
var m__4551__auto__ = (cljs.core.async.toggle_STAR_[goog.typeOf(x__4550__auto__)]);
if((!((m__4551__auto__ == null)))){
return (m__4551__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4551__auto__.cljs$core$IFn$_invoke$arity$2(m,state_map) : m__4551__auto__.call(null,m,state_map));
} else {
var m__4549__auto__ = (cljs.core.async.toggle_STAR_["_"]);
if((!((m__4549__auto__ == null)))){
return (m__4549__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4549__auto__.cljs$core$IFn$_invoke$arity$2(m,state_map) : m__4549__auto__.call(null,m,state_map));
} else {
throw cljs.core.missing_protocol("Mix.toggle*",m);
}
}
});
cljs.core.async.toggle_STAR_ = (function cljs$core$async$toggle_STAR_(m,state_map){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mix$toggle_STAR_$arity$2 == null)))))){
return m.cljs$core$async$Mix$toggle_STAR_$arity$2(m,state_map);
} else {
return cljs$core$async$Mix$toggle_STAR_$dyn_34844(m,state_map);
}
});

var cljs$core$async$Mix$solo_mode_STAR_$dyn_34846 = (function (m,mode){
var x__4550__auto__ = (((m == null))?null:m);
var m__4551__auto__ = (cljs.core.async.solo_mode_STAR_[goog.typeOf(x__4550__auto__)]);
if((!((m__4551__auto__ == null)))){
return (m__4551__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4551__auto__.cljs$core$IFn$_invoke$arity$2(m,mode) : m__4551__auto__.call(null,m,mode));
} else {
var m__4549__auto__ = (cljs.core.async.solo_mode_STAR_["_"]);
if((!((m__4549__auto__ == null)))){
return (m__4549__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4549__auto__.cljs$core$IFn$_invoke$arity$2(m,mode) : m__4549__auto__.call(null,m,mode));
} else {
throw cljs.core.missing_protocol("Mix.solo-mode*",m);
}
}
});
cljs.core.async.solo_mode_STAR_ = (function cljs$core$async$solo_mode_STAR_(m,mode){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mix$solo_mode_STAR_$arity$2 == null)))))){
return m.cljs$core$async$Mix$solo_mode_STAR_$arity$2(m,mode);
} else {
return cljs$core$async$Mix$solo_mode_STAR_$dyn_34846(m,mode);
}
});

cljs.core.async.ioc_alts_BANG_ = (function cljs$core$async$ioc_alts_BANG_(var_args){
var args__4870__auto__ = [];
var len__4864__auto___34847 = arguments.length;
var i__4865__auto___34848 = (0);
while(true){
if((i__4865__auto___34848 < len__4864__auto___34847)){
args__4870__auto__.push((arguments[i__4865__auto___34848]));

var G__34849 = (i__4865__auto___34848 + (1));
i__4865__auto___34848 = G__34849;
continue;
} else {
}
break;
}

var argseq__4871__auto__ = ((((3) < args__4870__auto__.length))?(new cljs.core.IndexedSeq(args__4870__auto__.slice((3)),(0),null)):null);
return cljs.core.async.ioc_alts_BANG_.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),argseq__4871__auto__);
});

(cljs.core.async.ioc_alts_BANG_.cljs$core$IFn$_invoke$arity$variadic = (function (state,cont_block,ports,p__33264){
var map__33265 = p__33264;
var map__33265__$1 = cljs.core.__destructure_map(map__33265);
var opts = map__33265__$1;
var statearr_33266_34850 = state;
(statearr_33266_34850[(1)] = cont_block);


var temp__5753__auto__ = cljs.core.async.do_alts((function (val){
var statearr_33267_34851 = state;
(statearr_33267_34851[(2)] = val);


return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state);
}),ports,opts);
if(cljs.core.truth_(temp__5753__auto__)){
var cb = temp__5753__auto__;
var statearr_33268_34853 = state;
(statearr_33268_34853[(2)] = cljs.core.deref(cb));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}));

(cljs.core.async.ioc_alts_BANG_.cljs$lang$maxFixedArity = (3));

/** @this {Function} */
(cljs.core.async.ioc_alts_BANG_.cljs$lang$applyTo = (function (seq33259){
var G__33260 = cljs.core.first(seq33259);
var seq33259__$1 = cljs.core.next(seq33259);
var G__33261 = cljs.core.first(seq33259__$1);
var seq33259__$2 = cljs.core.next(seq33259__$1);
var G__33262 = cljs.core.first(seq33259__$2);
var seq33259__$3 = cljs.core.next(seq33259__$2);
var self__4851__auto__ = this;
return self__4851__auto__.cljs$core$IFn$_invoke$arity$variadic(G__33260,G__33261,G__33262,seq33259__$3);
}));

/**
 * Creates and returns a mix of one or more input channels which will
 *   be put on the supplied out channel. Input sources can be added to
 *   the mix with 'admix', and removed with 'unmix'. A mix supports
 *   soloing, muting and pausing multiple inputs atomically using
 *   'toggle', and can solo using either muting or pausing as determined
 *   by 'solo-mode'.
 * 
 *   Each channel can have zero or more boolean modes set via 'toggle':
 * 
 *   :solo - when true, only this (ond other soloed) channel(s) will appear
 *        in the mix output channel. :mute and :pause states of soloed
 *        channels are ignored. If solo-mode is :mute, non-soloed
 *        channels are muted, if :pause, non-soloed channels are
 *        paused.
 * 
 *   :mute - muted channels will have their contents consumed but not included in the mix
 *   :pause - paused channels will not have their contents consumed (and thus also not included in the mix)
 */
cljs.core.async.mix = (function cljs$core$async$mix(out){
var cs = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.PersistentArrayMap.EMPTY);
var solo_modes = new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"pause","pause",-2095325672),null,new cljs.core.Keyword(null,"mute","mute",1151223646),null], null), null);
var attrs = cljs.core.conj.cljs$core$IFn$_invoke$arity$2(solo_modes,new cljs.core.Keyword(null,"solo","solo",-316350075));
var solo_mode = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"mute","mute",1151223646));
var change = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(cljs.core.async.sliding_buffer((1)));
var changed = (function (){
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2(change,true);
});
var pick = (function (attr,chs){
return cljs.core.reduce_kv((function (ret,c,v){
if(cljs.core.truth_((attr.cljs$core$IFn$_invoke$arity$1 ? attr.cljs$core$IFn$_invoke$arity$1(v) : attr.call(null,v)))){
return cljs.core.conj.cljs$core$IFn$_invoke$arity$2(ret,c);
} else {
return ret;
}
}),cljs.core.PersistentHashSet.EMPTY,chs);
});
var calc_state = (function (){
var chs = cljs.core.deref(cs);
var mode = cljs.core.deref(solo_mode);
var solos = pick(new cljs.core.Keyword(null,"solo","solo",-316350075),chs);
var pauses = pick(new cljs.core.Keyword(null,"pause","pause",-2095325672),chs);
return new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"solos","solos",1441458643),solos,new cljs.core.Keyword(null,"mutes","mutes",1068806309),pick(new cljs.core.Keyword(null,"mute","mute",1151223646),chs),new cljs.core.Keyword(null,"reads","reads",-1215067361),cljs.core.conj.cljs$core$IFn$_invoke$arity$2(((((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(mode,new cljs.core.Keyword(null,"pause","pause",-2095325672))) && ((!(cljs.core.empty_QMARK_(solos))))))?cljs.core.vec(solos):cljs.core.vec(cljs.core.remove.cljs$core$IFn$_invoke$arity$2(pauses,cljs.core.keys(chs)))),change)], null);
});
var m = (function (){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async33271 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.async.Mix}
 * @implements {cljs.core.async.Mux}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async33271 = (function (change,solo_mode,pick,cs,calc_state,out,changed,solo_modes,attrs,meta33272){
this.change = change;
this.solo_mode = solo_mode;
this.pick = pick;
this.cs = cs;
this.calc_state = calc_state;
this.out = out;
this.changed = changed;
this.solo_modes = solo_modes;
this.attrs = attrs;
this.meta33272 = meta33272;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(cljs.core.async.t_cljs$core$async33271.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_33273,meta33272__$1){
var self__ = this;
var _33273__$1 = this;
return (new cljs.core.async.t_cljs$core$async33271(self__.change,self__.solo_mode,self__.pick,self__.cs,self__.calc_state,self__.out,self__.changed,self__.solo_modes,self__.attrs,meta33272__$1));
}));

(cljs.core.async.t_cljs$core$async33271.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_33273){
var self__ = this;
var _33273__$1 = this;
return self__.meta33272;
}));

(cljs.core.async.t_cljs$core$async33271.prototype.cljs$core$async$Mux$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async33271.prototype.cljs$core$async$Mux$muxch_STAR_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return self__.out;
}));

(cljs.core.async.t_cljs$core$async33271.prototype.cljs$core$async$Mix$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async33271.prototype.cljs$core$async$Mix$admix_STAR_$arity$2 = (function (_,ch){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$4(self__.cs,cljs.core.assoc,ch,cljs.core.PersistentArrayMap.EMPTY);

return (self__.changed.cljs$core$IFn$_invoke$arity$0 ? self__.changed.cljs$core$IFn$_invoke$arity$0() : self__.changed.call(null));
}));

(cljs.core.async.t_cljs$core$async33271.prototype.cljs$core$async$Mix$unmix_STAR_$arity$2 = (function (_,ch){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(self__.cs,cljs.core.dissoc,ch);

return (self__.changed.cljs$core$IFn$_invoke$arity$0 ? self__.changed.cljs$core$IFn$_invoke$arity$0() : self__.changed.call(null));
}));

(cljs.core.async.t_cljs$core$async33271.prototype.cljs$core$async$Mix$unmix_all_STAR_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
cljs.core.reset_BANG_(self__.cs,cljs.core.PersistentArrayMap.EMPTY);

return (self__.changed.cljs$core$IFn$_invoke$arity$0 ? self__.changed.cljs$core$IFn$_invoke$arity$0() : self__.changed.call(null));
}));

(cljs.core.async.t_cljs$core$async33271.prototype.cljs$core$async$Mix$toggle_STAR_$arity$2 = (function (_,state_map){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(self__.cs,cljs.core.partial.cljs$core$IFn$_invoke$arity$2(cljs.core.merge_with,cljs.core.merge),state_map);

return (self__.changed.cljs$core$IFn$_invoke$arity$0 ? self__.changed.cljs$core$IFn$_invoke$arity$0() : self__.changed.call(null));
}));

(cljs.core.async.t_cljs$core$async33271.prototype.cljs$core$async$Mix$solo_mode_STAR_$arity$2 = (function (_,mode){
var self__ = this;
var ___$1 = this;
if(cljs.core.truth_((self__.solo_modes.cljs$core$IFn$_invoke$arity$1 ? self__.solo_modes.cljs$core$IFn$_invoke$arity$1(mode) : self__.solo_modes.call(null,mode)))){
} else {
throw (new Error(["Assert failed: ",["mode must be one of: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(self__.solo_modes)].join(''),"\n","(solo-modes mode)"].join('')));
}

cljs.core.reset_BANG_(self__.solo_mode,mode);

return (self__.changed.cljs$core$IFn$_invoke$arity$0 ? self__.changed.cljs$core$IFn$_invoke$arity$0() : self__.changed.call(null));
}));

(cljs.core.async.t_cljs$core$async33271.getBasis = (function (){
return new cljs.core.PersistentVector(null, 10, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"change","change",477485025,null),new cljs.core.Symbol(null,"solo-mode","solo-mode",2031788074,null),new cljs.core.Symbol(null,"pick","pick",1300068175,null),new cljs.core.Symbol(null,"cs","cs",-117024463,null),new cljs.core.Symbol(null,"calc-state","calc-state",-349968968,null),new cljs.core.Symbol(null,"out","out",729986010,null),new cljs.core.Symbol(null,"changed","changed",-2083710852,null),new cljs.core.Symbol(null,"solo-modes","solo-modes",882180540,null),new cljs.core.Symbol(null,"attrs","attrs",-450137186,null),new cljs.core.Symbol(null,"meta33272","meta33272",1185083518,null)], null);
}));

(cljs.core.async.t_cljs$core$async33271.cljs$lang$type = true);

(cljs.core.async.t_cljs$core$async33271.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async33271");

(cljs.core.async.t_cljs$core$async33271.cljs$lang$ctorPrWriter = (function (this__4491__auto__,writer__4492__auto__,opt__4493__auto__){
return cljs.core._write(writer__4492__auto__,"cljs.core.async/t_cljs$core$async33271");
}));

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async33271.
 */
cljs.core.async.__GT_t_cljs$core$async33271 = (function cljs$core$async$mix_$___GT_t_cljs$core$async33271(change__$1,solo_mode__$1,pick__$1,cs__$1,calc_state__$1,out__$1,changed__$1,solo_modes__$1,attrs__$1,meta33272){
return (new cljs.core.async.t_cljs$core$async33271(change__$1,solo_mode__$1,pick__$1,cs__$1,calc_state__$1,out__$1,changed__$1,solo_modes__$1,attrs__$1,meta33272));
});

}

return (new cljs.core.async.t_cljs$core$async33271(change,solo_mode,pick,cs,calc_state,out,changed,solo_modes,attrs,cljs.core.PersistentArrayMap.EMPTY));
})()
;
var c__32010__auto___34857 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__32011__auto__ = (function (){var switch__31873__auto__ = (function (state_33346){
var state_val_33347 = (state_33346[(1)]);
if((state_val_33347 === (7))){
var inst_33305 = (state_33346[(2)]);
var state_33346__$1 = state_33346;
if(cljs.core.truth_(inst_33305)){
var statearr_33348_34858 = state_33346__$1;
(statearr_33348_34858[(1)] = (8));

} else {
var statearr_33349_34859 = state_33346__$1;
(statearr_33349_34859[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33347 === (20))){
var inst_33298 = (state_33346[(7)]);
var state_33346__$1 = state_33346;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_33346__$1,(23),out,inst_33298);
} else {
if((state_val_33347 === (1))){
var inst_33280 = calc_state();
var inst_33281 = cljs.core.__destructure_map(inst_33280);
var inst_33282 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_33281,new cljs.core.Keyword(null,"solos","solos",1441458643));
var inst_33283 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_33281,new cljs.core.Keyword(null,"mutes","mutes",1068806309));
var inst_33284 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_33281,new cljs.core.Keyword(null,"reads","reads",-1215067361));
var inst_33285 = inst_33280;
var state_33346__$1 = (function (){var statearr_33351 = state_33346;
(statearr_33351[(8)] = inst_33282);

(statearr_33351[(9)] = inst_33283);

(statearr_33351[(10)] = inst_33285);

(statearr_33351[(11)] = inst_33284);

return statearr_33351;
})();
var statearr_33352_34860 = state_33346__$1;
(statearr_33352_34860[(2)] = null);

(statearr_33352_34860[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33347 === (24))){
var inst_33288 = (state_33346[(12)]);
var inst_33285 = inst_33288;
var state_33346__$1 = (function (){var statearr_33353 = state_33346;
(statearr_33353[(10)] = inst_33285);

return statearr_33353;
})();
var statearr_33354_34861 = state_33346__$1;
(statearr_33354_34861[(2)] = null);

(statearr_33354_34861[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33347 === (4))){
var inst_33298 = (state_33346[(7)]);
var inst_33300 = (state_33346[(13)]);
var inst_33297 = (state_33346[(2)]);
var inst_33298__$1 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_33297,(0),null);
var inst_33299 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_33297,(1),null);
var inst_33300__$1 = (inst_33298__$1 == null);
var state_33346__$1 = (function (){var statearr_33356 = state_33346;
(statearr_33356[(7)] = inst_33298__$1);

(statearr_33356[(14)] = inst_33299);

(statearr_33356[(13)] = inst_33300__$1);

return statearr_33356;
})();
if(cljs.core.truth_(inst_33300__$1)){
var statearr_33358_34863 = state_33346__$1;
(statearr_33358_34863[(1)] = (5));

} else {
var statearr_33359_34864 = state_33346__$1;
(statearr_33359_34864[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33347 === (15))){
var inst_33289 = (state_33346[(15)]);
var inst_33319 = (state_33346[(16)]);
var inst_33319__$1 = cljs.core.empty_QMARK_(inst_33289);
var state_33346__$1 = (function (){var statearr_33360 = state_33346;
(statearr_33360[(16)] = inst_33319__$1);

return statearr_33360;
})();
if(inst_33319__$1){
var statearr_33361_34865 = state_33346__$1;
(statearr_33361_34865[(1)] = (17));

} else {
var statearr_33362_34866 = state_33346__$1;
(statearr_33362_34866[(1)] = (18));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33347 === (21))){
var inst_33288 = (state_33346[(12)]);
var inst_33285 = inst_33288;
var state_33346__$1 = (function (){var statearr_33363 = state_33346;
(statearr_33363[(10)] = inst_33285);

return statearr_33363;
})();
var statearr_33364_34867 = state_33346__$1;
(statearr_33364_34867[(2)] = null);

(statearr_33364_34867[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33347 === (13))){
var inst_33312 = (state_33346[(2)]);
var inst_33313 = calc_state();
var inst_33285 = inst_33313;
var state_33346__$1 = (function (){var statearr_33365 = state_33346;
(statearr_33365[(17)] = inst_33312);

(statearr_33365[(10)] = inst_33285);

return statearr_33365;
})();
var statearr_33366_34868 = state_33346__$1;
(statearr_33366_34868[(2)] = null);

(statearr_33366_34868[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33347 === (22))){
var inst_33339 = (state_33346[(2)]);
var state_33346__$1 = state_33346;
var statearr_33367_34869 = state_33346__$1;
(statearr_33367_34869[(2)] = inst_33339);

(statearr_33367_34869[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33347 === (6))){
var inst_33299 = (state_33346[(14)]);
var inst_33303 = cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(inst_33299,change);
var state_33346__$1 = state_33346;
var statearr_33368_34870 = state_33346__$1;
(statearr_33368_34870[(2)] = inst_33303);

(statearr_33368_34870[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33347 === (25))){
var state_33346__$1 = state_33346;
var statearr_33369_34871 = state_33346__$1;
(statearr_33369_34871[(2)] = null);

(statearr_33369_34871[(1)] = (26));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33347 === (17))){
var inst_33291 = (state_33346[(18)]);
var inst_33299 = (state_33346[(14)]);
var inst_33321 = (inst_33291.cljs$core$IFn$_invoke$arity$1 ? inst_33291.cljs$core$IFn$_invoke$arity$1(inst_33299) : inst_33291.call(null,inst_33299));
var inst_33322 = cljs.core.not(inst_33321);
var state_33346__$1 = state_33346;
var statearr_33370_34872 = state_33346__$1;
(statearr_33370_34872[(2)] = inst_33322);

(statearr_33370_34872[(1)] = (19));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33347 === (3))){
var inst_33343 = (state_33346[(2)]);
var state_33346__$1 = state_33346;
return cljs.core.async.impl.ioc_helpers.return_chan(state_33346__$1,inst_33343);
} else {
if((state_val_33347 === (12))){
var state_33346__$1 = state_33346;
var statearr_33371_34873 = state_33346__$1;
(statearr_33371_34873[(2)] = null);

(statearr_33371_34873[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33347 === (2))){
var inst_33288 = (state_33346[(12)]);
var inst_33285 = (state_33346[(10)]);
var inst_33288__$1 = cljs.core.__destructure_map(inst_33285);
var inst_33289 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_33288__$1,new cljs.core.Keyword(null,"solos","solos",1441458643));
var inst_33291 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_33288__$1,new cljs.core.Keyword(null,"mutes","mutes",1068806309));
var inst_33292 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_33288__$1,new cljs.core.Keyword(null,"reads","reads",-1215067361));
var state_33346__$1 = (function (){var statearr_33374 = state_33346;
(statearr_33374[(12)] = inst_33288__$1);

(statearr_33374[(18)] = inst_33291);

(statearr_33374[(15)] = inst_33289);

return statearr_33374;
})();
return cljs.core.async.ioc_alts_BANG_(state_33346__$1,(4),inst_33292);
} else {
if((state_val_33347 === (23))){
var inst_33330 = (state_33346[(2)]);
var state_33346__$1 = state_33346;
if(cljs.core.truth_(inst_33330)){
var statearr_33375_34874 = state_33346__$1;
(statearr_33375_34874[(1)] = (24));

} else {
var statearr_33376_34879 = state_33346__$1;
(statearr_33376_34879[(1)] = (25));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33347 === (19))){
var inst_33325 = (state_33346[(2)]);
var state_33346__$1 = state_33346;
var statearr_33377_34880 = state_33346__$1;
(statearr_33377_34880[(2)] = inst_33325);

(statearr_33377_34880[(1)] = (16));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33347 === (11))){
var inst_33299 = (state_33346[(14)]);
var inst_33309 = cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(cs,cljs.core.dissoc,inst_33299);
var state_33346__$1 = state_33346;
var statearr_33378_34881 = state_33346__$1;
(statearr_33378_34881[(2)] = inst_33309);

(statearr_33378_34881[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33347 === (9))){
var inst_33289 = (state_33346[(15)]);
var inst_33299 = (state_33346[(14)]);
var inst_33316 = (state_33346[(19)]);
var inst_33316__$1 = (inst_33289.cljs$core$IFn$_invoke$arity$1 ? inst_33289.cljs$core$IFn$_invoke$arity$1(inst_33299) : inst_33289.call(null,inst_33299));
var state_33346__$1 = (function (){var statearr_33379 = state_33346;
(statearr_33379[(19)] = inst_33316__$1);

return statearr_33379;
})();
if(cljs.core.truth_(inst_33316__$1)){
var statearr_33380_34882 = state_33346__$1;
(statearr_33380_34882[(1)] = (14));

} else {
var statearr_33381_34883 = state_33346__$1;
(statearr_33381_34883[(1)] = (15));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33347 === (5))){
var inst_33300 = (state_33346[(13)]);
var state_33346__$1 = state_33346;
var statearr_33382_34884 = state_33346__$1;
(statearr_33382_34884[(2)] = inst_33300);

(statearr_33382_34884[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33347 === (14))){
var inst_33316 = (state_33346[(19)]);
var state_33346__$1 = state_33346;
var statearr_33383_34885 = state_33346__$1;
(statearr_33383_34885[(2)] = inst_33316);

(statearr_33383_34885[(1)] = (16));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33347 === (26))){
var inst_33335 = (state_33346[(2)]);
var state_33346__$1 = state_33346;
var statearr_33384_34886 = state_33346__$1;
(statearr_33384_34886[(2)] = inst_33335);

(statearr_33384_34886[(1)] = (22));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33347 === (16))){
var inst_33327 = (state_33346[(2)]);
var state_33346__$1 = state_33346;
if(cljs.core.truth_(inst_33327)){
var statearr_33385_34887 = state_33346__$1;
(statearr_33385_34887[(1)] = (20));

} else {
var statearr_33386_34888 = state_33346__$1;
(statearr_33386_34888[(1)] = (21));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33347 === (10))){
var inst_33341 = (state_33346[(2)]);
var state_33346__$1 = state_33346;
var statearr_33387_34889 = state_33346__$1;
(statearr_33387_34889[(2)] = inst_33341);

(statearr_33387_34889[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33347 === (18))){
var inst_33319 = (state_33346[(16)]);
var state_33346__$1 = state_33346;
var statearr_33388_34890 = state_33346__$1;
(statearr_33388_34890[(2)] = inst_33319);

(statearr_33388_34890[(1)] = (19));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33347 === (8))){
var inst_33298 = (state_33346[(7)]);
var inst_33307 = (inst_33298 == null);
var state_33346__$1 = state_33346;
if(cljs.core.truth_(inst_33307)){
var statearr_33390_34891 = state_33346__$1;
(statearr_33390_34891[(1)] = (11));

} else {
var statearr_33391_34892 = state_33346__$1;
(statearr_33391_34892[(1)] = (12));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$mix_$_state_machine__31874__auto__ = null;
var cljs$core$async$mix_$_state_machine__31874__auto____0 = (function (){
var statearr_33393 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_33393[(0)] = cljs$core$async$mix_$_state_machine__31874__auto__);

(statearr_33393[(1)] = (1));

return statearr_33393;
});
var cljs$core$async$mix_$_state_machine__31874__auto____1 = (function (state_33346){
while(true){
var ret_value__31875__auto__ = (function (){try{while(true){
var result__31876__auto__ = switch__31873__auto__(state_33346);
if(cljs.core.keyword_identical_QMARK_(result__31876__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__31876__auto__;
}
break;
}
}catch (e33394){var ex__31877__auto__ = e33394;
var statearr_33395_34893 = state_33346;
(statearr_33395_34893[(2)] = ex__31877__auto__);


if(cljs.core.seq((state_33346[(4)]))){
var statearr_33396_34894 = state_33346;
(statearr_33396_34894[(1)] = cljs.core.first((state_33346[(4)])));

} else {
throw ex__31877__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__31875__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__34895 = state_33346;
state_33346 = G__34895;
continue;
} else {
return ret_value__31875__auto__;
}
break;
}
});
cljs$core$async$mix_$_state_machine__31874__auto__ = function(state_33346){
switch(arguments.length){
case 0:
return cljs$core$async$mix_$_state_machine__31874__auto____0.call(this);
case 1:
return cljs$core$async$mix_$_state_machine__31874__auto____1.call(this,state_33346);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$mix_$_state_machine__31874__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$mix_$_state_machine__31874__auto____0;
cljs$core$async$mix_$_state_machine__31874__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$mix_$_state_machine__31874__auto____1;
return cljs$core$async$mix_$_state_machine__31874__auto__;
})()
})();
var state__32012__auto__ = (function (){var statearr_33397 = f__32011__auto__();
(statearr_33397[(6)] = c__32010__auto___34857);

return statearr_33397;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__32012__auto__);
}));


return m;
});
/**
 * Adds ch as an input to the mix
 */
cljs.core.async.admix = (function cljs$core$async$admix(mix,ch){
return cljs.core.async.admix_STAR_(mix,ch);
});
/**
 * Removes ch as an input to the mix
 */
cljs.core.async.unmix = (function cljs$core$async$unmix(mix,ch){
return cljs.core.async.unmix_STAR_(mix,ch);
});
/**
 * removes all inputs from the mix
 */
cljs.core.async.unmix_all = (function cljs$core$async$unmix_all(mix){
return cljs.core.async.unmix_all_STAR_(mix);
});
/**
 * Atomically sets the state(s) of one or more channels in a mix. The
 *   state map is a map of channels -> channel-state-map. A
 *   channel-state-map is a map of attrs -> boolean, where attr is one or
 *   more of :mute, :pause or :solo. Any states supplied are merged with
 *   the current state.
 * 
 *   Note that channels can be added to a mix via toggle, which can be
 *   used to add channels in a particular (e.g. paused) state.
 */
cljs.core.async.toggle = (function cljs$core$async$toggle(mix,state_map){
return cljs.core.async.toggle_STAR_(mix,state_map);
});
/**
 * Sets the solo mode of the mix. mode must be one of :mute or :pause
 */
cljs.core.async.solo_mode = (function cljs$core$async$solo_mode(mix,mode){
return cljs.core.async.solo_mode_STAR_(mix,mode);
});

/**
 * @interface
 */
cljs.core.async.Pub = function(){};

var cljs$core$async$Pub$sub_STAR_$dyn_34896 = (function (p,v,ch,close_QMARK_){
var x__4550__auto__ = (((p == null))?null:p);
var m__4551__auto__ = (cljs.core.async.sub_STAR_[goog.typeOf(x__4550__auto__)]);
if((!((m__4551__auto__ == null)))){
return (m__4551__auto__.cljs$core$IFn$_invoke$arity$4 ? m__4551__auto__.cljs$core$IFn$_invoke$arity$4(p,v,ch,close_QMARK_) : m__4551__auto__.call(null,p,v,ch,close_QMARK_));
} else {
var m__4549__auto__ = (cljs.core.async.sub_STAR_["_"]);
if((!((m__4549__auto__ == null)))){
return (m__4549__auto__.cljs$core$IFn$_invoke$arity$4 ? m__4549__auto__.cljs$core$IFn$_invoke$arity$4(p,v,ch,close_QMARK_) : m__4549__auto__.call(null,p,v,ch,close_QMARK_));
} else {
throw cljs.core.missing_protocol("Pub.sub*",p);
}
}
});
cljs.core.async.sub_STAR_ = (function cljs$core$async$sub_STAR_(p,v,ch,close_QMARK_){
if((((!((p == null)))) && ((!((p.cljs$core$async$Pub$sub_STAR_$arity$4 == null)))))){
return p.cljs$core$async$Pub$sub_STAR_$arity$4(p,v,ch,close_QMARK_);
} else {
return cljs$core$async$Pub$sub_STAR_$dyn_34896(p,v,ch,close_QMARK_);
}
});

var cljs$core$async$Pub$unsub_STAR_$dyn_34904 = (function (p,v,ch){
var x__4550__auto__ = (((p == null))?null:p);
var m__4551__auto__ = (cljs.core.async.unsub_STAR_[goog.typeOf(x__4550__auto__)]);
if((!((m__4551__auto__ == null)))){
return (m__4551__auto__.cljs$core$IFn$_invoke$arity$3 ? m__4551__auto__.cljs$core$IFn$_invoke$arity$3(p,v,ch) : m__4551__auto__.call(null,p,v,ch));
} else {
var m__4549__auto__ = (cljs.core.async.unsub_STAR_["_"]);
if((!((m__4549__auto__ == null)))){
return (m__4549__auto__.cljs$core$IFn$_invoke$arity$3 ? m__4549__auto__.cljs$core$IFn$_invoke$arity$3(p,v,ch) : m__4549__auto__.call(null,p,v,ch));
} else {
throw cljs.core.missing_protocol("Pub.unsub*",p);
}
}
});
cljs.core.async.unsub_STAR_ = (function cljs$core$async$unsub_STAR_(p,v,ch){
if((((!((p == null)))) && ((!((p.cljs$core$async$Pub$unsub_STAR_$arity$3 == null)))))){
return p.cljs$core$async$Pub$unsub_STAR_$arity$3(p,v,ch);
} else {
return cljs$core$async$Pub$unsub_STAR_$dyn_34904(p,v,ch);
}
});

var cljs$core$async$Pub$unsub_all_STAR_$dyn_34906 = (function() {
var G__34908 = null;
var G__34908__1 = (function (p){
var x__4550__auto__ = (((p == null))?null:p);
var m__4551__auto__ = (cljs.core.async.unsub_all_STAR_[goog.typeOf(x__4550__auto__)]);
if((!((m__4551__auto__ == null)))){
return (m__4551__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4551__auto__.cljs$core$IFn$_invoke$arity$1(p) : m__4551__auto__.call(null,p));
} else {
var m__4549__auto__ = (cljs.core.async.unsub_all_STAR_["_"]);
if((!((m__4549__auto__ == null)))){
return (m__4549__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4549__auto__.cljs$core$IFn$_invoke$arity$1(p) : m__4549__auto__.call(null,p));
} else {
throw cljs.core.missing_protocol("Pub.unsub-all*",p);
}
}
});
var G__34908__2 = (function (p,v){
var x__4550__auto__ = (((p == null))?null:p);
var m__4551__auto__ = (cljs.core.async.unsub_all_STAR_[goog.typeOf(x__4550__auto__)]);
if((!((m__4551__auto__ == null)))){
return (m__4551__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4551__auto__.cljs$core$IFn$_invoke$arity$2(p,v) : m__4551__auto__.call(null,p,v));
} else {
var m__4549__auto__ = (cljs.core.async.unsub_all_STAR_["_"]);
if((!((m__4549__auto__ == null)))){
return (m__4549__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4549__auto__.cljs$core$IFn$_invoke$arity$2(p,v) : m__4549__auto__.call(null,p,v));
} else {
throw cljs.core.missing_protocol("Pub.unsub-all*",p);
}
}
});
G__34908 = function(p,v){
switch(arguments.length){
case 1:
return G__34908__1.call(this,p);
case 2:
return G__34908__2.call(this,p,v);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
G__34908.cljs$core$IFn$_invoke$arity$1 = G__34908__1;
G__34908.cljs$core$IFn$_invoke$arity$2 = G__34908__2;
return G__34908;
})()
;
cljs.core.async.unsub_all_STAR_ = (function cljs$core$async$unsub_all_STAR_(var_args){
var G__33428 = arguments.length;
switch (G__33428) {
case 1:
return cljs.core.async.unsub_all_STAR_.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.unsub_all_STAR_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.unsub_all_STAR_.cljs$core$IFn$_invoke$arity$1 = (function (p){
if((((!((p == null)))) && ((!((p.cljs$core$async$Pub$unsub_all_STAR_$arity$1 == null)))))){
return p.cljs$core$async$Pub$unsub_all_STAR_$arity$1(p);
} else {
return cljs$core$async$Pub$unsub_all_STAR_$dyn_34906(p);
}
}));

(cljs.core.async.unsub_all_STAR_.cljs$core$IFn$_invoke$arity$2 = (function (p,v){
if((((!((p == null)))) && ((!((p.cljs$core$async$Pub$unsub_all_STAR_$arity$2 == null)))))){
return p.cljs$core$async$Pub$unsub_all_STAR_$arity$2(p,v);
} else {
return cljs$core$async$Pub$unsub_all_STAR_$dyn_34906(p,v);
}
}));

(cljs.core.async.unsub_all_STAR_.cljs$lang$maxFixedArity = 2);


/**
 * Creates and returns a pub(lication) of the supplied channel,
 *   partitioned into topics by the topic-fn. topic-fn will be applied to
 *   each value on the channel and the result will determine the 'topic'
 *   on which that value will be put. Channels can be subscribed to
 *   receive copies of topics using 'sub', and unsubscribed using
 *   'unsub'. Each topic will be handled by an internal mult on a
 *   dedicated channel. By default these internal channels are
 *   unbuffered, but a buf-fn can be supplied which, given a topic,
 *   creates a buffer with desired properties.
 * 
 *   Each item is distributed to all subs in parallel and synchronously,
 *   i.e. each sub must accept before the next item is distributed. Use
 *   buffering/windowing to prevent slow subs from holding up the pub.
 * 
 *   Items received when there are no matching subs get dropped.
 * 
 *   Note that if buf-fns are used then each topic is handled
 *   asynchronously, i.e. if a channel is subscribed to more than one
 *   topic it should not expect them to be interleaved identically with
 *   the source.
 */
cljs.core.async.pub = (function cljs$core$async$pub(var_args){
var G__33445 = arguments.length;
switch (G__33445) {
case 2:
return cljs.core.async.pub.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.pub.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.pub.cljs$core$IFn$_invoke$arity$2 = (function (ch,topic_fn){
return cljs.core.async.pub.cljs$core$IFn$_invoke$arity$3(ch,topic_fn,cljs.core.constantly(null));
}));

(cljs.core.async.pub.cljs$core$IFn$_invoke$arity$3 = (function (ch,topic_fn,buf_fn){
var mults = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.PersistentArrayMap.EMPTY);
var ensure_mult = (function (topic){
var or__4253__auto__ = cljs.core.get.cljs$core$IFn$_invoke$arity$2(cljs.core.deref(mults),topic);
if(cljs.core.truth_(or__4253__auto__)){
return or__4253__auto__;
} else {
return cljs.core.get.cljs$core$IFn$_invoke$arity$2(cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$2(mults,(function (p1__33438_SHARP_){
if(cljs.core.truth_((p1__33438_SHARP_.cljs$core$IFn$_invoke$arity$1 ? p1__33438_SHARP_.cljs$core$IFn$_invoke$arity$1(topic) : p1__33438_SHARP_.call(null,topic)))){
return p1__33438_SHARP_;
} else {
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(p1__33438_SHARP_,topic,cljs.core.async.mult(cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((buf_fn.cljs$core$IFn$_invoke$arity$1 ? buf_fn.cljs$core$IFn$_invoke$arity$1(topic) : buf_fn.call(null,topic)))));
}
})),topic);
}
});
var p = (function (){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async33456 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.Pub}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.async.Mux}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async33456 = (function (ch,topic_fn,buf_fn,mults,ensure_mult,meta33457){
this.ch = ch;
this.topic_fn = topic_fn;
this.buf_fn = buf_fn;
this.mults = mults;
this.ensure_mult = ensure_mult;
this.meta33457 = meta33457;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(cljs.core.async.t_cljs$core$async33456.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_33458,meta33457__$1){
var self__ = this;
var _33458__$1 = this;
return (new cljs.core.async.t_cljs$core$async33456(self__.ch,self__.topic_fn,self__.buf_fn,self__.mults,self__.ensure_mult,meta33457__$1));
}));

(cljs.core.async.t_cljs$core$async33456.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_33458){
var self__ = this;
var _33458__$1 = this;
return self__.meta33457;
}));

(cljs.core.async.t_cljs$core$async33456.prototype.cljs$core$async$Mux$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async33456.prototype.cljs$core$async$Mux$muxch_STAR_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return self__.ch;
}));

(cljs.core.async.t_cljs$core$async33456.prototype.cljs$core$async$Pub$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async33456.prototype.cljs$core$async$Pub$sub_STAR_$arity$4 = (function (p,topic,ch__$1,close_QMARK_){
var self__ = this;
var p__$1 = this;
var m = (self__.ensure_mult.cljs$core$IFn$_invoke$arity$1 ? self__.ensure_mult.cljs$core$IFn$_invoke$arity$1(topic) : self__.ensure_mult.call(null,topic));
return cljs.core.async.tap.cljs$core$IFn$_invoke$arity$3(m,ch__$1,close_QMARK_);
}));

(cljs.core.async.t_cljs$core$async33456.prototype.cljs$core$async$Pub$unsub_STAR_$arity$3 = (function (p,topic,ch__$1){
var self__ = this;
var p__$1 = this;
var temp__5753__auto__ = cljs.core.get.cljs$core$IFn$_invoke$arity$2(cljs.core.deref(self__.mults),topic);
if(cljs.core.truth_(temp__5753__auto__)){
var m = temp__5753__auto__;
return cljs.core.async.untap(m,ch__$1);
} else {
return null;
}
}));

(cljs.core.async.t_cljs$core$async33456.prototype.cljs$core$async$Pub$unsub_all_STAR_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.reset_BANG_(self__.mults,cljs.core.PersistentArrayMap.EMPTY);
}));

(cljs.core.async.t_cljs$core$async33456.prototype.cljs$core$async$Pub$unsub_all_STAR_$arity$2 = (function (_,topic){
var self__ = this;
var ___$1 = this;
return cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(self__.mults,cljs.core.dissoc,topic);
}));

(cljs.core.async.t_cljs$core$async33456.getBasis = (function (){
return new cljs.core.PersistentVector(null, 6, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"topic-fn","topic-fn",-862449736,null),new cljs.core.Symbol(null,"buf-fn","buf-fn",-1200281591,null),new cljs.core.Symbol(null,"mults","mults",-461114485,null),new cljs.core.Symbol(null,"ensure-mult","ensure-mult",1796584816,null),new cljs.core.Symbol(null,"meta33457","meta33457",-1994511716,null)], null);
}));

(cljs.core.async.t_cljs$core$async33456.cljs$lang$type = true);

(cljs.core.async.t_cljs$core$async33456.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async33456");

(cljs.core.async.t_cljs$core$async33456.cljs$lang$ctorPrWriter = (function (this__4491__auto__,writer__4492__auto__,opt__4493__auto__){
return cljs.core._write(writer__4492__auto__,"cljs.core.async/t_cljs$core$async33456");
}));

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async33456.
 */
cljs.core.async.__GT_t_cljs$core$async33456 = (function cljs$core$async$__GT_t_cljs$core$async33456(ch__$1,topic_fn__$1,buf_fn__$1,mults__$1,ensure_mult__$1,meta33457){
return (new cljs.core.async.t_cljs$core$async33456(ch__$1,topic_fn__$1,buf_fn__$1,mults__$1,ensure_mult__$1,meta33457));
});

}

return (new cljs.core.async.t_cljs$core$async33456(ch,topic_fn,buf_fn,mults,ensure_mult,cljs.core.PersistentArrayMap.EMPTY));
})()
;
var c__32010__auto___34943 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__32011__auto__ = (function (){var switch__31873__auto__ = (function (state_33582){
var state_val_33583 = (state_33582[(1)]);
if((state_val_33583 === (7))){
var inst_33577 = (state_33582[(2)]);
var state_33582__$1 = state_33582;
var statearr_33588_34944 = state_33582__$1;
(statearr_33588_34944[(2)] = inst_33577);

(statearr_33588_34944[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33583 === (20))){
var state_33582__$1 = state_33582;
var statearr_33592_34945 = state_33582__$1;
(statearr_33592_34945[(2)] = null);

(statearr_33592_34945[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33583 === (1))){
var state_33582__$1 = state_33582;
var statearr_33599_34949 = state_33582__$1;
(statearr_33599_34949[(2)] = null);

(statearr_33599_34949[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33583 === (24))){
var inst_33553 = (state_33582[(7)]);
var inst_33569 = cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(mults,cljs.core.dissoc,inst_33553);
var state_33582__$1 = state_33582;
var statearr_33603_34950 = state_33582__$1;
(statearr_33603_34950[(2)] = inst_33569);

(statearr_33603_34950[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33583 === (4))){
var inst_33477 = (state_33582[(8)]);
var inst_33477__$1 = (state_33582[(2)]);
var inst_33478 = (inst_33477__$1 == null);
var state_33582__$1 = (function (){var statearr_33605 = state_33582;
(statearr_33605[(8)] = inst_33477__$1);

return statearr_33605;
})();
if(cljs.core.truth_(inst_33478)){
var statearr_33612_34951 = state_33582__$1;
(statearr_33612_34951[(1)] = (5));

} else {
var statearr_33613_34952 = state_33582__$1;
(statearr_33613_34952[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33583 === (15))){
var inst_33546 = (state_33582[(2)]);
var state_33582__$1 = state_33582;
var statearr_33615_34955 = state_33582__$1;
(statearr_33615_34955[(2)] = inst_33546);

(statearr_33615_34955[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33583 === (21))){
var inst_33574 = (state_33582[(2)]);
var state_33582__$1 = (function (){var statearr_33616 = state_33582;
(statearr_33616[(9)] = inst_33574);

return statearr_33616;
})();
var statearr_33617_34959 = state_33582__$1;
(statearr_33617_34959[(2)] = null);

(statearr_33617_34959[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33583 === (13))){
var inst_33503 = (state_33582[(10)]);
var inst_33527 = cljs.core.chunked_seq_QMARK_(inst_33503);
var state_33582__$1 = state_33582;
if(inst_33527){
var statearr_33621_34963 = state_33582__$1;
(statearr_33621_34963[(1)] = (16));

} else {
var statearr_33622_34964 = state_33582__$1;
(statearr_33622_34964[(1)] = (17));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33583 === (22))){
var inst_33565 = (state_33582[(2)]);
var state_33582__$1 = state_33582;
if(cljs.core.truth_(inst_33565)){
var statearr_33627_34967 = state_33582__$1;
(statearr_33627_34967[(1)] = (23));

} else {
var statearr_33628_34968 = state_33582__$1;
(statearr_33628_34968[(1)] = (24));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33583 === (6))){
var inst_33477 = (state_33582[(8)]);
var inst_33553 = (state_33582[(7)]);
var inst_33557 = (state_33582[(11)]);
var inst_33553__$1 = (topic_fn.cljs$core$IFn$_invoke$arity$1 ? topic_fn.cljs$core$IFn$_invoke$arity$1(inst_33477) : topic_fn.call(null,inst_33477));
var inst_33556 = cljs.core.deref(mults);
var inst_33557__$1 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_33556,inst_33553__$1);
var state_33582__$1 = (function (){var statearr_33635 = state_33582;
(statearr_33635[(7)] = inst_33553__$1);

(statearr_33635[(11)] = inst_33557__$1);

return statearr_33635;
})();
if(cljs.core.truth_(inst_33557__$1)){
var statearr_33637_34973 = state_33582__$1;
(statearr_33637_34973[(1)] = (19));

} else {
var statearr_33638_34974 = state_33582__$1;
(statearr_33638_34974[(1)] = (20));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33583 === (25))){
var inst_33571 = (state_33582[(2)]);
var state_33582__$1 = state_33582;
var statearr_33642_34978 = state_33582__$1;
(statearr_33642_34978[(2)] = inst_33571);

(statearr_33642_34978[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33583 === (17))){
var inst_33503 = (state_33582[(10)]);
var inst_33536 = cljs.core.first(inst_33503);
var inst_33538 = cljs.core.async.muxch_STAR_(inst_33536);
var inst_33539 = cljs.core.async.close_BANG_(inst_33538);
var inst_33540 = cljs.core.next(inst_33503);
var inst_33487 = inst_33540;
var inst_33488 = null;
var inst_33489 = (0);
var inst_33490 = (0);
var state_33582__$1 = (function (){var statearr_33644 = state_33582;
(statearr_33644[(12)] = inst_33487);

(statearr_33644[(13)] = inst_33539);

(statearr_33644[(14)] = inst_33488);

(statearr_33644[(15)] = inst_33489);

(statearr_33644[(16)] = inst_33490);

return statearr_33644;
})();
var statearr_33648_34979 = state_33582__$1;
(statearr_33648_34979[(2)] = null);

(statearr_33648_34979[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33583 === (3))){
var inst_33579 = (state_33582[(2)]);
var state_33582__$1 = state_33582;
return cljs.core.async.impl.ioc_helpers.return_chan(state_33582__$1,inst_33579);
} else {
if((state_val_33583 === (12))){
var inst_33548 = (state_33582[(2)]);
var state_33582__$1 = state_33582;
var statearr_33650_34981 = state_33582__$1;
(statearr_33650_34981[(2)] = inst_33548);

(statearr_33650_34981[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33583 === (2))){
var state_33582__$1 = state_33582;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_33582__$1,(4),ch);
} else {
if((state_val_33583 === (23))){
var state_33582__$1 = state_33582;
var statearr_33654_34982 = state_33582__$1;
(statearr_33654_34982[(2)] = null);

(statearr_33654_34982[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33583 === (19))){
var inst_33477 = (state_33582[(8)]);
var inst_33557 = (state_33582[(11)]);
var inst_33563 = cljs.core.async.muxch_STAR_(inst_33557);
var state_33582__$1 = state_33582;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_33582__$1,(22),inst_33563,inst_33477);
} else {
if((state_val_33583 === (11))){
var inst_33487 = (state_33582[(12)]);
var inst_33503 = (state_33582[(10)]);
var inst_33503__$1 = cljs.core.seq(inst_33487);
var state_33582__$1 = (function (){var statearr_33659 = state_33582;
(statearr_33659[(10)] = inst_33503__$1);

return statearr_33659;
})();
if(inst_33503__$1){
var statearr_33661_34983 = state_33582__$1;
(statearr_33661_34983[(1)] = (13));

} else {
var statearr_33662_34987 = state_33582__$1;
(statearr_33662_34987[(1)] = (14));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33583 === (9))){
var inst_33551 = (state_33582[(2)]);
var state_33582__$1 = state_33582;
var statearr_33665_34988 = state_33582__$1;
(statearr_33665_34988[(2)] = inst_33551);

(statearr_33665_34988[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33583 === (5))){
var inst_33484 = cljs.core.deref(mults);
var inst_33485 = cljs.core.vals(inst_33484);
var inst_33486 = cljs.core.seq(inst_33485);
var inst_33487 = inst_33486;
var inst_33488 = null;
var inst_33489 = (0);
var inst_33490 = (0);
var state_33582__$1 = (function (){var statearr_33668 = state_33582;
(statearr_33668[(12)] = inst_33487);

(statearr_33668[(14)] = inst_33488);

(statearr_33668[(15)] = inst_33489);

(statearr_33668[(16)] = inst_33490);

return statearr_33668;
})();
var statearr_33672_34989 = state_33582__$1;
(statearr_33672_34989[(2)] = null);

(statearr_33672_34989[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33583 === (14))){
var state_33582__$1 = state_33582;
var statearr_33677_34991 = state_33582__$1;
(statearr_33677_34991[(2)] = null);

(statearr_33677_34991[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33583 === (16))){
var inst_33503 = (state_33582[(10)]);
var inst_33529 = cljs.core.chunk_first(inst_33503);
var inst_33530 = cljs.core.chunk_rest(inst_33503);
var inst_33531 = cljs.core.count(inst_33529);
var inst_33487 = inst_33530;
var inst_33488 = inst_33529;
var inst_33489 = inst_33531;
var inst_33490 = (0);
var state_33582__$1 = (function (){var statearr_33681 = state_33582;
(statearr_33681[(12)] = inst_33487);

(statearr_33681[(14)] = inst_33488);

(statearr_33681[(15)] = inst_33489);

(statearr_33681[(16)] = inst_33490);

return statearr_33681;
})();
var statearr_33683_34992 = state_33582__$1;
(statearr_33683_34992[(2)] = null);

(statearr_33683_34992[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33583 === (10))){
var inst_33487 = (state_33582[(12)]);
var inst_33488 = (state_33582[(14)]);
var inst_33489 = (state_33582[(15)]);
var inst_33490 = (state_33582[(16)]);
var inst_33497 = cljs.core._nth(inst_33488,inst_33490);
var inst_33498 = cljs.core.async.muxch_STAR_(inst_33497);
var inst_33499 = cljs.core.async.close_BANG_(inst_33498);
var inst_33500 = (inst_33490 + (1));
var tmp33674 = inst_33487;
var tmp33675 = inst_33488;
var tmp33676 = inst_33489;
var inst_33487__$1 = tmp33674;
var inst_33488__$1 = tmp33675;
var inst_33489__$1 = tmp33676;
var inst_33490__$1 = inst_33500;
var state_33582__$1 = (function (){var statearr_33687 = state_33582;
(statearr_33687[(12)] = inst_33487__$1);

(statearr_33687[(14)] = inst_33488__$1);

(statearr_33687[(15)] = inst_33489__$1);

(statearr_33687[(16)] = inst_33490__$1);

(statearr_33687[(17)] = inst_33499);

return statearr_33687;
})();
var statearr_33689_34993 = state_33582__$1;
(statearr_33689_34993[(2)] = null);

(statearr_33689_34993[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33583 === (18))){
var inst_33543 = (state_33582[(2)]);
var state_33582__$1 = state_33582;
var statearr_33693_34997 = state_33582__$1;
(statearr_33693_34997[(2)] = inst_33543);

(statearr_33693_34997[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33583 === (8))){
var inst_33489 = (state_33582[(15)]);
var inst_33490 = (state_33582[(16)]);
var inst_33494 = (inst_33490 < inst_33489);
var inst_33495 = inst_33494;
var state_33582__$1 = state_33582;
if(cljs.core.truth_(inst_33495)){
var statearr_33694_34998 = state_33582__$1;
(statearr_33694_34998[(1)] = (10));

} else {
var statearr_33696_34999 = state_33582__$1;
(statearr_33696_34999[(1)] = (11));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__31874__auto__ = null;
var cljs$core$async$state_machine__31874__auto____0 = (function (){
var statearr_33702 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_33702[(0)] = cljs$core$async$state_machine__31874__auto__);

(statearr_33702[(1)] = (1));

return statearr_33702;
});
var cljs$core$async$state_machine__31874__auto____1 = (function (state_33582){
while(true){
var ret_value__31875__auto__ = (function (){try{while(true){
var result__31876__auto__ = switch__31873__auto__(state_33582);
if(cljs.core.keyword_identical_QMARK_(result__31876__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__31876__auto__;
}
break;
}
}catch (e33706){var ex__31877__auto__ = e33706;
var statearr_33707_35001 = state_33582;
(statearr_33707_35001[(2)] = ex__31877__auto__);


if(cljs.core.seq((state_33582[(4)]))){
var statearr_33708_35002 = state_33582;
(statearr_33708_35002[(1)] = cljs.core.first((state_33582[(4)])));

} else {
throw ex__31877__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__31875__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__35006 = state_33582;
state_33582 = G__35006;
continue;
} else {
return ret_value__31875__auto__;
}
break;
}
});
cljs$core$async$state_machine__31874__auto__ = function(state_33582){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__31874__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__31874__auto____1.call(this,state_33582);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__31874__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__31874__auto____0;
cljs$core$async$state_machine__31874__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__31874__auto____1;
return cljs$core$async$state_machine__31874__auto__;
})()
})();
var state__32012__auto__ = (function (){var statearr_33709 = f__32011__auto__();
(statearr_33709[(6)] = c__32010__auto___34943);

return statearr_33709;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__32012__auto__);
}));


return p;
}));

(cljs.core.async.pub.cljs$lang$maxFixedArity = 3);

/**
 * Subscribes a channel to a topic of a pub.
 * 
 *   By default the channel will be closed when the source closes,
 *   but can be determined by the close? parameter.
 */
cljs.core.async.sub = (function cljs$core$async$sub(var_args){
var G__33711 = arguments.length;
switch (G__33711) {
case 3:
return cljs.core.async.sub.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
case 4:
return cljs.core.async.sub.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.sub.cljs$core$IFn$_invoke$arity$3 = (function (p,topic,ch){
return cljs.core.async.sub.cljs$core$IFn$_invoke$arity$4(p,topic,ch,true);
}));

(cljs.core.async.sub.cljs$core$IFn$_invoke$arity$4 = (function (p,topic,ch,close_QMARK_){
return cljs.core.async.sub_STAR_(p,topic,ch,close_QMARK_);
}));

(cljs.core.async.sub.cljs$lang$maxFixedArity = 4);

/**
 * Unsubscribes a channel from a topic of a pub
 */
cljs.core.async.unsub = (function cljs$core$async$unsub(p,topic,ch){
return cljs.core.async.unsub_STAR_(p,topic,ch);
});
/**
 * Unsubscribes all channels from a pub, or a topic of a pub
 */
cljs.core.async.unsub_all = (function cljs$core$async$unsub_all(var_args){
var G__33713 = arguments.length;
switch (G__33713) {
case 1:
return cljs.core.async.unsub_all.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.unsub_all.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.unsub_all.cljs$core$IFn$_invoke$arity$1 = (function (p){
return cljs.core.async.unsub_all_STAR_(p);
}));

(cljs.core.async.unsub_all.cljs$core$IFn$_invoke$arity$2 = (function (p,topic){
return cljs.core.async.unsub_all_STAR_(p,topic);
}));

(cljs.core.async.unsub_all.cljs$lang$maxFixedArity = 2);

/**
 * Takes a function and a collection of source channels, and returns a
 *   channel which contains the values produced by applying f to the set
 *   of first items taken from each source channel, followed by applying
 *   f to the set of second items from each channel, until any one of the
 *   channels is closed, at which point the output channel will be
 *   closed. The returned channel will be unbuffered by default, or a
 *   buf-or-n can be supplied
 */
cljs.core.async.map = (function cljs$core$async$map(var_args){
var G__33715 = arguments.length;
switch (G__33715) {
case 2:
return cljs.core.async.map.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.map.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.map.cljs$core$IFn$_invoke$arity$2 = (function (f,chs){
return cljs.core.async.map.cljs$core$IFn$_invoke$arity$3(f,chs,null);
}));

(cljs.core.async.map.cljs$core$IFn$_invoke$arity$3 = (function (f,chs,buf_or_n){
var chs__$1 = cljs.core.vec(chs);
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
var cnt = cljs.core.count(chs__$1);
var rets = cljs.core.object_array.cljs$core$IFn$_invoke$arity$1(cnt);
var dchan = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
var dctr = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(null);
var done = cljs.core.mapv.cljs$core$IFn$_invoke$arity$2((function (i){
return (function (ret){
(rets[i] = ret);

if((cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$2(dctr,cljs.core.dec) === (0))){
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2(dchan,rets.slice((0)));
} else {
return null;
}
});
}),cljs.core.range.cljs$core$IFn$_invoke$arity$1(cnt));
if((cnt === (0))){
cljs.core.async.close_BANG_(out);
} else {
var c__32010__auto___35021 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__32011__auto__ = (function (){var switch__31873__auto__ = (function (state_33769){
var state_val_33770 = (state_33769[(1)]);
if((state_val_33770 === (7))){
var state_33769__$1 = state_33769;
var statearr_33775_35022 = state_33769__$1;
(statearr_33775_35022[(2)] = null);

(statearr_33775_35022[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33770 === (1))){
var state_33769__$1 = state_33769;
var statearr_33779_35023 = state_33769__$1;
(statearr_33779_35023[(2)] = null);

(statearr_33779_35023[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33770 === (4))){
var inst_33721 = (state_33769[(7)]);
var inst_33720 = (state_33769[(8)]);
var inst_33723 = (inst_33721 < inst_33720);
var state_33769__$1 = state_33769;
if(cljs.core.truth_(inst_33723)){
var statearr_33781_35024 = state_33769__$1;
(statearr_33781_35024[(1)] = (6));

} else {
var statearr_33782_35025 = state_33769__$1;
(statearr_33782_35025[(1)] = (7));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33770 === (15))){
var inst_33755 = (state_33769[(9)]);
var inst_33760 = cljs.core.apply.cljs$core$IFn$_invoke$arity$2(f,inst_33755);
var state_33769__$1 = state_33769;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_33769__$1,(17),out,inst_33760);
} else {
if((state_val_33770 === (13))){
var inst_33755 = (state_33769[(9)]);
var inst_33755__$1 = (state_33769[(2)]);
var inst_33756 = cljs.core.some(cljs.core.nil_QMARK_,inst_33755__$1);
var state_33769__$1 = (function (){var statearr_33786 = state_33769;
(statearr_33786[(9)] = inst_33755__$1);

return statearr_33786;
})();
if(cljs.core.truth_(inst_33756)){
var statearr_33787_35026 = state_33769__$1;
(statearr_33787_35026[(1)] = (14));

} else {
var statearr_33789_35027 = state_33769__$1;
(statearr_33789_35027[(1)] = (15));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33770 === (6))){
var state_33769__$1 = state_33769;
var statearr_33790_35029 = state_33769__$1;
(statearr_33790_35029[(2)] = null);

(statearr_33790_35029[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33770 === (17))){
var inst_33762 = (state_33769[(2)]);
var state_33769__$1 = (function (){var statearr_33801 = state_33769;
(statearr_33801[(10)] = inst_33762);

return statearr_33801;
})();
var statearr_33802_35032 = state_33769__$1;
(statearr_33802_35032[(2)] = null);

(statearr_33802_35032[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33770 === (3))){
var inst_33767 = (state_33769[(2)]);
var state_33769__$1 = state_33769;
return cljs.core.async.impl.ioc_helpers.return_chan(state_33769__$1,inst_33767);
} else {
if((state_val_33770 === (12))){
var _ = (function (){var statearr_33810 = state_33769;
(statearr_33810[(4)] = cljs.core.rest((state_33769[(4)])));

return statearr_33810;
})();
var state_33769__$1 = state_33769;
var ex33800 = (state_33769__$1[(2)]);
var statearr_33812_35033 = state_33769__$1;
(statearr_33812_35033[(5)] = ex33800);


if((ex33800 instanceof Object)){
var statearr_33813_35034 = state_33769__$1;
(statearr_33813_35034[(1)] = (11));

(statearr_33813_35034[(5)] = null);

} else {
throw ex33800;

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33770 === (2))){
var inst_33718 = cljs.core.reset_BANG_(dctr,cnt);
var inst_33720 = cnt;
var inst_33721 = (0);
var state_33769__$1 = (function (){var statearr_33822 = state_33769;
(statearr_33822[(7)] = inst_33721);

(statearr_33822[(8)] = inst_33720);

(statearr_33822[(11)] = inst_33718);

return statearr_33822;
})();
var statearr_33826_35037 = state_33769__$1;
(statearr_33826_35037[(2)] = null);

(statearr_33826_35037[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33770 === (11))){
var inst_33729 = (state_33769[(2)]);
var inst_33734 = cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$2(dctr,cljs.core.dec);
var state_33769__$1 = (function (){var statearr_33828 = state_33769;
(statearr_33828[(12)] = inst_33729);

return statearr_33828;
})();
var statearr_33829_35038 = state_33769__$1;
(statearr_33829_35038[(2)] = inst_33734);

(statearr_33829_35038[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33770 === (9))){
var inst_33721 = (state_33769[(7)]);
var _ = (function (){var statearr_33830 = state_33769;
(statearr_33830[(4)] = cljs.core.cons((12),(state_33769[(4)])));

return statearr_33830;
})();
var inst_33740 = (chs__$1.cljs$core$IFn$_invoke$arity$1 ? chs__$1.cljs$core$IFn$_invoke$arity$1(inst_33721) : chs__$1.call(null,inst_33721));
var inst_33741 = (done.cljs$core$IFn$_invoke$arity$1 ? done.cljs$core$IFn$_invoke$arity$1(inst_33721) : done.call(null,inst_33721));
var inst_33742 = cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$2(inst_33740,inst_33741);
var ___$1 = (function (){var statearr_33832 = state_33769;
(statearr_33832[(4)] = cljs.core.rest((state_33769[(4)])));

return statearr_33832;
})();
var state_33769__$1 = state_33769;
var statearr_33833_35039 = state_33769__$1;
(statearr_33833_35039[(2)] = inst_33742);

(statearr_33833_35039[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33770 === (5))){
var inst_33753 = (state_33769[(2)]);
var state_33769__$1 = (function (){var statearr_33834 = state_33769;
(statearr_33834[(13)] = inst_33753);

return statearr_33834;
})();
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_33769__$1,(13),dchan);
} else {
if((state_val_33770 === (14))){
var inst_33758 = cljs.core.async.close_BANG_(out);
var state_33769__$1 = state_33769;
var statearr_33835_35040 = state_33769__$1;
(statearr_33835_35040[(2)] = inst_33758);

(statearr_33835_35040[(1)] = (16));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33770 === (16))){
var inst_33765 = (state_33769[(2)]);
var state_33769__$1 = state_33769;
var statearr_33840_35041 = state_33769__$1;
(statearr_33840_35041[(2)] = inst_33765);

(statearr_33840_35041[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33770 === (10))){
var inst_33721 = (state_33769[(7)]);
var inst_33745 = (state_33769[(2)]);
var inst_33747 = (inst_33721 + (1));
var inst_33721__$1 = inst_33747;
var state_33769__$1 = (function (){var statearr_33844 = state_33769;
(statearr_33844[(14)] = inst_33745);

(statearr_33844[(7)] = inst_33721__$1);

return statearr_33844;
})();
var statearr_33846_35042 = state_33769__$1;
(statearr_33846_35042[(2)] = null);

(statearr_33846_35042[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33770 === (8))){
var inst_33751 = (state_33769[(2)]);
var state_33769__$1 = state_33769;
var statearr_33847_35043 = state_33769__$1;
(statearr_33847_35043[(2)] = inst_33751);

(statearr_33847_35043[(1)] = (5));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__31874__auto__ = null;
var cljs$core$async$state_machine__31874__auto____0 = (function (){
var statearr_33851 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_33851[(0)] = cljs$core$async$state_machine__31874__auto__);

(statearr_33851[(1)] = (1));

return statearr_33851;
});
var cljs$core$async$state_machine__31874__auto____1 = (function (state_33769){
while(true){
var ret_value__31875__auto__ = (function (){try{while(true){
var result__31876__auto__ = switch__31873__auto__(state_33769);
if(cljs.core.keyword_identical_QMARK_(result__31876__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__31876__auto__;
}
break;
}
}catch (e33853){var ex__31877__auto__ = e33853;
var statearr_33854_35045 = state_33769;
(statearr_33854_35045[(2)] = ex__31877__auto__);


if(cljs.core.seq((state_33769[(4)]))){
var statearr_33855_35046 = state_33769;
(statearr_33855_35046[(1)] = cljs.core.first((state_33769[(4)])));

} else {
throw ex__31877__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__31875__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__35047 = state_33769;
state_33769 = G__35047;
continue;
} else {
return ret_value__31875__auto__;
}
break;
}
});
cljs$core$async$state_machine__31874__auto__ = function(state_33769){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__31874__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__31874__auto____1.call(this,state_33769);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__31874__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__31874__auto____0;
cljs$core$async$state_machine__31874__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__31874__auto____1;
return cljs$core$async$state_machine__31874__auto__;
})()
})();
var state__32012__auto__ = (function (){var statearr_33856 = f__32011__auto__();
(statearr_33856[(6)] = c__32010__auto___35021);

return statearr_33856;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__32012__auto__);
}));

}

return out;
}));

(cljs.core.async.map.cljs$lang$maxFixedArity = 3);

/**
 * Takes a collection of source channels and returns a channel which
 *   contains all values taken from them. The returned channel will be
 *   unbuffered by default, or a buf-or-n can be supplied. The channel
 *   will close after all the source channels have closed.
 */
cljs.core.async.merge = (function cljs$core$async$merge(var_args){
var G__33863 = arguments.length;
switch (G__33863) {
case 1:
return cljs.core.async.merge.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.merge.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.merge.cljs$core$IFn$_invoke$arity$1 = (function (chs){
return cljs.core.async.merge.cljs$core$IFn$_invoke$arity$2(chs,null);
}));

(cljs.core.async.merge.cljs$core$IFn$_invoke$arity$2 = (function (chs,buf_or_n){
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
var c__32010__auto___35050 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__32011__auto__ = (function (){var switch__31873__auto__ = (function (state_33911){
var state_val_33912 = (state_33911[(1)]);
if((state_val_33912 === (7))){
var inst_33883 = (state_33911[(7)]);
var inst_33884 = (state_33911[(8)]);
var inst_33883__$1 = (state_33911[(2)]);
var inst_33884__$1 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_33883__$1,(0),null);
var inst_33885 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_33883__$1,(1),null);
var inst_33886 = (inst_33884__$1 == null);
var state_33911__$1 = (function (){var statearr_33918 = state_33911;
(statearr_33918[(7)] = inst_33883__$1);

(statearr_33918[(9)] = inst_33885);

(statearr_33918[(8)] = inst_33884__$1);

return statearr_33918;
})();
if(cljs.core.truth_(inst_33886)){
var statearr_33919_35051 = state_33911__$1;
(statearr_33919_35051[(1)] = (8));

} else {
var statearr_33920_35052 = state_33911__$1;
(statearr_33920_35052[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33912 === (1))){
var inst_33869 = cljs.core.vec(chs);
var inst_33870 = inst_33869;
var state_33911__$1 = (function (){var statearr_33925 = state_33911;
(statearr_33925[(10)] = inst_33870);

return statearr_33925;
})();
var statearr_33926_35053 = state_33911__$1;
(statearr_33926_35053[(2)] = null);

(statearr_33926_35053[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33912 === (4))){
var inst_33870 = (state_33911[(10)]);
var state_33911__$1 = state_33911;
return cljs.core.async.ioc_alts_BANG_(state_33911__$1,(7),inst_33870);
} else {
if((state_val_33912 === (6))){
var inst_33904 = (state_33911[(2)]);
var state_33911__$1 = state_33911;
var statearr_33927_35054 = state_33911__$1;
(statearr_33927_35054[(2)] = inst_33904);

(statearr_33927_35054[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33912 === (3))){
var inst_33906 = (state_33911[(2)]);
var state_33911__$1 = state_33911;
return cljs.core.async.impl.ioc_helpers.return_chan(state_33911__$1,inst_33906);
} else {
if((state_val_33912 === (2))){
var inst_33870 = (state_33911[(10)]);
var inst_33876 = cljs.core.count(inst_33870);
var inst_33877 = (inst_33876 > (0));
var state_33911__$1 = state_33911;
if(cljs.core.truth_(inst_33877)){
var statearr_33929_35055 = state_33911__$1;
(statearr_33929_35055[(1)] = (4));

} else {
var statearr_33930_35056 = state_33911__$1;
(statearr_33930_35056[(1)] = (5));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33912 === (11))){
var inst_33870 = (state_33911[(10)]);
var inst_33897 = (state_33911[(2)]);
var tmp33928 = inst_33870;
var inst_33870__$1 = tmp33928;
var state_33911__$1 = (function (){var statearr_33931 = state_33911;
(statearr_33931[(11)] = inst_33897);

(statearr_33931[(10)] = inst_33870__$1);

return statearr_33931;
})();
var statearr_33932_35057 = state_33911__$1;
(statearr_33932_35057[(2)] = null);

(statearr_33932_35057[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33912 === (9))){
var inst_33884 = (state_33911[(8)]);
var state_33911__$1 = state_33911;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_33911__$1,(11),out,inst_33884);
} else {
if((state_val_33912 === (5))){
var inst_33902 = cljs.core.async.close_BANG_(out);
var state_33911__$1 = state_33911;
var statearr_33937_35058 = state_33911__$1;
(statearr_33937_35058[(2)] = inst_33902);

(statearr_33937_35058[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33912 === (10))){
var inst_33900 = (state_33911[(2)]);
var state_33911__$1 = state_33911;
var statearr_33939_35059 = state_33911__$1;
(statearr_33939_35059[(2)] = inst_33900);

(statearr_33939_35059[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33912 === (8))){
var inst_33883 = (state_33911[(7)]);
var inst_33885 = (state_33911[(9)]);
var inst_33870 = (state_33911[(10)]);
var inst_33884 = (state_33911[(8)]);
var inst_33891 = (function (){var cs = inst_33870;
var vec__33879 = inst_33883;
var v = inst_33884;
var c = inst_33885;
return (function (p1__33860_SHARP_){
return cljs.core.not_EQ_.cljs$core$IFn$_invoke$arity$2(c,p1__33860_SHARP_);
});
})();
var inst_33893 = cljs.core.filterv(inst_33891,inst_33870);
var inst_33870__$1 = inst_33893;
var state_33911__$1 = (function (){var statearr_33940 = state_33911;
(statearr_33940[(10)] = inst_33870__$1);

return statearr_33940;
})();
var statearr_33941_35060 = state_33911__$1;
(statearr_33941_35060[(2)] = null);

(statearr_33941_35060[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__31874__auto__ = null;
var cljs$core$async$state_machine__31874__auto____0 = (function (){
var statearr_33942 = [null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_33942[(0)] = cljs$core$async$state_machine__31874__auto__);

(statearr_33942[(1)] = (1));

return statearr_33942;
});
var cljs$core$async$state_machine__31874__auto____1 = (function (state_33911){
while(true){
var ret_value__31875__auto__ = (function (){try{while(true){
var result__31876__auto__ = switch__31873__auto__(state_33911);
if(cljs.core.keyword_identical_QMARK_(result__31876__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__31876__auto__;
}
break;
}
}catch (e33943){var ex__31877__auto__ = e33943;
var statearr_33944_35061 = state_33911;
(statearr_33944_35061[(2)] = ex__31877__auto__);


if(cljs.core.seq((state_33911[(4)]))){
var statearr_33945_35062 = state_33911;
(statearr_33945_35062[(1)] = cljs.core.first((state_33911[(4)])));

} else {
throw ex__31877__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__31875__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__35063 = state_33911;
state_33911 = G__35063;
continue;
} else {
return ret_value__31875__auto__;
}
break;
}
});
cljs$core$async$state_machine__31874__auto__ = function(state_33911){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__31874__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__31874__auto____1.call(this,state_33911);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__31874__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__31874__auto____0;
cljs$core$async$state_machine__31874__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__31874__auto____1;
return cljs$core$async$state_machine__31874__auto__;
})()
})();
var state__32012__auto__ = (function (){var statearr_33947 = f__32011__auto__();
(statearr_33947[(6)] = c__32010__auto___35050);

return statearr_33947;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__32012__auto__);
}));


return out;
}));

(cljs.core.async.merge.cljs$lang$maxFixedArity = 2);

/**
 * Returns a channel containing the single (collection) result of the
 *   items taken from the channel conjoined to the supplied
 *   collection. ch must close before into produces a result.
 */
cljs.core.async.into = (function cljs$core$async$into(coll,ch){
return cljs.core.async.reduce(cljs.core.conj,coll,ch);
});
/**
 * Returns a channel that will return, at most, n items from ch. After n items
 * have been returned, or ch has been closed, the return chanel will close.
 * 
 *   The output channel is unbuffered by default, unless buf-or-n is given.
 */
cljs.core.async.take = (function cljs$core$async$take(var_args){
var G__33950 = arguments.length;
switch (G__33950) {
case 2:
return cljs.core.async.take.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.take.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.take.cljs$core$IFn$_invoke$arity$2 = (function (n,ch){
return cljs.core.async.take.cljs$core$IFn$_invoke$arity$3(n,ch,null);
}));

(cljs.core.async.take.cljs$core$IFn$_invoke$arity$3 = (function (n,ch,buf_or_n){
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
var c__32010__auto___35065 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__32011__auto__ = (function (){var switch__31873__auto__ = (function (state_33974){
var state_val_33975 = (state_33974[(1)]);
if((state_val_33975 === (7))){
var inst_33956 = (state_33974[(7)]);
var inst_33956__$1 = (state_33974[(2)]);
var inst_33957 = (inst_33956__$1 == null);
var inst_33958 = cljs.core.not(inst_33957);
var state_33974__$1 = (function (){var statearr_33976 = state_33974;
(statearr_33976[(7)] = inst_33956__$1);

return statearr_33976;
})();
if(inst_33958){
var statearr_33977_35066 = state_33974__$1;
(statearr_33977_35066[(1)] = (8));

} else {
var statearr_33978_35067 = state_33974__$1;
(statearr_33978_35067[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33975 === (1))){
var inst_33951 = (0);
var state_33974__$1 = (function (){var statearr_33979 = state_33974;
(statearr_33979[(8)] = inst_33951);

return statearr_33979;
})();
var statearr_33980_35068 = state_33974__$1;
(statearr_33980_35068[(2)] = null);

(statearr_33980_35068[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33975 === (4))){
var state_33974__$1 = state_33974;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_33974__$1,(7),ch);
} else {
if((state_val_33975 === (6))){
var inst_33969 = (state_33974[(2)]);
var state_33974__$1 = state_33974;
var statearr_33981_35069 = state_33974__$1;
(statearr_33981_35069[(2)] = inst_33969);

(statearr_33981_35069[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33975 === (3))){
var inst_33971 = (state_33974[(2)]);
var inst_33972 = cljs.core.async.close_BANG_(out);
var state_33974__$1 = (function (){var statearr_33982 = state_33974;
(statearr_33982[(9)] = inst_33971);

return statearr_33982;
})();
return cljs.core.async.impl.ioc_helpers.return_chan(state_33974__$1,inst_33972);
} else {
if((state_val_33975 === (2))){
var inst_33951 = (state_33974[(8)]);
var inst_33953 = (inst_33951 < n);
var state_33974__$1 = state_33974;
if(cljs.core.truth_(inst_33953)){
var statearr_33983_35074 = state_33974__$1;
(statearr_33983_35074[(1)] = (4));

} else {
var statearr_33984_35075 = state_33974__$1;
(statearr_33984_35075[(1)] = (5));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33975 === (11))){
var inst_33951 = (state_33974[(8)]);
var inst_33961 = (state_33974[(2)]);
var inst_33962 = (inst_33951 + (1));
var inst_33951__$1 = inst_33962;
var state_33974__$1 = (function (){var statearr_33985 = state_33974;
(statearr_33985[(10)] = inst_33961);

(statearr_33985[(8)] = inst_33951__$1);

return statearr_33985;
})();
var statearr_33986_35076 = state_33974__$1;
(statearr_33986_35076[(2)] = null);

(statearr_33986_35076[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33975 === (9))){
var state_33974__$1 = state_33974;
var statearr_33987_35077 = state_33974__$1;
(statearr_33987_35077[(2)] = null);

(statearr_33987_35077[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33975 === (5))){
var state_33974__$1 = state_33974;
var statearr_33988_35078 = state_33974__$1;
(statearr_33988_35078[(2)] = null);

(statearr_33988_35078[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33975 === (10))){
var inst_33966 = (state_33974[(2)]);
var state_33974__$1 = state_33974;
var statearr_33989_35079 = state_33974__$1;
(statearr_33989_35079[(2)] = inst_33966);

(statearr_33989_35079[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33975 === (8))){
var inst_33956 = (state_33974[(7)]);
var state_33974__$1 = state_33974;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_33974__$1,(11),out,inst_33956);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__31874__auto__ = null;
var cljs$core$async$state_machine__31874__auto____0 = (function (){
var statearr_33990 = [null,null,null,null,null,null,null,null,null,null,null];
(statearr_33990[(0)] = cljs$core$async$state_machine__31874__auto__);

(statearr_33990[(1)] = (1));

return statearr_33990;
});
var cljs$core$async$state_machine__31874__auto____1 = (function (state_33974){
while(true){
var ret_value__31875__auto__ = (function (){try{while(true){
var result__31876__auto__ = switch__31873__auto__(state_33974);
if(cljs.core.keyword_identical_QMARK_(result__31876__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__31876__auto__;
}
break;
}
}catch (e33991){var ex__31877__auto__ = e33991;
var statearr_33992_35081 = state_33974;
(statearr_33992_35081[(2)] = ex__31877__auto__);


if(cljs.core.seq((state_33974[(4)]))){
var statearr_33993_35082 = state_33974;
(statearr_33993_35082[(1)] = cljs.core.first((state_33974[(4)])));

} else {
throw ex__31877__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__31875__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__35083 = state_33974;
state_33974 = G__35083;
continue;
} else {
return ret_value__31875__auto__;
}
break;
}
});
cljs$core$async$state_machine__31874__auto__ = function(state_33974){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__31874__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__31874__auto____1.call(this,state_33974);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__31874__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__31874__auto____0;
cljs$core$async$state_machine__31874__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__31874__auto____1;
return cljs$core$async$state_machine__31874__auto__;
})()
})();
var state__32012__auto__ = (function (){var statearr_33995 = f__32011__auto__();
(statearr_33995[(6)] = c__32010__auto___35065);

return statearr_33995;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__32012__auto__);
}));


return out;
}));

(cljs.core.async.take.cljs$lang$maxFixedArity = 3);

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.map_LT_ = (function cljs$core$async$map_LT_(f,ch){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async33998 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Channel}
 * @implements {cljs.core.async.impl.protocols.WritePort}
 * @implements {cljs.core.async.impl.protocols.ReadPort}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async33998 = (function (f,ch,meta33999){
this.f = f;
this.ch = ch;
this.meta33999 = meta33999;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(cljs.core.async.t_cljs$core$async33998.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_34000,meta33999__$1){
var self__ = this;
var _34000__$1 = this;
return (new cljs.core.async.t_cljs$core$async33998(self__.f,self__.ch,meta33999__$1));
}));

(cljs.core.async.t_cljs$core$async33998.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_34000){
var self__ = this;
var _34000__$1 = this;
return self__.meta33999;
}));

(cljs.core.async.t_cljs$core$async33998.prototype.cljs$core$async$impl$protocols$Channel$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async33998.prototype.cljs$core$async$impl$protocols$Channel$close_BANG_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.close_BANG_(self__.ch);
}));

(cljs.core.async.t_cljs$core$async33998.prototype.cljs$core$async$impl$protocols$Channel$closed_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.closed_QMARK_(self__.ch);
}));

(cljs.core.async.t_cljs$core$async33998.prototype.cljs$core$async$impl$protocols$ReadPort$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async33998.prototype.cljs$core$async$impl$protocols$ReadPort$take_BANG_$arity$2 = (function (_,fn1){
var self__ = this;
var ___$1 = this;
var ret = cljs.core.async.impl.protocols.take_BANG_(self__.ch,(function (){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async34003 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Handler}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async34003 = (function (f,ch,meta33999,_,fn1,meta34004){
this.f = f;
this.ch = ch;
this.meta33999 = meta33999;
this._ = _;
this.fn1 = fn1;
this.meta34004 = meta34004;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(cljs.core.async.t_cljs$core$async34003.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_34005,meta34004__$1){
var self__ = this;
var _34005__$1 = this;
return (new cljs.core.async.t_cljs$core$async34003(self__.f,self__.ch,self__.meta33999,self__._,self__.fn1,meta34004__$1));
}));

(cljs.core.async.t_cljs$core$async34003.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_34005){
var self__ = this;
var _34005__$1 = this;
return self__.meta34004;
}));

(cljs.core.async.t_cljs$core$async34003.prototype.cljs$core$async$impl$protocols$Handler$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async34003.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = (function (___$1){
var self__ = this;
var ___$2 = this;
return cljs.core.async.impl.protocols.active_QMARK_(self__.fn1);
}));

(cljs.core.async.t_cljs$core$async34003.prototype.cljs$core$async$impl$protocols$Handler$blockable_QMARK_$arity$1 = (function (___$1){
var self__ = this;
var ___$2 = this;
return true;
}));

(cljs.core.async.t_cljs$core$async34003.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = (function (___$1){
var self__ = this;
var ___$2 = this;
var f1 = cljs.core.async.impl.protocols.commit(self__.fn1);
return (function (p1__33997_SHARP_){
var G__34008 = (((p1__33997_SHARP_ == null))?null:(self__.f.cljs$core$IFn$_invoke$arity$1 ? self__.f.cljs$core$IFn$_invoke$arity$1(p1__33997_SHARP_) : self__.f.call(null,p1__33997_SHARP_)));
return (f1.cljs$core$IFn$_invoke$arity$1 ? f1.cljs$core$IFn$_invoke$arity$1(G__34008) : f1.call(null,G__34008));
});
}));

(cljs.core.async.t_cljs$core$async34003.getBasis = (function (){
return new cljs.core.PersistentVector(null, 6, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"f","f",43394975,null),new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"meta33999","meta33999",-246167549,null),cljs.core.with_meta(new cljs.core.Symbol(null,"_","_",-1201019570,null),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"tag","tag",-1290361223),new cljs.core.Symbol("cljs.core.async","t_cljs$core$async33998","cljs.core.async/t_cljs$core$async33998",-1337626152,null)], null)),new cljs.core.Symbol(null,"fn1","fn1",895834444,null),new cljs.core.Symbol(null,"meta34004","meta34004",685900949,null)], null);
}));

(cljs.core.async.t_cljs$core$async34003.cljs$lang$type = true);

(cljs.core.async.t_cljs$core$async34003.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async34003");

(cljs.core.async.t_cljs$core$async34003.cljs$lang$ctorPrWriter = (function (this__4491__auto__,writer__4492__auto__,opt__4493__auto__){
return cljs.core._write(writer__4492__auto__,"cljs.core.async/t_cljs$core$async34003");
}));

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async34003.
 */
cljs.core.async.__GT_t_cljs$core$async34003 = (function cljs$core$async$map_LT__$___GT_t_cljs$core$async34003(f__$1,ch__$1,meta33999__$1,___$2,fn1__$1,meta34004){
return (new cljs.core.async.t_cljs$core$async34003(f__$1,ch__$1,meta33999__$1,___$2,fn1__$1,meta34004));
});

}

return (new cljs.core.async.t_cljs$core$async34003(self__.f,self__.ch,self__.meta33999,___$1,fn1,cljs.core.PersistentArrayMap.EMPTY));
})()
);
if(cljs.core.truth_((function (){var and__4251__auto__ = ret;
if(cljs.core.truth_(and__4251__auto__)){
return (!((cljs.core.deref(ret) == null)));
} else {
return and__4251__auto__;
}
})())){
return cljs.core.async.impl.channels.box((function (){var G__34009 = cljs.core.deref(ret);
return (self__.f.cljs$core$IFn$_invoke$arity$1 ? self__.f.cljs$core$IFn$_invoke$arity$1(G__34009) : self__.f.call(null,G__34009));
})());
} else {
return ret;
}
}));

(cljs.core.async.t_cljs$core$async33998.prototype.cljs$core$async$impl$protocols$WritePort$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async33998.prototype.cljs$core$async$impl$protocols$WritePort$put_BANG_$arity$3 = (function (_,val,fn1){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.put_BANG_(self__.ch,val,fn1);
}));

(cljs.core.async.t_cljs$core$async33998.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"f","f",43394975,null),new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"meta33999","meta33999",-246167549,null)], null);
}));

(cljs.core.async.t_cljs$core$async33998.cljs$lang$type = true);

(cljs.core.async.t_cljs$core$async33998.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async33998");

(cljs.core.async.t_cljs$core$async33998.cljs$lang$ctorPrWriter = (function (this__4491__auto__,writer__4492__auto__,opt__4493__auto__){
return cljs.core._write(writer__4492__auto__,"cljs.core.async/t_cljs$core$async33998");
}));

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async33998.
 */
cljs.core.async.__GT_t_cljs$core$async33998 = (function cljs$core$async$map_LT__$___GT_t_cljs$core$async33998(f__$1,ch__$1,meta33999){
return (new cljs.core.async.t_cljs$core$async33998(f__$1,ch__$1,meta33999));
});

}

return (new cljs.core.async.t_cljs$core$async33998(f,ch,cljs.core.PersistentArrayMap.EMPTY));
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.map_GT_ = (function cljs$core$async$map_GT_(f,ch){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async34012 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Channel}
 * @implements {cljs.core.async.impl.protocols.WritePort}
 * @implements {cljs.core.async.impl.protocols.ReadPort}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async34012 = (function (f,ch,meta34013){
this.f = f;
this.ch = ch;
this.meta34013 = meta34013;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(cljs.core.async.t_cljs$core$async34012.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_34014,meta34013__$1){
var self__ = this;
var _34014__$1 = this;
return (new cljs.core.async.t_cljs$core$async34012(self__.f,self__.ch,meta34013__$1));
}));

(cljs.core.async.t_cljs$core$async34012.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_34014){
var self__ = this;
var _34014__$1 = this;
return self__.meta34013;
}));

(cljs.core.async.t_cljs$core$async34012.prototype.cljs$core$async$impl$protocols$Channel$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async34012.prototype.cljs$core$async$impl$protocols$Channel$close_BANG_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.close_BANG_(self__.ch);
}));

(cljs.core.async.t_cljs$core$async34012.prototype.cljs$core$async$impl$protocols$ReadPort$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async34012.prototype.cljs$core$async$impl$protocols$ReadPort$take_BANG_$arity$2 = (function (_,fn1){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.take_BANG_(self__.ch,fn1);
}));

(cljs.core.async.t_cljs$core$async34012.prototype.cljs$core$async$impl$protocols$WritePort$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async34012.prototype.cljs$core$async$impl$protocols$WritePort$put_BANG_$arity$3 = (function (_,val,fn1){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.put_BANG_(self__.ch,(self__.f.cljs$core$IFn$_invoke$arity$1 ? self__.f.cljs$core$IFn$_invoke$arity$1(val) : self__.f.call(null,val)),fn1);
}));

(cljs.core.async.t_cljs$core$async34012.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"f","f",43394975,null),new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"meta34013","meta34013",62864698,null)], null);
}));

(cljs.core.async.t_cljs$core$async34012.cljs$lang$type = true);

(cljs.core.async.t_cljs$core$async34012.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async34012");

(cljs.core.async.t_cljs$core$async34012.cljs$lang$ctorPrWriter = (function (this__4491__auto__,writer__4492__auto__,opt__4493__auto__){
return cljs.core._write(writer__4492__auto__,"cljs.core.async/t_cljs$core$async34012");
}));

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async34012.
 */
cljs.core.async.__GT_t_cljs$core$async34012 = (function cljs$core$async$map_GT__$___GT_t_cljs$core$async34012(f__$1,ch__$1,meta34013){
return (new cljs.core.async.t_cljs$core$async34012(f__$1,ch__$1,meta34013));
});

}

return (new cljs.core.async.t_cljs$core$async34012(f,ch,cljs.core.PersistentArrayMap.EMPTY));
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.filter_GT_ = (function cljs$core$async$filter_GT_(p,ch){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async34021 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Channel}
 * @implements {cljs.core.async.impl.protocols.WritePort}
 * @implements {cljs.core.async.impl.protocols.ReadPort}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async34021 = (function (p,ch,meta34022){
this.p = p;
this.ch = ch;
this.meta34022 = meta34022;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(cljs.core.async.t_cljs$core$async34021.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_34023,meta34022__$1){
var self__ = this;
var _34023__$1 = this;
return (new cljs.core.async.t_cljs$core$async34021(self__.p,self__.ch,meta34022__$1));
}));

(cljs.core.async.t_cljs$core$async34021.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_34023){
var self__ = this;
var _34023__$1 = this;
return self__.meta34022;
}));

(cljs.core.async.t_cljs$core$async34021.prototype.cljs$core$async$impl$protocols$Channel$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async34021.prototype.cljs$core$async$impl$protocols$Channel$close_BANG_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.close_BANG_(self__.ch);
}));

(cljs.core.async.t_cljs$core$async34021.prototype.cljs$core$async$impl$protocols$Channel$closed_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.closed_QMARK_(self__.ch);
}));

(cljs.core.async.t_cljs$core$async34021.prototype.cljs$core$async$impl$protocols$ReadPort$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async34021.prototype.cljs$core$async$impl$protocols$ReadPort$take_BANG_$arity$2 = (function (_,fn1){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.take_BANG_(self__.ch,fn1);
}));

(cljs.core.async.t_cljs$core$async34021.prototype.cljs$core$async$impl$protocols$WritePort$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async34021.prototype.cljs$core$async$impl$protocols$WritePort$put_BANG_$arity$3 = (function (_,val,fn1){
var self__ = this;
var ___$1 = this;
if(cljs.core.truth_((self__.p.cljs$core$IFn$_invoke$arity$1 ? self__.p.cljs$core$IFn$_invoke$arity$1(val) : self__.p.call(null,val)))){
return cljs.core.async.impl.protocols.put_BANG_(self__.ch,val,fn1);
} else {
return cljs.core.async.impl.channels.box(cljs.core.not(cljs.core.async.impl.protocols.closed_QMARK_(self__.ch)));
}
}));

(cljs.core.async.t_cljs$core$async34021.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"p","p",1791580836,null),new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"meta34022","meta34022",-1316386353,null)], null);
}));

(cljs.core.async.t_cljs$core$async34021.cljs$lang$type = true);

(cljs.core.async.t_cljs$core$async34021.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async34021");

(cljs.core.async.t_cljs$core$async34021.cljs$lang$ctorPrWriter = (function (this__4491__auto__,writer__4492__auto__,opt__4493__auto__){
return cljs.core._write(writer__4492__auto__,"cljs.core.async/t_cljs$core$async34021");
}));

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async34021.
 */
cljs.core.async.__GT_t_cljs$core$async34021 = (function cljs$core$async$filter_GT__$___GT_t_cljs$core$async34021(p__$1,ch__$1,meta34022){
return (new cljs.core.async.t_cljs$core$async34021(p__$1,ch__$1,meta34022));
});

}

return (new cljs.core.async.t_cljs$core$async34021(p,ch,cljs.core.PersistentArrayMap.EMPTY));
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.remove_GT_ = (function cljs$core$async$remove_GT_(p,ch){
return cljs.core.async.filter_GT_(cljs.core.complement(p),ch);
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.filter_LT_ = (function cljs$core$async$filter_LT_(var_args){
var G__34038 = arguments.length;
switch (G__34038) {
case 2:
return cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$2 = (function (p,ch){
return cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$3(p,ch,null);
}));

(cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$3 = (function (p,ch,buf_or_n){
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
var c__32010__auto___35111 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__32011__auto__ = (function (){var switch__31873__auto__ = (function (state_34078){
var state_val_34082 = (state_34078[(1)]);
if((state_val_34082 === (7))){
var inst_34071 = (state_34078[(2)]);
var state_34078__$1 = state_34078;
var statearr_34086_35113 = state_34078__$1;
(statearr_34086_35113[(2)] = inst_34071);

(statearr_34086_35113[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34082 === (1))){
var state_34078__$1 = state_34078;
var statearr_34087_35114 = state_34078__$1;
(statearr_34087_35114[(2)] = null);

(statearr_34087_35114[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34082 === (4))){
var inst_34053 = (state_34078[(7)]);
var inst_34053__$1 = (state_34078[(2)]);
var inst_34054 = (inst_34053__$1 == null);
var state_34078__$1 = (function (){var statearr_34088 = state_34078;
(statearr_34088[(7)] = inst_34053__$1);

return statearr_34088;
})();
if(cljs.core.truth_(inst_34054)){
var statearr_34089_35115 = state_34078__$1;
(statearr_34089_35115[(1)] = (5));

} else {
var statearr_34090_35116 = state_34078__$1;
(statearr_34090_35116[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34082 === (6))){
var inst_34053 = (state_34078[(7)]);
var inst_34058 = (p.cljs$core$IFn$_invoke$arity$1 ? p.cljs$core$IFn$_invoke$arity$1(inst_34053) : p.call(null,inst_34053));
var state_34078__$1 = state_34078;
if(cljs.core.truth_(inst_34058)){
var statearr_34091_35118 = state_34078__$1;
(statearr_34091_35118[(1)] = (8));

} else {
var statearr_34092_35119 = state_34078__$1;
(statearr_34092_35119[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34082 === (3))){
var inst_34073 = (state_34078[(2)]);
var state_34078__$1 = state_34078;
return cljs.core.async.impl.ioc_helpers.return_chan(state_34078__$1,inst_34073);
} else {
if((state_val_34082 === (2))){
var state_34078__$1 = state_34078;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_34078__$1,(4),ch);
} else {
if((state_val_34082 === (11))){
var inst_34065 = (state_34078[(2)]);
var state_34078__$1 = state_34078;
var statearr_34093_35124 = state_34078__$1;
(statearr_34093_35124[(2)] = inst_34065);

(statearr_34093_35124[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34082 === (9))){
var state_34078__$1 = state_34078;
var statearr_34094_35125 = state_34078__$1;
(statearr_34094_35125[(2)] = null);

(statearr_34094_35125[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34082 === (5))){
var inst_34056 = cljs.core.async.close_BANG_(out);
var state_34078__$1 = state_34078;
var statearr_34096_35126 = state_34078__$1;
(statearr_34096_35126[(2)] = inst_34056);

(statearr_34096_35126[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34082 === (10))){
var inst_34068 = (state_34078[(2)]);
var state_34078__$1 = (function (){var statearr_34100 = state_34078;
(statearr_34100[(8)] = inst_34068);

return statearr_34100;
})();
var statearr_34101_35133 = state_34078__$1;
(statearr_34101_35133[(2)] = null);

(statearr_34101_35133[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34082 === (8))){
var inst_34053 = (state_34078[(7)]);
var state_34078__$1 = state_34078;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_34078__$1,(11),out,inst_34053);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__31874__auto__ = null;
var cljs$core$async$state_machine__31874__auto____0 = (function (){
var statearr_34102 = [null,null,null,null,null,null,null,null,null];
(statearr_34102[(0)] = cljs$core$async$state_machine__31874__auto__);

(statearr_34102[(1)] = (1));

return statearr_34102;
});
var cljs$core$async$state_machine__31874__auto____1 = (function (state_34078){
while(true){
var ret_value__31875__auto__ = (function (){try{while(true){
var result__31876__auto__ = switch__31873__auto__(state_34078);
if(cljs.core.keyword_identical_QMARK_(result__31876__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__31876__auto__;
}
break;
}
}catch (e34103){var ex__31877__auto__ = e34103;
var statearr_34104_35139 = state_34078;
(statearr_34104_35139[(2)] = ex__31877__auto__);


if(cljs.core.seq((state_34078[(4)]))){
var statearr_34105_35141 = state_34078;
(statearr_34105_35141[(1)] = cljs.core.first((state_34078[(4)])));

} else {
throw ex__31877__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__31875__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__35142 = state_34078;
state_34078 = G__35142;
continue;
} else {
return ret_value__31875__auto__;
}
break;
}
});
cljs$core$async$state_machine__31874__auto__ = function(state_34078){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__31874__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__31874__auto____1.call(this,state_34078);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__31874__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__31874__auto____0;
cljs$core$async$state_machine__31874__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__31874__auto____1;
return cljs$core$async$state_machine__31874__auto__;
})()
})();
var state__32012__auto__ = (function (){var statearr_34106 = f__32011__auto__();
(statearr_34106[(6)] = c__32010__auto___35111);

return statearr_34106;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__32012__auto__);
}));


return out;
}));

(cljs.core.async.filter_LT_.cljs$lang$maxFixedArity = 3);

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.remove_LT_ = (function cljs$core$async$remove_LT_(var_args){
var G__34109 = arguments.length;
switch (G__34109) {
case 2:
return cljs.core.async.remove_LT_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.remove_LT_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.remove_LT_.cljs$core$IFn$_invoke$arity$2 = (function (p,ch){
return cljs.core.async.remove_LT_.cljs$core$IFn$_invoke$arity$3(p,ch,null);
}));

(cljs.core.async.remove_LT_.cljs$core$IFn$_invoke$arity$3 = (function (p,ch,buf_or_n){
return cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$3(cljs.core.complement(p),ch,buf_or_n);
}));

(cljs.core.async.remove_LT_.cljs$lang$maxFixedArity = 3);

cljs.core.async.mapcat_STAR_ = (function cljs$core$async$mapcat_STAR_(f,in$,out){
var c__32010__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__32011__auto__ = (function (){var switch__31873__auto__ = (function (state_34185){
var state_val_34186 = (state_34185[(1)]);
if((state_val_34186 === (7))){
var inst_34181 = (state_34185[(2)]);
var state_34185__$1 = state_34185;
var statearr_34187_35146 = state_34185__$1;
(statearr_34187_35146[(2)] = inst_34181);

(statearr_34187_35146[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34186 === (20))){
var inst_34144 = (state_34185[(7)]);
var inst_34155 = (state_34185[(2)]);
var inst_34156 = cljs.core.next(inst_34144);
var inst_34130 = inst_34156;
var inst_34131 = null;
var inst_34132 = (0);
var inst_34133 = (0);
var state_34185__$1 = (function (){var statearr_34190 = state_34185;
(statearr_34190[(8)] = inst_34130);

(statearr_34190[(9)] = inst_34132);

(statearr_34190[(10)] = inst_34155);

(statearr_34190[(11)] = inst_34131);

(statearr_34190[(12)] = inst_34133);

return statearr_34190;
})();
var statearr_34193_35147 = state_34185__$1;
(statearr_34193_35147[(2)] = null);

(statearr_34193_35147[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34186 === (1))){
var state_34185__$1 = state_34185;
var statearr_34194_35148 = state_34185__$1;
(statearr_34194_35148[(2)] = null);

(statearr_34194_35148[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34186 === (4))){
var inst_34119 = (state_34185[(13)]);
var inst_34119__$1 = (state_34185[(2)]);
var inst_34120 = (inst_34119__$1 == null);
var state_34185__$1 = (function (){var statearr_34196 = state_34185;
(statearr_34196[(13)] = inst_34119__$1);

return statearr_34196;
})();
if(cljs.core.truth_(inst_34120)){
var statearr_34197_35150 = state_34185__$1;
(statearr_34197_35150[(1)] = (5));

} else {
var statearr_34198_35151 = state_34185__$1;
(statearr_34198_35151[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34186 === (15))){
var state_34185__$1 = state_34185;
var statearr_34205_35152 = state_34185__$1;
(statearr_34205_35152[(2)] = null);

(statearr_34205_35152[(1)] = (16));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34186 === (21))){
var state_34185__$1 = state_34185;
var statearr_34207_35153 = state_34185__$1;
(statearr_34207_35153[(2)] = null);

(statearr_34207_35153[(1)] = (23));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34186 === (13))){
var inst_34130 = (state_34185[(8)]);
var inst_34132 = (state_34185[(9)]);
var inst_34131 = (state_34185[(11)]);
var inst_34133 = (state_34185[(12)]);
var inst_34140 = (state_34185[(2)]);
var inst_34141 = (inst_34133 + (1));
var tmp34200 = inst_34130;
var tmp34201 = inst_34132;
var tmp34202 = inst_34131;
var inst_34130__$1 = tmp34200;
var inst_34131__$1 = tmp34202;
var inst_34132__$1 = tmp34201;
var inst_34133__$1 = inst_34141;
var state_34185__$1 = (function (){var statearr_34211 = state_34185;
(statearr_34211[(8)] = inst_34130__$1);

(statearr_34211[(9)] = inst_34132__$1);

(statearr_34211[(14)] = inst_34140);

(statearr_34211[(11)] = inst_34131__$1);

(statearr_34211[(12)] = inst_34133__$1);

return statearr_34211;
})();
var statearr_34212_35160 = state_34185__$1;
(statearr_34212_35160[(2)] = null);

(statearr_34212_35160[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34186 === (22))){
var state_34185__$1 = state_34185;
var statearr_34213_35164 = state_34185__$1;
(statearr_34213_35164[(2)] = null);

(statearr_34213_35164[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34186 === (6))){
var inst_34119 = (state_34185[(13)]);
var inst_34128 = (f.cljs$core$IFn$_invoke$arity$1 ? f.cljs$core$IFn$_invoke$arity$1(inst_34119) : f.call(null,inst_34119));
var inst_34129 = cljs.core.seq(inst_34128);
var inst_34130 = inst_34129;
var inst_34131 = null;
var inst_34132 = (0);
var inst_34133 = (0);
var state_34185__$1 = (function (){var statearr_34214 = state_34185;
(statearr_34214[(8)] = inst_34130);

(statearr_34214[(9)] = inst_34132);

(statearr_34214[(11)] = inst_34131);

(statearr_34214[(12)] = inst_34133);

return statearr_34214;
})();
var statearr_34215_35165 = state_34185__$1;
(statearr_34215_35165[(2)] = null);

(statearr_34215_35165[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34186 === (17))){
var inst_34144 = (state_34185[(7)]);
var inst_34148 = cljs.core.chunk_first(inst_34144);
var inst_34149 = cljs.core.chunk_rest(inst_34144);
var inst_34150 = cljs.core.count(inst_34148);
var inst_34130 = inst_34149;
var inst_34131 = inst_34148;
var inst_34132 = inst_34150;
var inst_34133 = (0);
var state_34185__$1 = (function (){var statearr_34216 = state_34185;
(statearr_34216[(8)] = inst_34130);

(statearr_34216[(9)] = inst_34132);

(statearr_34216[(11)] = inst_34131);

(statearr_34216[(12)] = inst_34133);

return statearr_34216;
})();
var statearr_34217_35168 = state_34185__$1;
(statearr_34217_35168[(2)] = null);

(statearr_34217_35168[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34186 === (3))){
var inst_34183 = (state_34185[(2)]);
var state_34185__$1 = state_34185;
return cljs.core.async.impl.ioc_helpers.return_chan(state_34185__$1,inst_34183);
} else {
if((state_val_34186 === (12))){
var inst_34164 = (state_34185[(2)]);
var state_34185__$1 = state_34185;
var statearr_34226_35171 = state_34185__$1;
(statearr_34226_35171[(2)] = inst_34164);

(statearr_34226_35171[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34186 === (2))){
var state_34185__$1 = state_34185;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_34185__$1,(4),in$);
} else {
if((state_val_34186 === (23))){
var inst_34179 = (state_34185[(2)]);
var state_34185__$1 = state_34185;
var statearr_34227_35172 = state_34185__$1;
(statearr_34227_35172[(2)] = inst_34179);

(statearr_34227_35172[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34186 === (19))){
var inst_34159 = (state_34185[(2)]);
var state_34185__$1 = state_34185;
var statearr_34228_35173 = state_34185__$1;
(statearr_34228_35173[(2)] = inst_34159);

(statearr_34228_35173[(1)] = (16));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34186 === (11))){
var inst_34130 = (state_34185[(8)]);
var inst_34144 = (state_34185[(7)]);
var inst_34144__$1 = cljs.core.seq(inst_34130);
var state_34185__$1 = (function (){var statearr_34229 = state_34185;
(statearr_34229[(7)] = inst_34144__$1);

return statearr_34229;
})();
if(inst_34144__$1){
var statearr_34230_35177 = state_34185__$1;
(statearr_34230_35177[(1)] = (14));

} else {
var statearr_34231_35178 = state_34185__$1;
(statearr_34231_35178[(1)] = (15));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34186 === (9))){
var inst_34166 = (state_34185[(2)]);
var inst_34169 = cljs.core.async.impl.protocols.closed_QMARK_(out);
var state_34185__$1 = (function (){var statearr_34232 = state_34185;
(statearr_34232[(15)] = inst_34166);

return statearr_34232;
})();
if(cljs.core.truth_(inst_34169)){
var statearr_34233_35187 = state_34185__$1;
(statearr_34233_35187[(1)] = (21));

} else {
var statearr_34234_35188 = state_34185__$1;
(statearr_34234_35188[(1)] = (22));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34186 === (5))){
var inst_34122 = cljs.core.async.close_BANG_(out);
var state_34185__$1 = state_34185;
var statearr_34235_35189 = state_34185__$1;
(statearr_34235_35189[(2)] = inst_34122);

(statearr_34235_35189[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34186 === (14))){
var inst_34144 = (state_34185[(7)]);
var inst_34146 = cljs.core.chunked_seq_QMARK_(inst_34144);
var state_34185__$1 = state_34185;
if(inst_34146){
var statearr_34236_35196 = state_34185__$1;
(statearr_34236_35196[(1)] = (17));

} else {
var statearr_34237_35197 = state_34185__$1;
(statearr_34237_35197[(1)] = (18));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34186 === (16))){
var inst_34162 = (state_34185[(2)]);
var state_34185__$1 = state_34185;
var statearr_34238_35198 = state_34185__$1;
(statearr_34238_35198[(2)] = inst_34162);

(statearr_34238_35198[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34186 === (10))){
var inst_34131 = (state_34185[(11)]);
var inst_34133 = (state_34185[(12)]);
var inst_34138 = cljs.core._nth(inst_34131,inst_34133);
var state_34185__$1 = state_34185;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_34185__$1,(13),out,inst_34138);
} else {
if((state_val_34186 === (18))){
var inst_34144 = (state_34185[(7)]);
var inst_34153 = cljs.core.first(inst_34144);
var state_34185__$1 = state_34185;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_34185__$1,(20),out,inst_34153);
} else {
if((state_val_34186 === (8))){
var inst_34132 = (state_34185[(9)]);
var inst_34133 = (state_34185[(12)]);
var inst_34135 = (inst_34133 < inst_34132);
var inst_34136 = inst_34135;
var state_34185__$1 = state_34185;
if(cljs.core.truth_(inst_34136)){
var statearr_34241_35199 = state_34185__$1;
(statearr_34241_35199[(1)] = (10));

} else {
var statearr_34242_35200 = state_34185__$1;
(statearr_34242_35200[(1)] = (11));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$mapcat_STAR__$_state_machine__31874__auto__ = null;
var cljs$core$async$mapcat_STAR__$_state_machine__31874__auto____0 = (function (){
var statearr_34243 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_34243[(0)] = cljs$core$async$mapcat_STAR__$_state_machine__31874__auto__);

(statearr_34243[(1)] = (1));

return statearr_34243;
});
var cljs$core$async$mapcat_STAR__$_state_machine__31874__auto____1 = (function (state_34185){
while(true){
var ret_value__31875__auto__ = (function (){try{while(true){
var result__31876__auto__ = switch__31873__auto__(state_34185);
if(cljs.core.keyword_identical_QMARK_(result__31876__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__31876__auto__;
}
break;
}
}catch (e34244){var ex__31877__auto__ = e34244;
var statearr_34248_35204 = state_34185;
(statearr_34248_35204[(2)] = ex__31877__auto__);


if(cljs.core.seq((state_34185[(4)]))){
var statearr_34249_35206 = state_34185;
(statearr_34249_35206[(1)] = cljs.core.first((state_34185[(4)])));

} else {
throw ex__31877__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__31875__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__35210 = state_34185;
state_34185 = G__35210;
continue;
} else {
return ret_value__31875__auto__;
}
break;
}
});
cljs$core$async$mapcat_STAR__$_state_machine__31874__auto__ = function(state_34185){
switch(arguments.length){
case 0:
return cljs$core$async$mapcat_STAR__$_state_machine__31874__auto____0.call(this);
case 1:
return cljs$core$async$mapcat_STAR__$_state_machine__31874__auto____1.call(this,state_34185);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$mapcat_STAR__$_state_machine__31874__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$mapcat_STAR__$_state_machine__31874__auto____0;
cljs$core$async$mapcat_STAR__$_state_machine__31874__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$mapcat_STAR__$_state_machine__31874__auto____1;
return cljs$core$async$mapcat_STAR__$_state_machine__31874__auto__;
})()
})();
var state__32012__auto__ = (function (){var statearr_34250 = f__32011__auto__();
(statearr_34250[(6)] = c__32010__auto__);

return statearr_34250;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__32012__auto__);
}));

return c__32010__auto__;
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.mapcat_LT_ = (function cljs$core$async$mapcat_LT_(var_args){
var G__34252 = arguments.length;
switch (G__34252) {
case 2:
return cljs.core.async.mapcat_LT_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.mapcat_LT_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.mapcat_LT_.cljs$core$IFn$_invoke$arity$2 = (function (f,in$){
return cljs.core.async.mapcat_LT_.cljs$core$IFn$_invoke$arity$3(f,in$,null);
}));

(cljs.core.async.mapcat_LT_.cljs$core$IFn$_invoke$arity$3 = (function (f,in$,buf_or_n){
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
cljs.core.async.mapcat_STAR_(f,in$,out);

return out;
}));

(cljs.core.async.mapcat_LT_.cljs$lang$maxFixedArity = 3);

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.mapcat_GT_ = (function cljs$core$async$mapcat_GT_(var_args){
var G__34254 = arguments.length;
switch (G__34254) {
case 2:
return cljs.core.async.mapcat_GT_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.mapcat_GT_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.mapcat_GT_.cljs$core$IFn$_invoke$arity$2 = (function (f,out){
return cljs.core.async.mapcat_GT_.cljs$core$IFn$_invoke$arity$3(f,out,null);
}));

(cljs.core.async.mapcat_GT_.cljs$core$IFn$_invoke$arity$3 = (function (f,out,buf_or_n){
var in$ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
cljs.core.async.mapcat_STAR_(f,in$,out);

return in$;
}));

(cljs.core.async.mapcat_GT_.cljs$lang$maxFixedArity = 3);

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.unique = (function cljs$core$async$unique(var_args){
var G__34256 = arguments.length;
switch (G__34256) {
case 1:
return cljs.core.async.unique.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.unique.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.unique.cljs$core$IFn$_invoke$arity$1 = (function (ch){
return cljs.core.async.unique.cljs$core$IFn$_invoke$arity$2(ch,null);
}));

(cljs.core.async.unique.cljs$core$IFn$_invoke$arity$2 = (function (ch,buf_or_n){
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
var c__32010__auto___35223 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__32011__auto__ = (function (){var switch__31873__auto__ = (function (state_34280){
var state_val_34281 = (state_34280[(1)]);
if((state_val_34281 === (7))){
var inst_34275 = (state_34280[(2)]);
var state_34280__$1 = state_34280;
var statearr_34282_35224 = state_34280__$1;
(statearr_34282_35224[(2)] = inst_34275);

(statearr_34282_35224[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34281 === (1))){
var inst_34257 = null;
var state_34280__$1 = (function (){var statearr_34283 = state_34280;
(statearr_34283[(7)] = inst_34257);

return statearr_34283;
})();
var statearr_34284_35225 = state_34280__$1;
(statearr_34284_35225[(2)] = null);

(statearr_34284_35225[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34281 === (4))){
var inst_34260 = (state_34280[(8)]);
var inst_34260__$1 = (state_34280[(2)]);
var inst_34261 = (inst_34260__$1 == null);
var inst_34262 = cljs.core.not(inst_34261);
var state_34280__$1 = (function (){var statearr_34286 = state_34280;
(statearr_34286[(8)] = inst_34260__$1);

return statearr_34286;
})();
if(inst_34262){
var statearr_34287_35226 = state_34280__$1;
(statearr_34287_35226[(1)] = (5));

} else {
var statearr_34288_35227 = state_34280__$1;
(statearr_34288_35227[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34281 === (6))){
var state_34280__$1 = state_34280;
var statearr_34289_35228 = state_34280__$1;
(statearr_34289_35228[(2)] = null);

(statearr_34289_35228[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34281 === (3))){
var inst_34277 = (state_34280[(2)]);
var inst_34278 = cljs.core.async.close_BANG_(out);
var state_34280__$1 = (function (){var statearr_34290 = state_34280;
(statearr_34290[(9)] = inst_34277);

return statearr_34290;
})();
return cljs.core.async.impl.ioc_helpers.return_chan(state_34280__$1,inst_34278);
} else {
if((state_val_34281 === (2))){
var state_34280__$1 = state_34280;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_34280__$1,(4),ch);
} else {
if((state_val_34281 === (11))){
var inst_34260 = (state_34280[(8)]);
var inst_34269 = (state_34280[(2)]);
var inst_34257 = inst_34260;
var state_34280__$1 = (function (){var statearr_34291 = state_34280;
(statearr_34291[(10)] = inst_34269);

(statearr_34291[(7)] = inst_34257);

return statearr_34291;
})();
var statearr_34292_35232 = state_34280__$1;
(statearr_34292_35232[(2)] = null);

(statearr_34292_35232[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34281 === (9))){
var inst_34260 = (state_34280[(8)]);
var state_34280__$1 = state_34280;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_34280__$1,(11),out,inst_34260);
} else {
if((state_val_34281 === (5))){
var inst_34257 = (state_34280[(7)]);
var inst_34260 = (state_34280[(8)]);
var inst_34264 = cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(inst_34260,inst_34257);
var state_34280__$1 = state_34280;
if(inst_34264){
var statearr_34294_35233 = state_34280__$1;
(statearr_34294_35233[(1)] = (8));

} else {
var statearr_34295_35234 = state_34280__$1;
(statearr_34295_35234[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34281 === (10))){
var inst_34272 = (state_34280[(2)]);
var state_34280__$1 = state_34280;
var statearr_34296_35235 = state_34280__$1;
(statearr_34296_35235[(2)] = inst_34272);

(statearr_34296_35235[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34281 === (8))){
var inst_34257 = (state_34280[(7)]);
var tmp34293 = inst_34257;
var inst_34257__$1 = tmp34293;
var state_34280__$1 = (function (){var statearr_34300 = state_34280;
(statearr_34300[(7)] = inst_34257__$1);

return statearr_34300;
})();
var statearr_34301_35236 = state_34280__$1;
(statearr_34301_35236[(2)] = null);

(statearr_34301_35236[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__31874__auto__ = null;
var cljs$core$async$state_machine__31874__auto____0 = (function (){
var statearr_34302 = [null,null,null,null,null,null,null,null,null,null,null];
(statearr_34302[(0)] = cljs$core$async$state_machine__31874__auto__);

(statearr_34302[(1)] = (1));

return statearr_34302;
});
var cljs$core$async$state_machine__31874__auto____1 = (function (state_34280){
while(true){
var ret_value__31875__auto__ = (function (){try{while(true){
var result__31876__auto__ = switch__31873__auto__(state_34280);
if(cljs.core.keyword_identical_QMARK_(result__31876__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__31876__auto__;
}
break;
}
}catch (e34303){var ex__31877__auto__ = e34303;
var statearr_34304_35243 = state_34280;
(statearr_34304_35243[(2)] = ex__31877__auto__);


if(cljs.core.seq((state_34280[(4)]))){
var statearr_34305_35244 = state_34280;
(statearr_34305_35244[(1)] = cljs.core.first((state_34280[(4)])));

} else {
throw ex__31877__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__31875__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__35245 = state_34280;
state_34280 = G__35245;
continue;
} else {
return ret_value__31875__auto__;
}
break;
}
});
cljs$core$async$state_machine__31874__auto__ = function(state_34280){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__31874__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__31874__auto____1.call(this,state_34280);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__31874__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__31874__auto____0;
cljs$core$async$state_machine__31874__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__31874__auto____1;
return cljs$core$async$state_machine__31874__auto__;
})()
})();
var state__32012__auto__ = (function (){var statearr_34306 = f__32011__auto__();
(statearr_34306[(6)] = c__32010__auto___35223);

return statearr_34306;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__32012__auto__);
}));


return out;
}));

(cljs.core.async.unique.cljs$lang$maxFixedArity = 2);

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.partition = (function cljs$core$async$partition(var_args){
var G__34312 = arguments.length;
switch (G__34312) {
case 2:
return cljs.core.async.partition.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.partition.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.partition.cljs$core$IFn$_invoke$arity$2 = (function (n,ch){
return cljs.core.async.partition.cljs$core$IFn$_invoke$arity$3(n,ch,null);
}));

(cljs.core.async.partition.cljs$core$IFn$_invoke$arity$3 = (function (n,ch,buf_or_n){
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
var c__32010__auto___35247 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__32011__auto__ = (function (){var switch__31873__auto__ = (function (state_34350){
var state_val_34351 = (state_34350[(1)]);
if((state_val_34351 === (7))){
var inst_34346 = (state_34350[(2)]);
var state_34350__$1 = state_34350;
var statearr_34352_35248 = state_34350__$1;
(statearr_34352_35248[(2)] = inst_34346);

(statearr_34352_35248[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34351 === (1))){
var inst_34313 = (new Array(n));
var inst_34314 = inst_34313;
var inst_34315 = (0);
var state_34350__$1 = (function (){var statearr_34353 = state_34350;
(statearr_34353[(7)] = inst_34315);

(statearr_34353[(8)] = inst_34314);

return statearr_34353;
})();
var statearr_34354_35249 = state_34350__$1;
(statearr_34354_35249[(2)] = null);

(statearr_34354_35249[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34351 === (4))){
var inst_34318 = (state_34350[(9)]);
var inst_34318__$1 = (state_34350[(2)]);
var inst_34319 = (inst_34318__$1 == null);
var inst_34320 = cljs.core.not(inst_34319);
var state_34350__$1 = (function (){var statearr_34355 = state_34350;
(statearr_34355[(9)] = inst_34318__$1);

return statearr_34355;
})();
if(inst_34320){
var statearr_34356_35250 = state_34350__$1;
(statearr_34356_35250[(1)] = (5));

} else {
var statearr_34364_35251 = state_34350__$1;
(statearr_34364_35251[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34351 === (15))){
var inst_34340 = (state_34350[(2)]);
var state_34350__$1 = state_34350;
var statearr_34372_35252 = state_34350__$1;
(statearr_34372_35252[(2)] = inst_34340);

(statearr_34372_35252[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34351 === (13))){
var state_34350__$1 = state_34350;
var statearr_34373_35253 = state_34350__$1;
(statearr_34373_35253[(2)] = null);

(statearr_34373_35253[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34351 === (6))){
var inst_34315 = (state_34350[(7)]);
var inst_34336 = (inst_34315 > (0));
var state_34350__$1 = state_34350;
if(cljs.core.truth_(inst_34336)){
var statearr_34380_35260 = state_34350__$1;
(statearr_34380_35260[(1)] = (12));

} else {
var statearr_34381_35261 = state_34350__$1;
(statearr_34381_35261[(1)] = (13));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34351 === (3))){
var inst_34348 = (state_34350[(2)]);
var state_34350__$1 = state_34350;
return cljs.core.async.impl.ioc_helpers.return_chan(state_34350__$1,inst_34348);
} else {
if((state_val_34351 === (12))){
var inst_34314 = (state_34350[(8)]);
var inst_34338 = cljs.core.vec(inst_34314);
var state_34350__$1 = state_34350;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_34350__$1,(15),out,inst_34338);
} else {
if((state_val_34351 === (2))){
var state_34350__$1 = state_34350;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_34350__$1,(4),ch);
} else {
if((state_val_34351 === (11))){
var inst_34330 = (state_34350[(2)]);
var inst_34331 = (new Array(n));
var inst_34314 = inst_34331;
var inst_34315 = (0);
var state_34350__$1 = (function (){var statearr_34382 = state_34350;
(statearr_34382[(7)] = inst_34315);

(statearr_34382[(8)] = inst_34314);

(statearr_34382[(10)] = inst_34330);

return statearr_34382;
})();
var statearr_34389_35262 = state_34350__$1;
(statearr_34389_35262[(2)] = null);

(statearr_34389_35262[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34351 === (9))){
var inst_34314 = (state_34350[(8)]);
var inst_34328 = cljs.core.vec(inst_34314);
var state_34350__$1 = state_34350;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_34350__$1,(11),out,inst_34328);
} else {
if((state_val_34351 === (5))){
var inst_34315 = (state_34350[(7)]);
var inst_34318 = (state_34350[(9)]);
var inst_34314 = (state_34350[(8)]);
var inst_34323 = (state_34350[(11)]);
var inst_34322 = (inst_34314[inst_34315] = inst_34318);
var inst_34323__$1 = (inst_34315 + (1));
var inst_34324 = (inst_34323__$1 < n);
var state_34350__$1 = (function (){var statearr_34398 = state_34350;
(statearr_34398[(11)] = inst_34323__$1);

(statearr_34398[(12)] = inst_34322);

return statearr_34398;
})();
if(cljs.core.truth_(inst_34324)){
var statearr_34405_35263 = state_34350__$1;
(statearr_34405_35263[(1)] = (8));

} else {
var statearr_34406_35264 = state_34350__$1;
(statearr_34406_35264[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34351 === (14))){
var inst_34343 = (state_34350[(2)]);
var inst_34344 = cljs.core.async.close_BANG_(out);
var state_34350__$1 = (function (){var statearr_34408 = state_34350;
(statearr_34408[(13)] = inst_34343);

return statearr_34408;
})();
var statearr_34409_35265 = state_34350__$1;
(statearr_34409_35265[(2)] = inst_34344);

(statearr_34409_35265[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34351 === (10))){
var inst_34334 = (state_34350[(2)]);
var state_34350__$1 = state_34350;
var statearr_34410_35266 = state_34350__$1;
(statearr_34410_35266[(2)] = inst_34334);

(statearr_34410_35266[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34351 === (8))){
var inst_34314 = (state_34350[(8)]);
var inst_34323 = (state_34350[(11)]);
var tmp34407 = inst_34314;
var inst_34314__$1 = tmp34407;
var inst_34315 = inst_34323;
var state_34350__$1 = (function (){var statearr_34411 = state_34350;
(statearr_34411[(7)] = inst_34315);

(statearr_34411[(8)] = inst_34314__$1);

return statearr_34411;
})();
var statearr_34414_35267 = state_34350__$1;
(statearr_34414_35267[(2)] = null);

(statearr_34414_35267[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__31874__auto__ = null;
var cljs$core$async$state_machine__31874__auto____0 = (function (){
var statearr_34415 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_34415[(0)] = cljs$core$async$state_machine__31874__auto__);

(statearr_34415[(1)] = (1));

return statearr_34415;
});
var cljs$core$async$state_machine__31874__auto____1 = (function (state_34350){
while(true){
var ret_value__31875__auto__ = (function (){try{while(true){
var result__31876__auto__ = switch__31873__auto__(state_34350);
if(cljs.core.keyword_identical_QMARK_(result__31876__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__31876__auto__;
}
break;
}
}catch (e34416){var ex__31877__auto__ = e34416;
var statearr_34417_35268 = state_34350;
(statearr_34417_35268[(2)] = ex__31877__auto__);


if(cljs.core.seq((state_34350[(4)]))){
var statearr_34419_35269 = state_34350;
(statearr_34419_35269[(1)] = cljs.core.first((state_34350[(4)])));

} else {
throw ex__31877__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__31875__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__35270 = state_34350;
state_34350 = G__35270;
continue;
} else {
return ret_value__31875__auto__;
}
break;
}
});
cljs$core$async$state_machine__31874__auto__ = function(state_34350){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__31874__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__31874__auto____1.call(this,state_34350);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__31874__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__31874__auto____0;
cljs$core$async$state_machine__31874__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__31874__auto____1;
return cljs$core$async$state_machine__31874__auto__;
})()
})();
var state__32012__auto__ = (function (){var statearr_34420 = f__32011__auto__();
(statearr_34420[(6)] = c__32010__auto___35247);

return statearr_34420;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__32012__auto__);
}));


return out;
}));

(cljs.core.async.partition.cljs$lang$maxFixedArity = 3);

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.partition_by = (function cljs$core$async$partition_by(var_args){
var G__34425 = arguments.length;
switch (G__34425) {
case 2:
return cljs.core.async.partition_by.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.partition_by.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.partition_by.cljs$core$IFn$_invoke$arity$2 = (function (f,ch){
return cljs.core.async.partition_by.cljs$core$IFn$_invoke$arity$3(f,ch,null);
}));

(cljs.core.async.partition_by.cljs$core$IFn$_invoke$arity$3 = (function (f,ch,buf_or_n){
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
var c__32010__auto___35272 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__32011__auto__ = (function (){var switch__31873__auto__ = (function (state_34470){
var state_val_34471 = (state_34470[(1)]);
if((state_val_34471 === (7))){
var inst_34466 = (state_34470[(2)]);
var state_34470__$1 = state_34470;
var statearr_34472_35273 = state_34470__$1;
(statearr_34472_35273[(2)] = inst_34466);

(statearr_34472_35273[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34471 === (1))){
var inst_34426 = [];
var inst_34427 = inst_34426;
var inst_34428 = new cljs.core.Keyword("cljs.core.async","nothing","cljs.core.async/nothing",-69252123);
var state_34470__$1 = (function (){var statearr_34473 = state_34470;
(statearr_34473[(7)] = inst_34428);

(statearr_34473[(8)] = inst_34427);

return statearr_34473;
})();
var statearr_34474_35274 = state_34470__$1;
(statearr_34474_35274[(2)] = null);

(statearr_34474_35274[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34471 === (4))){
var inst_34431 = (state_34470[(9)]);
var inst_34431__$1 = (state_34470[(2)]);
var inst_34432 = (inst_34431__$1 == null);
var inst_34433 = cljs.core.not(inst_34432);
var state_34470__$1 = (function (){var statearr_34475 = state_34470;
(statearr_34475[(9)] = inst_34431__$1);

return statearr_34475;
})();
if(inst_34433){
var statearr_34476_35275 = state_34470__$1;
(statearr_34476_35275[(1)] = (5));

} else {
var statearr_34477_35276 = state_34470__$1;
(statearr_34477_35276[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34471 === (15))){
var inst_34427 = (state_34470[(8)]);
var inst_34458 = cljs.core.vec(inst_34427);
var state_34470__$1 = state_34470;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_34470__$1,(18),out,inst_34458);
} else {
if((state_val_34471 === (13))){
var inst_34453 = (state_34470[(2)]);
var state_34470__$1 = state_34470;
var statearr_34478_35277 = state_34470__$1;
(statearr_34478_35277[(2)] = inst_34453);

(statearr_34478_35277[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34471 === (6))){
var inst_34427 = (state_34470[(8)]);
var inst_34455 = inst_34427.length;
var inst_34456 = (inst_34455 > (0));
var state_34470__$1 = state_34470;
if(cljs.core.truth_(inst_34456)){
var statearr_34479_35279 = state_34470__$1;
(statearr_34479_35279[(1)] = (15));

} else {
var statearr_34480_35280 = state_34470__$1;
(statearr_34480_35280[(1)] = (16));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34471 === (17))){
var inst_34463 = (state_34470[(2)]);
var inst_34464 = cljs.core.async.close_BANG_(out);
var state_34470__$1 = (function (){var statearr_34481 = state_34470;
(statearr_34481[(10)] = inst_34463);

return statearr_34481;
})();
var statearr_34482_35281 = state_34470__$1;
(statearr_34482_35281[(2)] = inst_34464);

(statearr_34482_35281[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34471 === (3))){
var inst_34468 = (state_34470[(2)]);
var state_34470__$1 = state_34470;
return cljs.core.async.impl.ioc_helpers.return_chan(state_34470__$1,inst_34468);
} else {
if((state_val_34471 === (12))){
var inst_34427 = (state_34470[(8)]);
var inst_34446 = cljs.core.vec(inst_34427);
var state_34470__$1 = state_34470;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_34470__$1,(14),out,inst_34446);
} else {
if((state_val_34471 === (2))){
var state_34470__$1 = state_34470;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_34470__$1,(4),ch);
} else {
if((state_val_34471 === (11))){
var inst_34431 = (state_34470[(9)]);
var inst_34435 = (state_34470[(11)]);
var inst_34427 = (state_34470[(8)]);
var inst_34443 = inst_34427.push(inst_34431);
var tmp34483 = inst_34427;
var inst_34427__$1 = tmp34483;
var inst_34428 = inst_34435;
var state_34470__$1 = (function (){var statearr_34484 = state_34470;
(statearr_34484[(12)] = inst_34443);

(statearr_34484[(7)] = inst_34428);

(statearr_34484[(8)] = inst_34427__$1);

return statearr_34484;
})();
var statearr_34485_35282 = state_34470__$1;
(statearr_34485_35282[(2)] = null);

(statearr_34485_35282[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34471 === (9))){
var inst_34428 = (state_34470[(7)]);
var inst_34439 = cljs.core.keyword_identical_QMARK_(inst_34428,new cljs.core.Keyword("cljs.core.async","nothing","cljs.core.async/nothing",-69252123));
var state_34470__$1 = state_34470;
var statearr_34486_35286 = state_34470__$1;
(statearr_34486_35286[(2)] = inst_34439);

(statearr_34486_35286[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34471 === (5))){
var inst_34431 = (state_34470[(9)]);
var inst_34435 = (state_34470[(11)]);
var inst_34428 = (state_34470[(7)]);
var inst_34436 = (state_34470[(13)]);
var inst_34435__$1 = (f.cljs$core$IFn$_invoke$arity$1 ? f.cljs$core$IFn$_invoke$arity$1(inst_34431) : f.call(null,inst_34431));
var inst_34436__$1 = cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(inst_34435__$1,inst_34428);
var state_34470__$1 = (function (){var statearr_34487 = state_34470;
(statearr_34487[(11)] = inst_34435__$1);

(statearr_34487[(13)] = inst_34436__$1);

return statearr_34487;
})();
if(inst_34436__$1){
var statearr_34488_35287 = state_34470__$1;
(statearr_34488_35287[(1)] = (8));

} else {
var statearr_34489_35288 = state_34470__$1;
(statearr_34489_35288[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34471 === (14))){
var inst_34431 = (state_34470[(9)]);
var inst_34435 = (state_34470[(11)]);
var inst_34448 = (state_34470[(2)]);
var inst_34449 = [];
var inst_34450 = inst_34449.push(inst_34431);
var inst_34427 = inst_34449;
var inst_34428 = inst_34435;
var state_34470__$1 = (function (){var statearr_34490 = state_34470;
(statearr_34490[(14)] = inst_34450);

(statearr_34490[(7)] = inst_34428);

(statearr_34490[(8)] = inst_34427);

(statearr_34490[(15)] = inst_34448);

return statearr_34490;
})();
var statearr_34491_35290 = state_34470__$1;
(statearr_34491_35290[(2)] = null);

(statearr_34491_35290[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34471 === (16))){
var state_34470__$1 = state_34470;
var statearr_34492_35291 = state_34470__$1;
(statearr_34492_35291[(2)] = null);

(statearr_34492_35291[(1)] = (17));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34471 === (10))){
var inst_34441 = (state_34470[(2)]);
var state_34470__$1 = state_34470;
if(cljs.core.truth_(inst_34441)){
var statearr_34495_35296 = state_34470__$1;
(statearr_34495_35296[(1)] = (11));

} else {
var statearr_34497_35297 = state_34470__$1;
(statearr_34497_35297[(1)] = (12));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34471 === (18))){
var inst_34460 = (state_34470[(2)]);
var state_34470__$1 = state_34470;
var statearr_34503_35298 = state_34470__$1;
(statearr_34503_35298[(2)] = inst_34460);

(statearr_34503_35298[(1)] = (17));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34471 === (8))){
var inst_34436 = (state_34470[(13)]);
var state_34470__$1 = state_34470;
var statearr_34504_35299 = state_34470__$1;
(statearr_34504_35299[(2)] = inst_34436);

(statearr_34504_35299[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__31874__auto__ = null;
var cljs$core$async$state_machine__31874__auto____0 = (function (){
var statearr_34508 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_34508[(0)] = cljs$core$async$state_machine__31874__auto__);

(statearr_34508[(1)] = (1));

return statearr_34508;
});
var cljs$core$async$state_machine__31874__auto____1 = (function (state_34470){
while(true){
var ret_value__31875__auto__ = (function (){try{while(true){
var result__31876__auto__ = switch__31873__auto__(state_34470);
if(cljs.core.keyword_identical_QMARK_(result__31876__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__31876__auto__;
}
break;
}
}catch (e34509){var ex__31877__auto__ = e34509;
var statearr_34510_35303 = state_34470;
(statearr_34510_35303[(2)] = ex__31877__auto__);


if(cljs.core.seq((state_34470[(4)]))){
var statearr_34511_35304 = state_34470;
(statearr_34511_35304[(1)] = cljs.core.first((state_34470[(4)])));

} else {
throw ex__31877__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__31875__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__35305 = state_34470;
state_34470 = G__35305;
continue;
} else {
return ret_value__31875__auto__;
}
break;
}
});
cljs$core$async$state_machine__31874__auto__ = function(state_34470){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__31874__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__31874__auto____1.call(this,state_34470);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__31874__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__31874__auto____0;
cljs$core$async$state_machine__31874__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__31874__auto____1;
return cljs$core$async$state_machine__31874__auto__;
})()
})();
var state__32012__auto__ = (function (){var statearr_34512 = f__32011__auto__();
(statearr_34512[(6)] = c__32010__auto___35272);

return statearr_34512;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__32012__auto__);
}));


return out;
}));

(cljs.core.async.partition_by.cljs$lang$maxFixedArity = 3);


//# sourceMappingURL=cljs.core.async.js.map
