(ns project.model
  (:require [reagent.core :as r]
                [datascript.core :as d]))

(def conn (d/create-conn))
(defonce app-state (r/atom {:modal-showing false, :temp-state 0}))

(def datoms [])
(defn query []
  (let [db (d/db conn)]
    (def n-data '[:find ?e ?n ?m
                  :where [?e :number ?n]
                  [?e :msg ?m]
                  ])
    (def number (d/q n-data db))
    (def sorted (sort-by first number))
    {:db number}))


(defn add-new-trans
  [msg key val]
  (d/transact! conn [{:db/add -1
                                :msg msg
                                key val}]))


(defn init-store
      [map-state]
      (doseq [[key val] map-state]
             (swap! app-state assoc key val)
             (add-new-trans "init" key val)))


(defn dispatch
      [type]
      (def current-val (:number @app-state))
      (let [new-val (case type
                              "increase" (inc current-val)
                              "decrease" (dec current-val))]
      (swap! app-state assoc :number new-val)
      (add-new-trans type :number new-val)))