(ns project.core
    (:require
     [reagent.core :as r]
     [reagent.dom :as d]
     [reagent.debug :as debug]
     [project.model :refer [app-state, init-store, add-new-trans, dispatch, query]]
     [reagent-modals.modals :as reagent-modals]))


(init-store {:number 0})

(defn click-active-state [value]
   (swap! app-state assoc :modal-showing true)
   (swap! app-state assoc :temp-state value))


(defn close-ttd []
   (swap! app-state assoc :modal-showing false))


(defn modal-window-button [show]
  [:div.btn.btn-primary
      {:on-click #(reagent-modals/modal!
            [:div {:style {:position "relative"}}
                  (for [[id value msg] show]
                        [:div {:style {:display "flex"}}
                              [:div {:on-click (fn [] (click-active-state value)) :style {:color "blue" :padding "10px"}} id]
                              [:div {:on-click (fn [] (click-active-state value)) :style {:color "blue" :padding "10px"}} msg]])
                  [:div
                        {:style {:position "absolute" :top "30px" :right "30px" :font-size "30px"}}
                        (:temp-state @app-state)]
                        ;; (if (:modal-showing @app-state) (:temp-state @app-state) (:number @app-state))]
                  [:div.btn.btn-info {:on-click (fn [] (close-ttd))} "close"]])}
      "TTD tool"])

;; -------------------------
;; Views
(defn home-page []
      (def show ((query) :db))
      (def current-number (:number @app-state))
      [:div
            "counter "
                  [:span {:style {:font-size "50px"}}
                  (if (:modal-showing @app-state) (:temp-state @app-state) current-number)]
            [:input {:type "button" :value "Increment!"
                  :on-click #(dispatch "increase")}]
            [:input {:type "button" :value "decrement!"
                  :on-click #(dispatch "decrease")}]
            [:div [reagent-modals/modal-window]
                    [modal-window-button show]]])

;; -------------------------
;; Initialize app

(defn mount-root []
  (d/render [home-page] (.getElementById js/document "app")))

(defn ^:export init! []
  (mount-root))
